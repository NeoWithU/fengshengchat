package com.fengshengchat.chat.view;

import com.fengshengchat.base.mvp.IBaseView;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.Group;

import java.util.List;

public interface IChatView extends IBaseView {
    void onSendMessage(ChatRecord msg);
    void onAddTip(ChatRecord msg);
    void onSendMessageSuccess(long seq, long serverSeq);
    void onSendMessageFailed(long seq, String tip);
    void onSendMessageRetry(long seq);
    void onReceiveMessage(ChatRecord msg);
    void onReceiveMessage(List<ChatRecord> msg);
    void onUpdateSingleChatHisInfo();
    void onUpdateGroupChatHisInfo();
    void onUpdateGroupInfo(Group group);
    void onGettingHistory();
    void onNewestBack(List<ChatRecord> list);
    void onHistory(List<ChatRecord> list);
    void onHistoryError(CharSequence tip);
    void onDeleteChatRecordCompleted(long msgSeq);

    void setShowGroupNotify(boolean show);
    void onReceiveGroupNotify( CharSequence msg);
}
