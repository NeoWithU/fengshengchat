package com.fengshengchat.chat.view;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.FriendBriefInfoFragment;
import com.fengshengchat.base.base.BaseActivity;

public class SingleChatInfoActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentForWindow(this);
        setContentView(R.layout.fragment_activity);

        long uid = getIntent().getLongExtra("id", 0);
        SingleChatInfoFragment fragment = SingleChatInfoFragment.newInstance(uid);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, FriendBriefInfoFragment.TAG)
                .commit();
    }
}
