package com.fengshengchat.chat.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hwangjr.rxbus.RxBus;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.db.AppDatabase;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.group.fragment.CreateGroupFragment;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.EmptyObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.widget.ItemLayout;

import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.group.Group;

public class SingleChatInfoFragment extends ToolbarFragment {

    public static final String TAG = "SingleChatInfoFragment";
    private static final String EXTRA_ID = "ID";

    private long id;
    @Nullable
    private Friend friend;

    @BindView(R.id.avatar_iv)
    ImageView avatarIv;
    @BindView(R.id.name_tv)
    TextView nameTv;
    @BindView(R.id.pin_session_layout)
    ItemLayout pinSessionLayout;
    @BindView(R.id.msg_not_disturb_layout)
    ItemLayout notDisturbLayout;
    @BindView(R.id.set_star_layout)
    ItemLayout setStarLayout;

    public static SingleChatInfoFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_ID, id);
        SingleChatInfoFragment fragment = new SingleChatInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.single_chat_info_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }

        id = args.getLong(EXTRA_ID);
    }

    @OnClick({R.id.avatar_iv, R.id.name_tv})
    public void clickAvatar() {
        Intent intent = new Intent(getContext(), FriendBriefInfoActivity.class);
        BriefData briefData = new BriefData();
        briefData.id = id;
        if (friend != null) {
            briefData.relative = BriefData.FRIEND;
            briefData.nickname = friend.nickname;
            briefData.avatar = friend.avatar;
            briefData.bigAvatar = friend.bigAvatar;
            briefData.gender = friend.gender;
            briefData.isMarkStar = friend.isMarkStar;
        }
        intent.putExtra(FriendBriefInfoActivity.KEY_DATA, briefData);
        startActivity(intent);
    }

    @OnClick(R.id.add_iv)
    public void createGroup() {
        CreateGroupFragment fragment = CreateGroupFragment.newInstance(id);
        openFragment(fragment, CreateGroupFragment.TAG);
    }

    @OnClick(R.id.clear_cache_layout)
    public void onClickClearCache() {
        Disposable subscribe = Observable.fromCallable((Callable<Object>) () -> {
            AppDatabase db = DbManager.getDb();
            db.beginTransaction();
            try {
                ChatListBean chatItem = db.getChatListDao().getChatItem(id);
                if (null != chatItem) {
                    chatItem.lastMessage = "";
                    db.getChatListDao().updateChatItem(chatItem);
                }
                db.getRecordDao().deleteAllChatRecord(id);
                db.setTransactionSuccessful();
                db.endTransaction();
                return "OK";
            } catch (Exception e) {
                e.printStackTrace();
                db.endTransaction();
            }
            return "Failed";
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                            if ("OK".equals(o)) {
                                RxBus.get().post(BusEvent.TAG_DELETED_CHAT_RECORD, id);
                                toast(R.string.clear_success);
                            } else {
                                toast(R.string.error);
                            }
                        },
                        throwable -> toast(R.string.error));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        //  置顶聊天
        SwitchCompat pinSessionLayoutSwitch = pinSessionLayout.getSwitchCompat();
        pinSessionLayoutSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonView.isPressed()) {
                return;
            }
            requestSetPinSession(isChecked);
        });

        //  消息免打扰
        SwitchCompat disturbLayoutSwitch = notDisturbLayout.getSwitchCompat();
        disturbLayoutSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonView.isPressed()) {
                return;
            }
            requestSetNotDisturb(isChecked);
        });

        SwitchCompat switchCompat = setStarLayout.getSwitchCompat();
        switchCompat.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonView.isPressed()) {
                return;
            }
            requestSetStar(isChecked);
        });
    }

    /**
     * 请求设置标星
     */
    private void requestSetStar(boolean star) {
        int starValue = star
                ? yiproto.yichat.friend.Friend.StarredType.STARRED_TYPE_STARRED_VALUE
                : yiproto.yichat.friend.Friend.StarredType.STARRED_TYPE_NON_STARRED_VALUE;

        yiproto.yichat.friend.Friend.SetContactTypeReq.Builder body = yiproto.yichat.friend.Friend.SetContactTypeReq
                .newBuilder()
                .setUserId(id)
                .setStarredType(starValue);

        PBMessage pbMessage = new PBMessage()
                .setCmd(yiproto.yichat.friend.Friend.FriendYimCMD.SET_CONTACT_TYPE_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> yiproto.yichat.friend.Friend.SetContactTypeRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<yiproto.yichat.friend.Friend.SetContactTypeRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(yiproto.yichat.friend.Friend.SetContactTypeRsp data) {
                        if (data.getResult() != yiproto.yichat.friend.Friend.SetContactTypeRsp.Result.OK_VALUE) {
                            setStarLayout.getSwitchCompat().setChecked(!star);
                            toast(data.getErrMsg());
                            return;
                        }

                        bindSetStar(star);
                    }
                });
    }

    /**
     * 绑定请求设置标星
     */
    private void bindSetStar(boolean star) {
        Observable.fromCallable((Callable<Object>) () -> DbManager.getDb().getAccountDao().updateSetStarState(id, star))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new EmptyObserver());
    }

    /**
     * 请求设置消息免打扰
     */
    private void requestSetNotDisturb(boolean isChecked) {
        int value = isChecked
                ? yiproto.yichat.friend.Friend.SetContactTypeReq.MuteNotificationType.NOTIFICATION_MUTE_VALUE
                : yiproto.yichat.friend.Friend.SetContactTypeReq.MuteNotificationType.NOTIFICATION_VALUE;

        yiproto.yichat.friend.Friend.SetContactTypeReq.Builder body = yiproto.yichat.friend.Friend.SetContactTypeReq
                .newBuilder()
                .setUserId(id)
                .setMuteNotification(value);

        PBMessage pbMessage = new PBMessage()
                .setCmd(yiproto.yichat.friend.Friend.FriendYimCMD.SET_CONTACT_TYPE_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.SetGroupMemberInfoRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Group.SetGroupMemberInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.SetGroupMemberInfoRsp data) {
                        if (data.getResult() != 0) {
                            toast(data.getErrMsg());
                            notDisturbLayout.getSwitchCompat().setChecked(!isChecked);
                            return;
                        }

                        saveMsgNotDisturbData(isChecked);
                    }

                    @Override
                    protected void onError(String msg) {
                        super.onError(msg);
                        notDisturbLayout.getSwitchCompat().setChecked(!isChecked);
                    }
                });
    }

    /**
     * 保存消息免打扰于数据库中
     */
    private void saveMsgNotDisturbData(boolean notDisturb) {
        Observable.fromCallable(() -> DbManager.getDb().getAccountDao().updateMsgNotDisturb(id, notDisturb))
                .compose(RxSchedulersHelper.applyIOSchedulers())
                .subscribe(new EmptyObserver());
    }

    /**
     * 请求设置置顶聊天
     */
    private void requestSetPinSession(boolean isChecked) {
        int value = isChecked
                ? yiproto.yichat.friend.Friend.SetContactTypeReq.OnTopType.ON_TOP_VALUE
                : yiproto.yichat.friend.Friend.SetContactTypeReq.OnTopType.NON_ON_TOP_VALUE;

        yiproto.yichat.friend.Friend.SetContactTypeReq.Builder body = yiproto.yichat.friend.Friend.SetContactTypeReq
                .newBuilder()
                .setUserId(id)
                .setOnTop(value);

        PBMessage pbMessage = new PBMessage()
                .setCmd(yiproto.yichat.friend.Friend.FriendYimCMD.SET_CONTACT_TYPE_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.SetGroupMemberInfoRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Group.SetGroupMemberInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.SetGroupMemberInfoRsp data) {
                        if (data.getResult() != 0) {
                            toast(data.getErrMsg());
                            pinSessionLayout.getSwitchCompat().setChecked(!isChecked);
                        }
                        savePinSession(isChecked);
                    }

                    @Override
                    protected void onError(String msg) {
                        super.onError(msg);
                        pinSessionLayout.getSwitchCompat().setChecked(!isChecked);
                    }
                });
    }

    /**
     * 只在置顶聊天于数据库中
     */
    private void savePinSession(boolean isPinSession) {
        Observable.fromCallable(() -> DbManager.getDb().getAccountDao().updatePinSession(id, isPinSession))
                .compose(RxSchedulersHelper.applyIOSchedulers())
                .subscribe(new EmptyObserver());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestData();
    }

    private void requestData() {
        Observable.fromCallable(() -> DbManager.getDb().getAccountDao().findById(id))
                .map(Friend::new)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<Friend>() {
                    @Override
                    public void onNext(Friend friend) {
                        bindUi(friend);
                    }
                });
    }

    private void bindUi(Friend friend) {
        this.friend = friend;
      //  ImageUtils.loadRadiusImage(avatarIv, friend.avatar, R.drawable.image_default_with_round, 5);
        ImageUtils.loadRadiusImage(avatarIv, friend.avatar, R.mipmap.default_head_icon, 5);
        nameTv.setText(friend.nickname);
        pinSessionLayout.getSwitchCompat().setChecked(friend.isPinSession);
        notDisturbLayout.getSwitchCompat().setChecked(friend.notDisturb);
        setStarLayout.getSwitchCompat().setChecked(friend.isMarkStar);
    }
}
