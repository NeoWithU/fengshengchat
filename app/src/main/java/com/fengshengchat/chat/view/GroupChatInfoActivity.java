package com.fengshengchat.chat.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseActivity;
import com.fengshengchat.base.base.BaseArrayListRecyclerAdapter;
import com.fengshengchat.base.base.ViewHolder;
import com.fengshengchat.chat.bean.GroupMemberBean;
import com.fengshengchat.chat.presenter.GroupChatInfoPresenter;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.ArrayList;

import yiproto.yichat.msg.MsgOuterClass;

public class GroupChatInfoActivity extends BaseActivity<GroupChatInfoPresenter> implements IGroupChatInfoView{
    private MemberAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat_info_layout);

        initView();
        initData();
    }

    private void initView(){
        RecyclerView memberList = findViewById(R.id.member_list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 6);
        memberList.setLayoutManager(gridLayoutManager);
        mAdapter = new MemberAdapter();
        mAdapter.setClickable(true);
        memberList.setAdapter(mAdapter);

        TitleBarLayout titleBarLayout = findViewById(R.id.title_bar);
        titleBarLayout.setTitleText("Group id: " + getIntent().getLongExtra("UID", 0L));
        titleBarLayout.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            public void onBackClick() {
                finish();
            }
            public void onActionImageClick() {}
            public void onActionClick() {}
        });
    }

    private void initData(){
        getPresenter().getGroupMemberList();
    }

    @Override
    protected void onReceiveMessage(MsgOuterClass.Msg message) {

    }

    @Override
    protected GroupChatInfoPresenter createPresenter() {
        long groupId = getIntent().getLongExtra("groupId", 0L);
        return new GroupChatInfoPresenter(groupId, this);
    }

    @Override
    public void setGroupMemberList(ArrayList<GroupMemberBean> list) {
        mAdapter.setData(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setGroupMemberListError(String tip) {
        toast(tip);
    }

    private class MemberAdapter extends BaseArrayListRecyclerAdapter<GroupMemberBean>{

        @Override
        protected void onBindData(ViewHolder holder, int position, GroupMemberBean data) {
            if(-1 == data.uid){ // Add button
                holder.getImageView(R.id.icon).setImageResource(R.mipmap.ic_launcher);
                holder.setText(R.id.name_tv, "");
            }else {
                holder.setText(R.id.name_tv, data.name);
                loadImage(holder.getImageView(R.id.icon), data.icon);
            }
        }

        @Override
        protected int getLayoutId(int viewType) {
            return R.layout.list_group_member_item_layout;
        }
    }
}
