package com.fengshengchat.chat.view;

import com.fengshengchat.base.mvp.IBaseView;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.Group;

import java.util.List;

/**
 * @author KRT
 * 2018/12/5
 */
public interface IChatListView extends IBaseView {
    void setChatList(List<ChatListBean> chatList);
    List<ChatListBean> getListData();
    void setChatListFailed(CharSequence msg);
    void setChatListItem(ChatListBean chatListBean);
    void updateItemFriendInfo(Account friend);
    void updateItemGroupInfo(Group group);
    void setCreateGroupFailed(String tip);
    void onDeleteChatItemCompleted(long hisUid, String tip);
    void updateUnreadCount(boolean increase, int unreadCount);
}
