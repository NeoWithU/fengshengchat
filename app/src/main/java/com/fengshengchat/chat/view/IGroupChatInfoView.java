package com.fengshengchat.chat.view;

import com.fengshengchat.base.mvp.IBaseView;
import com.fengshengchat.chat.bean.GroupMemberBean;

import java.util.ArrayList;

public interface IGroupChatInfoView extends IBaseView {
    void setGroupMemberList(ArrayList<GroupMemberBean> list);
    void setGroupMemberListError(String tip);
}
