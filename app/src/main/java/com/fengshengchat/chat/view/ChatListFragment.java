package com.fengshengchat.chat.view;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.MainActivity;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.activity.GroupBriefInfoActivity;
import com.fengshengchat.account.fragment.SearchFriendOrGroupFragment;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.app.SearchActivity;
import com.fengshengchat.app.activity.QrcodeCaptureActivity;
import com.fengshengchat.app.activity.ScanResultActivity;
import com.fengshengchat.base.BaseFragment;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.base.base.BaseRecyclerAdapter;
import com.fengshengchat.base.base.RecyclerViewDivider;
import com.fengshengchat.base.base.ViewHolder;
import com.fengshengchat.base.utils.TimeUtils;
import com.fengshengchat.chat.presenter.ChatListPresenter;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.DbChatList;
import com.fengshengchat.db.DbFriend;
import com.fengshengchat.db.Group;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.db.helper.TaskHelper;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.TitleBarLayout;
import com.zaaach.toprightmenu.MenuItem;
import com.zaaach.toprightmenu.TopRightMenu;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.chat.Chat;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/12/5
 */
public class ChatListFragment extends BaseFragment<ChatListPresenter> implements IChatListView {
    private ChatListAdapter mAdapter;
    private TopRightMenu mMoreMenu;
    private TitleBarLayout mTitleBarLayout;
    private TextView mSocketState;
    private View mSocketProgress;
    private View mEmptyView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RxBus.get().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RxBus.get().unregister(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_chat_list_layout;
    }

    @Override
    public void initView(View root) {
        mEmptyView = findViewById(R.id.empty_view);
        RecyclerView chatList = findViewById(R.id.chat_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        chatList.setLayoutManager(layoutManager);
        chatList.addItemDecoration(new RecyclerViewDivider(1, 0xFFDCDCDC));
        mAdapter = new ChatListAdapter();
        mAdapter.setClickable(true);
        chatList.setAdapter(mAdapter);

        initTitleBar();
    }

    private int titleResId = R.string.socket_connect_failed;
    private void setTitle(int title){
        if(/*null == mTitleBarLayout || */null == mSocketState){
            titleResId = title;
        }else {
//            mTitleBarLayout.setTitleText(title);
            mSocketState.setText(title);
            animateProgress(R.string.socket_connecting == title);
        }
    }

    private ValueAnimator rotateAnimator;
    private void animateProgress(boolean show){
        if(null == rotateAnimator){
            rotateAnimator = ValueAnimator.ofInt(0, 359);
            rotateAnimator.setRepeatMode(ValueAnimator.RESTART);
            rotateAnimator.setRepeatCount(ValueAnimator.INFINITE);
            rotateAnimator.setDuration(1000);
            rotateAnimator.setInterpolator(new LinearInterpolator());
            rotateAnimator.addUpdateListener(animation -> {
                Object animatedValue = animation.getAnimatedValue();
                if(animatedValue instanceof Integer){
                    mSocketProgress.setRotation((Integer) animatedValue);
                }
            });
        }
        mSocketProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        try {
            if (show) {
                if (!rotateAnimator.isStarted()) {
                    rotateAnimator.start();
                }
            } else {
                rotateAnimator.cancel();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void onSocketConnectStateChange(int state){
        int resId = R.string.socket_connect_ok;
        switch(state){
            case MainActivity.STATE_OK: resId = R.string.socket_connect_ok;
                break;
            case MainActivity.STATE_CONNECTING: resId = R.string.socket_connecting;
                break;
            case MainActivity.STATE_CONNECT_FAILED: resId = R.string.socket_connect_failed;
                break;

        }
        setTitle(resId);
        getPresenter().onSocketConnectStateChange(state);
    }

    public void onBaseInfoRefreshOK(){
        getPresenter().onBaseInfoRefreshOK();
    }

    private void initTitleBar(){
        mSocketState = findViewById(R.id.socket_state);
        mSocketProgress = findViewById(R.id.socket_progress);

        mTitleBarLayout = findViewById(R.id.title_bar);
        ToolbarFragment.hackStatusBar(mTitleBarLayout);
        if(titleResId > 0) {
//            mTitleBarLayout.setTitleText(titleResId);
            mSocketState.setText(titleResId);
        }
        mTitleBarLayout.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            public void onBackClick() {}
            public void onActionClick() {}
            public void onActionImageClick() {
                showMoreMenu(mTitleBarLayout/*.findViewById(R.id.titlebar_action_image)*/);
            }
        });
    }

    private void showMoreMenu(View more){
        FragmentActivity activity = getActivity();
        if(null == activity){
            return;
        }

        if(null == mMoreMenu){
            mMoreMenu = new TopRightMenu(activity);
            List<MenuItem> menuItems = new ArrayList<>();
            menuItems.add(new MenuItem(R.mipmap.create_group, getString(R.string.group_cheat)));
            menuItems.add(new MenuItem(R.mipmap.add_friend, getString(R.string.add_friend)));
            menuItems.add(new MenuItem(R.mipmap.scan, getString(R.string.scan)));
//            menuItems.add(new MenuItem(R.mipmap.private_chat, getString(R.string.private_chat)));

            mMoreMenu.showIcon(true).dimBackground(true).needAnimationStyle(true)
                    .setWidth(dp2px(120))
                    .setHeight(dp2px(44 * menuItems.size() + 10))
                    .setAnimationStyle(R.style.TRM_ANIM_STYLE)
                    .addMenuList(menuItems)
                    .setOnMenuItemClickListener(this::onMoreItemClick);
        }
        mMoreMenu.showAsDropDown(more, dp2px(330), 0);
    }

    private void onMoreItemClick(int position){
        switch(position){
            case 0: getPresenter().createGroupV2();
                break;
            case 1: getPresenter().addFriend();
                break;
            case 2: openQrCodeUI();
                break;
            case 3:
                break;
        }
    }

    public void openQrCodeUI() {
        SearchFriendOrGroupFragment.openQrcodeActivity(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (null != intentResult && null != intentResult.getContents()) {
            String s = intentResult.getContents();
            handleQrcode(s);
            return;
        }  else {
            if (IntentIntegrator.REQUEST_CODE == requestCode && resultCode == Activity.RESULT_OK) {
                String qrCode = data.getStringExtra(QrcodeCaptureActivity.EXTRA_CONTENT);
                if (!TextUtils.isEmpty(qrCode)) {
                    handleQrcode(qrCode);
                }
                return;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleQrcode(String s) {
        try {
            if (Protocol.isAuthUrl(s)) {
                Protocol.requestAuth(s);
                return;
            }

            if(Protocol.isWebLogin(s)){
                Protocol.requestWebLogin(s);
                return;
            }

            long id = Protocol.extractGid(s);

            if (0L != id) {
                requestSearchGroup(id, Protocol.extractUid(s));
            } else {
                id = Protocol.extractUid(s);
                if (0L != id) {
                    if (id == UserManager.getUser().uid) {
                        toast(R.string.search_self_uid);
                        return;
                    }
                    requestSearchFriend(id);
                } else {
                    Intent intent = new Intent(getContext(), ScanResultActivity.class);
                    intent.putExtra(ScanResultActivity.EXTRA_DATA, s);
                    startActivity(intent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 请求搜索好友
     */
    private void requestSearchFriend(long friendId) {
        Friend.SearchFriendReq.Builder body = Friend.SearchFriendReq.newBuilder()
                .setTargetUserId(friendId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.SEARCH_FRIEND_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> Friend.SearchFriendRsp.parseFrom(message.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(data -> {
                    if (data.getResult() != Friend.SearchFriendRsp.Result.OK_VALUE) {
                        toast(data.getErrMsg());
                        return;
                    }

                    if (!data.hasUserInfo()) {
                        toast(R.string.search_not_exist_friend);
                    }
                })
                .filter(data -> data.getResult() == Friend.SearchFriendRsp.Result.OK_VALUE)
                .observeOn(Schedulers.io())
                .map(SearchFriendOrGroupFragment::getBriefData)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new HttpRequestDialogObserver<BriefData>(getFragmentManager()) {
                    @Override
                    public void onNext(BriefData briefData) {
                        handleSearchFriendRes(briefData);
                    }
                });
    }

    /**
     * 处理搜索好友结果
     */
    private void handleSearchFriendRes(BriefData briefData) {
        Intent intent = new Intent(getContext(), FriendBriefInfoActivity.class);
        intent.putExtra(FriendBriefInfoActivity.KEY_DATA, briefData);
        startActivity(intent);
    }

    /**
     * 请求搜索群
     */
    private void requestSearchGroup(long groupId, long userId) {
        yiproto.yichat.group.Group.SearchGroupReq.Builder body = yiproto.yichat.group.Group.SearchGroupReq
                .newBuilder()
                .addGroupIdList(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(yiproto.yichat.group.Group.GroupYimCMD.SEARCH_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> yiproto.yichat.group.Group.SearchGroupRsp.parseFrom(message.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<yiproto.yichat.group.Group.SearchGroupRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(yiproto.yichat.group.Group.SearchGroupRsp data) {
                        handleSearchGroupRes(data, groupId, userId);
                    }
                });
    }

    /**
     * 处理请求搜索群结果
     */
    private void handleSearchGroupRes(yiproto.yichat.group.Group.SearchGroupRsp data, long groupId, long userId) {
        if (data.getResult() != yiproto.yichat.group.Group.SearchGroupRsp.SearchGroupResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        if (data.getGroupInfoListCount() == 0) {
            toast(R.string.group_not_exist);
            return;
        }

        Intent intent = new Intent(getContext(), GroupBriefInfoActivity.class);
        intent.putExtra("groupId", groupId);
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().getLocalChatList(false);
    }

    private void getChatList(){
        getPresenter().getLocalChatList(true);
    }

    @Override
    protected ChatListPresenter createPresenter() {
        return new ChatListPresenter(this);
    }

    private int mUnreadCount = 0;
    @Override
    public void updateUnreadCount(boolean increase, int unreadCount){
        if(increase){
            mUnreadCount += unreadCount;
        }else{
            mUnreadCount = unreadCount;
        }

        FragmentActivity activity = getActivity();
        if(activity instanceof MainActivity){
            ((MainActivity) activity).updateNewMessageCount(mUnreadCount);
        }
    }

    @Override
    public void setChatList(List<ChatListBean> chatList) {
        if(null != chatList) {
            mAdapter.getData().clear();
            mAdapter.getData().addAll(chatList);
            mAdapter.notifyDataSetChanged();
        }
        checkEmpty();
    }

    @Override
    public List<ChatListBean> getListData(){
        return mAdapter.getData();
    }

    @Override
    public void setChatListFailed(CharSequence msg) {
        toast(msg);
    }

    @Override
    public void setChatListItem(ChatListBean chatListBean) {
        if(null != chatListBean
            && yiproto.yichat.group.Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE == chatListBean.groupType){
            chatListBean.disableNotify = true;
        }

        LinkedList<ChatListBean> data = mAdapter.getData();
        for(int i = 0; i < data.size(); i++){
            if(data.get(i).hisUid == chatListBean.hisUid){
                data.remove(i);
                break;
            }
        }
        if(chatListBean.isPin) {//index 0 is search view
            data.add(1, chatListBean); //data.addFirst(chatListBean);
        }else{
            int i = 1;//index 0 is search view
            for(; i < data.size(); i++){
                if(!data.get(i).isPin){
                    break;
                }
            }
            data.add(i, chatListBean);
        }
        mAdapter.notifyDataSetChanged();
        checkEmpty();

        getPresenter().calcUnreadCount(data);
        sortList();
    }

    private void sortList(){
//        mTitleBarLayout.removeCallbacks(sortRunnable);
//        mTitleBarLayout.postDelayed(sortRunnable, 1000);
    }

    private Runnable sortRunnable = () -> getPresenter().getLocalChatList(false);

    @Override
    public void updateItemFriendInfo(Account friend){
        LinkedList<ChatListBean> data = mAdapter.getData();
        ChatListBean chatListBean;
        for(int i = 0; i < data.size(); i++){
            chatListBean = data.get(i);
            if(chatListBean.hisUid == friend.id){
                chatListBean.name = friend.nickname;
                chatListBean.icon = friend.avatar;
                chatListBean.disableNotify = friend.notDisturb;
                chatListBean.isPin = friend.isPinSession;
                if(null == chatListBean.icon || chatListBean.icon.length() <= 0){
                    chatListBean.icon = "icon is null";
                }
                mAdapter.notifyDataSetChanged();
                break;
            }
        }
        checkEmpty();
        sortList();
    }

    @Override
    public void updateItemGroupInfo(Group group){
        LinkedList<ChatListBean> data = mAdapter.getData();
        ChatListBean chatListBean;
        for(int i = 0; i < data.size(); i++){
            chatListBean = data.get(i);
            if(chatListBean.hisUid == group.id){
                chatListBean.name = group.name;
                chatListBean.icon = group.avatar;
                chatListBean.disableNotify = group.notDisturb;
                chatListBean.isPin = group.isPinSession;
                chatListBean.groupMemberCount = group.memberCount;
                chatListBean.groupType = group.type;
                if(null == chatListBean.icon || chatListBean.icon.length() <= 0){
                    chatListBean.icon = "icon is null";
                }
                if(yiproto.yichat.group.Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE == group.type){
                    chatListBean.disableNotify = true;
                }
                mAdapter.notifyDataSetChanged();
                break;
            }
        }
        checkEmpty();
        sortList();
    }

    private void checkEmpty(){
        boolean isEmpty = mAdapter.getItemCount() - 1 <= 0; //index 0 is search view
        mEmptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setCreateGroupFailed(String tip) {
        toast(tip);
    }

    @Override
    public void onDeleteChatItemCompleted(long hisUid, String tip) {
        if(hisUid > 0){
            List<ChatListBean> data = mAdapter.getData();
            for(int i = 0; i < data.size(); i++){
                if(hisUid == data.get(i).hisUid){
                    data.remove(i);
                    mAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }
//        toast(tip);
        checkEmpty();
    }

    @Override
    public void onPusherReady() {
        super.onPusherReady();
        getPresenter().setPusher(getPusher());
        getChatList();
    }

    @Override
    public boolean handleMessage(MsgOuterClass.Msg message) {
        return getPresenter().handleMessage(message);
    }

    private void showMessageMenu(int pos, View v){
        PopupMenu popupMenu = new PopupMenu(getContext(), v);
        Menu menu = popupMenu.getMenu();
        menu.add(0, 1, 1, R.string.delete);
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(item -> {
            switch(item.getItemId()){
                case 1: getPresenter().deleteChatItem(mAdapter.getData().get(pos));
                    break;
            }
            return false;
        });
    }

    public void startChat(long uid){
        LinkedList<ChatListBean> datas = mAdapter.getData();
        for(ChatListBean data : datas){
            if(data.hisUid == uid){
                getPresenter().setAlreadyRead(data);
                data.unreadCount = 0;
                mAdapter.notifyDataSetChanged();
                if(Constant.CHAT_TYPE_SEARCH == data.chatType) {
                    startActivity(new Intent(getContext(), SearchActivity.class));
                }else if(Constant.CHAT_TYPE_GROUP == data.chatType) {
                    ChatActivity.startChat(getContext(), data.hisUid, data.name, data.icon, true, data.groupType, data.groupMemberCount);
                }else{
                    ChatActivity.startSingleChat(getContext(), data.hisUid, data.name);
                }
                break;
            }
        }
    }

    private class ChatListAdapter extends BaseRecyclerAdapter<ChatListBean, LinkedList<ChatListBean>> {
        private static final int VIEW_TYPE_SEARCH = 1;
        @Override
        protected LinkedList<ChatListBean> getList() {
            return new LinkedList<>();
        }

        protected void onBindData(ViewHolder holder, int position, ChatListBean data) {
            if(Constant.CHAT_TYPE_SEARCH == data.chatType){
                return;
            }

            holder.setText(R.id.time, convertTime(data.time));
            switch(data.messageType) {
                default:
                case Chat.MsgType.MSGTYPE_TEXT_VALUE:
                    holder.setText(R.id.content, data.lastMessage);
                    break;
                case Chat.MsgType.MSGTYPE_IMAGE_VALUE:
                    holder.setText(R.id.content, data.lastMessage);
                    break;
                case Chat.MsgType.MSGTYPE_VOICE_VALUE:
                    holder.setText(R.id.content, data.lastMessage);
                    break;
                case Chat.MsgType.MSGTYPE_CARD_VALUE:
                    holder.setText(R.id.content, data.lastMessage);
                    break;
            }

            long unreadCount = data.unreadCount;
            TextView unreadTextView = holder.getView(R.id.unread_count);
            if(unreadCount > 0) {
                if(data.disableNotify){
                    unreadTextView.setTextSize(6);
                    unreadTextView.setText("");
                }else {
                    unreadTextView.setTextSize(11);
                    unreadTextView.setText(unreadCount > 99 ? "99+" : (unreadCount + ""));
                }
            }

            unreadTextView.setVisibility(unreadCount > 0 ? View.VISIBLE : View.GONE);
            holder.getView(R.id.disable_notify).setVisibility(data.disableNotify ? View.VISIBLE : View.GONE);
            holder.getRootView().setBackgroundResource(data.isPin
                    ? R.drawable.selector_chat_list_pin_item
                    : R.drawable.selector_chat_list_item);

            if(null == data.name || data.name.length() <= 0) {
                data.name = data.hisUid + "";
                holder.setText(R.id.name_tv, data.name);
                updateHisInfoFromServer(Constant.CHAT_TYPE_GROUP == data.chatType, data.hisUid);
            }else{
                holder.setText(R.id.name_tv, data.name);
            }
            showIconAndName(holder.getImageView(R.id.icon), data);
        }

        @Override
        protected int getLayoutId(int viewType) {
            if(VIEW_TYPE_SEARCH == viewType){
                return R.layout.search_btn;
            }
            return R.layout.list_chat_list_item_layout;
        }

        @Override
        protected int getViewType(int position, ChatListBean data) {
            if(Constant.CHAT_TYPE_SEARCH == data.chatType){
                return VIEW_TYPE_SEARCH;
            }
            return super.getViewType(position, data);
        }

        private long lastClickTime = 0;
        @Override
        protected void onItemClick(int position, ChatListBean data, ViewHolder viewHolder) {
            long cur = System.currentTimeMillis();
            if(cur - lastClickTime < 1200){
                return;
            }
            lastClickTime = cur;

            getPresenter().setAlreadyRead(data);
            data.unreadCount = 0;
            notifyDataSetChanged();
            if(Constant.CHAT_TYPE_SEARCH == data.chatType) {
                startActivity(new Intent(getContext(), SearchActivity.class));
            }else if(Constant.CHAT_TYPE_GROUP == data.chatType) {
                ChatActivity.startChat(getContext(), data.hisUid, data.name, data.icon, true, data.groupType, data.groupMemberCount);
            }else{
                ChatActivity.startSingleChat(getContext(), data.hisUid, data.name);
            }
        }

        @Override
        protected boolean onItemLongClick(int position, ChatListBean data, ViewHolder viewHolder) {
            if(Constant.CHAT_TYPE_SEARCH != data.chatType) {
                showMessageMenu(position, viewHolder.getView(R.id.just_for_popup_menu));
            }
            return super.onItemLongClick(position, data, viewHolder);
        }

        private String convertTime(long time){
            return TimeUtils.getFriendlyForList(getContext(), time * 1000);
        }

        private void showIconAndName(ImageView imageView, ChatListBean bean){
            FragmentActivity activity = getActivity();
            if(activity instanceof BaseActivity){
                String url = bean.icon;
                boolean isSingle = Constant.CHAT_TYPE_SINGLE == bean.chatType;
                int defaultIcon = isSingle ? R.mipmap.default_head_icon : R.drawable.group_ic_default;
                if(null == url || url.length() <= 0){
                    bean.icon = "Already request";
                    if(isSingle) {
                        getPresenter().getFriendInfo(bean.hisUid);
                    }else{
                        getPresenter().getGroupInfo(bean.hisUid);
                    }
                    imageView.setImageResource(defaultIcon);
                }else {
                    ((BaseActivity) activity).loadRadiusImage(imageView, url, defaultIcon,5);
                }
            }
        }
    }

    private void updateHisInfoFromServer(boolean isGroup, long id){
        if(isGroup){
            printe("----1-----更新群资料-------------");
            TaskHelper.getInstance().post(
                    new com.fengshengchat.db.helper.Task(com.fengshengchat.db.helper.Task.TASK_UPDATE_GROUP_INFO, id));
        }else{
            printe("----1-----更新好友资料 找后台-------------");
//            TaskHelper.getInstance().post(
//                    new com.yichat.db.helper.Task(com.yichat.db.helper.Task.TASK_UPDATE_GROUP_INFO, hisUid));
        }
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_GROUP)})
    public void updatedDeletedGroup(Long groupId) {
        LinkedList<ChatListBean> data = mAdapter.getData();
        ChatListBean chatListBean;
        for(int i = 0; i < data.size(); i++){
            chatListBean = data.get(i);
            if(Constant.CHAT_TYPE_GROUP == chatListBean.chatType && chatListBean.hisUid == groupId){
                getPresenter().deleteChatItem(chatListBean);
                break;
            }
        }
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_FRIEND)})
    public void updateDeleteFriend(Long uid){
        LinkedList<ChatListBean> data = mAdapter.getData();
        ChatListBean chatListBean;
        for(int i = 0; i < data.size(); i++){
            chatListBean = data.get(i);
            if(Constant.CHAT_TYPE_SINGLE == chatListBean.chatType && chatListBean.hisUid == uid){
                getPresenter().deleteChatItem(chatListBean);
                break;
            }
        }
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_GROUP_INFO)})
    public void groupInfoFromServer(Long gid) {
        Disposable subscribe = Observable.fromCallable(() -> {
            com.fengshengchat.db.Group groupInfo = DbFriend.getGroupInfo(gid);
            printe("----2------groupInfo: " + groupInfo);
            if(null != groupInfo) {
                DbChatList.updateChatItemGroupInfo(groupInfo);
            }
            return groupInfo;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(group -> {
                    if(null != group){
                        updateItemGroupInfo(group);
                    }
                }, throwable -> printe(throwable.getMessage()));
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_SINGLE_FRIEND)})
    public void friendInfoFromServer(Long uid) {
        Disposable subscribe = Observable.fromCallable(() -> {
            Account friend = DbFriend.getFriend(uid);
            printe("----1------friend: " + friend);
            if(null != friend) {
                DbChatList.updateChatItemSingleInfo(friend);
            }
            return friend;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(friend -> {
                    if(null != friend){
                        getPresenter().getFriendInfo(friend.id);
                    }
                }, throwable -> printe(throwable.getMessage()));
    }

}
