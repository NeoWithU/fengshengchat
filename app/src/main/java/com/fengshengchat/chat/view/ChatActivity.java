package com.fengshengchat.chat.view;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.arialyy.annotations.Download;
import com.arialyy.aria.core.Aria;
import com.arialyy.aria.core.download.DownloadTask;
import com.blankj.utilcode.util.SDCardUtils;
import com.blankj.utilcode.util.SPUtils;
import com.google.gson.Gson;
import com.google.protobuf.InvalidProtocolBufferException;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.base.BaseActivity;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.base.BaseRecyclerAdapter;
import com.fengshengchat.base.base.ViewHolder;
import com.fengshengchat.base.bean.ChatInputMediaBean;
import com.fengshengchat.base.bean.IChatInputListener;
import com.fengshengchat.base.bean.PressRecordResult;
import com.fengshengchat.base.utils.NetUtils;
import com.fengshengchat.base.utils.PermissionUtils;
import com.fengshengchat.base.utils.TimeUtils;
import com.fengshengchat.base.utils.ToastUtils;
import com.fengshengchat.base.view.ChatInputView;
import com.fengshengchat.chat.bean.CardContentBean;
import com.fengshengchat.chat.bean.ImageContentBean;
import com.fengshengchat.chat.presenter.ChatPresenter;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.DbChatList;
import com.fengshengchat.db.DbFriend;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.GroupMember;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.db.helper.TaskHelper;
import com.fengshengchat.file.FileConstant;
import com.fengshengchat.fun.FunGroupActivity;
import com.fengshengchat.fun.FunGroupNotifyActivity;
import com.fengshengchat.group.activity.GroupInfoActivity;
import com.fengshengchat.group.activity.SelectContactActivity;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.push.NotifyUtils;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.AutoSplitTextView;
import com.fengshengchat.widget.ChatVoiceView;
import com.fengshengchat.widget.ImagePreviewActivity;
import com.fengshengchat.widget.TitleBarLayout;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.chat.Chat;
import yiproto.yichat.group.Group;
import yiproto.yichat.msg.MsgOuterClass;

public class ChatActivity extends BaseActivity<ChatPresenter> implements IChatView {
    protected ChatAdapter mAdapter;
    private RecyclerView mListView;
    private LinearLayoutManager linearLayoutManager;
    protected ChatInputView mInputView;
    private View mRecordVoiceStateRoot;
    private View mRecordStateCancelIcon;
    private ImageView mRecordStateSpeakingIcon;
    private TextView mRecordVoiceState;
    private TitleBarLayout titleBarLayout;

    private TextView mNotifyContent;
    private View mNotifyRoot;

    protected boolean isGroup;
    protected int groupType;
    protected long hisUid;

    public static void startChat(Context context, long hisUid, boolean isGroup){
        Disposable subscribe = Observable.fromCallable(() -> {
                ChatListBean chatItem = DbChatList.getChatItem(hisUid);
                if(null == chatItem){
                    chatItem = new ChatListBean();
                    chatItem.hisUid = hisUid;
                    chatItem.name = "--";
                    chatItem.icon = "";
                    chatItem.chatType = isGroup ? Constant.CHAT_TYPE_GROUP : Constant.CHAT_TYPE_SINGLE;
                    chatItem.groupType = Group.GROUP_TYPE.GROUP_TYPE_NORMAL_VALUE;
    //                        DbChatList.insertEmptyGroupItem(chatItem);
                }

                if(isGroup){
                    com.fengshengchat.db.Group groupInfo = DbFriend.getGroupInfo(hisUid);
                    if(null != groupInfo){
                        chatItem.name = groupInfo.name;
                        chatItem.icon = groupInfo.avatar;
                        chatItem.chatType = Constant.CHAT_TYPE_GROUP;
                        chatItem.groupType = groupInfo.type;
                    }
                }else{
                    Account friend = DbFriend.getFriend(hisUid);
                    if(null != friend){
                        chatItem.name = friend.nickname;
                        chatItem.icon = friend.avatar;
                        chatItem.chatType = Constant.CHAT_TYPE_SINGLE;
                    }
                }
                return chatItem;
            })
            .compose(RxSchedulersHelper.applyIO2MainSchedulers())
            .subscribe(chatItem -> {
                if (null != chatItem) {
                    Intent intent = new Intent();
                    intent.putExtra("who", hisUid);
                    intent.putExtra("name", chatItem.name);
                    intent.putExtra("icon", chatItem.icon);
                    boolean group = Constant.CHAT_TYPE_GROUP == chatItem.chatType;
                    intent.putExtra("isGroup", group);
                    intent.putExtra("groupType", chatItem.groupType);
                    if (group && Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE == chatItem.groupType) {
                        intent.setComponent(new ComponentName(context, FunGroupActivity.class));
                    }else{
                        intent.setComponent(new ComponentName(context, ChatActivity.class));
                    }
                    context.startActivity(intent);
                } else {
                    ToastUtils.show(R.string.start_chat_failed);
                }
            });
    }

    public static void startSingleChat(Context context, long hisUid, String name){
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("who", hisUid);
        intent.putExtra("name", name);
        intent.putExtra("isGroup", false);
        context.startActivity(intent);
    }

    public static void startChat(Context context, long hisUid, String name, String icon, boolean isGroup, int groupType, int groupMemberCount){
        Intent intent = new Intent();
        intent.putExtra("who", hisUid);
        intent.putExtra("name", name);
        intent.putExtra("icon", icon);
        intent.putExtra("isGroup", isGroup);
        intent.putExtra("groupType", groupType);
        intent.putExtra("groupMemberCount", groupMemberCount);
        if (isGroup && Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE == groupType) {
            intent.setComponent(new ComponentName(context, FunGroupActivity.class));
        }else{
            intent.setComponent(new ComponentName(context, ChatActivity.class));
        }
        context.startActivity(intent);
    }

    private static final List<Activity> mChatActivityList = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        printe("start chat: " + mChatActivityList);
        for(Activity activity : mChatActivityList){
            if(activity instanceof ChatActivity){
                activity.finish();
            }
        }
        mChatActivityList.clear();
        mChatActivityList.add(this);

//        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        super.onCreate(savedInstanceState);
        setContentView(getContentLayout());

        if(0 == hisUid){
            toast(R.string.invalid_uid);
            finish();
        }else {
            initView();
            runDelay(this::getHistoryMessage, 200);
        }
        super.addPushObserver();
        RxBus.get().register(this);
        Aria.download(this).register();
    }

    protected int getContentLayout(){
        return R.layout.activity_chat_layout;
    }

    @Override
    protected void onDestroy() {
        mChatActivityList.remove(this);
        printe("exit chat" + mChatActivityList);

        super.removePushObserver();
        releaseMediaPlayer();
        RxBus.get().unregister(this);
        Aria.download(this).unRegister();
        super.onDestroy();
    }

    private void tagAlreadyRead(){
        getPresenter().updateAlreadyRead(hisUid);
    }

    private void updateHisInfoFromServer(){
        if(isGroup){
            printe("---------更新群资料-------------");
            TaskHelper.getInstance().post(
                    new com.fengshengchat.db.helper.Task(com.fengshengchat.db.helper.Task.TASK_UPDATE_GROUP_INFO, hisUid));
        }else{
            printe("---------更新好友资料 找后台-------------");
//            TaskHelper.getInstance().post(
//                    new com.yichat.db.helper.Task(com.yichat.db.helper.Task.TASK_UPDATE_GROUP_INFO, hisUid));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotifyUtils.setCurrrentChatID(hisUid);
        updateHisInfoFromServer();
    }

    @Override
    protected void onPause() {
        if(null != popupMenu){
            popupMenu.dismiss();
            popupMenu = null;
        }
        stopPlayVoiceFile();
        if (null != mCurrentMediaPlayBean) {
            mCurrentMediaPlayBean.isVoicePlaying = false;
            mCurrentMediaPlayBean = null;
            mAdapter.notifyDataSetChanged();
        }
        super.onPause();
        NotifyUtils.setCurrrentChatID(-1L);
    }

    @Override
    protected void addPushObserver() {}

    @Override
    protected void removePushObserver() {}

    @Override
    protected void onReceiveMessage(MsgOuterClass.Msg message) {
        getPresenter().handleMessage(message);
    }

    @Override
    protected void onPusherReady() {
        super.onPusherReady();
        getPresenter().setPusher(getPusher());
        tagAlreadyRead();
    }

    @Override
    protected ChatPresenter createPresenter() {
        hisUid = getIntent().getLongExtra("who", 0);
        isGroup = getIntent().getBooleanExtra("isGroup", false);
        groupType = getIntent().getIntExtra("groupType", Group.GROUP_TYPE.GROUP_TYPE_UNKNOW_VALUE);
        return new ChatPresenter(hisUid, isGroup, groupType, this);
    }

    private void getHistoryMessage(){
        if(isGettingHistory || noMoreData)
            return;
        printe("------------getHistoryMessage------------- " + offset);
        isGettingHistory = true;
        getPresenter().getHistoryMessage(offset);
    }

    @CallSuper
    protected void initView(){
        String name = getIntent().getStringExtra("name");
        if(null == name || name.length() <= 0){
            name = hisUid + "";
        }
        String icon = getIntent().getStringExtra("icon");
        int groupMemberCount = getIntent().getIntExtra("groupMemberCount", 0);
        getPresenter().setGroupInfo(name, icon);
        initTitleBar(name + (groupMemberCount <= 0 ? "" : ("(" + groupMemberCount + ")")));
        initContentList();
        initEditArea();
        initRoot();
        initNotify();
    }

    private void getGroupInfo(){
        PBMessage pbMessage = new PBMessage();
        pbMessage.setCmd(Group.GroupYimCMD.GET_GROUP_INFO_REQ_CMD_VALUE)
                .setBody(Group.GetGroupInfoReq.newBuilder()
                        .addGroupIdList(hisUid));

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<MsgOuterClass.Msg>(getSupportFragmentManager()) {
                    public void onNext(MsgOuterClass.Msg msg) {
                        onGroupInfoBack(msg);
                    }
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }

    private void onGroupInfoBack(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                Group.GetGroupInfoRsp getGroupInfoRsp = Group.GetGroupInfoRsp.parseFrom(msg.getBody());
                if(Group.GetGroupInfoRsp.GetGroupInfoResult.OK_VALUE == getGroupInfoRsp.getResult()){
                    List<Group.GroupInfo> groupInfoListList = getGroupInfoRsp.getGroupInfoListList();
                    if(null != groupInfoListList && groupInfoListList.size() > 0){
                        Group.GroupInfo groupInfo = groupInfoListList.get(0);
                        if(Group.GroupInfo.NoticeType.NOTICE_ON_TOP_VALUE == groupInfo.getOnTopNotice()){
                            onGroupNotifyBack(groupInfo.getGroupNotice());
                        }
                    }
                }
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
    }

    private void initNotify(){
        mNotifyContent = findViewById(R.id.notify_content);
        mNotifyRoot = findViewById(R.id.notify);

        View.OnClickListener l = v -> {
            switch (v.getId()) {
                case R.id.notify:
                    Intent intent = new Intent(getContext(), FunGroupNotifyActivity.class);
                    intent.putExtra("content", mNotifyContent.getText());
                    intent.putExtra("UID", hisUid);
                    startActivity(intent);
                    break;
                case R.id.close_tip:
                    closeTip();
                    break;
            }
        };
        mNotifyRoot.setOnClickListener(l);
        findViewById(R.id.close_tip).setOnClickListener(l);
        closeTip = SPUtils.getInstance().getBoolean(UserManager.getUser().uid + "_" + hisUid + "_closeTip", false);
        if(!closeTip){
            getGroupInfo();
        }
    }

    public void setShowGroupNotify(boolean show){
        mNotifyRoot.setVisibility(show && !closeTip ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onReceiveGroupNotify(CharSequence msg) {
        getGroupInfo();
    }

    private void onGroupNotifyBack(CharSequence msg){
        if(null == msg || msg.length() <= 0){
            mNotifyRoot.setVisibility(View.GONE);
            return;
        }

        mNotifyContent.setText(msg);
        mNotifyRoot.setVisibility(closeTip ? View.GONE : View.VISIBLE);
    }

    private boolean closeTip = false;
    private void closeTip(){
        closeTip = true;
        mNotifyRoot.setVisibility(View.GONE);
        long uid = UserManager.getUser().uid;
        SPUtils.getInstance().put(uid + "_" + hisUid + "_closeTip", true);
    }

    private void initRoot(){
        mListView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                int offset = oldBottom - bottom;
                if(Math.abs(offset) > dp2px(150)){
                    int canScroll = mListView.computeVerticalScrollRange()
                            - mListView.computeVerticalScrollExtent()
                            - mListView.computeVerticalScrollOffset();
                    if(offset > 0 || canScroll > -offset) {
                        mListView.scrollBy(0, offset);
                    }
                }
            }
        });
    }

    private void initEditArea(){
        mRecordVoiceStateRoot = findViewById(R.id.speak_state_root);
        mRecordStateCancelIcon = findViewById(R.id.cancel_icon);
        mRecordStateSpeakingIcon = findViewById(R.id.speak_icon);
        mRecordVoiceState = findViewById(R.id.record_voice_state);

        mInputView = findViewById(R.id.input_area);
        int wh = dp2px(50);
        mInputView.setMediaIconSize(wh, wh);
        mInputView.setCancelOffset(wh);
        mInputView.setMaxSeconds(60);
        mInputView.setHint(R.string.edit_hint);
        mInputView.setHintColor(0xFFBBBBBB);
        mInputView.setMediaData(generateChatInputMediaList());
        mInputView.setMaxInputLength(Protocol.MAX_INPUT_LENGTH);
        mInputView.setChatInputListener(new IChatInputListener() {
            public void onShowPanelTypeChanged(int showType) {
                onShowPanelTypeChange(showType);
            }
            public void onSendTextClick(String content) {
                getPresenter().sendTextMessage(content);
            }
            public String getSaveFilePath() {
                if (FileConstant.isMediaMounted()) {
                    String fileName = TimeUtils.getTimeMillis() + ".amr";
                    return FileConstant.getFileExternalStorageVoiceAbsolutePath() + "/" + fileName;
                }else{
                    return "";
                }
            }
            public void onInputLengthExceed(CharSequence s, int maxLength) {
                printe(s.toString());
                toast(R.string.input_exceed);
            }
            public void onPressed() {
                stopVoicePlay();
                mRecordVoiceState.setText(R.string.move_up_cancel);
                mRecordVoiceStateRoot.setVisibility(View.VISIBLE);
                mRecordStateSpeakingIcon.setVisibility(View.VISIBLE);
                mRecordStateCancelIcon.setVisibility(View.INVISIBLE);
                startRecordSpeakingAnimation(true);
            }
            public void onReleased(boolean isCanceled, PressRecordResult recordResult) {
                startRecordSpeakingAnimation(false);
                mRecordVoiceStateRoot.setVisibility(View.GONE);
                onRecordCompleted(recordResult);
            }
            public void onWantToCanceled(boolean wantToCancel) {
                mRecordStateCancelIcon.setVisibility(wantToCancel ? View.VISIBLE : View.INVISIBLE);
                mRecordStateSpeakingIcon.setVisibility(wantToCancel ? View.INVISIBLE : View.VISIBLE);
                startRecordSpeakingAnimation(!wantToCancel);

                mRecordVoiceState.setText(wantToCancel ? R.string.release_to_cancel : R.string.move_up_cancel);
            }
            public void onRecording(int seconds) {}
        });
    }

    protected void onShowPanelTypeChange(int showType) {
        if(ChatInputView.SHOW_TYPE_PANEL == showType){
            mListView.post(new Runnable() {
                public void run() {
                    mListView.scrollBy(0, mInputView.getHeight() - dp2px(50));
                }
            });
        }else if(ChatInputView.SHOW_TYPE_CLOSE == showType){
//            mListView.scrollBy(0, -mInputView.getHeight());
        }
    }

    private void stopVoicePlay(){
        if(null != mCurrentMediaPlayBean) {
            stopPlayVoiceFile();
            mCurrentMediaPlayBean.isVoicePlaying = false;
            mAdapter.notifyDataSetChanged();
            mCurrentMediaPlayBean = null;
        }
    }

    private AnimationDrawable mSpeakingIconAnimator;
    private void startRecordSpeakingAnimation(boolean start){
        if(null == mSpeakingIconAnimator) {
            Drawable drawable = mRecordStateSpeakingIcon.getDrawable();
            if (drawable instanceof AnimationDrawable) {
                mSpeakingIconAnimator = (AnimationDrawable) drawable;
            }
        }
        if(null != mSpeakingIconAnimator){
            if(start) {
                mSpeakingIconAnimator.start();
            }else{
                mSpeakingIconAnimator.stop();
            }
        }
    }

    private void onRecordCompleted(PressRecordResult record) {
        getPresenter().sendVoiceMessage(record);
    }

    private ArrayList<ChatInputMediaBean> generateChatInputMediaList(){
        ArrayList<ChatInputMediaBean> list = new ArrayList<>();
        list.add(new ChatInputMediaBean(R.string.chat_media_image, R.mipmap.chat_media_image,
                this::pickImage));
        list.add(new ChatInputMediaBean(R.string.chat_media_camera, R.mipmap.chat_media_camera,
                this::takePhoto));
//        list.add(new ChatInputMediaBean(R.string.chat_media_file, R.mipmap.chat_media_file,
//                this::pickFile));
        list.add(new ChatInputMediaBean(R.string.chat_media_contact, R.mipmap.chat_media_contact,
                this::pickContact));
        return list;
    }

    private void pickImage(){
        requestPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, granted -> {
            if(granted){
                Matisse.from(ChatActivity.this)
                        .choose(MimeType.ofImage(), true)
                        .countable(true)
                        .maxSelectable(9)
//                .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//                .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .thumbnailScale(0.85f)
                        .imageEngine(new GlideEngine())
                        .forResult(REQUEST_IMAGE_CODE);
            }else{
                PermissionUtils.tipPermissionDenied(ChatActivity.this, getString(R.string.chat_media_image));
            }
        });
    }

    protected void hideInputView(){
        if(!mInputView.isSpeakButtonShow()) {
            mInputView.resetPanel();
        }
    }

    private static final int REQUEST_IMAGE_CODE = 100;
    private static final int TAKE_PHOTO_REQUEST_CODE = 101;
    private static final int SELECT_FRIEND = 102;
    private static final int SELECT_FRIEND_CARD = 103;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(RESULT_OK != resultCode){
            return;
        }

        switch(requestCode){
            case REQUEST_IMAGE_CODE:
                List<String> uris = Matisse.obtainPathResult(data);
                getPresenter().sendImageMessage(uris);
                if(null != uris && uris.size() > 0){
                    hideInputView();
                }
                break;
            case TAKE_PHOTO_REQUEST_CODE:
                if(null != captureImageFile){
                    getPresenter().sendImageMessage(captureImageFile.getAbsolutePath());
                    captureImageFile = null;
                }
                break;
            case SELECT_FRIEND:
                if(null != data) {
                    int dataIndex = data.getIntExtra("dataIndex", -1);
                    long uid = data.getLongExtra("uid", 0);
                    sendToFriend(uid, dataIndex);
                }
                break;
            case SELECT_FRIEND_CARD:
                if(null != data) {
                    long uid = data.getLongExtra("uid", 0);
                    String name = data.getStringExtra("name");
                    String icon = data.getStringExtra("icon");
                    String text = data.getStringExtra("textMessage");
                    sendCard(uid, name, icon, text);
                }
                break;
        }
    }

    private void sendCard(long uid, String name, String icon, String text){
        getPresenter().sendCardMessage(uid, name, icon);
        if(null != text && text.length() > 0){
            getPresenter().sendTextMessage(text);
        }
    }

    private void sendToFriend(long uid, int dataIndex){
        ChatRecord dataAt = mAdapter.getDataAt(dataIndex);
        if(null != dataAt){
            switch(dataAt.messageType){
                case Chat.MsgType.MSGTYPE_TEXT_VALUE://other type send after upload file success
                    getPresenter().sendTextMessageToOther(uid, dataAt);
                    break;
                case Chat.MsgType.MSGTYPE_IMAGE_VALUE:
                    getPresenter().sendImageMessageToOther(uid, dataAt);
                    break;
                case Chat.MsgType.MSGTYPE_VOICE_VALUE:
                    getPresenter().sendVoiceMessageToOther(uid, dataAt);
                    break;
                case Chat.MsgType.MSGTYPE_CARD_VALUE:
                    getPresenter().sendCardMessageToOther(uid, dataAt);
                    break;
            }
        }
    }

    private final String[] takePhotoPermissions = {Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private void takePhoto(){
        requestPermission(takePhotoPermissions, granted -> {
            if(granted){
                startCaptureImage();
            }else{
//                toast(R.string.permission_deny);
                PermissionUtils.tipPermissionDenied(ChatActivity.this, getString(R.string.chat_media_camera));
            }
        });
    }

    private void startCaptureImage() {
        File file = getCaptureImageFile();
        if (file == null) {
            return;
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authority = getContext().getPackageName() + ".fileprovider";
            uri = FileProvider.getUriForFile(getContext(), authority, file);
        } else {
            uri = Uri.fromFile(file);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, TAKE_PHOTO_REQUEST_CODE);
    }

    private static final String CAPTURE_TEMP_FILENAME = "temp.jpg";
    private File captureImageFile;

    private File getCaptureImageFile() {
        if (!SDCardUtils.isSDCardEnable()) {
            toast(R.string.sdcard_not_available);
            return null;
        }

        String path = FileConstant.getFileExternalStorageImageAbsolutePath();
        if (captureImageFile == null) {
            captureImageFile = new File(path, "/" + System.currentTimeMillis() + ".jpg");
            captureImageFile.getParentFile().mkdirs();
        }
        return captureImageFile;
    }

    private void pickFile(){
        toast("file");
    }

    private void pickContact(){
        selectFriendForCard();
    }

    protected ChatAdapter getAdapter(){
        return new ChatAdapter();
    }

    private void initContentList(){
        mListView = findViewById(R.id.chat_content_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mListView.setLayoutManager(linearLayoutManager);
        mAdapter = getAdapter();
        mAdapter.setAscend(false);
        chatRecord = new ChatRecord();
        chatRecord.content = getString(R.string.no_more_data);
        chatRecord.messageType = Constant.MESSAGE_TYPE_LOAD_DATA;
        chatRecord.messageState = Constant.MESSAGE_STATE_SENDING;
        mAdapter.getData().add(chatRecord);

        addTimeTitle();
//        mAdapter.setClickable(true);
        mListView.setAdapter(mAdapter);
        mListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if(RecyclerView.SCROLL_STATE_IDLE == newState){
                    int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                    if(firstVisibleItemPosition <= 0){
                        getHistoryMessage();
                    }
                }
            }
        });
        mListView.setOnTouchListener((v, event) -> {
            hideInputView();
            return false;
        });
    }

    public class TimeLine extends RecyclerView.ItemDecoration {
        private int mDividerSpan;
        private final Paint mPaint = new Paint();
        private final int offsetY;

        TimeLine(){
            mDividerSpan = dp2px(16);
            offsetY = -dp2px(3);
            mPaint.setTextSize(dp2px(10));
            mPaint.setColor(0xFFAAAAAA);
            mPaint.setAntiAlias(true);
        }

        private String lastShowString = "";
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int span = 0;
            int childCount = parent.getChildCount();
            View child;

            lastShowString = "";
            int position;
            Object timestamp;
            Object positionO;
            String friendlyTime;
            String prevFriendlyTime;

            for(int i = 0; i < childCount; i++){
                child = parent.getChildAt(i);
                positionO = child.getTag();
                timestamp = child.getTag(R.id.id_chat_record_data_time_long);
                child.setTag(R.id.id_chat_record_data_time_string, "");

                if(positionO instanceof Integer && timestamp instanceof Long){
                    position = (int) positionO;
                    if(0 == (Long)timestamp){
                        continue;
                    }

                    friendlyTime = TimeUtils.getFriendlyDate(getContext(), (Long)timestamp * 1000);

                    if(0 == i && position > 0){
                        prevFriendlyTime = TimeUtils.getFriendlyDate(getContext(),
                                mAdapter.getDataAt(position - 1).getTime() * 1000);
                        if(friendlyTime.equals(prevFriendlyTime)){
                            lastShowString = friendlyTime;
                            continue;
                        }
                    }

                    if(lastShowString.equals(friendlyTime)){
                        continue;
                    }

                    lastShowString = friendlyTime;
                    child.setTag(R.id.id_chat_record_data_time_string, lastShowString);
                    if(view == child){
                        span = mDividerSpan;
                    }
                }
            }
            super.getItemOffsets(outRect, view, parent, state);
            outRect.top += span;
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            drawHorizontalDivider(c, parent);
        }

        private void drawHorizontalDivider(Canvas c, RecyclerView parent){
            int childCount = parent.getChildCount();
            View child;

            String friendlyTime;
            float textWidth;

            for(int i = 0; i < childCount; i++){
                child = parent.getChildAt(i);
                friendlyTime = (String) child.getTag(R.id.id_chat_record_data_time_string);
                if(null != friendlyTime && friendlyTime.length() > 0) {
                    textWidth = mPaint.measureText(friendlyTime);
                    c.drawText(friendlyTime, (child.getWidth() - textWidth) / 2, child.getTop() + offsetY, mPaint);
                }
            }
        }
    }

    private void addTimeTitle(){
        mListView.addItemDecoration(new TimeLine());
    }

    private void initTitleBar(String title){
        titleBarLayout = findViewById(R.id.title_bar);
        titleBarLayout.setTitleText(title);
        titleBarLayout.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            public void onBackClick() {finish();}
            public void onActionClick() {}
            public void onActionImageClick() {
                if(isGroup){
                    Intent intent = new Intent(getContext(), GroupInfoActivity.class);
                    intent.putExtra("UID", hisUid);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getContext(), SingleChatInfoActivity.class);
                    intent.putExtra("id", hisUid);
                    startActivity(intent);
                }
            }
        });
    }

//    private void jumpToUserInfoActivity(long uid, boolean isFriend){
//        int relative = isFriend ? BriefData.FRIEND : BriefData.HAVE_TOGETHER_GROUP;
//        String avatar = isFriend ? com.yichat.user.UserManager.getUser().icon : null;
//        String nickname = isFriend ? com.yichat.user.UserManager.getUser().nickname : null;
//        jumpToUserInfoActivity(uid, relative, avatar, nickname);
//    }

    private void jumpToUserInfoActivity(BriefData briefData){
        Intent intent = new Intent(getContext(), FriendBriefInfoActivity.class);
        intent.putExtra(FriendBriefInfoActivity.KEY_DATA, briefData);
        startActivity(intent);
    }

    private void jumpToUserInfoActivity(long uid, int type, String avatar, String name){
        BriefData briefData = new BriefData();
        briefData.id = uid;
        briefData.relative = type;
        briefData.avatar = avatar;
        briefData.nickname = name;
        jumpToUserInfoActivity(briefData);
    }

    @Override
    public void onSendMessage(ChatRecord msg) {
        offset++;
        mAdapter.addNewMessage(msg);
        mAdapter.notifyDataSetChanged();
        handleScrollToNewMessage(true);
    }

    @Override
    public void onAddTip(ChatRecord msg) {
        offset++;
        mAdapter.addNewMessage(msg);
        mAdapter.notifyDataSetChanged();
        handleScrollToNewMessage(false, 1);
    }

    @Override
    public void onSendMessageSuccess(long seq, long serverSeq) {
        updateMessageState(seq, Constant.MESSAGE_STATE_OK, serverSeq);
    }

    @Override
    public void onSendMessageFailed(long seq, String tip) {
        updateMessageState(seq, Constant.MESSAGE_STATE_FAILED, seq);
    }

    @Override
    public void onSendMessageRetry(long seq) {
        updateMessageState(seq, Constant.MESSAGE_STATE_SENDING, seq);
    }

    private void updateMessageState(long whichSeq, int state, long serverSeq){
        List<ChatRecord> data = mAdapter.getData();
        for(ChatRecord record : data){
            if(record.getMessageSequence() == whichSeq){
                record.setMessageState(state);
                record.setMessageSequence(serverSeq);
                break;
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onReceiveMessage(ChatRecord chatRecord) {
        offset++;
        mAdapter.addNewMessage(chatRecord);
        mAdapter.notifyDataSetChanged();
        handleScrollToNewMessage(false, 1);
    }

    @Override
    public void onReceiveMessage(List<ChatRecord> list) {
        mAdapter.addNewMessages(list);
        mAdapter.notifyDataSetChanged();
        handleScrollToNewMessage(false, list.size());
    }

    @Override
    public void onUpdateSingleChatHisInfo() {
        titleBarLayout.setTitleText(getPresenter().getHisName());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onUpdateGroupChatHisInfo() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onUpdateGroupInfo(com.fengshengchat.db.Group group) {
        titleBarLayout.setTitleText(group.name + "(" + group.memberCount + ")");
    }

    public static final int LOAD_PAGE_SIZE = 20;
    protected boolean noMoreData = false;
    private boolean isGettingHistory = false;
    protected int offset = 0;
    private int loadDataHeight = 0;
    @Override
    public void onHistory(List<ChatRecord> list) {
        printe("------onHistory------");
        if(loadDataHeight <= 0){
            loadDataHeight = dp2px(40 + 16);
        }

        if(null != list && list.size() > 0) {
            LinkedList<ChatRecord> data = mAdapter.getData();
            ChatRecord load = null;
            try {
                load = data.removeLast();
            }catch(Exception e){
                e.printStackTrace();
            }
            data.addAll(list);
            if(null != load){
                data.addLast(load);
            }
            mAdapter.notifyDataSetChanged();
            if(0 == offset){
                handleScrollToNewMessage(true);
            }else{
                int pos = mAdapter.getItemCount() - offset;
                if(pos >= 0 && pos < mAdapter.getItemCount()) {
                    linearLayoutManager.scrollToPositionWithOffset(pos, loadDataHeight);
                }
            }
            offset += list.size();
        }
        if(null == list || list.size() < LOAD_PAGE_SIZE){
            onNoMoreData();
        }
        isGettingHistory = false;
    }

    private void handleScrollToNewMessage(boolean force){
        handleScrollToNewMessage(force, 0);
    }

    private void handleScrollToNewMessage(boolean force, int newMessageCount){
        int itemCount = mAdapter.getItemCount();
        if(force) {
            mListView.scrollToPosition(itemCount - 1);
        }else{
            int p = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            if(p == itemCount - 1 - newMessageCount){
                mListView.scrollToPosition(itemCount - 1);
            }
        }
    }

    private ChatRecord chatRecord;
    protected void onNoMoreData(){
        printe("------onNoMoreData------");
        noMoreData = true;
//        chatRecord.content = getString(R.string.no_more_data);
        chatRecord.messageState = Constant.MESSAGE_STATE_OK;
//        mAdapter.getData().add(chatRecord);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onGettingHistory() {
        printe("------onGettingHistory------");
//        if(null == chatRecord){
//            chatRecord = new ChatRecord();
////            chatRecord.content = getString(R.string.no_more_data);
//            chatRecord.messageType = Constant.MESSAGE_TYPE_LOAD_DATA;
//        }
//        chatRecord.messageState = Constant.MESSAGE_STATE_SENDING;
//        mAdapter.getData().add(chatRecord);
//        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNewestBack(List<ChatRecord> list){}

    @Override
    public void onHistoryError(CharSequence tip) {
        printe("------onHistoryError------");
        LinkedList<ChatRecord> data = mAdapter.getData();
//        data.removeLast();
//        chatRecord.content = getString(R.string.error);
//        chatRecord.messageState = Constant.MESSAGE_STATE_OK;//Constant.MESSAGE_STATE_FAILED;
        chatRecord.messageState = Constant.MESSAGE_STATE_FAILED;
        mAdapter.notifyDataSetChanged();
        isGettingHistory = false;
    }

    @Override
    public void onDeleteChatRecordCompleted(long msgSeq) {
        if(msgSeq > 0){
            LinkedList<ChatRecord> data = mAdapter.getData();
            for(int i = 0; i < data.size(); i++){
                if(msgSeq == data.get(i).messageSequence){
                    data.remove(i);
                    mAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }
    }

    protected void resend(ChatRecord chatRecord){
        if(Constant.MESSAGE_STATE_FAILED == chatRecord.messageState){
            getPresenter().resend(chatRecord);
        }
    }

    protected void onContentRootClick(View v, ChatRecord chatRecord){
        if(Chat.MsgType.MSGTYPE_IMAGE_VALUE == chatRecord.messageType){

            Intent intent = new Intent(getContext(), ImagePreviewActivity.class);
            ImageContentBean imageContentBean = new Gson().fromJson(chatRecord.rawContent, ImageContentBean.class);
            intent.putExtra("thumbImagePath", imageContentBean.thumb_info.uri);
            intent.putExtra("imagePath", imageContentBean.general_info.uri);

            if (Build.VERSION.SDK_INT >= 22) {
                View view = v.findViewById(R.id.image);
                ActivityOptionsCompat options = ActivityOptionsCompat
                        .makeSceneTransitionAnimation(ChatActivity.this, view, getString(R.string.share_element_tag));
                startActivity(intent, options.toBundle());
            } else {
                startActivity(intent);
            }
        }else if(Chat.MsgType.MSGTYPE_CARD_VALUE == chatRecord.messageType){
            try {
                CardContentBean card = new Gson().fromJson(chatRecord.content, CardContentBean.class);
                jumpToUserInfoActivity(card.uid, BriefData.NOT_SURE_FRIEND, card.icon, card.nick);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private PopupMenu popupMenu;
    protected void showMessageMenu(int pos, View v){
        ChatRecord dataAt = mAdapter.getDataAt(pos);

        popupMenu = new PopupMenu(this, v);
        Menu menu = popupMenu.getMenu();
        if(Chat.MsgType.MSGTYPE_TEXT_VALUE == dataAt.messageType){
            menu.add(0, 0, 0,R.string.copy);
        }
//        if(Chat.MsgType.MSGTYPE_VOICE_VALUE != dataAt.messageType) {
//            menu.add(0, 1, 1, R.string.send_to_friend);
//        }
        menu.add(0, 2, 2, R.string.delete);
//        if(mAdapter.getDataAt(pos).messageState == Constant.MESSAGE_STATE_FAILED){
//            menu.add(0, 3, 3, R.string.resend);
//        }
        if(Chat.MsgType.MSGTYPE_TEXT_VALUE == dataAt.messageType) {
            menu.add(0, 4, 4, R.string.quote);
        }
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(item -> {
            switch(item.getItemId()){
                case 0: getPresenter().copyText2SystemClipboard(mAdapter.getDataAt(pos));
                    toast(R.string.user_copy_success);
                    break;
                case 1: selectFriendForSend(pos);
                    break;
                case 2: getPresenter().deleteChatRecord(mAdapter.getDataAt(pos));
                    break;
                case 3: getPresenter().resend(mAdapter.getDataAt(pos));
                    break;
                case 4: getPresenter().quote(mAdapter.getDataAt(pos));
                    break;
            }
            return false;
        });
    }

    private void selectFriendForSend(int dataIndex){
        Intent intent = new Intent(getContext(), SelectContactActivity.class);
        intent.putExtra("dataIndex", dataIndex);
        intent.putExtra(SelectContactActivity.ENTER_KEY, SelectContactActivity.ENTER_SELECT_CONTACT_SEND_TO);
        startActivityForResult(intent, SELECT_FRIEND);
    }

    private void selectFriendForCard(){
        Intent intent = new Intent(getContext(), SelectContactActivity.class);
        intent.putExtra(SelectContactActivity.ENTER_KEY, SelectContactActivity.ENTER_SELECT_CONTACT_CARD);
        intent.putExtra("sendToName", getPresenter().getName());
        intent.putExtra("sendToIcon", getPresenter().getIcon());
        intent.putExtra("isGroup", isGroup);
        intent.putExtra("isShareCard", true);
        startActivityForResult(intent, SELECT_FRIEND_CARD);
    }

    protected ChatRecord mCurrentMediaPlayBean;
    private MediaPlayer mMediaPlayer;
    private MediaPlayer getMediaPlayer(){
        if(null == mMediaPlayer){
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setOnPreparedListener(MediaPlayer::start);
            mMediaPlayer.setOnCompletionListener(mp -> onPlayEnd());
            mMediaPlayer.setOnErrorListener((mp, what, extra) -> {
                onPlayEnd();
                return false;
            });
        }
        return mMediaPlayer;
    }

    private void onPlayEnd(){
        if(null != mCurrentMediaPlayBean) {
            mCurrentMediaPlayBean.isVoicePlaying = false;
            mCurrentMediaPlayBean = null;
        }
        mAdapter.notifyDataSetChanged();
    }

    private void downloadVoiceFile(String url, String filePath){
        Aria.download(this).load(url).setFilePath(filePath).start();
    }

    @Download.onTaskRunning
    public void running(DownloadTask task) {
//        if(task.getUrl().eques(url)){
//        }
//        int p = task.getPercent();	//任务进度百分比
//        String speed = task.getConvertSpeed();	//转换单位后的下载速度，单位转换需要在配置文件中打开
//        String speed1 = task.getSpeed(); //原始byte长度速度
    }

    @Download.onTaskComplete
    public void taskComplete(DownloadTask task) {
        printe("-----------voice file download completed----------");
        if(null != mCurrentMediaPlayBean
                && mCurrentMediaPlayBean.isVoicePlaying
                && mCurrentMediaPlayBean.content.equals(task.getKey())){
            playVoiceFile(task.getDownloadPath());
        }
    }

    private boolean isFileExist(String filePath){
        return new File(filePath).exists();
    }

    private String generateFilePath(String url){
        try {
            String[] split = url.split("/");
            File f = new File(FileConstant.getFileExternalStorageVoiceAbsolutePath(), split[split.length - 1]);
            return f.getAbsolutePath();
        }catch(Exception e){
            e.printStackTrace();
        }
        return new File(FileConstant.getFileExternalStorageVoiceAbsolutePath(), url).getAbsolutePath();

    }

    protected boolean prePlayVoiceFile(String url){
        String file = generateFilePath(url);
        boolean fileExist = isFileExist(file);
        if(fileExist){
            return playVoiceFile(file);
        }else{
            if(NetUtils.isConnected()) {
                downloadVoiceFile(url, file);
                return playVoiceFile(url);
            }else{
                toast(R.string.network_available);
                return false;
            }
        }
    }

    protected boolean playVoiceFile(String filePath){
        MediaPlayer mediaPlayer = getMediaPlayer();
        if (null != filePath && null != mediaPlayer) {
            mediaPlayer.stop();
            mediaPlayer.reset();

            try {
                mediaPlayer.setDataSource(filePath);
                mediaPlayer.prepareAsync();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        toast(R.string.error);
        return false;
    }

    protected void stopPlayVoiceFile(){
        if(null == mMediaPlayer)
            return;
        mMediaPlayer.stop();
        mMediaPlayer.reset();
    }

    private void releaseMediaPlayer(){
        if(null != mMediaPlayer){
            mMediaPlayer.stop();
            mMediaPlayer.release();
        }
        mMediaPlayer = null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(mInputView.isPanelShow()){
            hideInputView();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public class ChatAdapter extends BaseRecyclerAdapter<ChatRecord, LinkedList<ChatRecord>>{
        protected static final int VIEW_TYPE_SEND = 0;
        protected static final int VIEW_TYPE_RECEIVE = 1;
        protected static final int VIEW_TYPE_TIP = 2;
        protected static final int VIEW_TYPE_GROUP_BROADCAST = 3;
        protected static final int VIEW_TYPE_LOAD_DATA = 4;
        protected static final int VIEW_TYPE_SEND_CARD = 5;
        protected static final int VIEW_TYPE_RECEIVE_CARD = 6;
        protected static final int VIEW_TYPE_NONE = 100;

        @Override
        protected LinkedList<ChatRecord> getList() {
            return new LinkedList<>();
        }

        public void addNewMessage(ChatRecord msg){
            getData().addFirst(msg);
        }

        public void addNewMessages(List<ChatRecord> list){
            LinkedList<ChatRecord> data = getData();
            for(ChatRecord record : list){
                data.addFirst(record);
            }
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            ViewHolder viewHolder = super.onCreateViewHolder(parent, viewType);
            switch(viewType){
                default:
                case VIEW_TYPE_TIP:
                case VIEW_TYPE_NONE:
                    return viewHolder;
                case VIEW_TYPE_LOAD_DATA:
                    return initLoadDataViewHolder(viewHolder);

                case VIEW_TYPE_SEND:
                case VIEW_TYPE_RECEIVE:
                case VIEW_TYPE_SEND_CARD:
                case VIEW_TYPE_RECEIVE_CARD:
                    return initNormalViewHolder(viewType, viewHolder);
            }
        }

        private ViewHolder initLoadDataViewHolder(ViewHolder viewHolder){
            return viewHolder;
        }

        private void jumpToUserInfo(ChatRecord dataAt){
            if(dataAt.isSendMessage){
                User user = UserManager.getUser();
                BriefData briefData = new BriefData();
                briefData.id = dataAt.my.uid;
                briefData.relative =  BriefData.SELF;
                briefData.avatar = user.icon;
                briefData.bigAvatar = user.bigIcon;
                briefData.nickname = user.nickname;
                jumpToUserInfoActivity(briefData);
            }else {
                BriefData briefData = new BriefData();
                briefData.id = isGroup ? dataAt.his.sendUid : dataAt.his.uid;;
                briefData.avatar = isGroup ? dataAt.his.icon: dataAt.his.icon;;
                if (isGroup) {
                    briefData.relative = BriefData.NOT_SURE_FRIEND;
                    briefData.groupId = hisUid;
                    briefData.groupType = groupType;
                } else {
                    briefData.relative = BriefData.FRIEND;
                }
                jumpToUserInfoActivity(briefData);
            }
        }

        protected ViewHolder initNormalViewHolder(int viewType, ViewHolder viewHolder){
            viewHolder.getImageView(R.id.icon).setOnClickListener(v -> {
                ChatRecord dataAt = getDataAt(viewHolder.getAdapterPosition());
                jumpToUserInfo(dataAt);
            });

            if(VIEW_TYPE_SEND == viewType || VIEW_TYPE_SEND_CARD == viewType) {
                viewHolder.getView(R.id.send_failed).setOnClickListener(v -> {
                    resend(getDataAt(viewHolder.getAdapterPosition()));
                });
            }
            viewHolder.getView(R.id.content_root).setOnLongClickListener(v -> {
                showMessageMenu(viewHolder.getAdapterPosition(), v);
                return true;
            });
            viewHolder.getView(R.id.content_root).setOnClickListener(
                    v -> onContentRootClick(v, getDataAt(viewHolder.getAdapterPosition())));

            if(VIEW_TYPE_SEND_CARD == viewType || VIEW_TYPE_RECEIVE_CARD == viewType){
                return viewHolder;
            }

            viewHolder.getView(R.id.voice).setOnLongClickListener(v -> {
                showMessageMenu(viewHolder.getAdapterPosition(), v);
                return true;
            });
            viewHolder.getView(R.id.voice).setOnClickListener(v -> {
                ChatRecord chatRecord = getDataAt(viewHolder.getAdapterPosition());
                if(chatRecord.isVoicePlaying){
                    stopPlayVoiceFile();
                    mCurrentMediaPlayBean = null;
                    chatRecord.isVoicePlaying = false;
                }else {
                    if(null != mCurrentMediaPlayBean){
                        mCurrentMediaPlayBean.isVoicePlaying = false;
                    }
                    mCurrentMediaPlayBean = chatRecord;
                    chatRecord.isVoicePlaying = prePlayVoiceFile(chatRecord.content);
                    if(!chatRecord.hasVoicePlay) {
                        chatRecord.hasVoicePlay = chatRecord.isVoicePlaying;
                        if (chatRecord.hasVoicePlay) {
                            getPresenter().updateVoiceReadState(chatRecord.his.uid, chatRecord.messageSequence);
                        }
                    }
                }
                notifyDataSetChanged();
            });
            return viewHolder;
        }

        protected boolean bindTipData(ViewHolder holder, int position, ChatRecord data){
            switch(data.messageType){
                case Constant.MESSAGE_TYPE_TIP:
                case Chat.MsgType.MSGTYPE_GROUP_SYSTEM_VALUE:
                    TextView textView = holder.getTextView(R.id.tip);
                    if(textView instanceof AutoSplitTextView){
                        ((AutoSplitTextView) textView).setMText(data.content);
                    }else {
                        textView.setText(data.content);
                    }
                    return true;
            }
            return false;
        }

        private boolean bindLoadData(ViewHolder holder, int position, ChatRecord data) {
            if(Constant.MESSAGE_TYPE_LOAD_DATA == data.messageType){
                switch (data.getMessageState()) {
                    case Constant.MESSAGE_STATE_OK:
                        holder.getView(R.id.progress).setVisibility(View.INVISIBLE);
                        holder.getView(R.id.tip).setVisibility(View.VISIBLE);
                        holder.setText(R.id.tip, data.content);
                        break;
                    case Constant.MESSAGE_STATE_FAILED:
                        holder.getView(R.id.progress).setVisibility(View.GONE);
                        holder.getView(R.id.tip).setVisibility(View.GONE);
                        break;
                    case Constant.MESSAGE_STATE_SENDING:
                        holder.getView(R.id.progress).setVisibility(View.VISIBLE);
                        holder.getView(R.id.tip).setVisibility(View.GONE);
                        break;
                }
                return true;
            }
            return false;
        }

        protected void onBindTag(ViewHolder holder, int position, ChatRecord data){
            holder.getRootView().setTag(position);
            holder.getRootView().setTag(R.id.id_chat_record_data_time_long, data.time);
        }

        protected void onBindData(ViewHolder holder, int position, ChatRecord data) {
            onBindTag(holder, position, data);
            int viewType = getViewType(position, data);
            if(VIEW_TYPE_NONE == viewType){
                return;
            }

            if(bindTipData(holder, position, data)){
                return;
            }

            if(bindLoadData(holder, position, data)){
                return;
            }

            TextView nickname = holder.getTextView(R.id.nick_name);
            ImageView imageView = holder.getImageView(R.id.icon);
            if(isGroup && !data.isSendMessage){
                loadGroupUserIcon(nickname, imageView, data);
            }else {
                String name = data.isSendMessage ? getPresenter().getMyName() : getPresenter().getHisName();
                String icon = data.isSendMessage ? getPresenter().getMyAvatar() : getPresenter().getHisIcon();
                nickname.setText(name);
                loadRadiusImage(imageView, icon, R.mipmap.default_head_icon, 5);
            }

            holder.setText(R.id.time, TimeUtils.getTime(data.time * 1000));

            if(VIEW_TYPE_SEND_CARD != viewType && VIEW_TYPE_RECEIVE_CARD != viewType) {
                holder.getView(R.id.text).setVisibility(View.GONE);
                holder.getView(R.id.image).setVisibility(View.GONE);
                holder.getView(R.id.voice).setVisibility(View.GONE);
                if (!data.isSendMessage) {
                    holder.getView(R.id.unread_voice).setVisibility(View.GONE);
                }
            }

            switch(data.messageType){
                case Chat.MsgType.MSGTYPE_TEXT_VALUE:
                    holder.setText(R.id.text, data.getContent());
                    holder.getView(R.id.text).setVisibility(View.VISIBLE);
                    break;
                case Chat.MsgType.MSGTYPE_IMAGE_VALUE:
                    loadAutoSizeImage(holder.getImageView(R.id.image), data);
                    holder.getView(R.id.image).setVisibility(View.VISIBLE);
                    break;
                case Chat.MsgType.MSGTYPE_VOICE_VALUE:
                    ChatVoiceView voice = holder.getView(R.id.voice);
                    voice.setDuration(data.duration);
                    voice.setVisibility(View.VISIBLE);
                    voice.voiceAnimation(data.isVoicePlaying);
                    if(!data.isSendMessage) {
                        holder.getView(R.id.unread_voice)
                                .setVisibility(data.hasVoicePlay ? View.GONE : View.VISIBLE);
                    }
                    break;
                case Chat.MsgType.MSGTYPE_CARD_VALUE:
                    bindCardData(holder, data);
                    break;
            }

            if(data.isSendMessage) {
                holder.getView(R.id.progress).setVisibility(View.INVISIBLE);
                holder.getView(R.id.send_failed).setVisibility(View.INVISIBLE);
                switch (data.getMessageState()) {
                    case Constant.MESSAGE_STATE_OK:
                        break;
                    case Constant.MESSAGE_STATE_FAILED:
                        holder.getView(R.id.send_failed).setVisibility(View.VISIBLE);
                        break;
                    case Constant.MESSAGE_STATE_SENDING:
                        holder.getView(R.id.progress).setVisibility(View.VISIBLE);
                        break;
                }
            }
        }

        @Override
        protected int getViewType(int position, ChatRecord data) {
            switch(data.messageType){
                case Constant.MESSAGE_TYPE_LOAD_DATA:
                    return VIEW_TYPE_LOAD_DATA;
                case Constant.MESSAGE_TYPE_TIP:
                case Chat.MsgType.MSGTYPE_GROUP_SYSTEM_VALUE:
                    return VIEW_TYPE_TIP;

                case Chat.MsgType.MSGTYPE_TEXT_VALUE:
                case Chat.MsgType.MSGTYPE_IMAGE_VALUE:
                case Chat.MsgType.MSGTYPE_VOICE_VALUE:
                    return data.isSendMessage ? VIEW_TYPE_SEND : VIEW_TYPE_RECEIVE;
                case Chat.MsgType.MSGTYPE_CARD_VALUE:
                    return data.isSendMessage ? VIEW_TYPE_SEND_CARD : VIEW_TYPE_RECEIVE_CARD;

                case Chat.MsgType.MSGTYPE_UNKNOW_VALUE:
                    break;
            }
            return VIEW_TYPE_NONE;
        }

        protected int getLayoutId(int viewType) {
            switch(viewType){
                default:
                case VIEW_TYPE_NONE:
                    return R.layout.empty;
                case VIEW_TYPE_RECEIVE:
                    return R.layout.list_chat_item_receive_layout;
                case VIEW_TYPE_SEND:
                    return R.layout.list_chat_item_send_layout;
                case VIEW_TYPE_TIP:
                    return R.layout.list_chat_item_tip;
                case VIEW_TYPE_LOAD_DATA:
                    return R.layout.list_chat_item_load_data;
                case VIEW_TYPE_SEND_CARD:
                    return R.layout.list_chat_item_send_card_layout;
                case VIEW_TYPE_RECEIVE_CARD:
                    return R.layout.list_chat_item_receive_card_layout;
            }
        }

        private void bindCardData(ViewHolder holder, ChatRecord chatRecord){
            try {
                CardContentBean card = new Gson().fromJson(chatRecord.content, CardContentBean.class);
                holder.setText(R.id.card_name, card.nick);
                loadRadiusImage(holder.getImageView(R.id.card_avatar), card.icon, R.mipmap.default_head_icon, 5);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        private int maxWidth = 0;
        private int maxHeight = 0;
        protected void loadAutoSizeImage(ImageView imageView, ChatRecord record){
            if(0 == maxWidth){
                maxWidth = dp2px(150);
            }
            if(0 == maxHeight){
                maxHeight = dp2px(160);
            }

            int dW = maxWidth;
            int dH = maxHeight;
            String path = record.content;
            try {
                ImageContentBean imageContentBean = new Gson().fromJson(record.rawContent, ImageContentBean.class);
                path = imageContentBean.thumb_info.uri;
                dW = imageContentBean.thumb_info.size.weight;
                dH = imageContentBean.thumb_info.size.hight;
                dW = 0 == dW ? maxWidth : dW;
                dH = 0 == dH ? maxHeight : dH;
                dW = dp2px(dW / 2);
                dH = dp2px(dH / 2);
            }catch(Exception e){
                e.printStackTrace();
                dW = maxWidth;
                dH = maxHeight;
            }

            float ratio = 1;
            if(dW > dH){
                if(dW > maxWidth){
                    ratio = (float)maxWidth / dW;
                    dW = maxWidth;
                    dH = (int)(dH * ratio);
                }
            }else{
                if(dH > maxHeight){
                    ratio = (float)maxHeight / dH;
                    dH = maxHeight;
                    dW = (int)(dW * ratio);
                }
            }

            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            layoutParams.width = dW;
            layoutParams.height = dH;

            loadImageAutoSize(imageView, path, dW, dH, R.mipmap.chat_img_default);
//            loadGif((SketchView) imageView, path);
        }

        protected void loadGroupUserIcon(TextView textView, ImageView imageView, ChatRecord bean){
            Account friend = getPresenter().getFriend(bean.his.sendUid);
            if(null != friend) {
                textView.setText(friend.nickname);
                loadRadiusImage(imageView, friend.avatar, R.mipmap.default_head_icon, 5);
            }else{
                textView.setText("");
                imageView.setImageResource(R.mipmap.default_head_icon);
            }
        }
    }

    @Override
    protected boolean isHideKeyBoardEvent() {
        return false;
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_GROUP)})
    public void updatedDeletedGroup(Long groupId) {
        if(hisUid == groupId){
            finish();
        }
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_FRIEND)})
    public void updateDeleteFriend(Long uid){
        if(hisUid == uid){
            finish();
        }
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_CHAT_RECORD)})
    public void updateDeleteChatRecord(Long uid){
        if(hisUid == uid){
            mAdapter.clearData();
            mAdapter.notifyDataSetChanged();
            noMoreData = false;
            offset = 0;
        }
    }

    private void hideMoreBtn(boolean hide){
        titleBarLayout.post(new Runnable() {
            public void run() {
                try{
                    Class clz = titleBarLayout.getClass();
                    Field mActionImageLayout = clz.getDeclaredField("mActionImageLayout");
                    mActionImageLayout.setAccessible(true);
                    Object o = mActionImageLayout.get(titleBarLayout);
                    if(o instanceof View){
                        ((View) o).setVisibility(hide ? View.INVISIBLE : View.VISIBLE);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_GROUP_INFO)})
    public void groupInfoFromServer(Long gid) {
        Disposable subscribe = Observable.fromCallable(() -> {
            com.fengshengchat.db.Group groupInfo = DbFriend.getGroupInfo(gid);
            printe("----1------groupInfo: " + groupInfo);
            if(null != groupInfo) {
                GroupMember member = DbManager.getDb().getGroupMemberDao().find(gid, UserManager.getUser().uid, Group.DeleteFlag.DELETE_UNDEFINED_VALUE);
                groupInfo.updatedTime = null == member ? 1 : 2; // 借用变量
                DbChatList.updateChatItemGroupInfo(groupInfo);
            }
            return groupInfo;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(group -> {
                    if(null != group && group.id == hisUid){
                        getPresenter().setGroupInfo(group.name, group.avatar);
                        onUpdateGroupInfo(group);
                        if(group.updatedTime == 1){ //不是该群的群成员
                            hideMoreBtn(true);
                        }else{
                            hideMoreBtn(false);
                        }
                    }
                }, throwable -> printe(throwable.getMessage()));
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_SINGLE_FRIEND)})
    public void friendInfoFromServer(Long uid) {
        Disposable subscribe = Observable.fromCallable(() -> {
            Account friend = DbFriend.getFriend(uid);
            printe("----1------friend: " + friend);
//            if(null != friend) {
//                DbChatList.updateChatItemSingleInfo(friend);
//            }
            return friend;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(friend -> {
                    if(null != friend){
                        if(isGroup){
                            getPresenter().updateFriendInfo(friend);
                        }else {
                            if(friend.id == hisUid) {
                                getPresenter().getFriendInfo(friend.id);
                            }
                        }
                    }
                }, throwable -> printe(throwable.getMessage()));
    }
}
