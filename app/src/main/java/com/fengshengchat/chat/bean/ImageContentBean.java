package com.fengshengchat.chat.bean;

public class ImageContentBean {
    public Info thumb_info;
    public Info general_info;
    public Info original_info;

    public static class Info{
        public String uri;
        public Size size;
        public Info(String uri){
            this.uri = uri;
        }
        public Info(String uri, int w, int h){
            this(uri);
            size = new Size(w, h);
        }
    }

    public static class Size{
        public int weight;
        public int hight;

        public Size(int w, int h){
            this.weight = w;
            this.hight = h;
        }
    }

//    public ImageContentBean(String uri){
//        general_info = new Info(uri);
//    }

    public ImageContentBean(String thumb, String uri){
        thumb_info = new Info(thumb);
        general_info = new Info(uri);
    }

    public ImageContentBean(Info thumb, Info uri){
        thumb_info = thumb;
        general_info = uri;
    }

//    public ImageContentBean(String thumb, String uri, String original){
//        thumb_info = new Info(thumb);
//        general_info = new Info(uri);
//        original_info = new Info(original);
//    }
}
