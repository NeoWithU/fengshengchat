package com.fengshengchat.chat.bean;

public class CardContentBean {
    public long uid;
    public String nick;
    public String icon;

    public CardContentBean(long uid, String nick, String icon){
        this.uid = uid;
        this.nick = nick;
        this.icon = icon;
    }
}
