package com.fengshengchat.chat.bean;

public class VoiceContentBean {
    public String uri;
    public MediaInfo media_info;

    public class MediaInfo{
        public String media_type;
        public String media_time;
        public String media_decode;
    }

    public VoiceContentBean(String uri, int duration){
        this.uri = uri;
        media_info = new MediaInfo();
        media_info.media_time = duration + "";
    }

}
