package com.fengshengchat.chat.presenter;

import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.base.HttpRequestObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.mvp.BasePresenter;
import com.fengshengchat.chat.bean.GroupMemberBean;
import com.fengshengchat.chat.model.GroupChatInfoModel;
import com.fengshengchat.chat.view.IGroupChatInfoView;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;

import java.util.ArrayList;
import java.util.List;

import yiproto.yichat.group.Group;
import yiproto.yichat.msg.MsgOuterClass;

public class GroupChatInfoPresenter extends BasePresenter<GroupChatInfoModel, IGroupChatInfoView> {
    private static final int MAX_SHOW_MEMBER = 60;
    private long mGroupId;

    public GroupChatInfoPresenter(long groupId, IGroupChatInfoView view) {
        super(new GroupChatInfoModel(), view);
        mGroupId = groupId;
    }

    public void getGroupMemberList(){
        PBMessage pbMessage = new PBMessage();
        pbMessage.setCmd(Group.GroupYimCMD.GET_GROUP_MEMBER_LIST_REQ_CMD_VALUE)
                .setBody(Group.GetGroupMemberListReq.newBuilder()
                        .setGroupId(mGroupId)
                        .setStart(0)
                        .setNum(MAX_SHOW_MEMBER));
        ApiManager.getInstance().post(new Request(pbMessage))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestObserver<MsgOuterClass.Msg>() {
                    public void onNext(MsgOuterClass.Msg msg) {
                        onMemberListResponse(msg);
                    }
                });
    }

    private void onMemberListResponse(MsgOuterClass.Msg msg){
        try {
            List<Long> memberListList = Group.GetGroupMemberListRsp.parseFrom(msg.getBody()).getMemberListList();
            GroupMemberBean memberBean;
            ArrayList<GroupMemberBean> list = new ArrayList<>();
            for(Long uid : memberListList){
                memberBean = new GroupMemberBean();
                memberBean.name = uid + "";
                memberBean.icon = "";
//                memberBean.uid = ;
                list.add(memberBean);
            }
            memberBean = new GroupMemberBean();
            memberBean.uid = -1; // For add button
            list.add(memberBean);
            getView().setGroupMemberList(list);
            return;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        getView().setGroupMemberListError(msg.getErrMsg());
    }

}
