package com.fengshengchat.chat.presenter;

import android.content.Context;
import android.content.Intent;

import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.MainActivity;
import com.fengshengchat.account.activity.SearchFriendOrGroupActivity;
import com.fengshengchat.base.mvp.BasePresenter;
import com.fengshengchat.chat.model.ChatListModel;
import com.fengshengchat.chat.view.IChatListView;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.Constant;
import com.fengshengchat.group.activity.SelectContactActivity;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.ChatHandler;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.push.HandlePushService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.chat.Chat;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.groupchat.Groupchat;
import yiproto.yichat.msg.MsgOuterClass;
import yiproto.yichat.sportfulgroup.Sportfulgroup;

/**
 * @author KRT
 * 2018/12/5
 */
public class ChatListPresenter extends BasePresenter<ChatListModel, IChatListView> {
    private HandlePushService.HandlePushServiceInterface mPusher;

    public ChatListPresenter(IChatListView view){
        super(new ChatListModel(view.getContext()), view);
    }

    public void setPusher(HandlePushService.HandlePushServiceInterface pusher){
        mPusher = pusher;
    }

    public void createGroupV2(){
        Context context = getView().getContext();
        Intent intent = new Intent(context, SelectContactActivity.class);
        context.startActivity(intent);
    }

    public void addFriend(){
        Context activity = getView().getActivity();
        Intent intent = new Intent(activity, SearchFriendOrGroupActivity.class);
        activity.startActivity(intent);
    }

    public void getFriendInfo(long uid){
        getModel().getFriend(uid, friend -> {
            printe("-----------get friend: " + friend);
            if (null != friend) {
                getView().updateItemFriendInfo(friend);
            }
        });
    }

    public void getGroupInfo(long gid){
        getModel().getGroupInfo(gid, group -> {
            if(null != group){
                printe("-----------get group: " + group);
                getView().updateItemGroupInfo(group);
            }
        });
    }

    public void onBaseInfoRefreshOK(){
        getModel().refreshInfo(list -> {
            if(null != list){
                ChatListBean remove = list.remove(list.size() - 1);
                getView().setChatList(list);
                getView().updateUnreadCount(false, remove.unreadCount);
            }
        });
    }

    public void onSocketConnectStateChange(int state){
        socketReady = MainActivity.STATE_OK == state;
        if(socketReady && localRecordReady){
            pullNewChatList(getView().getListData());
            loadBaseData();
        }
    }

    public void onAddFriend(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                Friend.NotifyUpdateReq notifyUpdateReq = Friend.NotifyUpdateReq.parseFrom(msg.getBody());
                Friend.NotifyUpdateReq.FriendGreetingSystemMsg greetingMsg = notifyUpdateReq.getGreetingMsg();
                if(greetingMsg.getRequestUserId() == getModel().getUid()){
                    getModel().getChatItem(greetingMsg.getAgreeUserId(),
                            chatListBean -> {
                                if(null != chatListBean) {
                                    getView().setChatListItem(chatListBean);
//                                    if (!chatListBean.disableNotify) {
//                                        getView().updateUnreadCount(true, 1);
//                                    }
                                }
                            });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean localRecordReady = false;
    private boolean socketReady = false;
    private boolean isGettingLocalRecord = false;
    public void getLocalChatList(boolean needPull){
        if(isGettingLocalRecord)
            return;

        isGettingLocalRecord = true;
        getModel().getLocalChatList(needPull, chatListBeans -> {
            ChatListBean remove = chatListBeans.remove(chatListBeans.size() - 1);
            getView().updateUnreadCount(false, remove.unreadCount);
            getView().setChatList(chatListBeans);
            localRecordReady = true; //只需 ready 一次
            if (needPull && socketReady) {
                pullNewChatList(chatListBeans);
            }
            if(needPull){
                loadBaseData();
            }
            isGettingLocalRecord = false;
        }, throwable -> isGettingLocalRecord = false);
    }

    private void loadBaseData(){
        Context activity = getView().getActivity();
        if(activity instanceof MainActivity){
            ((MainActivity) activity).loadBasicData();
        }
    }

    public void pullNewChatList(List<ChatListBean> chatList){
        printe("------pullNewChatList start--------");
        Map<Long, Long> mapSingle = new HashMap<>();
        Map<Long, Long> mapGroup = new HashMap<>();
        if(null != chatList) {
            for (ChatListBean bean : chatList) {
                if (Constant.CHAT_TYPE_SINGLE == bean.chatType) {
                    mapSingle.put(bean.hisUid, bean.alreadyPullSequence);
                } else {
                    if(0 != bean.hisUid) {
                        mapGroup.put(bean.hisUid,
                                yiproto.yichat.group.Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE == bean.groupType
                                        ? 0 : bean.alreadyPullSequence);
                    }
                }
            }
        }

        //request pull single chat
        PBMessage pbMessageSingle = new PBMessage();
        pbMessageSingle.setCmd(MsgOuterClass.YimCMD.GET_SESSION_NEW_MSG_INFO_REQ_CMD_VALUE);
        pbMessageSingle.setBody(Chat.GetSessNewMsgInfoReq.newBuilder()
                .setUid(getModel().getUid())
                .putAllCurSeqMap(mapSingle));
        mPusher.push(pbMessageSingle);

        //request pull group chat
        PBMessage pbMessageGroup = new PBMessage();
        pbMessageGroup.setCmd(MsgOuterClass.YimCMD.GET_GROUP_SESSION_NEW_MSG_INFO_REQ_CMD_VALUE);
        pbMessageGroup.setBody(Groupchat.GetGroupSessNewInfoReq.newBuilder()
                .setUid(getModel().getUid())
                .putAllCurSeqMap(mapGroup));
        mPusher.push(pbMessageGroup);

        printe(" pull single list: " + mapSingle);
        printe(" pull group list: " + mapGroup);
    }

    private boolean handleInternalCmd(MsgOuterClass.Msg message){
        if(Constant.INTERNAL_CMD_TYPE != message.getBigCmd()) {
            return false;
        }
        printe("-----------<internal> cmd: " + message.getCmd());
        switch(message.getCmd()){
            case Constant.INTERNAL_CMD_REFRESH_GROUP_INFO:
                getModel().getGroupInfo(message.getUid(), group -> {
                    if(null != group){
                        getView().updateItemGroupInfo(group);
                    }
                });
                return true;
        }
        return false;
    }

    public boolean handleMessage(MsgOuterClass.Msg message){
        if(Constant.INTERNAL_CMD_TYPE == message.getBigCmd()){
            return handleInternalCmd(message);
        }

        printi("handleMessage -> " + message.getCmd() + " errCode: " + message.getCode() + "  errMsg: " + message.getErrMsg());
        switch (message.getCmd()) {
            default:
                return false;
                //single
            case MsgOuterClass.YimCMD.GET_SESSION_NEW_MSG_INFO_RSP_CMD_VALUE:
                handlePullSingleChatListResponse(message);
                break;
            case MsgOuterClass.YimCMD.GET_SESSION_NEW_MSG_RSP_CMD_VALUE:
                pullSingleNewMessageResponse(message);
                break;
            case MsgOuterClass.YimCMD.CHAT_MSG_PUSH_CMD_VALUE:
                handleReceiveSingleChatMessage(message);
                break;

                //Group
            case MsgOuterClass.YimCMD.GET_GROUP_SESSION_NEW_MSG_INFO_RSP_CMD_VALUE:
                handlePullGroupChatListResponse(message);
                break;
            case MsgOuterClass.YimCMD.GET_GROUP_NEW_MSG_RSP_CMD_VALUE:
                pullGroupNewMessageResponse(message);
                break;
            case MsgOuterClass.YimCMD.GROUP_CHAT_MSG_PUSH_CMD_VALUE:
                handleReceiveGroupMessageResponse(message);
                break;

            case MsgOuterClass.YimCMD.SPORTFULGROUP_CHAT_MSG_PUSH_CMD_VALUE:
                onReceiveFunGroupMessage(message);
                break;

            case MsgOuterClass.YimCMD.GROUP_SYSTEM_BROADCAST_CMD_VALUE:
                handleGroupBroadcast(message);
                break;

            case Friend.FriendYimCMD.NOTIFY_UPDATE_REQ_CMD_VALUE:
                onAddFriend(message);
                break;
        }
        return true;
    }

    private void handleGroupBroadcast(MsgOuterClass.Msg msg){
        if (BaseHandler.isNoError(msg.getCode())) {
            try {
                Groupchat.GroupSystemBroadcast groupBroadcast = Groupchat.GroupSystemBroadcast.parseFrom(msg.getBody());
                if(Groupchat.GROUP_SYS_MSG_TYPE.INITIATIVE_OUT_VALUE == groupBroadcast.getMsgType()
                        || Groupchat.GROUP_SYS_MSG_TYPE.KICT_OUT_VALUE == groupBroadcast.getMsgType()){
                    Disposable subscribe = Observable.fromCallable(new Callable<Boolean>() {
                        @Override
                        public Boolean call() throws Exception {
                            return ChatHandler.needHandle(groupBroadcast);
                        }
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Consumer<Boolean>() {
                                @Override
                                public void accept(Boolean aBoolean) throws Exception {
                                    if (aBoolean) {
                                        getModel().getGroupInfoDelay(groupBroadcast.getGroupId(), chatListBean -> {
                                            if (null != chatListBean) {
                                                getView().setChatListItem(chatListBean);
                                            }
                                        });
                                    }
                                }
                            }, throwable -> {

                            });
                    return;
                }else if(Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_ANNOUNCEMENT_OPEN_VALUE == groupBroadcast.getMsgType()
                        || Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_ANNOUNCEMENT_OFF_VALUE == groupBroadcast.getMsgType()){
                    return;
                }
                getModel().getGroupInfoDelay(groupBroadcast.getGroupId(), chatListBean -> {
                    if(null != chatListBean){
                        getView().setChatListItem(chatListBean);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void calcUnreadCount(List<ChatListBean> list){
        int count = 0;
        if(null != list){
            for(ChatListBean chatListBean : list){
                if(!chatListBean.disableNotify) {
                    count += chatListBean.unreadCount;
                }
            }
        }
        getView().updateUnreadCount(false, count);
    }

    private void onReceiveFunGroupMessage(MsgOuterClass.Msg msg) {
        if(BaseHandler.isNoError(msg.getCode())) {
            try {
                Sportfulgroup.SportfulGroupChat groupChatMsg = Sportfulgroup.SportfulGroupChat.parseFrom(msg.getBody());
                getModel().getChatItem(groupChatMsg.getGroupId(),
                        chatListBean -> {
                            if(null != chatListBean) {
                                getView().setChatListItem(chatListBean);
//                                if(!chatListBean.disableNotify) {
//                                    getView().updateUnreadCount(true, 1);
//                                }
                            }
                        });
                printi("--- onReceiveFunGroupMessage success --- ");
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printi("--- onReceiveFunGroupMessage failed --- " + msg.getErrMsg());
    }

    private void handleReceiveGroupMessageResponse(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())) {
            try {
                Groupchat.GroupChatMsg groupChatMsg = Groupchat.GroupChatMsg.parseFrom(msg.getBody());
                getModel().getChatItem(groupChatMsg.getGroupId(),
                        chatListBean -> {
                            if(null != chatListBean) {
                                getView().setChatListItem(chatListBean);
//                                if(!chatListBean.disableNotify) {
//                                    getView().updateUnreadCount(true, 1);
//                                }
                            }
                        });
                printi("--- Receive group chat message success --- ");
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printi("--- Receive group chat failed --- " + msg.getErrMsg());
    }

    private void pullSingleNewMessageResponse(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                Chat.GetSessNewMsgRsp newMsgList = Chat.GetSessNewMsgRsp.parseFrom(msg.getBody());
                List<Chat.ChatMsg> listList = newMsgList.getListList();
                if(null != listList && listList.size() > 0) {
                    getModel().getChatItem(listList.get(listList.size() - 1).getFromUid(),
                            chatListBean -> {
                                if(null != chatListBean) {
                                    getView().setChatListItem(chatListBean);
//                                    if (!chatListBean.disableNotify) {
//                                        getView().updateUnreadCount(true, listList.size());
//                                    }
                                }
                            });
                }
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printe("--- pull Single NewMessageResponse ---");
    }

    private void pullGroupNewMessageResponse(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                Groupchat.GetGroupNewMsgRsp groupNewMsgRsp = Groupchat.GetGroupNewMsgRsp.parseFrom(msg.getBody());
                List<Groupchat.GroupChatMsg> listList = groupNewMsgRsp.getListList();
                if(null != listList && listList.size() > 0) {
                    getModel().getChatItem(listList.get(listList.size() - 1).getGroupId(),
                            chatListBean -> {
                                if(null != chatListBean) {
                                    getView().setChatListItem(chatListBean);
//                                    if(!chatListBean.disableNotify){
//                                        getView().updateUnreadCount(true, listList.size());
//                                    }
                                }
                            });
                }
                printe("--- pull Group NewMessageResponse --- count: " + listList.size());
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printe("--- pull Group NewMessageResponse ---");
    }

    private void handleReceiveSingleChatMessage(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())){
            printi("--- Receive single chat message --- " + msg);
            try {
                Chat.ChatMsg chatMsg = Chat.ChatMsg.parseFrom(msg.getBody());
                getModel().getChatItem(chatMsg.getFromUid(),
                        chatListBean -> {
                            if(null != chatListBean) {
                                getView().setChatListItem(chatListBean);
//                                if (!chatListBean.disableNotify) {
//                                    getView().updateUnreadCount(true, 1);
//                                }
                            }
                        });
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printi("--- Receive single chat failed: " + msg.getErrMsg());
    }

    @Deprecated
    public void setAlreadyRead(ChatListBean data){
        if(!data.disableNotify) {
            getView().updateUnreadCount(true, -data.unreadCount);
        }
//        if(0 != data.unreadCount) {
//            data.unreadCount = 0;
//            data.alreadyPullSequence = data.lastReceiveSequence;
//            getView().setChatListItem(data);
//            getModel().updateAlreadyReadSequence(data.hisUid, data.lastReceiveSequence);
//
//            if(Constant.CHAT_TYPE_SINGLE == data.chatType) {
//                sendSingleAlreadyRead(data.hisUid, data.lastReceiveSequence);
//            }else{
//                sendGroupAlreadyRead(data.hisUid, data.groupType, data.lastReceiveSequence);
//            }
//        }
    }

    public void deleteChatItem(ChatListBean chatListBean){
        getModel().deleteChatItem(chatListBean.hisUid,
                    o -> {
                    if(!chatListBean.disableNotify) {
                        getView().updateUnreadCount(true, -chatListBean.unreadCount);
                    }
                    getView().onDeleteChatItemCompleted(chatListBean.hisUid, "success");
                },
                throwable -> getView().onDeleteChatItemCompleted(0,"failed"));
    }

    private void sendSingleAlreadyRead(long hisUid, long readSeq){
        printe("--- send single AlreadyRead --- uid: " + hisUid + "  seq: " + readSeq);
        PBMessage pbMessage = new PBMessage()
                .setCmd(MsgOuterClass.YimCMD.TAG_SESSION_MSG_READ_REQ_CMD_VALUE)
                .setBody(Chat.TagSessNewReadReq.newBuilder()
                        .setUid(getModel().getUid())
                        .setFromUid(hisUid)
                        .setMaxSeq(readSeq));
        mPusher.push(pbMessage);
    }

    private void sendGroupAlreadyRead(long hisUid, int groupType, long readSeq){
        printe("--- send group AlreadyRead --- uid: " + hisUid + "  seq: " + readSeq);
        PBMessage pbMessage = new PBMessage()
                .setCmd(MsgOuterClass.YimCMD.TAG_GROUP_NEW_MSG_READ_REQ_CMD_VALUE)
                .setBody(Groupchat.TagGroupMsgReadReq.newBuilder()
                        .setUid(getModel().getUid())
                        .setGroupId(hisUid)
                        .setGroupType(groupType)
                        .setMaxSeq(readSeq));
        mPusher.push(pbMessage);
    }

    private void handlePullSingleChatListResponse(MsgOuterClass.Msg message){
        //nothing
    }

    private void handlePullGroupChatListResponse(MsgOuterClass.Msg message){
        //nothing
    }

}
