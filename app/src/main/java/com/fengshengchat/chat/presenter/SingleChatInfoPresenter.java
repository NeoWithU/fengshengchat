package com.fengshengchat.chat.presenter;

import com.fengshengchat.base.mvp.BasePresenter;
import com.fengshengchat.chat.model.SingleChatInfoModel;
import com.fengshengchat.chat.view.ISingleChatInfoView;

public class SingleChatInfoPresenter extends BasePresenter<SingleChatInfoModel, ISingleChatInfoView> {
    public SingleChatInfoPresenter(ISingleChatInfoView view) {
        super(new SingleChatInfoModel(), view);
    }
}
