package com.fengshengchat.chat.presenter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.util.LongSparseArray;

import com.google.gson.Gson;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.R;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.bean.PressRecordResult;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.mvp.BasePresenter;
import com.fengshengchat.base.utils.TimeUtils;
import com.fengshengchat.chat.bean.CardContentBean;
import com.fengshengchat.chat.bean.ImageContentBean;
import com.fengshengchat.chat.bean.VoiceContentBean;
import com.fengshengchat.chat.model.ChatModel;
import com.fengshengchat.chat.view.ChatActivity;
import com.fengshengchat.chat.view.IChatView;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.DbChatRecord;
import com.fengshengchat.db.Group;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.ChatHandler;
import com.fengshengchat.protocol.DataTypeConverter;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.push.HandlePushService;
import com.fengshengchat.user.User;
import com.fengshengchat.utils.UploadUtils;
import com.fengshengchat.utils.bean.UploadChatImageResponse;
import com.fengshengchat.utils.bean.UploadResponse;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Call;
import yiproto.yichat.chat.Chat;
import yiproto.yichat.groupchat.Groupchat;
import yiproto.yichat.msg.MsgOuterClass;
import yiproto.yichat.sportfulgroup.Sportfulgroup;

public class ChatPresenter extends BasePresenter<ChatModel, IChatView> {
    private HandlePushService.HandlePushServiceInterface mPusher;
    protected long mToUid;
    private boolean isGroup;
    protected String groupName;
    protected String groupIcon;
    protected int groupType;
    protected int groupMemberCount;
    protected User self;
    private final Friend singleChatHe = new Friend();
    private final LongSparseArray<Account> groupFriendList = new LongSparseArray<>();

    public ChatPresenter(long toUid, boolean isGroup, int groupType, IChatView view) {
        this(new ChatModel(), toUid, isGroup, groupType, view);
    }

    public ChatPresenter(ChatModel chatModel, long toUid, boolean isGroup, int groupType, IChatView view) {
        super(chatModel, view);
        this.mToUid = toUid;
        this.isGroup = isGroup;
        this.groupType = groupType;

        self = getModel().getUser();
        if(!isGroup){
            singleChatHe.id = toUid;
            getFriendInfo(toUid);
        }
    }

    public void getFriendInfo(long toUid){
        getModel().getFriend(toUid, friend -> {
            if(null != friend){
                singleChatHe.nickname = friend.nickname;
                singleChatHe.avatar = friend.avatar;
                getView().onUpdateSingleChatHisInfo();
            }
        });
    }

    public void setGroupInfo(String groupName, String groupIcon){
        this.groupName = groupName;
        this.groupIcon = groupIcon;
    }

    public void setPusher(HandlePushService.HandlePushServiceInterface pusher){
        mPusher = pusher;
    }

    public void sendTextMessageToOther(long uid, ChatRecord chatRecord){
        //todo 发送到群
        sendTextMessage(uid, false, 0, chatRecord.content);
    }

    public void sendTextMessage(String content){
        sendTextMessage(mToUid, isGroup, groupType, content);
    }

    public void sendTextMessage(long uid, boolean isGroup, int groupType, String content){
        sendStringMessage(uid, isGroup, groupType, Chat.MsgType.MSGTYPE_TEXT_VALUE, content);
    }

    public void sendStringMessage(int msgType, String content){
        sendStringMessage(mToUid, isGroup, groupType, msgType, content);
    }

    public void sendStringMessageToOther(long uid, int msgType, String content){
        //todo 处理群
        sendStringMessage(uid, false, 0, msgType, content);
    }

    private void sendStringMessage(long uid, boolean isGroup, int groupType, int msgType, String content){
        PBMessage pbMessage = generatePBMessage(uid, isGroup, groupType, msgType, content);
        onGenerateSendMessage(uid, isGroup, groupType, pbMessage.getMessageSequence(), msgType, content, content, 0);
        mPusher.push(pbMessage);

        printe("Send to: " + (isGroup?"Group ?? ":"Single ?? ") + uid + " type: " + msgType
                + " seq: " + pbMessage.getMessageSequence() + " content: " + content);
    }

    public void sendImageMessage(String imagePath){
        if(null == imagePath){
            return;
        }

        sendImageMessage(mToUid, isGroup, groupType, imagePath);
    }

    public void sendImageMessageToOther(long uid, ChatRecord chatRecord){
        //todo 发送到群
        sendImageMessage(uid, false, 0, chatRecord.content);
    }

    public void sendImageMessage(long uid, boolean isGroup, int groupType, String imagePath){
        if(null == imagePath){
            return;
        }

        int msgType = Chat.MsgType.MSGTYPE_IMAGE_VALUE;
        String imageContent = convertImageContent(imagePath, imagePath);
        PBMessage pbMessage = generatePBMessage(uid, isGroup, groupType, msgType, imageContent);
        onGenerateSendMessage(uid, isGroup, groupType, pbMessage.getMessageSequence(), msgType, imagePath, imageContent, 0);
        handleUploadImageFile(pbMessage, imagePath);
    }

    public void sendImageMessage(List<String> list){
        if(null == list){
            return;
        }

        for(String path : list){
            sendImageMessage(path);
        }
    }

    public void sendVoiceMessageToOther(long uid, ChatRecord chatRecord){
        //todo 发送到群
        int duration = chatRecord.duration;
        int msgType = Chat.MsgType.MSGTYPE_VOICE_VALUE;
        String voiceContent = convertVoiceContent(chatRecord.content, duration);
        PBMessage pbMessage = generatePBMessage(uid, false, 0, msgType, voiceContent);
        onGenerateSendMessage(uid, false, 0, pbMessage.getMessageSequence(), msgType,
                chatRecord.content, voiceContent, duration);
        uploadVoiceFile(pbMessage, chatRecord.content, duration);
    }

    public void sendVoiceMessage(PressRecordResult record){
        sendVoiceMessage(mToUid, isGroup, groupType, record);
    }

    public void sendVoiceMessage(long uid, boolean isGroup, int groupType, PressRecordResult record){
        if(PressRecordResult.ERROR_NO_ERROR == record.errorCode && null != record.recordFilePath) {
            File recordFile = new File(record.recordFilePath);
            if (recordFile.isFile()) {
                int duration = record.duration;
                int msgType = Chat.MsgType.MSGTYPE_VOICE_VALUE;
                String voiceContent = convertVoiceContent(record.recordFilePath, duration);
                PBMessage pbMessage = generatePBMessage(uid, isGroup, groupType, msgType, voiceContent);
                onGenerateSendMessage(uid, isGroup, groupType, pbMessage.getMessageSequence(), msgType,
                                record.recordFilePath, voiceContent, duration);
                uploadVoiceFile(pbMessage, recordFile.getAbsolutePath(), duration);
            }
        }else if(PressRecordResult.ERROR_TOO_SHORT == record.errorCode){
            getView().toast(R.string.speak_too_short);
        }
    }

    public void sendCardMessage(long uid, String name, String icon){
        sendStringMessage(Chat.MsgType.MSGTYPE_CARD_VALUE, convertCardContent(uid, name, icon));
    }

    public void sendCardMessageToOther(long toUid, ChatRecord chatRecord){
        sendStringMessageToOther(toUid, Chat.MsgType.MSGTYPE_CARD_VALUE, chatRecord.content);
    }

    private String convertVoiceContent(String uri, int duration){
        return new Gson().toJson(new VoiceContentBean(uri, duration));
    }

    private String convertImageContent(String thumb, String original){
        ImageContentBean imageContentBean = new ImageContentBean(getWH(thumb), getWH(original));
        return new Gson().toJson(imageContentBean);
    }

    private String updateImageContent(PBMessage pbMessage, String thumb, String original){
        ImageContentBean imageContentBean = new ImageContentBean(getWH(thumb), getWH(original));
        try {
            GeneratedMessageV3.Builder bodyBuilder = pbMessage.getBodyBuilder();
            String oriMsg = null;
            if (bodyBuilder instanceof Chat.SendChatReq.Builder) {
                oriMsg = ((Chat.SendChatReq.Builder) bodyBuilder).getMsg();
            } else if (bodyBuilder instanceof Groupchat.SendGroupChatReq.Builder) {
                oriMsg = ((Groupchat.SendGroupChatReq.Builder) bodyBuilder).getMsgContent();
            } else if (bodyBuilder instanceof Sportfulgroup.SendSportfulGroupReq.Builder) {
                oriMsg = ((Sportfulgroup.SendSportfulGroupReq.Builder) bodyBuilder).getMsgContent();
            }
            oriMsg = Protocol.decodeBase64ToString(oriMsg);
            imageContentBean = new Gson().fromJson(oriMsg, ImageContentBean.class);
            imageContentBean.thumb_info.uri = thumb;
            imageContentBean.general_info.uri = original;
//        imageContentBean.original_info.uri = ;
        }catch(Exception e){
            e.printStackTrace();
        }
        return new Gson().toJson(imageContentBean);
    }

    private int dp2px(int dp){
        try {
            return (int) (getView().getContext().getResources().getDisplayMetrics().density * dp);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dp;
    }

    private ImageContentBean.Info getWH(String filePath){
        File file = new File(filePath);
        if(file.isFile() && file.exists()){
            try{
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(filePath, options);
                int dW = options.outWidth;
                int dH = options.outHeight;
                try {
                    ExifInterface exifInterface = new ExifInterface(filePath);
                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
                    if(ExifInterface.ORIENTATION_ROTATE_90 == orientation
                            || ExifInterface.ORIENTATION_ROTATE_270 == orientation){
                        dW = options.outHeight;
                        dH = options.outWidth;
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                float ratio = 1;
                int maxWidth = dp2px(150);
                int maxHeight = dp2px(160);
                if(dW > dH){
                    if(dW > maxWidth){
                        ratio = (float)maxWidth / dW;
                        dW = maxWidth;
                        dH = (int)(dH * ratio);
                    }
                }else{
                    if(dH > maxHeight){
                        ratio = (float)maxHeight / dH;
                        dH = maxHeight;
                        dW = (int)(dW * ratio);
                    }
                }
                printe("w: " + dW + " h: " + dH);
                return new ImageContentBean.Info(filePath, dW, dH);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return new ImageContentBean.Info(filePath);
    }

    private String convertCardContent(long uid, String name, String icon){
        return new Gson().toJson(new CardContentBean(uid, name, icon));
    }

    public String getMyName(){
        return self.nickname;
    }

    public String getMyAvatar(){
        return self.icon;
    }

    private void getFriendFromModel(long uid){
        getModel().getFriend(uid, friend -> {
            if(null != friend){
                groupFriendList.put(uid, friend);
                getView().onUpdateGroupChatHisInfo();
            }else{
                getModel().getNonFriendInfoFromGroup(mToUid, uid, groupMember -> {
                    if(null != groupMember){
                        Account account = new Account();
                        account.id = uid;
                        account.nickname = groupMember.nickname;
                        account.avatar = groupMember.avatar;
                        groupFriendList.put(uid, account);
                        getView().onUpdateGroupChatHisInfo();
                    }
                });
            }
        });
    }

    public Account getFriend(long uid){
        Account friend = groupFriendList.get(uid, null);
        if(null == friend){
            groupFriendList.put(uid, new Account());
            getFriendFromModel(uid);
            return null;
        }else if(0 == friend.id) {
            return null;
        }else{
            return friend;
        }
    }

    public void updateFriendInfo(Account f){
        Account cache = groupFriendList.get(f.id, null);
        if(null != cache){
            cache.nickname = f.nickname;
            cache.avatar = f.avatar;
            groupFriendList.put(f.id, cache);
            getView().onUpdateGroupChatHisInfo();
        }
    }

    public String getHisName(){
        return singleChatHe.nickname;
    }

    public String getHisIcon(){
        return singleChatHe.avatar;
    }

    public String getName(){
        return isGroup ? groupName : getHisName();
    }

    public String getIcon(){
        return isGroup ? groupIcon : getHisIcon();
    }

    protected PBMessage generatePBMessage(long uid, boolean isGroup, int groupType, int msgType, String content){
        content = encodeMessageContent(content);

        PBMessage pbMessage;
        if(isGroup){
            pbMessage = new PBMessage()
                    .setCmd(MsgOuterClass.YimCMD.SEND_GROUP_CHAT_REQ_CMD_VALUE)
                    .setBody(Groupchat.SendGroupChatReq.newBuilder()
                            .setGroupId(uid)
                            .setGroupType(groupType)
                            .setMsgContent(content)
                            .setUid(getModel().getUid())
                            .setMsgType(msgType));
        }else {
            pbMessage = new PBMessage()
                    .setCmd(MsgOuterClass.YimCMD.SEND_CHAT_REQ_CMD_VALUE)
                    .setBody(Chat.SendChatReq.newBuilder()
                            .setFromUid(getModel().getUid())
                            .setToUid(uid)
                            .setMsg(content)
                            .setMsgType(msgType));
        }

        return pbMessage;
    }

    private PBMessage updatePBMessageContent(PBMessage pbMessage, String newContent){
        newContent = encodeMessageContent(newContent);
        GeneratedMessageV3.Builder bodyBuilder = pbMessage.getBodyBuilder();
        if(bodyBuilder instanceof Chat.SendChatReq.Builder){
            ((Chat.SendChatReq.Builder) bodyBuilder).setMsg(newContent);
        }else if(bodyBuilder instanceof Groupchat.SendGroupChatReq.Builder){
            ((Groupchat.SendGroupChatReq.Builder) bodyBuilder).setMsgContent(newContent);
        }else if(bodyBuilder instanceof Sportfulgroup.SendSportfulGroupReq.Builder){
            ((Sportfulgroup.SendSportfulGroupReq.Builder) bodyBuilder).setMsgContent(newContent);
        }
        pbMessage.setBody(bodyBuilder);
        return pbMessage;
    }

    protected String encodeMessageContent(String src){
        return Protocol.encodeBase64ToString(src);
    }

    //Just on send, no care result
    protected void onGenerateSendMessage(long uid, boolean isGroup, int groupType, int sequence, int type, String content, String rawContent, int duration){
        ChatRecord chatRecord = new ChatRecord();
        chatRecord.content = content;
        chatRecord.rawContent = rawContent;
        chatRecord.chatType = isGroup ? Constant.CHAT_TYPE_GROUP : Constant.CHAT_TYPE_SINGLE;
        chatRecord.groupType = groupType;
        chatRecord.messageType = type;
        chatRecord.time = TimeUtils.getTimeSeconds();
        chatRecord.isSendMessage = true;
        chatRecord.messageSequence = sequence;
        chatRecord.messageState = Constant.MESSAGE_STATE_SENDING;
        chatRecord.duration = duration;
        chatRecord.setMy(new com.fengshengchat.db.User(self.uid));
        com.fengshengchat.db.User he = new com.fengshengchat.db.User(uid);
        he.nickName = isGroup ? groupName : singleChatHe.nickname;
        he.icon = isGroup ? groupIcon : singleChatHe.avatar;
        chatRecord.setHis(he);

        if(uid == mToUid) {
            getView().onSendMessage(chatRecord);
        }
        getModel().saveMessage(chatRecord, true);
    }

    public void quote(ChatRecord chatRecord){
        sendTextMessage(chatRecord.content);
    }

    public void resend(ChatRecord chatRecord){
        long time = TimeUtils.getTimeSeconds();
//        chatRecord.time = time;
        PBMessage pbMessage = generatePBMessage(mToUid, isGroup, groupType, chatRecord.messageType, chatRecord.content);
        pbMessage.getMsgBuilder().setSeq((int)chatRecord.messageSequence)
                .setTimestamp(time);

        switch(chatRecord.messageType){
            case Chat.MsgType.MSGTYPE_TEXT_VALUE://other type send after upload file success
            case Chat.MsgType.MSGTYPE_CARD_VALUE:
                mPusher.push(pbMessage);
                break;
            case Chat.MsgType.MSGTYPE_IMAGE_VALUE:
                handleUploadImageFile(pbMessage, chatRecord.content);
                break;
            case Chat.MsgType.MSGTYPE_VOICE_VALUE:
                uploadVoiceFile(pbMessage, chatRecord.content, chatRecord.duration);
                break;
        }

        getView().onSendMessageRetry(chatRecord.messageSequence);
        printe("<Retry> send to: " + (isGroup?"Group ":"Single ") + mToUid +
                " seq: " + pbMessage.getMessageSequence() + " content: " + chatRecord.content);
    }

    private void handleUploadImageFile(PBMessage pbMessage, String filePath){
        printe("---send image --- " + filePath);
        getModel().uploadImageFileWithThumb(getView().getContext(), false, filePath, new UploadUtils.UploadChatImageResponseCallback() {
            @Override
            public void onResponse(Call call, UploadChatImageResponse response) throws Exception {
                printe(response.getMsg());
                Disposable subscribe = Observable.just("")
                        .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                        .subscribe(new Consumer<String>() {
                            public void accept(String s) throws Exception {
                                printe("---send image completed res: " + response.isOK() + " " + filePath);
                                if (response.isOK()) {
                                    mPusher.push(updatePBMessageContent(
                                            pbMessage, updateImageContent(
                                                    pbMessage, response.getFile1(), response.getFile2())));
                                } else {
                                    getView().onSendMessageFailed(pbMessage.getMessageSequence(), "");
                                }
                            }
                        });
            }

            public void onFailure(Call call, Exception e) {
                printe("---send image failure " + filePath);
                e.printStackTrace();
                Disposable subscribe = Observable.fromCallable(() -> {
                    DbChatRecord.updateSendResultState(
                            pbMessage.getMessageSequence(), pbMessage.getMessageSequence(),
                            Constant.MESSAGE_STATE_FAILED);
                    return "";
                }).compose(RxSchedulersHelper.applyIO2MainSchedulers())
                        .subscribe(t -> {
                            getView().onSendMessageFailed(pbMessage.getMessageSequence(), "");
                        });
            }
            public void onResponse(Call call, UploadResponse response) throws Exception {

            }
        });
    }

    private void uploadVoiceFile(PBMessage pbMessage, String filePath, int duration){
        getModel().uploadFile(filePath, new UploadUtils.UploadResponseCallback() {
            public void onFailure(Call call, Exception e) {
                Disposable subscribe = Observable.fromCallable(() -> {
                    DbChatRecord.updateSendResultState(
                            pbMessage.getMessageSequence(), pbMessage.getMessageSequence(),
                            Constant.MESSAGE_STATE_FAILED);
                        return "";
                }).compose(RxSchedulersHelper.applyIO2MainSchedulers())
                        .subscribe(t -> {
                            getView().onSendMessageFailed(pbMessage.getMessageSequence(), "");
                        });
            }
            public void onResponse(Call call, UploadResponse response) throws Exception {
                if(response.isOK()){
                    mPusher.push(updatePBMessageContent(
                                    pbMessage, convertVoiceContent(response.getMsg(), duration)));
                }else{
                    getView().onSendMessageFailed(pbMessage.getMessageSequence(), "");
                }
            }
        });
    }

    public void updateVoiceReadState(long hidUid, long msgSeq){
        getModel().updateVoiceReadState(hidUid, msgSeq);
    }

    protected boolean handleInternalCmd(MsgOuterClass.Msg message){
        if(Constant.INTERNAL_CMD_TYPE != message.getBigCmd()) {
            return false;
        }
        printe("-----------<internal> cmd: " + message.getCmd());
        switch(message.getCmd()){
            case Constant.INTERNAL_CMD_REFRESH_GROUP_INFO:
                if(mToUid != message.getUid())
                    return true;

                getModel().getGroupInfo(message.getUid(), group -> {
                    if(null != group){
                        getView().onUpdateGroupInfo(group);
                    }
                });
                return true;
        }
        return false;
    }

    public boolean handleMessage(MsgOuterClass.Msg message){
        if(Constant.INTERNAL_CMD_TYPE == message.getBigCmd()){
            return handleInternalCmd(message);
        }

        printi("handleMessage -> " + message.getCmd() + " errCode: " + message.getCode() + "  errMsg: " + message.getErrMsg());
        switch (message.getCmd()) {
            default:
                return false;
            case MsgOuterClass.YimCMD.SEND_CHAT_RSP_CMD_VALUE:
                sendSingleMessageResponse(message);
                break;
            case MsgOuterClass.YimCMD.CHAT_MSG_PUSH_CMD_VALUE:
                onReceiveSingleMessage(message);
                break;
            case MsgOuterClass.YimCMD.SEND_GROUP_CHAT_RSP_CMD_VALUE:
                sendGroupMessageResponse(message);
                break;
            case MsgOuterClass.YimCMD.GROUP_CHAT_MSG_PUSH_CMD_VALUE:
                onReceiveGroupMessage(message);
                break;
            case MsgOuterClass.YimCMD.GET_SESSION_NEW_MSG_RSP_CMD_VALUE:
                onReceiveNewMessageFromPull(message);
                break;
            case MsgOuterClass.YimCMD.GET_GROUP_NEW_MSG_RSP_CMD_VALUE:
                handlePullGroupNewMessageResponse(message);
                break;
            case MsgOuterClass.YimCMD.GROUP_SYSTEM_BROADCAST_CMD_VALUE:
                handleGroupBroadcast(message);
                break;
            case yiproto.yichat.friend.Friend.FriendYimCMD.NOTIFY_UPDATE_REQ_CMD_VALUE:
                onAddFriend(message);
                break;
        }
        return true;
    }

    public void onAddFriend(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                yiproto.yichat.friend.Friend.NotifyUpdateReq notifyUpdateReq = yiproto.yichat.friend.Friend.NotifyUpdateReq.parseFrom(msg.getBody());
                yiproto.yichat.friend.Friend.NotifyUpdateReq.FriendGreetingSystemMsg greetingMsg = notifyUpdateReq.getGreetingMsg();
                if(greetingMsg.getAgreeUserId() == getModel().getUid()
                        && greetingMsg.getRequestUserId() == mToUid){
                    ChatRecord chatRecord = DataTypeConverter.convertChatRecord(greetingMsg);
                    if(null != chatRecord) {
                        getView().onReceiveMessage(chatRecord);
//                        getModel().updateAlreadyReadSequence(chatRecord.his.uid, chatRecord.messageSequence);
//                        sendAlreadyRead(chatRecord.his.uid, chatRecord.messageSequence);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean confirmEnterNewGroup = false;
    private void enterNewGroup(long gid, String groupName, String groupIcon, int groupType, int groupMemberCount){
        ChatActivity.startChat(getView().getActivity(), gid, groupName, groupIcon, true, groupType, groupMemberCount);
    }

    private void onGroupTypeChange(Groupchat.GroupSystemBroadcast groupBroadcast){
        if(groupBroadcast.getGroupType() != groupType){
            AlertDialog alertDialog = new AlertDialog.Builder(getView().getActivity())
                    .setTitle(R.string.tip)
                    .setMessage(Protocol.decodeBase64ToString(groupBroadcast.getMsgContent()))
                    .setPositiveButton(R.string.confirm, (dialog, which) -> {
                        confirmEnterNewGroup = true;
                        dialog.dismiss();
                    })
                    .setOnDismissListener(dialog -> {
                        try {
                            if (confirmEnterNewGroup) {
                                enterNewGroup(groupBroadcast.getGroupId(), groupName, groupIcon,
                                        groupBroadcast.getGroupType(), groupMemberCount);
                            }
                            ((Activity) getView().getActivity()).finish();
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    })
                    .create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }
    }

    protected boolean preHandleGroupBroadcast(Groupchat.GroupSystemBroadcast groupBroadcast){
        switch(groupBroadcast.getMsgType()){
            default:
                return false;
            case Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_TYPE_VALUE:
                onGroupTypeChange(groupBroadcast);
                return true;
            case Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_ANNOUNCEMENT_VALUE:
                getView().onReceiveGroupNotify(Protocol.decodeBase64ToString(groupBroadcast.getMsgContent()));
                return false;
            case Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_ANNOUNCEMENT_OPEN_VALUE:
                getView().onReceiveGroupNotify(Protocol.decodeBase64ToString(groupBroadcast.getMsgContent()));
                return true;
            case Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_ANNOUNCEMENT_OFF_VALUE:
                getView().setShowGroupNotify(false);
                return true;
        }
    }

    private void handleGroupBroadcast(MsgOuterClass.Msg msg){
        if (BaseHandler.isNoError(msg.getCode())) {
            try {
                Groupchat.GroupSystemBroadcast groupBroadcast = Groupchat.GroupSystemBroadcast.parseFrom(msg.getBody());
                printe("---------group: " + groupBroadcast);

                if(mToUid != groupBroadcast.getGroupId()){
                    return;
                }

                if(preHandleGroupBroadcast(groupBroadcast)){
                    getModel().updateAlreadyReadSequence(groupBroadcast.getGroupId(), groupBroadcast.getMsgSeq());
                    return;
                }

                if(Groupchat.GROUP_SYS_MSG_TYPE.INITIATIVE_OUT_VALUE == groupBroadcast.getMsgType()
                        || Groupchat.GROUP_SYS_MSG_TYPE.KICT_OUT_VALUE == groupBroadcast.getMsgType()){
                    Disposable subscribe = Observable.fromCallable(new Callable<Boolean>() {
                        @Override
                        public Boolean call() throws Exception {
                            return ChatHandler.needHandle(groupBroadcast);
                        }
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Consumer<Boolean>() {
                                @Override
                                public void accept(Boolean aBoolean) throws Exception {
                                    if (aBoolean) {
                                        ChatRecord record = DataTypeConverter.convertChatRecord(groupBroadcast);

                                        getView().onReceiveMessage(record);
                                        getModel().getGroupInfo(mToUid, new Consumer<Group>() {
                                            public void accept(Group group) throws Exception {
                                                if(null != group) {
                                                    getView().onUpdateGroupInfo(group);
                                                }
                                            }
                                        });
                                        getModel().updateAlreadyReadSequence(record.his.uid, record.messageSequence);
                                    }
                                }
                            }, throwable -> {

                            });
                    return;
                }

                ChatRecord record = DataTypeConverter.convertChatRecord(groupBroadcast);

                getView().onReceiveMessage(record);
                getModel().getGroupInfo(mToUid, new Consumer<Group>() {
                    public void accept(Group group) throws Exception {
                        if(null != group) {
                            getView().onUpdateGroupInfo(group);
                        }
                    }
                });


                getModel().updateAlreadyReadSequence(record.his.uid, record.messageSequence);
//                    sendAlreadyRead(chatRecord.his.uid, chatRecord.messageSequence);






            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onReceiveNewMessageFromPull(MsgOuterClass.Msg msg){
        //TODO
        printe("----------onReceiveNewMessageFromPull ********----------");
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                Chat.GetSessNewMsgRsp newMsgList = Chat.GetSessNewMsgRsp.parseFrom(msg.getBody());

                List<Chat.ChatMsg> listList = newMsgList.getListList();
                if(null != listList && listList.size() > 0) {
                    List<ChatRecord> chatRecords = DataTypeConverter.convertSingleChatRecords(listList);

                    if(chatRecords.size() > 0) {
                        ChatRecord chatRecord = chatRecords.get(chatRecords.size() - 1);
                        if(chatRecord.his.uid != mToUid){
                            return;
                        }
                        getView().onReceiveMessage(chatRecords);
                        getModel().updateAlreadyReadSequence(chatRecord.his.uid, chatRecord.messageSequence);
                        sendAlreadyRead(chatRecord.his.uid, chatRecord.messageSequence);
                    }
                    //TODO
                    Debug.e("ChatPresenter", "---------处理群消息--------");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handlePullGroupNewMessageResponse(MsgOuterClass.Msg msg){
        printe("---------------pull group NewMessageResponse-----------");
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                Groupchat.GetGroupNewMsgRsp groupNewMsgRsp = Groupchat.GetGroupNewMsgRsp.parseFrom(msg.getBody());
                List<Groupchat.GroupChatMsg> listList = groupNewMsgRsp.getListList();

                if(null != listList && listList.size() > 0) {
                    List<ChatRecord> chatRecords = DataTypeConverter.convertGroupChatRecords(listList);

                    if(chatRecords.size() > 0) {
                        ChatRecord chatRecord = chatRecords.get(chatRecords.size() - 1);
                        if(chatRecord.his.uid != mToUid){
                            return;
                        }
                        getView().onReceiveMessage(chatRecords);
                        getModel().updateAlreadyReadSequence(chatRecord.his.uid, chatRecord.messageSequence);
                        sendAlreadyRead(chatRecord.his.uid, chatRecord.messageSequence);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void onReceiveGroupMessage(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())) {
            try {
                ChatRecord chatRecord = null;
                Groupchat.GroupChatMsg groupChatMsg = Groupchat.GroupChatMsg.parseFrom(msg.getBody());
                if(groupChatMsg.getGroupId() == mToUid) {
                    chatRecord = DataTypeConverter.convertChatRecord(
                            Constant.CHAT_TYPE_GROUP,
                            groupChatMsg.getUid() == getModel().getUid(),
                            groupChatMsg);
                }

                if (null != chatRecord) {
                    getView().onReceiveMessage(chatRecord);
                    getModel().updateAlreadyReadSequence(chatRecord.his.uid, chatRecord.messageSequence);
                    sendAlreadyRead(chatRecord.his.uid, chatRecord.messageSequence);
                }
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendGroupMessageResponse(MsgOuterClass.Msg message){
        if (BaseHandler.isNoError(message.getCode())) {
            try {
                Groupchat.SendGroupChatRsp sendGroupChatRsp = Groupchat.SendGroupChatRsp.parseFrom(message.getBody());
                getView().onSendMessageSuccess(message.getSeq(), sendGroupChatRsp.getMsgSeq());
                sendAlreadyRead(mToUid, sendGroupChatRsp.getMsgSeq());
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }else if(MsgOuterClass.YiChatErrCode.YICHAT_SEND_GROUP_CHAT_NOT_EXIST_VALUE == message.getCode()
                || MsgOuterClass.YiChatErrCode.YICHAT_SEND_GROUP_CHAT_NOT_IN_VALUE == message.getCode()){
            try {
                Groupchat.SendGroupChatRsp sendGroupChatRsp = Groupchat.SendGroupChatRsp.parseFrom(message.getBody());
                addTip(sendGroupChatRsp.getGroupId(), message.getErrMsg());
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        getView().onSendMessageFailed(message.getSeq(), message.getErrMsg());
    }

    private void sendSingleMessageResponse(MsgOuterClass.Msg message){
        if (BaseHandler.isNoError(message.getCode())) {
            try {
                Chat.SendChatRsp sendChatRsp = Chat.SendChatRsp.parseFrom(message.getBody());
                if(sendChatRsp.getToUid() == mToUid) {
                    getView().onSendMessageSuccess(message.getSeq(), sendChatRsp.getMsgSeq());
                    sendAlreadyRead(mToUid, sendChatRsp.getMsgSeq());
                }
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }else if(MsgOuterClass.YiChatErrCode.YICHAT_SEND_CHAT_NON_FRIEND_VALUE == message.getCode()
                || MsgOuterClass.YiChatErrCode.YICHAT_SEND_CHAT_BLACKLIST_VALUE == message.getCode()){
            try {
                Chat.SendChatRsp sendChatRsp = Chat.SendChatRsp.parseFrom(message.getBody());
                if(sendChatRsp.getToUid() == mToUid) {
                    addTip(sendChatRsp.getToUid(), message.getErrMsg());
                }
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        getView().onSendMessageFailed(message.getSeq(), message.getErrMsg());
    }

    protected void addTip(long uid, String msg){
        if(uid != mToUid){
            return;
        }
        ChatRecord tip = new ChatRecord();
        tip.messageType = Constant.MESSAGE_TYPE_TIP;
        tip.content = msg;
        com.fengshengchat.db.User his = new com.fengshengchat.db.User();
        his.uid = mToUid;
        tip.his = his;
//        getModel().saveMessage(tip, false);
        getView().onAddTip(tip);
    }

    private void onReceiveSingleMessage(MsgOuterClass.Msg message){
        try {
            ChatRecord chatRecord = null;
            Chat.ChatMsg chatMsg = Chat.ChatMsg.parseFrom(message.getBody());
            if(chatMsg.getFromUid() == mToUid){
                chatRecord = DataTypeConverter.convertChatRecord(Constant.CHAT_TYPE_SINGLE, false, chatMsg);
            }else if(chatMsg.getFromUid() == getModel().getUid() && chatMsg.getToUid() == mToUid){ //self send from web
                chatRecord = DataTypeConverter.convertChatRecord(Constant.CHAT_TYPE_SINGLE, true, chatMsg);
            }

            if(null != chatRecord) {
                getView().onReceiveMessage(chatRecord);
                getModel().updateAlreadyReadSequence(chatRecord.his.uid, chatRecord.messageSequence);
                sendAlreadyRead(chatRecord.his.uid, chatRecord.messageSequence);
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    public void updateAlreadyRead(long hisUid){
        getModel().updateAlreadyReadSequence(hisUid, readSeq -> {
            if(0 != readSeq) {
                sendAlreadyRead(hisUid, readSeq);
            }
        });
    }

    protected void sendAlreadyRead(long hisUid, long lastReceiveSequence){
        if(isGroup) {
            sendGroupAlreadyRead(hisUid, groupType, lastReceiveSequence);
        }else{
            sendSingleAlreadyRead(hisUid, lastReceiveSequence);
        }
    }

    private void sendSingleAlreadyRead(long hisUid, long readSeq){
        printe("--- send single AlreadyRead --- uid: " + hisUid + "  seq: " + readSeq);
        PBMessage pbMessage = new PBMessage()
                .setCmd(MsgOuterClass.YimCMD.TAG_SESSION_MSG_READ_REQ_CMD_VALUE)
                .setBody(Chat.TagSessNewReadReq.newBuilder()
                        .setUid(getModel().getUid())
                        .setFromUid(hisUid)
                        .setMaxSeq(readSeq));
        mPusher.push(pbMessage);
    }

    private void sendGroupAlreadyRead(long hisUid, int groupType, long readSeq){
        printe("--- send group AlreadyRead --- uid: " + hisUid + "  seq: " + readSeq);
        PBMessage pbMessage = new PBMessage()
                .setCmd(MsgOuterClass.YimCMD.TAG_GROUP_NEW_MSG_READ_REQ_CMD_VALUE)
                .setBody(Groupchat.TagGroupMsgReadReq.newBuilder()
                        .setUid(getModel().getUid())
                        .setGroupId(hisUid)
                        .setGroupType(groupType)
                        .setMaxSeq(readSeq));
        mPusher.push(pbMessage);
    }

    protected HandlePushService.HandlePushServiceInterface getPusher(){
        return mPusher;
    }

    public void getHistoryMessage(int offset){
        getView().onGettingHistory();
        getModel().getHistoryMessage(mToUid, offset, chatRecords -> getView().onHistory(chatRecords));
    }

    public void copyText2SystemClipboard(ChatRecord record){
        ClipboardManager cm = (ClipboardManager) getView().getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        cm.setPrimaryClip(ClipData.newPlainText("fromYiChat", record.content));
    }

    public void deleteChatRecord(ChatRecord chatRecord){
        getModel().deleteOneChatRecord(chatRecord, new Consumer<Object>() {
            public void accept(Object o) throws Exception {
                getView().onDeleteChatRecordCompleted(chatRecord.messageSequence);
            }
        }, new Consumer<Throwable>() {
            public void accept(Throwable throwable) throws Exception {
                getView().onDeleteChatRecordCompleted(-1);
            }
        });
    }
}
