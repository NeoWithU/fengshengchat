package com.fengshengchat.chat.model;

import android.content.Context;

import com.fengshengchat.R;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.DbChatList;
import com.fengshengchat.db.DbChatRecord;
import com.fengshengchat.db.DbFriend;
import com.fengshengchat.db.Group;
import com.fengshengchat.db.GroupMember;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.functions.Consumer;
import yiproto.yichat.chat.Chat;

/**
 * @author KRT
 * 2018/12/5
 */
public class ChatListModel extends BaseChatModel {
    private String image;
    private String voice;
    private String card;

    public ChatListModel(Context context) {
        image = context.getResources().getString(R.string.image);
        voice = context.getResources().getString(R.string.voice);
        card = context.getResources().getString(R.string.card);
    }

    public void refreshInfo(Consumer<List<ChatListBean>> callback){
        asyncTask(this::refreshList, callback, Throwable::printStackTrace);
    }

    private ChatListBean generateSearchBean(){
        ChatListBean chatListBean = new ChatListBean();
        chatListBean.chatType = Constant.CHAT_TYPE_SEARCH;
        return chatListBean;
    }

    private List<ChatListBean> refreshList(){
        List<ChatListBean> chatList = DbChatList.getChatList();
        List<Account> allFriend = DbFriend.getAllFriend();
        List<Group> allGroup = DbFriend.getAllGroup();
        for(ChatListBean bean : chatList){
            switch(bean.chatType){
                case Constant.CHAT_TYPE_SINGLE:
                    refreshFriendInfo(allFriend, bean);
                    break;
                case Constant.CHAT_TYPE_GROUP:
                    refreshGroupInfo(allGroup, bean);
                    break;
            }
        }

        List<ChatListBean> res = DbChatList.getChatList();//get new order //chatList;
        res.add(0, generateSearchBean());
        res = calcUnreadCount(res);
        res = updateSendNickName(res);
        return res;
    }

    private List<ChatListBean> updateSendNickName(List<ChatListBean> list){
        if(null == list || list.size() <= 0)
            return list;

        try {
            for (ChatListBean bean : list) {
                setSendNickName(bean);
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return list;
    }

    private void setSendNickName(ChatListBean bean){
        long myUid = getUid();
        long sendUid = 0;
        String sendNickName = "";

        try {
            sendUid = Long.parseLong(bean.ext_1);
        }catch(Exception e){
//            e.printStackTrace();
        }

        if(Constant.CHAT_TYPE_GROUP == bean.chatType){
            try{
                if(myUid != sendUid && 0 != sendUid) {
                    Account friend = DbFriend.getFriend(sendUid);
                    if(null == friend) {
                        GroupMember nonFriendInfoFromGroup = DbFriend.getNonFriendInfoFromGroup(bean.hisUid, sendUid);
                        if(null != nonFriendInfoFromGroup){
                            sendNickName = nonFriendInfoFromGroup.nickname;
                        }
                    }else{
                        sendNickName = friend.nickname;
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        if(null == sendNickName){
            sendNickName = "";
        }

            switch(bean.messageType) {
                default:
                case Chat.MsgType.MSGTYPE_TEXT_VALUE:
                    if(sendNickName.length() > 0){
                        bean.lastMessage = sendNickName + ":" + bean.lastMessage;
                    }
                    break;

                case Chat.MsgType.MSGTYPE_IMAGE_VALUE:
                    if(sendNickName.length() > 0){
                        bean.lastMessage = sendNickName + ":" + image;
                    }else{
                        bean.lastMessage = image;
                    }
                    break;
                case Chat.MsgType.MSGTYPE_VOICE_VALUE:
                    if(sendNickName.length() > 0){
                        bean.lastMessage = sendNickName + ":" + voice;
                    }else{
                        bean.lastMessage = voice;
                    }
                    break;
                case Chat.MsgType.MSGTYPE_CARD_VALUE:
                    if(sendNickName.length() > 0){
                        bean.lastMessage = sendNickName + ":" + card;
                    }else{
                        bean.lastMessage = card;
                    }
                    break;
            }
    }

    private void refreshFriendInfo(List<Account> allFriend, ChatListBean friend){
        if(null == allFriend || null == friend){
            return;
        }

        for(Account account : allFriend){
            if(friend.hisUid == account.id){
                friend.name = account.nickname;
                friend.icon = account.avatar;
                friend.isPin = account.isPinSession;
                friend.disableNotify = account.notDisturb;
                DbChatList.updateChatItem(friend);
                break;
            }
        }
    }

    private void refreshGroupInfo(List<Group> allGroup, ChatListBean group){
        if(null == allGroup || null == group){
            return;
        }

        for(Group g : allGroup){
            if(group.hisUid == g.id){
                group.name = g.name;
                group.icon = g.avatar;
                group.groupType = g.type;
                group.isPin = g.isPinSession;
                group.disableNotify = g.notDisturb;
                group.groupMemberCount = g.memberCount;

                if(yiproto.yichat.group.Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE == g.type){
                    group.disableNotify = true;
                }
                DbChatList.updateChatItem(group);
                break;
            }
        }
    }

    private List<ChatListBean> calcUnreadCount(List<ChatListBean> chatList){
        ChatListBean justUnreadCount = new ChatListBean();
        if(null == chatList){
            chatList = new ArrayList<>();
            chatList.add(justUnreadCount);
            return chatList;
        }

        for(ChatListBean chatListBean : chatList){//avoid do it on main thread
            if(!chatListBean.disableNotify) {
                justUnreadCount.unreadCount += chatListBean.unreadCount;
            }
        }
        chatList.add(justUnreadCount);
        return chatList;
    }

    public void getLocalChatList(boolean needPull, Consumer<List<ChatListBean>> callback, Consumer<Throwable> error){
        asyncTask(() -> {
            List<ChatListBean> chatList;
            if(needPull){
                chatList = DbChatList.getChatList();
                chatList.add(0, generateSearchBean());
                chatList = calcUnreadCount(chatList);
                chatList = updateSendNickName(chatList);
            }else{
                chatList = refreshList();
            }

            return chatList;
        }, callback, error);
    }

    public void getLocalChatListWithTempEmptyGroup(Consumer<List<ChatListBean>> callback){
        asyncTask(DbChatList::getChatListWithTempEmptyGroup, callback);
    }

    public void getChatItem(long hisUid, Consumer<ChatListBean> callback){
        asyncTask(() -> {
            ChatListBean chatItem = DbChatList.getChatItem(hisUid);
            setSendNickName(chatItem);
            return chatItem;
        }, callback);
    }

    @Deprecated
    public void updateAlreadyReadSequence(long hisUid, long readSeq){
        asyncTask(() -> {
            DbChatList.updateAlreadyReadSequence(hisUid, readSeq);
            return "";
        });
    }

    public void saveChatItem(ChatListBean chatListBean, Consumer<ChatListBean> callback){
        asyncTask(() -> {
            DbChatList.insertChatItem(chatListBean);
            return chatListBean;
        }, callback);
    }

    public void deleteChatItem(long hisUid, Consumer<Object> callback, Consumer<Throwable> error){
        asyncTask(() -> {
            DbChatList.deleteChatItem(hisUid);
            DbChatRecord.deleteAllChatRecord(hisUid);
            return "";
        }, callback, error);
    }

    private void updateItemFriendInfo(Account friend){
        asyncTask((Callable<Object>) () -> {
            DbChatList.updateItemFriendInfo(friend);
            return "";
        });
    }

    private void updateItemGroupInfo(Group group){
        asyncTask((Callable<Object>) () -> {
            DbChatList.updateItemGroupInfo(group);
            return "";
        });
    }

    public Account getFriend(long uid){
        return DbFriend.getFriend(uid);
    }

    public void getFriend(long uid, Consumer<Account> callback){
        asyncTask(() -> {
            Account friend = getFriend(uid);
            if(null != friend) {
                updateItemFriendInfo(friend);
            }
            return friend;
        }, callback);
    }

    public void getGroupInfo(long gid, Consumer<Group> callback){
        asyncTask(() -> {
            Group group = DbFriend.getGroupInfo(gid);
            if(null != group) {
                updateItemGroupInfo(group);
            }
            return group;
        }, callback);
    }

    public void getGroupInfoDelay(long gid, Consumer<ChatListBean> callback){
        asyncTask(() -> {
            try {
                Thread.sleep(800);
            }catch(Exception e){
                e.printStackTrace();
            }
            Group group = DbFriend.getGroupInfo(gid);
            if(null != group) {
                ChatListBean chatListBean = DbChatList.getChatItem(group.id);
                if(null != chatListBean) {
                    chatListBean.name = group.name;
                    chatListBean.icon = group.avatar;
                    chatListBean.groupType = group.type;
                    chatListBean.isPin = group.isPinSession;
                    chatListBean.disableNotify = group.notDisturb;
                    DbChatList.updateChatItem(chatListBean);
                    return chatListBean;
                }
            }
            return null;
        }, callback);
    }

    @Override
    public void onDestroy() {

    }
}
