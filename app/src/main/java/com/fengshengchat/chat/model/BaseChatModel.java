package com.fengshengchat.chat.model;

import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.mvp.IBaseModel;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseChatModel implements IBaseModel {
    private final String TAG = this.getClass().getSimpleName();
    private User mUser;

    public User getUser(){
        if(null == mUser){
            synchronized (ChatModel.class) {
                if(null == mUser) {
                    mUser = UserManager.getUser();
                }
            }
        }
        return mUser;
    }

    public long getUid(){
        return getUser().uid;
    }

    public String getToken(){
        return getUser().token;
    }

    public <T> Disposable asyncTask(Callable<T> task) {
        //TODO
        return Observable.fromCallable(task)
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public <T> Disposable asyncTask(List<T> list, Consumer<T> handler){
        return Observable.fromIterable(list)
                .subscribeOn(Schedulers.computation())
                .subscribe(handler, throwable -> {
                    printe(throwable.getMessage());
                    handler.accept(null);
                });
    }

    public <T> Disposable asyncTask(Callable<T> task, Consumer<T> callback){
        return Observable.fromCallable(task)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback, throwable -> {
                    printe(throwable.getMessage());
                    callback.accept(null);
                });
    }

    public <T> Disposable asyncTask(Callable<T> task, Consumer<T> callback, Consumer<Throwable> error){
        return Observable.fromCallable(task)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback, error);
    }

    private void printe(String s){
        Debug.e(TAG, s);
    }

}
