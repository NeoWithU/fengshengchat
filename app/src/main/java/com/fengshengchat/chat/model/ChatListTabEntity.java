package com.fengshengchat.chat.model;

import com.flyco.tablayout.listener.CustomTabEntity;
import com.fengshengchat.R;

/**
 * @author KRT
 * 2018/12/5
 */
public class ChatListTabEntity implements CustomTabEntity {
    private String title;

    public ChatListTabEntity(String title){
        this.title = title;
    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public int getTabSelectedIcon() {
        return R.mipmap.msg_sel;
    }

    @Override
    public int getTabUnselectedIcon() {
        return R.mipmap.msg_nor;
    }
}
