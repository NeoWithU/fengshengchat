package com.fengshengchat.chat.model;

import com.fengshengchat.net.bean.Response;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * @author KRT
 * 2018/12/5
 */
public interface IChatListModel {

    @POST("web_gateway")
    @Headers("Content-Type:application/json; charset=utf-8")
    Observable<Response> getChatList(@Body RequestBody body);

}
