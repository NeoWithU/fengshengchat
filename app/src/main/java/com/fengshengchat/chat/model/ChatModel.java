package com.fengshengchat.chat.model;

import android.content.Context;

import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.DbChatList;
import com.fengshengchat.db.DbChatRecord;
import com.fengshengchat.db.DbFriend;
import com.fengshengchat.db.Group;
import com.fengshengchat.db.GroupMember;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.DataTypeConverter;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.utils.UploadUtils;
import com.fengshengchat.utils.bean.UploadChatImageResponse;
import com.fengshengchat.utils.bean.UploadResponse;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

import io.github.lizhangqu.coreprogress.ProgressUIListener;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import yiproto.yichat.msg.MsgOuterClass;

public class ChatModel extends BaseChatModel {

    public void saveMessage(ChatRecord chatRecord, boolean syncChatList){
        asyncTask((Callable<Object>) () -> {
            DbChatRecord.saveSendMessage(chatRecord);
            if(syncChatList) {
                ChatListBean chatItem = DbChatList.getChatItem(chatRecord.his.uid);
                DbChatList.updateChatItemKeepState(0,
                        DataTypeConverter.convertChatRecord(chatItem, chatRecord));
            }
            Debug.i("ChatModel", "--- save message --- syncChatList: " + syncChatList);
            return "";
        });
    }

    public void getHistoryMessage(long toUid, int offset, Consumer<List<ChatRecord>> callback){
        asyncTask(() -> DbChatRecord.getChatRecords(toUid, offset), callback);
    }

    public void getFriend(long uid, Consumer<Account> callback){
        asyncTask(() -> DbFriend.getFriend(uid), callback);
    }

    public void getNonFriendInfoFromGroup(long gid, long uid, Consumer<GroupMember> callback){
        asyncTask(() -> DbFriend.getNonFriendInfoFromGroup(gid, uid), callback);
    }

    public void updateAlreadyReadSequence(long toUid, long sequence){
        asyncTask(() -> {
            DbChatList.updateAlreadyReadSequence(toUid, sequence);
            return "";
        });
    }

    public void updateAlreadyReadSequence(long toUid, Consumer<Long> callback){
        asyncTask(() -> {
            ChatListBean chatItem = DbChatList.getChatItem(toUid);
            if(null != chatItem) {
                if(chatItem.unreadCount > 0) {
                    DbChatList.updateAlreadyReadSequence(toUid, chatItem.lastReceiveSequence);
                }
                return chatItem.lastReceiveSequence;
            }
            return 0L;
        }, callback);
    }

    public void deleteOneChatRecord(ChatRecord chatRecord, Consumer<Object> callback, Consumer<Throwable> error){
        asyncTask(() -> {
            ChatRecord lastRecord = DbChatRecord.getChatLastRecord(chatRecord.his.uid);
            DbChatRecord.deleteOneChatRecord(chatRecord);
            if(lastRecord.messageSequence == chatRecord.messageSequence) {
                DbChatList.updateOnDeleteLastChatRecord(chatRecord.his.uid);
            }
            return "";
        }, callback, error);
    }

    public void uploadFile(String filePath, UploadUtils.UploadResponseCallback callback){
        if(null == filePath){
            return;
        }
        if(filePath.startsWith("http")){
            try {
                UploadResponse response = new UploadResponse();
                response.RetMsg = filePath;
                response.RetCode = "0";
                callback.onResponse(null, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        UploadUtils.uploadFile(filePath, callback);
    }

    public void uploadImageFileWithThumb(Context context, boolean isOriginal, String filePath,
                                         UploadUtils.UploadChatImageResponseCallback callback){
        if(null == filePath){
            return;
        }

        if(filePath.startsWith("http")){
            try {
                UploadChatImageResponse response = new UploadChatImageResponse();
                response.RetCode = "0";
                response.setFile1(filePath);
                response.setFile2(filePath);
                callback.onResponse(null, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        Disposable subscribe = Observable.fromCallable(() -> {
            List<File> files = ImageUtils.compressImageSync(context, filePath);
            return files;
        })
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(files -> {
                    if(null == files || files.size() < 2){
                        if(null != callback){
                            callback.onFailure(null, new NullPointerException());
                        }
                        return;
                    }
                    UploadUtils.uploadChatImage(files.get(0), files.get(1), new ProgressUIListener() {
                        public void onUIProgressChanged(long numBytes, long totalBytes, float percent, float speed) {
                            Debug.e("ChatModel", "--------percent: " + percent);
                        }
                    }, callback);
                });
    }

    public void updateVoiceReadState(long hisUid, long msgSeq){
        asyncTask(() -> {
            DbChatRecord.updateVoiceReadState(hisUid, msgSeq);
            return "";
        });
    }

    public void getGroupInfo(long gid, Consumer<Group> callback){
        asyncTask(() -> DbFriend.getGroupInfo(gid), callback);
    }

    public void getGroupInfoFromServer(long gid, Consumer<MsgOuterClass.Msg> callback){
        PBMessage pbMessage = new PBMessage()
                .setCmd(yiproto.yichat.group.Group.GroupYimCMD.GET_GROUP_INFO_REQ_CMD_VALUE)
                .setBody(yiproto.yichat.group.Group.GetGroupInfoReq.newBuilder()
                        .addGroupIdList(gid));
        Disposable subscribe = ApiManager.getInstance().post(new Request(pbMessage))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(callback, Throwable::printStackTrace);
    }

    @Override
    public void onDestroy() {

    }
}
