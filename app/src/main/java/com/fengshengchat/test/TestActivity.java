package com.fengshengchat.test;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.IPushAidlInterface;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.error.ErrorDialogHandler;
import com.fengshengchat.base.error.IErrorHandler;
import com.fengshengchat.base.utils.PermissionUtils;
import com.fengshengchat.base.utils.ToastUtils;
import com.fengshengchat.base.utils.Utilities;
import com.fengshengchat.push.YiPushService;
import com.fengshengchat.test.getcity.model.CityBean;
import com.fengshengchat.test.getcity.presenter.GetCityPresenter;
import com.fengshengchat.test.getcity.view.ICityView;
import com.fengshengchat.test.login.presenter.LoginPresenter;
import com.fengshengchat.test.login.view.ILoginView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/11/20
 */
public class TestActivity extends BaseActivity implements ICityView, ILoginView {
    private GetCityPresenter mGetCityPresenter;
    private TextView mCityTextView;

    private Disposable testPushObservable;
    private IPushAidlInterface pushInterface;
    private ServiceConnection serviceConn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            Debug.e("TestActivity", "---------service connected----------");
            pushInterface = IPushAidlInterface.Stub.asInterface(service);
//            try {
//                pushInterface.registerCallback(new IOnReceiveInterface.Stub() {
//                    public void onReceive(IPCMessage msg) {
//                        Debug.e("test", "-----Remote callback------ ");// + new String(msg.getData()));
////                        PBHandler.handleMessage(getContext(), BaseMessage.getMessage(msg.getBytes()));
//                    }
//                });
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
        }
        public void onServiceDisconnected(ComponentName name) {
            Debug.e("TestActivity", "---------service disconnected----------");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_layout);

        init();
        testPermission();
        testNet();
        testProtocol();
        testLongConnection();
    }

    @Override
    protected void onDestroy() {
        testPushObservable.dispose();
        unbindService(serviceConn);
        mGetCityPresenter.onDestroy();
        super.onDestroy();
    }

    private void testLongConnection(){
        Intent pushService = new Intent(getContext(), YiPushService.class);
        startService(pushService);
        bindService(pushService, serviceConn, Context.BIND_AUTO_CREATE);
        testPushObservable = Observable.interval(1, 3, TimeUnit.SECONDS)
                .subscribe(aLong -> {
//                    Message message = new Message();
//                    message.setData(message.mock());
//                    Debug.e("tet", "----------client send--------" + Thread.currentThread().getName());
//                    pushInterface.push(message);
                });
    }

    private TextView protocol;
    private LoginPresenter loginPresenter;
    public void setLogin(MsgOuterClass.LoginInfo loginInfo) {
        protocol.setText("Login success: " + loginInfo.getToken());
    }

    public void setLoginError(CharSequence error) {
        protocol.setText(error);
        onDataError(error);
    }

    private void testProtocol(){
        loginPresenter = new LoginPresenter(this);
        protocol = findViewById(R.id.test_protocol);
        protocol.setOnClickListener(v -> {
            showLoading("登录中...");
//            Request request = new Request();
//
//            MsgOuterClass.Msg.Builder messageBuilder = request.getDefaultMessageBuilder();
//
//            messageBuilder.setCmd(5);
//            messageBuilder.setSeq(1);
//            messageBuilder.setCode(1);
//            messageBuilder.setLoginInfo(MsgOuterClass.LoginInfo.newBuilder().build());
//            messageBuilder.setBody(ByteString.copyFromUtf8(Heartbeat.ConnPing.newBuilder().build().toString()));
//
//            loginPresenter.login(request);
        });
    }

    private void init(){
        Utilities.init(getContext());
    }

    private void testPermission(){
        findViewById(R.id.test).setOnClickListener(v -> {
            String[] p = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            requestPermission(p, granted -> {
                if (granted) {
                    ToastUtils.show("do some thing");
                } else {
                    startActivity(PermissionUtils.getAppDetailsSettingIntent(getContext()));
                }
            });
        });
    }

    private void testNet(){
        mGetCityPresenter = new GetCityPresenter(this);
        mCityTextView = findViewById(R.id.test_net);
        mCityTextView.setOnClickListener(v -> {
            showLoading();
            mGetCityPresenter.getCityInfo();
        });
    }

    public void setCity(CityBean cityBean) {
        mCityTextView.setText(cityBean.city);
    }

    public void setCityError(CharSequence error) {
        mCityTextView.setText(error);
        onNetError("没网");
    }

    @Override
    public void onHandle(CharSequence tip) {

    }

    @Override
    public void onCancel(CharSequence tip) {

    }

    @Override
    public void onUpdateViewFinally() {
        dismissLoading();
    }

    @Override
    public void clearErrorState() {
        resetErrorState();
    }

    @Override
    public Context getActivity() {
        return this;
    }

    @Override
    protected IErrorHandler getDefaultErrorHandler() {
        return new ErrorDialogHandler(this);
//        ErrorPageHandler errorPageHandler = new ErrorPageHandler(this, getRootView());
//        errorPageHandler.setBackgroundColor(0xCC000000);
//        errorPageHandler.setOnRetryClickListener(v -> {
//            showLoading();
//            Debug.d("test", "----------click");
//            mGetCityPresenter.getCityInfo();
//        });
//        return errorPageHandler;
    }

}
