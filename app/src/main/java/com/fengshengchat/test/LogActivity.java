package com.fengshengchat.test;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ScrollView;
import android.widget.TextView;

import com.blankj.utilcode.util.Utils;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.protocol.Protocol;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class LogActivity extends BaseActivity {

    private ScrollView scrollView;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scrollView = new ScrollView(this);
        int bgColor = 0xFF333333;
        scrollView.setBackgroundColor(bgColor);
        mTextView = new TextView(this);
        setNavigationBarColor(bgColor);
        mTextView.setTextColor(0xFF888888);
        mTextView.setTextSize(12);
        scrollView.addView(mTextView);
        setContentView(scrollView);
        init();

        String s = "没有加密";
        switch(Protocol.ENCRYPT_VERSION){
            case Protocol.ENCRYPT_VERSION_FIXED_KEY:
                s = "固定秘钥加密";
                break;
            case Protocol.ENCRYPT_VERSION_DH:
                s = "Diffie-Hellman密钥交换算法";
                break;
        }
        toast(Protocol.getBusinessUrl() + "\n加密模式: " + (s));

        new DataThread().start();

        test();
    }

    private void test(){
        String s = "123";
        printe("-----------要加密的字符串: " + s);
        byte[] encode = Protocol.encodeAESWithFixedKey(s.getBytes());
        printe("-----------加密后的字符串: " + new String(encode));
        byte[] decode = Protocol.decodeAESWithFixedKey(encode);
        printe("-----------解密后的字符串: " + new String(decode));
    }

    private String logPath;
    private static final ThreadLocal<SimpleDateFormat> SDF_THREAD_LOCAL = new ThreadLocal<>();
    private void init(){
        Date now = new Date(System.currentTimeMillis());
        String format = getSdf().format(now);
        String date = format.substring(0, 10);
        logPath = Utils.getApp().getExternalCacheDir()
                + System.getProperty("file.separator") + "log" + System.getProperty("file.separator")
                + "util" + "-" + date + ".txt";
    }

    private static SimpleDateFormat getSdf() {
        SimpleDateFormat simpleDateFormat = SDF_THREAD_LOCAL.get();
        if (simpleDateFormat == null) {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            SDF_THREAD_LOCAL.set(simpleDateFormat);
        }
        return simpleDateFormat;
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg)
        {
            try {
                mTextView.setText((String) msg.obj);
//                scrollView.post(() -> scrollView.fullScroll(ScrollView.FOCUS_DOWN));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    };

    class DataThread extends Thread{

        @Override
        public void run() {

            while(true)
            {
                try
                {
                    BufferedReader in = new BufferedReader(new FileReader(logPath));
                    String line;
                    StringBuilder sb = new StringBuilder();
                    while((line = in.readLine()) != null)
                    {
                        sb.append(line+'\n');
                    }
                    mHandler.sendMessage(mHandler.obtainMessage(0, sb.toString()));
                    in.close();

                    Thread.sleep(2000);
                } catch(Exception e){
//                    e.printStackTrace();
                }
            }
        }
    }
}
