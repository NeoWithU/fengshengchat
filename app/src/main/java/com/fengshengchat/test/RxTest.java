package com.fengshengchat.test;

import com.blankj.utilcode.util.LogUtils;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.net.BaseObserver;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class RxTest {
    private void load() {
        Observable.combineLatest(getAccountObservable(), getGroupObservable(), (aBoolean, aBoolean2) -> aBoolean && aBoolean2).compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean aBoolean) {
                        LogUtils.e("thread: " + Thread.currentThread() + ", result: " + aBoolean);
                    }
                });
    }

    public Observable<Boolean> getAccountObservable() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                LogUtils.e("AAA: " + Thread.currentThread());
                Thread.sleep(3000);
                LogUtils.e("AAA");
                return Boolean.TRUE;
            }
        }).onErrorReturnItem(Boolean.FALSE).subscribeOn(Schedulers.io());
    }


    public Observable<Boolean> getGroupObservable() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                LogUtils.e("BBB: " + Thread.currentThread());
                Thread.sleep(2000);
                LogUtils.e("BBB");
                if (true) {
                    int b = 1 / 0;
                }
                return Boolean.TRUE;
            }
        }).onErrorResumeNext(new Function<Throwable, ObservableSource<Boolean>>() {
            @Override
            public ObservableSource<Boolean> apply(Throwable throwable) throws Exception {
                LogUtils.e("出错错误");
                return Observable.just(Boolean.FALSE);
            }
        }).map(new Function<Boolean, Boolean>() {
            @Override
            public Boolean apply(Boolean aBoolean) throws Exception {
                LogUtils.e("BBB-map: " + Thread.currentThread());
                return Boolean.TRUE;
            }
        })
                .subscribeOn(Schedulers.io());
    }
}
