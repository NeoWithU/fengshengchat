package com.fengshengchat.test.login.view;

import com.fengshengchat.base.mvp.IBaseView;

import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/11/20
 */
public interface ILoginView extends IBaseView {
    void setLogin(MsgOuterClass.LoginInfo loginInfo);
    void setLoginError(CharSequence error);
}
