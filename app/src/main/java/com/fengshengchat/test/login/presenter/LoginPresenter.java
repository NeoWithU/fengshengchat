package com.fengshengchat.test.login.presenter;

import com.fengshengchat.base.mvp.IBasePresenter;
import com.fengshengchat.net.DefaultObserverCallback;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.net.bean.Response;
import com.fengshengchat.test.login.model.LoginModel;
import com.fengshengchat.test.login.view.ILoginView;

import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/11/20
 */
public class LoginPresenter implements IBasePresenter {
    private LoginModel mLoginModel = new LoginModel();
    private ILoginView mLoginView;

    public LoginPresenter(ILoginView loginView){
        mLoginView = loginView;
    }

    public void login(Request request){
        if(null == mLoginView){
            return;
        }

        mLoginModel.login(request, new DefaultObserverCallback<Response>() {
            public void onResponse(Response response) {
                MsgOuterClass.Msg message = response.getMessage();
                if(null != message){
                    try {
                        MsgOuterClass.LoginInfo loginInfo = MsgOuterClass.LoginInfo.parseFrom(message.getBody());
                        mLoginView.setLogin(loginInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            public void onError(Throwable e) {
                mLoginView.setLoginError(e.getMessage());
            }
            public void onComplete() {
                mLoginView.clearErrorState();
            }

            public void onFinally() {
                mLoginView.onUpdateViewFinally();
            }
        });
    }

    public void onDestroy() {
        if(null != mLoginModel){
            mLoginModel.onDestroy();
        }
        mLoginView = null;
        mLoginModel = null;
    }
}
