package com.fengshengchat.test.login.model;

import com.fengshengchat.net.bean.NetModel;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.net.bean.Response;

import io.reactivex.Observer;

/**
 * @author KRT
 * 2018/11/26
 */
public class LoginModel extends NetModel<ILogin> {

    public LoginModel(){
        super(ILogin.class);
    }

    public void login(Request request, Observer<Response> callback){
        request(getAPI().login(createJsonRequestBody(request)), callback);
    }

}
