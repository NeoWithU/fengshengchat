package com.fengshengchat.test;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.fengshengchat.R;
import com.fengshengchat.app.dialog.BottomDialogFragment;
import com.fengshengchat.app.dialog.ShareDialogFragment;
import com.fengshengchat.base.TipsDialogFragment;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
//import com.yichat.db.User;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.login.Login;
import yiproto.yichat.msg.MsgOuterClass;

public class TempActivity extends BaseActivity implements TipsDialogFragment.Callback {

    private EditText friendIdEt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        friendIdEt = findViewById(R.id.friend_id_et);

        findViewById(R.id.del_data_btn).setOnClickListener(v -> delData());
        findViewById(R.id.update_data_btn).setOnClickListener(v -> updateData());
        findViewById(R.id.add_btn).setOnClickListener(v -> add());
        findViewById(R.id.get_btn).setOnClickListener(v -> get());

        findViewById(R.id.submit_btn).setOnClickListener(v -> login("13510950000", "000000"));
        findViewById(R.id.add_friend_btn).setOnClickListener(v -> requestAddFriend());
        findViewById(R.id.get_friend_request_list_btn).setOnClickListener(v -> getFriendRequestList());
        findViewById(R.id.agree_add_friend_request_btn).setOnClickListener(v -> agreeAddFriendRequest(88893481008307L));
        findViewById(R.id.friend_list_btn).setOnClickListener(v -> getFriendList());
        findViewById(R.id.refuse_friend_req_btn).setOnClickListener(v -> refuseAddFriendRequest());
        findViewById(R.id.del_friend_req_btn).setOnClickListener(v -> delFriend());
        findViewById(R.id.show_dialog_tv).setOnClickListener(v -> showTestDialog());
        findViewById(R.id.show_bottom_dialog_tv).setOnClickListener(v -> showBottomDialog());
        findViewById(R.id.show_share_dialog_tv).setOnClickListener(v -> showShareDialog());
    }

    /**
     * 登录
     */
    public void login(String account, String password) {
//        Login.DeviceInfo.Builder deviceInfo = Login.DeviceInfo
//                .newBuilder()
//                .setMac(DeviceUtils.getMacAddress());
//
//        Login.AccountInfo.Builder accountInfo = Login.AccountInfo
//                .newBuilder()
//                .setAccount(account);
//
//        Login.LoginReq.Builder loginReq = Login.LoginReq.newBuilder()
//                .setPlatform(MsgOuterClass.ClientType.CLIENT_TYPE_ANDRIOD_VALUE)
//                .setPasswd(Protocol.encodePassword(password))
//                .setAccountInfo(accountInfo)
//                .setDeviceInfo(deviceInfo);
//
//        //  创建登录请求
//        Request request = new Request();
//        PBMessage pbMessage = new PBMessage();
//        pbMessage.setCmd(MsgOuterClass.YimCMD.LOGIN_REQ_CMD_VALUE);
//        pbMessage.setBody(loginReq);
//        request.setMessage(pbMessage.PB());
//
//        ApiManager.getInstance()
//                .post(request)
//                .map(message -> Login.LoginRsp.parseFrom(message.getBody()))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<Login.LoginRsp>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onNext(Login.LoginRsp data) {
//                        handleLoginRes(data);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        LogUtils.e(e);
//                    }
//
//                    @Override
//                    public void onComplete() {
//                    }
//                });
    }

    private void handleLoginRes(Login.LoginRsp data) {
        if (data.getResult() != Login.LoginRsp.LoginResult.OK_VALUE) {
            ToastUtils.showShort(data.getErrMsg());
            return;
        }

        LogUtils.e("uid: " + data.getUid());
        LogUtils.e("token: " + data.getToken());
        SPUtils.getInstance().put("uid", data.getUid());
        SPUtils.getInstance().put("token", data.getToken());
        ToastUtils.showShort("登录成功");
    }

    /**
     * 请求添加为好友
     */
    private void requestAddFriend() {
        String friendIdStr = friendIdEt.getText().toString();
        if (TextUtils.isEmpty(friendIdStr)) {
            ToastUtils.showShort("请输入好友ID");
            return;
        }

        long friendId = Long.parseLong(friendIdStr);
        byte[] body = Friend.AddFriendReq
                .newBuilder()
                .setTargetUserId(friendId)
                .build()
                .toByteArray();

        MsgOuterClass.LoginInfo loginInfo = MsgOuterClass.LoginInfo
                .newBuilder()
//                .setToken(SPUtils.getInstance().getString("token"))
                .setToken("4vIPLTy+SFXFdZgUKzdjzU/K2Pg182QE93NZnV3gH/Q=")
                .build();

        //  创建登录请求
        Request request = new Request();
        PBMessage pbMessage = new PBMessage();
//        MsgOuterClass.Msg msg = request.getDefaultMessageBuilder()
//                .setCmd(MsgOuterClass.YimCMD.ADD_FRIEND_REQ_CMD_VALUE)
//                .setLoginInfo(loginInfo)
//                .setUid(88893481008307L)
//                .setBody(Protocol.encodeBody(body))
//                .build();

        pbMessage.setCmd(Friend.FriendYimCMD.ADD_FRIEND_REQ_CMD_VALUE);
        pbMessage.setLoginInfoToken("4vIPLTy+SFXFdZgUKzdjzU/K2Pg182QE93NZnV3gH/Q=");
        pbMessage.setUid(88893481008307L);
        pbMessage.setBody(Friend.AddFriendReq
                .newBuilder()
                .setTargetUserId(friendId));
        request.setMessage(pbMessage.PB());

        ApiManager.getInstance()
                .post(request)
                .map(message -> Friend.AddFriendRsp.parseFrom(message.getBody()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Friend.AddFriendRsp>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Friend.AddFriendRsp data) {
                        handleAddFriendRes(data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void handleAddFriendRes(Friend.AddFriendRsp data) {
        if (data.getResult() != Friend.AddFriendRsp.AddFriendResult.OK_VALUE) {
            ToastUtils.showShort(data.getErrMsg());
            return;
        }

        ToastUtils.showShort("添加好友成功");
    }

    /**
     * 获取好友请求列表
     */
    private void getFriendRequestList() {
        byte[] body = Friend.GetFriendRequestListReq
                .newBuilder()
                .setStart(0)
                .setNum(10)
                .build()
                .toByteArray();

        MsgOuterClass.LoginInfo loginInfo = MsgOuterClass.LoginInfo
                .newBuilder()
                .setToken("NiAoY9UvJ3ilmTiRohp6ml4o4i898OoiB9fNKLndnL4=")
                .build();

        Request request = new Request();
        PBMessage pbMessage = new PBMessage();
//        MsgOuterClass.Msg msg = request.getDefaultMessageBuilder()
//                .setCmd(MsgOuterClass.YimCMD.GET_FRIEND_REQUEST_LIST_REQ_CMD_VALUE)
//                .setLoginInfo(loginInfo)
//                .setBody(Protocol.encodeBody(body))
//                .setUid(119160452853055L)
//                .build();

        pbMessage.setCmd(Friend.FriendYimCMD.GET_FRIEND_REQUEST_LIST_REQ_CMD_VALUE);
        pbMessage.setLoginInfoToken("NiAoY9UvJ3ilmTiRohp6ml4o4i898OoiB9fNKLndnL4=");
        pbMessage.setUid(119160452853055L);
        pbMessage.setBody(Friend.GetFriendRequestListReq
                .newBuilder()
                .setStart(0)
                .setNum(10));
        request.setMessage(pbMessage.PB());

        ApiManager.getInstance()
                .post(request)
                .map(message -> Friend.GetFriendRequestListRsp.parseFrom(message.getBody()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Friend.GetFriendRequestListRsp>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Friend.GetFriendRequestListRsp data) {
                        if (data.getResult() != Friend.GetFriendRequestListRsp.GetFriendRequestListResult.OK_VALUE) {
                            ToastUtils.showShort(data.getErrMsg());
                            return;
                        }

                        List<Friend.RequestUserInfo> requestListList = data.getRequestListList();
                        for (Friend.RequestUserInfo item : requestListList) {
                            LogUtils.e(item);
                        }

                        ToastUtils.showShort("size: " + requestListList.size());
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    /**
     * 同意好友请求
     */
    private void agreeAddFriendRequest(long targetUserId) {
        byte[] body = Friend.AgreeAddFriendReq
                .newBuilder()
                .setTargetUserId(targetUserId)
                .build()
                .toByteArray();

        MsgOuterClass.LoginInfo loginInfo = MsgOuterClass.LoginInfo
                .newBuilder()
                .setToken("NiAoY9UvJ3ilmTiRohp6ml4o4i898OoiB9fNKLndnL4=")
                .build();

        Request request = new Request();
        PBMessage pbMessage = new PBMessage();

//        MsgOuterClass.Msg msg = request.getDefaultMessageBuilder()
//                .setCmd(MsgOuterClass.YimCMD.AGREE_ADD_FRIEND_REQ_CMD_VALUE)
//                .setLoginInfo(loginInfo)
//                .setBody(Protocol.encodeBody(body))
//                .setUid(119160452853055L)
//                .build();

        pbMessage.setCmd(Friend.FriendYimCMD.AGREE_ADD_FRIEND_REQ_CMD_VALUE);
        pbMessage.setUid(119160452853055L);
        pbMessage.setBody(Friend.AgreeAddFriendReq
                .newBuilder()
                .setTargetUserId(targetUserId));
        pbMessage.setLoginInfoToken("NiAoY9UvJ3ilmTiRohp6ml4o4i898OoiB9fNKLndnL4=");
        request.setMessage(pbMessage.PB());

        ApiManager.getInstance()
                .post(request)
                .map(message -> Friend.AgreeAddFriendRsp.parseFrom(message.getBody()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Friend.AgreeAddFriendRsp>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Friend.AgreeAddFriendRsp data) {
                        if (data.getResult() != Friend.AgreeAddFriendRsp.AgreeAddFriendResult.OK_VALUE) {
                            ToastUtils.showShort(data.getErrMsg());
                            return;
                        }

                        ToastUtils.showShort("添加好友成功");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    /**
     * 获取好友列表
     */
    private void getFriendList() {
        byte[] body = Friend.GetFriendListReq
                .newBuilder()
                .setStart(0)
                .setNum(10)
                .build()
                .toByteArray();

        MsgOuterClass.LoginInfo loginInfo = MsgOuterClass.LoginInfo
                .newBuilder()
                .setToken("NiAoY9UvJ3ilmTiRohp6ml4o4i898OoiB9fNKLndnL4=")
                .build();

        Request request = new Request();
        PBMessage pbMessage = new PBMessage();

//        MsgOuterClass.Msg msg = request.getDefaultMessageBuilder()
//                .setCmd(MsgOuterClass.YimCMD.GET_FRIEND_LIST_REQ_CMD_VALUE)
//                .setLoginInfo(loginInfo)
//                .setBody(Protocol.encodeBody(body))
//                .setUid(119160452853055L)
//                .build();

        pbMessage.setCmd(Friend.FriendYimCMD.GET_FRIEND_LIST_REQ_CMD_VALUE);
        pbMessage.setUid(119160452853055L);
        pbMessage.setLoginInfoToken("NiAoY9UvJ3ilmTiRohp6ml4o4i898OoiB9fNKLndnL4=");
        pbMessage.setBody(Friend.GetFriendListReq
                .newBuilder()
                .setStart(0)
                .setNum(10));
        request.setMessage(pbMessage.PB());

        ApiManager.getInstance()
                .post(request)
                .map(message -> Friend.GetFriendListRsp.parseFrom(message.getBody()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Friend.GetFriendListRsp>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Friend.GetFriendListRsp data) {
                        for (Friend.FriendUserInfo item : data.getFriendListList()) {
                            LogUtils.e(item);
                        }

                        ToastUtils.showShort("好友数量：" + data.getFriendListCount());
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * 拒绝好友请求
     */
    private void refuseAddFriendRequest() {
        Friend.RefuseAddFriendReq.Builder body = Friend.RefuseAddFriendReq
                .newBuilder()
                .setTargetUserId(88893481008307L);

        PBMessage msg = new PBMessage()
                .setCmd(Friend.FriendYimCMD.REFUSE_ADD_FRIEND_REQ_CMD_VALUE)
                .setUid(195289993946013L)
                .setLoginInfoToken("v4SR7hpNMxMMnEPoCfJZ2Ec0jMCyRjwoaHLmxWvwguU=")
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(msg))
                .map(message -> Friend.RefuseAddFriendRsp.parseFrom(message.getBody()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Friend.RefuseAddFriendRsp>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Friend.RefuseAddFriendRsp data) {
                        if (data.getResult() != Friend.RefuseAddFriendRsp.RefuseAddFriendResult.OK_VALUE) {
                            ToastUtils.showShort(data.getErrMsg());
                            return;
                        }

                        ToastUtils.showShort("拒绝好友请求");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * 删除好友
     */
    private void delFriend() {
        Friend.DelFriendReq.Builder body = Friend.DelFriendReq
                .newBuilder()
                .setTargetUserId(88893481008307L);

        PBMessage msg = new PBMessage()
                .setCmd(Friend.FriendYimCMD.DEL_FRIEND_REQ_CMD_VALUE)
                .setUid(119160452853055L)
                .setLoginInfoToken("NiAoY9UvJ3ilmTiRohp6ml4o4i898OoiB9fNKLndnL4=")
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(msg))
                .map(message -> Friend.DelFriendRsp.parseFrom(message.getBody()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Friend.DelFriendRsp>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Friend.DelFriendRsp data) {
                        if (data.getResult() != Friend.DelFriendRsp.DelFriendResult.OK_VALUE) {
                            ToastUtils.showShort(data.getErrMsg());
                            return;
                        }
                        ToastUtils.showShort("删除好友成功");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    /**
     * 搜索好友
     */
    private void searchFriend() {
    }

    /**
     * 检查好友关系
     */
    private void checkFriend() {

    }

    public static final int REQ_CODE_CLEAR_DATA = 1;

    private void showTestDialog() {
        TipsDialogFragment dialogFragment = TipsDialogFragment.newInstance("确认退出群聊？退出之后群将自动解散。");
        dialogFragment.show(getSupportFragmentManager(), TipsDialogFragment.TAG);
    }

    @Override
    public void onClickButton(int whichButton) {
        ToastUtils.showShort("which button: " + whichButton);
    }

    public void showShareDialog() {
        ShareDialogFragment shareDialogFragment = ShareDialogFragment.newInstance();
        shareDialogFragment.show(getSupportFragmentManager(), ShareDialogFragment.TAG);
    }

    public void showBottomDialog() {
        String[] items = {
                "邀请他人入群",
                "管理员邀请",
                "所有人邀请",
        };
        BottomDialogFragment bottomDialogFragment = BottomDialogFragment.newInstance(items);
        bottomDialogFragment.show(getSupportFragmentManager(), BottomDialogFragment.TAG);
    }


    private void add() {
//        User user1 = new User();
//        user1.accountName = "aaa";
//        user1.nickName = "bbb";
//
//        User user2 = new User();
//        user2.accountName = "111";
//        user2.nickName = "222";

//        Observable.fromCallable(() -> DbManager.getDb().getUserDao().insertAll(user1, user2))
//                .compose(this.bindUntilEvent(ActivityEvent.DESTROY))
//                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
//                .subscribe(new BaseObserver<List<Long>>() {
//                    @Override
//                    public void onNext(List<Long> longs) {
//                        for (Long aLong : longs) {
//                            LogUtils.e(aLong);
//                        }
//                    }
//                });
    }

    private void get() {
//        DbManager.getDb().getUserDao()
//                .getAll()
//                .subscribeOn(Schedulers.io())
//                .observeOn(Schedulers.io())
//                .subscribe(new SingleObserver<List<User>>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                    }
//
//                    @Override
//                    public void onSuccess(List<User> users) {
//                        for (User user : users) {
//                            LogUtils.e(user);
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//                });
    }

    private void delData() {
//        User user = new User();
//        user.id = 1;

//        Observable.fromCallable(() -> DbManager.getDb().getUserDao().delete(user))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new BaseObserver<Integer>() {
//                    @Override
//                    public void onNext(Integer integer) {
//                        LogUtils.e("index: " + integer);
//                    }
//                });
    }

    private void updateData() {
//        User user = new User();
//        user.accountName = "p";
//        user.nickName = "j";
//        user.id = 3;

//        Observable.fromCallable(() -> DbManager.getDb().getUserDao().update(user))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new BaseObserver<Integer>() {
//                    @Override
//                    public void onNext(Integer integer) {
//                        LogUtils.e("index: " + integer);
//                    }
//                });
    }

}
