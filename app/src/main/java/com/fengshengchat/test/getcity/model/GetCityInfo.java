package com.fengshengchat.test.getcity.model;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * GetCityInfo
 */

public interface GetCityInfo {

    @GET("ipJson.jsp?json=true")
    Observable<CityBean> getCity();
}
