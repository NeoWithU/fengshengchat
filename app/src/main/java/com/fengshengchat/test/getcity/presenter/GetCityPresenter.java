package com.fengshengchat.test.getcity.presenter;

import com.fengshengchat.base.mvp.IBasePresenter;
import com.fengshengchat.net.DefaultObserverCallback;
import com.fengshengchat.test.getcity.model.CityBean;
import com.fengshengchat.test.getcity.model.GetCityModel;
import com.fengshengchat.test.getcity.view.ICityView;

/**
 * @author KRT
 * 2018/11/20
 */
public class GetCityPresenter implements IBasePresenter {
    private GetCityModel mGetCityModel = new GetCityModel();
    private ICityView mCityView;

    public GetCityPresenter(ICityView cityView){
        mCityView = cityView;
    }

    public void getCityInfo(){
        if(null == mCityView){
            return;
        }

        mGetCityModel.getCity(new DefaultObserverCallback<CityBean>() {
            public void onResponse(CityBean response) {
                mCityView.setCity(response);
            }
            public void onError(Throwable e) {
                mCityView.setCityError(e.getMessage());
            }
            public void onComplete() {
                mCityView.clearErrorState();
            }

            public void onFinally() {
                mCityView.onUpdateViewFinally();
            }
        });
    }

    public void onDestroy() {
        mCityView = null;
        mGetCityModel = null;
    }
}
