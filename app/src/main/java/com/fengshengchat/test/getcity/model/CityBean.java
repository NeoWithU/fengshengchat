package com.fengshengchat.test.getcity.model;

import java.io.Serializable;

/**
 * CityBean
 */

public class CityBean implements Serializable {
    public String ip;
    public String city;
}
