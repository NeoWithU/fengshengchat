package com.fengshengchat.test.getcity.model;

import com.fengshengchat.net.bean.NetModel;

import io.reactivex.Observer;

/**
 * @author KRT
 * 2018/11/20
 */
public class GetCityModel extends NetModel<GetCityInfo> {

    public GetCityModel(){
        super(GetCityInfo.class);
    }

    public void getCity(Observer<CityBean> callback){
        request(getAPI().getCity(), callback);
    }

}
