package com.fengshengchat.test.getcity.view;

import com.fengshengchat.base.mvp.IBaseView;
import com.fengshengchat.test.getcity.model.CityBean;

/**
 * @author KRT
 * 2018/11/20
 */
public interface ICityView extends IBaseView {
    void setCity(CityBean cityBean);
    void setCityError(CharSequence error);
}
