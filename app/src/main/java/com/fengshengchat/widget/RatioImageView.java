package com.fengshengchat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.fengshengchat.R;


/**
 * 等比 ImageView
 * <p>
 * Created by Jinjia on 16/10/20.
 */
public class RatioImageView extends AppCompatImageView {

    public RatioImageView(Context context) {
        super(context);
        init(context, null);
    }

    public RatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RatioImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private float mRatio = 1.0f;

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RatioImageView);
            mRatio = a.getFloat(R.styleable.RatioImageView_ratio, mRatio);
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = (int) (measuredWidth * mRatio);
        setMeasuredDimension(measuredWidth, measuredHeight);
    }
}
