package com.fengshengchat.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.fengshengchat.R;

/**
 * 可以清除所有内容的编辑框
 * <p/>
 * Created by Jinjia on 17/6/15.
 */
public class SearchEditText extends ClearEditText {

    public SearchEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getClearIconResId() {
        return R.drawable.common_ic_clear;
    }
}
