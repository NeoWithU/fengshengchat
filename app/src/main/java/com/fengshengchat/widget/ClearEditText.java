package com.fengshengchat.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.fengshengchat.R;

import static android.text.InputType.TYPE_CLASS_NUMBER;
import static android.text.InputType.TYPE_CLASS_TEXT;
import static android.text.InputType.TYPE_NUMBER_VARIATION_PASSWORD;
import static android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD;

/**
 * 可以清除所有内容的编辑框
 * <p/>
 * Created by Jinjia on 17/6/15.
 */
public class ClearEditText extends AppCompatEditText implements View.OnTouchListener,
        View.OnFocusChangeListener, TextWatcher {

    private Drawable mIconDrawable;
    private OnFocusChangeListener mOnFocusChangeListener;
    private OnTouchListener mOnTouchListener;

    public ClearEditText(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    public void setOnFocusChangeListener(final OnFocusChangeListener onFocusChangeListener) {
        mOnFocusChangeListener = onFocusChangeListener;
    }

    @Override
    public void setOnTouchListener(final OnTouchListener onTouchListener) {
        mOnTouchListener = onTouchListener;
    }

    private void init() {
        setIcon(getInitIconResId());
        setClearIconVisible(false);
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        addTextChangedListener(this);
    }

    private boolean mIsPasswordType;

    private int getInitIconResId() {
        int inputType = getInputType();
        mIsPasswordType = inputType == (TYPE_CLASS_NUMBER | TYPE_NUMBER_VARIATION_PASSWORD)
                || inputType == (TYPE_CLASS_TEXT | TYPE_TEXT_VARIATION_PASSWORD);
        return mIsPasswordType ? R.drawable.ic_et_password_invisiable : getClearIconResId();
    }

    private void setIcon(int resId) {
        mIconDrawable = ContextCompat.getDrawable(getContext(), resId);
        mIconDrawable.setBounds(0, 0, mIconDrawable.getIntrinsicWidth(), mIconDrawable.getIntrinsicHeight());
        final Drawable[] compoundDrawables = getCompoundDrawables();
        setCompoundDrawables(compoundDrawables[0], compoundDrawables[1], mIconDrawable, compoundDrawables[3]);
    }

    protected int getClearIconResId() {
        return R.drawable.ic_et_clear;
    }

    @Override
    public void onFocusChange(final View view, final boolean hasFocus) {
        if (hasFocus) {
            setClearIconVisible(getText().length() > 0);
        } else {
            setClearIconVisible(false);
        }

        if (mOnFocusChangeListener != null) {
            mOnFocusChangeListener.onFocusChange(view, hasFocus);
        }
    }

    private boolean mIsVisible;

    @Override
    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        final int x = (int) motionEvent.getX();
        if (mIconDrawable.isVisible()
                && x > getWidth() - getPaddingRight() - mIconDrawable.getIntrinsicWidth()) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (mIsPasswordType) {
                    mIsVisible = !mIsVisible;
                    if (mIsVisible) {
                        setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        setIcon(R.drawable.ic_et_password_visiable);
                    } else {
                        setTransformationMethod(PasswordTransformationMethod.getInstance());
                        setIcon(R.drawable.ic_et_password_invisiable);
                    }
                    setSelection(getText().length());
                } else {
                    setError(null);
                    setText(null);
                }
            }
            return true;
        }

        return mOnTouchListener != null && mOnTouchListener.onTouch(view, motionEvent);
    }

    @Override
    public final void onTextChanged(final CharSequence s, final int start, final int before,
                                    final int count) {
        if (isFocused()) {
            setClearIconVisible(s.length() > 0);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    /**
     * 设置清除图标是否可见
     *
     * @param visible 是否可见
     */
    private void setClearIconVisible(boolean visible) {
        mIconDrawable.setVisible(visible, false);
        Drawable[] compoundDrawables = getCompoundDrawables();
        setCompoundDrawables(compoundDrawables[0], compoundDrawables[1], visible ? mIconDrawable : null, compoundDrawables[3]);
    }
}
