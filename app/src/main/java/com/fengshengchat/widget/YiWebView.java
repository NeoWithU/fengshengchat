package com.fengshengchat.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.just.agentweb.AgentWeb;
import com.just.agentweb.AgentWebConfig;
import com.fengshengchat.R;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.UserManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;

public class YiWebView extends FrameLayout {
    private CookieManager mCookieManager = CookieManager.getInstance();
    private AgentWeb mAgentWeb;

    public YiWebView(Context context) {
        super(context);
    }

    public YiWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void start(Activity activity, String url){
        init(AgentWeb.with(activity), url);
    }

    public void start(Fragment fragment, String url){
        init(AgentWeb.with(fragment), url);
    }

    private void init(AgentWeb.AgentBuilder agentBuilder, String url){
        if(null == agentBuilder){
            throw new IllegalArgumentException("YiWebView: agentBuilder is null");
        }

        loadCookie(url);

        mAgentWeb = agentBuilder.setAgentWebParent(this, 0,
                    new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT))
                .useDefaultIndicator(getResources().getColor(R.color.colorPrimary))
                .setWebViewClient(new YiWebViewClient())
                .setMainFrameErrorView(initErrorView())
                .createAgentWeb()
                .ready()
                .go(url);
    }

    private ViewGroup errorViewRoot;
    private View initErrorView(){
        errorViewRoot = new FrameLayout(getContext());
        errorViewRoot.setBackgroundColor(0xFFFFFFFF);

        TextView errorView = new TextView(getContext());
        errorView.setText(R.string.web_error);
        errorView.setTextSize(13);
        errorView.setTextColor(0xFF666666);
        errorView.setGravity(Gravity.CENTER_HORIZONTAL);
        errorView.setCompoundDrawablePadding(dp2px(20));
        errorView.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.web_error, 0, 0);

        errorViewRoot.addView(errorView,
                new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER));
        return errorViewRoot;
    }

    public void showErrorView(boolean show){
//        errorViewRoot.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    protected int dp2px(int dp){
        return (int)(getResources().getDisplayMetrics().density * dp + 0.5f);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    public AgentWeb getAgentWeb(){
        return mAgentWeb;
    }

    public boolean handleKeyEvent(int keyCode, KeyEvent keyEvent) {
        return false;//return null != mAgentWeb && mAgentWeb.handleKeyEvent(keyCode, keyEvent);
    }

    public void clearCache(){
        if(null != mAgentWeb){
            mAgentWeb.clearWebCache();
        }
    }

    public void destroy(){
        if(null != mAgentWeb){
            mAgentWeb.clearWebCache();
            AgentWebConfig.clearDiskCache(getContext());
            mAgentWeb.destroy();
        }
    }

    public interface OnTitleListener{
        void onTitle(String title);
    }

    private String keyPrefix;
    private String getKeyPrefix(){
        if(null == keyPrefix){
            keyPrefix = "u" + UserManager.getUser().uid + "_g" + gid;
            printe("key: " + keyPrefix);
        }
        return keyPrefix;
    }
    private String generateKey(String url, String key){
        return getKeyPrefix() + "_" + url + "_" + key;
    }

    private void loadCookie(String url){
        try {
            String cookies = getCookie(url);
            mCookieManager.setAcceptCookie(true);
            mCookieManager.setCookie(url, cookies);
            mCookieManager.flush();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private String getNoName(File dir){
        File[] files = dir.listFiles();
        if(null == files){
            return "";
        }
        FileInputStream fileInputStream = null;
        byte[] buf = new byte[1024];
        int len = 0;
        StringBuilder stringBuilder = new StringBuilder();
        for(File f : files){
            if(f.isDirectory()){
                stringBuilder.append(getNoName(f));
            }
            try {
                fileInputStream = new FileInputStream(f);
                while ((len = fileInputStream.read(buf)) != -1) {
                    stringBuilder.append(f.getName());
                    stringBuilder.append("=");
                    stringBuilder.append(new String(buf, 0, len));
                }
                printe("get cookie : " + f.getName() + "  va: " + stringBuilder.toString());
                stringBuilder.append(", ");
            }catch(Exception e){
                e.printStackTrace();
            }finally {
                if(null != fileInputStream){
                    try {
                        fileInputStream.close();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
        return stringBuilder.toString();
    }

    private String getCookie(String url){
        String res = "";
        try {
            File cacheDir = getContext().getCacheDir();
//            String cacheDir = FileConstant.getFileExternalStorageFileAbsolutePath();
            File dir = new File(cacheDir, getKeyPrefix() + File.separator + new URL(url).getHost());
            if(!dir.exists()){
                return res;
            }
            String noName = getNoName(dir);
            printe("-----THIS: " + noName);
            return noName;
        }catch(Exception e){
            e.printStackTrace();
        }
        return res;
    }

    private void saveCookie(String url, String cookie){
        if(null == cookie || cookie.length() <= 0){
            return;
        }
        try {
            File cacheDir = getContext().getCacheDir();
//            String cacheDir = FileConstant.getFileExternalStorageFileAbsolutePath();
            File dir = new File(cacheDir, getKeyPrefix() + File.separator + new URL(url).getHost());
            dir.mkdirs();

            String[] split = cookie.split("; ");
            String[] temp;
            File f;
            for (String s : split) {
                temp = s.split("=");
                f = new File(dir, temp[0]);
                f.createNewFile();
                OutputStream outputStream = null;
                try {
                    printe("---save cookie key: " + temp[0]);
                    outputStream = new FileOutputStream(f);
                    outputStream.write(temp[1].getBytes());
                    outputStream.flush();
                }catch(Exception e){
                    e.printStackTrace();
                }finally {
                    if(null != outputStream){
                        try {
                            outputStream.close();
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private long gid;
    public void setGid(long gid){
        this.gid = gid;
    }

    private OnTitleListener mOnTitleListener;
    public void setOnTitleListener(OnTitleListener l){
        mOnTitleListener = l;
    }

    private class YiWebViewClient extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            printe("---request old : " + url);
            Protocol.requestSilentAuth(gid, url);
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            printe("---request new : " + url);
            Protocol.requestSilentAuth(gid, url);
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            String cookieStr = mCookieManager.getCookie(url);
            printe("--- onPageStarted url: " + url);
            printe("--- onPageStarted cookie: " + cookieStr);
            saveCookie(url, cookieStr);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            String cookieStr = mCookieManager.getCookie(url);
            printe("--- onPageFinished url: " + url);
            printe("--- onPageFinished cookie: " + cookieStr);
            saveCookie(url, cookieStr);
            if(null != mOnTitleListener){
                mOnTitleListener.onTitle(view.getTitle());
            }
        }

    }

    private void printe(String s){
        Debug.e("YiWebView", s);
    }

}
