package com.fengshengchat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fengshengchat.R;

public class ItemLayout extends RelativeLayout {

    private TextView titleTv;
    private TextView msgTv;
    private View dividerView;
    private SwitchCompat switchCompat;
    private EditText editText;

    public ItemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData(context, attrs);
        setBackgroundColor(Color.WHITE);
        initTitleTv();
        initWidget();
        initDivider();
    }

    public static final int TYPE_TEXT_VIEW = 1;
    public static final int TYPE_SWITCH = 2;
    public static final int TYPE_EDIT_TEXT = 3;

    private String title;
    private Drawable icon;

    private String msg;
    private String hintMsg;
    private int msgColor;
    private boolean showArrow;
    private Drawable msgIcon;

    private int widgetType;
    private boolean switchEnabled;
    private String editTextHint;

    private boolean showDivider;

    private void initData(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ItemLayout);
        title = a.getString(R.styleable.ItemLayout_title);
        icon = a.getDrawable(R.styleable.ItemLayout_icon);

        msg = a.getString(R.styleable.ItemLayout_msg);
        hintMsg = a.getString(R.styleable.ItemLayout_hintMsg);
        msgColor = a.getColor(R.styleable.ItemLayout_msgColor, Color.BLACK);
        showArrow = a.getBoolean(R.styleable.ItemLayout_showArrow, true);
        msgIcon = a.getDrawable(R.styleable.ItemLayout_msgIcon);

        widgetType = a.getInt(R.styleable.ItemLayout_widgetType, TYPE_TEXT_VIEW);
        switchEnabled = a.getBoolean(R.styleable.ItemLayout_switchEnabled, true);
        editTextHint = a.getString(R.styleable.ItemLayout_editTextHint);

        showDivider = a.getBoolean(R.styleable.ItemLayout_showDivider, true);
        a.recycle();
    }

    /**
     * 初始化 Title Text View
     */
    private void initTitleTv() {
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(dp2px(10), dp2px(15), dp2px(10), dp2px(15));

        TextView titleTv = new TextView(getContext());
        titleTv.setId(R.id.item_layout_title_tv);
        titleTv.setTextSize(15);
        titleTv.setText(title);
        titleTv.setTextColor(Color.parseColor("#212121"));
        titleTv.setCompoundDrawablePadding(dp2px(10));
        titleTv.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
        titleTv.setLayoutParams(params);
        addView(titleTv);
        this.titleTv = titleTv;
    }

    /**
     * 初始化 Msg TextView
     */
    private void initMsgTv() {
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.END_OF, R.id.item_layout_title_tv);
        params.setMargins(dp2px(8), dp2px(15), dp2px(10), dp2px(15));

        TextView msgTv = new TextView(getContext());
        msgTv.setId(R.id.item_layout_msg_tv);
        msgTv.setTextSize(15);
        msgTv.setText(msg);
        msgTv.setTextColor(msgColor);
        msgTv.setHint(hintMsg);
        msgTv.setSingleLine();
        msgTv.setEllipsize(TextUtils.TruncateAt.END);
        msgTv.setGravity(Gravity.END);
        msgTv.setHintTextColor(Color.parseColor("#999999"));
        msgTv.setCompoundDrawablePadding(dp2px(10));
        msgTv.setLayoutParams(params);

        if (showArrow) {
            if (msgIcon != null) {
                msgTv.setCompoundDrawablesWithIntrinsicBounds(null, null, msgIcon, null);
            } else {
                Drawable arrow = ContextCompat.getDrawable(getContext(), R.drawable.user_ic_right_arrow);
                msgTv.setCompoundDrawablesWithIntrinsicBounds(null, null, arrow, null);
            }
        }
        addView(msgTv);
        this.msgTv = msgTv;
    }

    public void setShowArrow(boolean showArrow) {
        if (showArrow) {
            Drawable arrow = ContextCompat.getDrawable(getContext(), R.drawable.user_ic_right_arrow);
            msgTv.setCompoundDrawablesWithIntrinsicBounds(null, null, arrow, null);
        } else {
            msgTv.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
    }

    /**
     * 初始化分割线
     */
    private void initDivider() {
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, 1);
        params.addRule(RelativeLayout.BELOW, R.id.item_layout_title_tv);
        params.setMargins(dp2px(10), 0, 0, 0);

        View divider = new View(getContext());
        divider.setId(R.id.item_divider);
        divider.setBackgroundColor(Color.parseColor("#dddddd"));
        divider.setVisibility(showDivider ? View.VISIBLE : View.GONE);
        divider.setLayoutParams(params);
        addView(divider);
        this.dividerView = divider;
    }

    /**
     * 初始化 Switch
     */
    private void initSwitch() {
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
        params.setMargins(0, 0, dp2px(6), 0);

        SwitchCompat switchCompat = new SwitchCompat(getContext());
        switchCompat.setId(R.id.item_switch);
        switchCompat.setLayoutParams(params);
        switchCompat.setEnabled(switchEnabled);
        addView(switchCompat);
        this.switchCompat = switchCompat;
    }

    /**
     * 初始化输入框
     */
    private void initEditText() {
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.END_OF, R.id.item_layout_title_tv);
        params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        params.setMargins(dp2px(15), 0, dp2px(15), 0);

        EditText editText = new EditText(getContext());
        editText.setGravity(Gravity.END);
        editText.setTextColor(Color.BLACK);
        editText.setHintTextColor(Color.parseColor("#999999"));
        editText.setTextSize(15);
        editText.setPadding(0, dp2px(15), 0, dp2px(15));
        editText.setHint(editTextHint);
        editText.setBackground(null);
        editText.setLayoutParams(params);
        editText.setSingleLine();
        addView(editText);
        this.editText = editText;
    }

    private void initWidget() {
        switch (widgetType) {
            case TYPE_TEXT_VIEW:
                initMsgTv();
                break;

            case TYPE_SWITCH:
                initSwitch();
                break;

            case TYPE_EDIT_TEXT:
                initEditText();
                break;

            default:
                break;
        }
    }

    private int dp2px(final float dpValue) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    @NonNull
    public TextView getTitleTv() {
        return titleTv;
    }

    public TextView getMsgTv() {
        return msgTv;
    }

    public View getDividerView() {
        return dividerView;
    }

    public SwitchCompat getSwitchCompat() {
        return switchCompat;
    }

    public EditText getEditText() {
        return editText;
    }
}
