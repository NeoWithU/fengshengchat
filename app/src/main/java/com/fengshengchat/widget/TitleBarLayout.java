package com.fengshengchat.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fengshengchat.R;

import static android.view.Gravity.CENTER;

/**
 * TitleBar Layout
 */
public class TitleBarLayout extends RelativeLayout {

    private static final int DEFAULT_BANK_ICON = R.drawable.title_bar_icon_back;
    private static final int DEFAULT_TEXT_COLOR = android.R.color.black;
    protected static final int DEFAULT_BACK_RES_ID = R.drawable.selector_action_title_click;

    protected boolean showHome = true;
    protected boolean homeBackClick = false;
    protected boolean showTitle = true;
    protected boolean showAction = false;
    protected boolean showActionImage = false;
    protected boolean showDivider = true;
    protected ColorStateList actionTextColor;

    protected String mTitle;
    protected String mActionText;

    private int iconHome = DEFAULT_BANK_ICON;
    private int iconActionImage = R.drawable.title_bar_icon_menu;
    private int backgroundColor = 0;
    protected int midTitleColor = DEFAULT_TEXT_COLOR;

    protected RelativeLayout mBackLayout;
    protected RelativeLayout mTitleLayout;
    protected RelativeLayout mActionLayout;
    protected RelativeLayout mActionImageLayout;

    protected TextView mTitleTextView;
    private TextView actionTv;
    private ImageView actionImage;

    private TitleBarListener mListener;

    public TitleBarListener getListener() {
        return mListener;
    }

    ViewClickListener viewClickListener = new ViewClickListener();

    public TitleBarLayout(Context context) {
        super(context);
        initType(context, null, 0, 0);
        initView();
    }

    public TitleBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initType(context, attrs, 0, 0);
        initView();
    }

    public TitleBarLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initType(context, attrs, defStyleAttr, 0);
        initView();
    }

    @SuppressLint("21")
    public TitleBarLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleRes);
        initType(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    protected void initType(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TitleBarLayout, defStyleAttr, defStyleRes);

        int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.TitleBarLayout_showHome:
                    showHome = a.getBoolean(attr, true);
                    break;
                case R.styleable.TitleBarLayout_showAction:
                    showAction = a.getBoolean(attr, false);
                    break;
                case R.styleable.TitleBarLayout_showActionImage:
                    showActionImage = a.getBoolean(attr, false);
                    break;
                case R.styleable.TitleBarLayout_iconHome:
                    iconHome = a.getResourceId(attr, DEFAULT_BANK_ICON);
                    break;
                case R.styleable.TitleBarLayout_iconActionImage:
                    iconActionImage = a.getResourceId(attr, DEFAULT_BANK_ICON);
                    break;
                case R.styleable.TitleBarLayout_backgroundColor:
                    backgroundColor = a.getResourceId(attr, 0);
                    break;
                case R.styleable.TitleBarLayout_titleText:
                    mTitle = a.getString(attr);
                    break;
                case R.styleable.TitleBarLayout_actionText:
                    mActionText = a.getString(attr);
                    break;
                case R.styleable.TitleBarLayout_homeBackClick:
                    homeBackClick = a.getBoolean(attr, false);
                    break;
                case R.styleable.TitleBarLayout_titleColor:
                    midTitleColor = a.getResourceId(attr, android.R.color.white);
                    break;
                case R.styleable.TitleBarLayout_showTitle:
                    showTitle = a.getBoolean(attr, true);
                    break;
                case R.styleable.TitleBarLayout_showDivider:
                    showDivider = a.getBoolean(attr, true);
                    break;
                case R.styleable.TitleBarLayout_actionTextColor:
                    actionTextColor = a.getColorStateList(attr);
                    break;
            }
        }
        a.recycle();
    }

    public void setShowHome(boolean showHome) {
        this.showHome = showHome;
        if (showHome) {
            if (null != mBackLayout && null == mBackLayout.getParent()) {
                addView(mBackLayout);
            }
        } else {
            if (null != mBackLayout) {
                removeView(mBackLayout);
            }
        }
        postInvalidate();
    }

    protected void initView() {
        setGravity(Gravity.CENTER_VERTICAL);
        if (showHome) {
            addView(addHomeBackView());
        }
        if (showActionImage) {
            addView(addActionImageView());
        }
        if (showAction) {
            addView(addActionView());
        }
        if (showTitle) {
            addView(addTitleView());
        }
        if (backgroundColor > 0) {
            setBackgroundResource(backgroundColor);
        } else {
            setBackgroundColor(Color.WHITE);
        }
        if (showDivider) {
            addView(addDividerView());
        }
    }

    private View addDividerView() {
        LayoutParams param = new LayoutParams(LayoutParams.MATCH_PARENT, 1);
        param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        View dividerView = new View(getContext());
        dividerView.setBackgroundColor(Color.parseColor("#DDDDDD"));
        dividerView.setLayoutParams(param);
        return dividerView;
    }

    @Override
    public void setAlpha(float alpha) {
        super.setAlpha(alpha);
    }

    public void setAlphaBack(float alpha) {
        if (alpha > 1.0f) {
            alpha = 1.0f;
        }
        getBackground().setAlpha((int) (alpha * 255));
        if (null != mActionLayout) {
            mActionLayout.setAlpha(alpha);
        }
        if (null != mActionImageLayout) {
            mActionImageLayout.setAlpha(alpha);
        }
        if (null != mTitleLayout) {
            mTitleLayout.setAlpha(alpha);
        }
    }

    private View addHomeBackView() {
        mBackLayout = new RelativeLayout(getContext());
        LayoutParams lp = new LayoutParams(dp2px(44), LayoutParams.MATCH_PARENT);
        lp.addRule(ALIGN_PARENT_LEFT, 1);
        mBackLayout.setId(R.id.titlebar_back);
        mBackLayout.setBackgroundResource(DEFAULT_BACK_RES_ID);
        mBackLayout.setLayoutParams(lp);
        mBackLayout.setOnClickListener(viewClickListener);
        ImageView back = new ImageView(getContext());
        LayoutParams param = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        back.setLayoutParams(param);
        back.setImageResource(iconHome);
        mBackLayout.addView(back);
        // mBackLayout.setVisibility(GONE);
        return mBackLayout;
    }

    private View addActionView() {
        mActionLayout = new RelativeLayout(getContext());
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        if (null == mActionImageLayout) {
            lp.addRule(ALIGN_PARENT_RIGHT);
        } else {
            lp.addRule(LEFT_OF, mActionImageLayout.getId());
        }
        mActionLayout.setId(R.id.titlebar_action);
        mActionLayout.setBackgroundResource(DEFAULT_BACK_RES_ID);
        mActionLayout.setLayoutParams(lp);
        if (showAction) {
            mActionLayout.setVisibility(VISIBLE);
        } else {
            mActionLayout.setVisibility(GONE);
        }
        mActionLayout.setOnClickListener(viewClickListener);
        actionTv = new TextView(getContext());
        actionTv.setCompoundDrawablePadding(dp2px(5));
        LayoutParams param = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        actionTv.setLayoutParams(param);
        actionTv.setPadding(dp2px(10), 0, dp2px(10), 0);
        actionTv.setSingleLine();
        actionTv.setTextSize(17);
        actionTv.setGravity(CENTER_IN_PARENT);
        if (actionTextColor == null) {
            actionTv.setTextColor(Color.BLACK);
        } else {
            actionTv.setTextColor(actionTextColor);
        }
        if (!TextUtils.isEmpty(mActionText)) {
            actionTv.setText(mActionText);
        }
        mActionLayout.addView(actionTv);

        return mActionLayout;
    }

    private View addActionImageView() {
        mActionImageLayout = new RelativeLayout(getContext());
        LayoutParams lp = new LayoutParams(dp2px(44), LayoutParams.MATCH_PARENT);
        lp.addRule(ALIGN_PARENT_RIGHT);
        mActionImageLayout.setId(R.id.titlebar_action_image);
        mActionImageLayout.setBackgroundResource(DEFAULT_BACK_RES_ID);
        mActionImageLayout.setLayoutParams(lp);
        if (showActionImage) {
            mActionImageLayout.setVisibility(VISIBLE);
        } else {
            mActionImageLayout.setVisibility(GONE);
        }
        mActionImageLayout.setOnClickListener(viewClickListener);
        actionImage = new ImageView(getContext());
        LayoutParams param = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        actionImage.setLayoutParams(param);
        actionImage.setImageResource(iconActionImage);
        mActionImageLayout.addView(actionImage);
        return mActionImageLayout;
    }

    protected View addTitleView() {
        mTitleLayout = new RelativeLayout(getContext());
        if (null == mActionImageLayout || mActionImageLayout.getVisibility() == GONE) {
            resetTitleBarTitleParams(false);
        } else {
            resetTitleBarTitleParams();
        }
        mTitleTextView = new TextView(getContext());
        mTitleTextView.setId(R.id.titlebar_title_text);
        LayoutParams param = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        mTitleTextView.setLayoutParams(param);
        mTitleTextView.setMaxWidth(dp2px(220));
        mTitleTextView.setTextSize(18);
        mTitleTextView.setSingleLine();
        mTitleTextView.setEllipsize(TextUtils.TruncateAt.MIDDLE);
        mTitleTextView.setMarqueeRepeatLimit(-1);
        mTitleTextView.setGravity(CENTER);
        mTitleTextView.setTextColor(getResources().getColor(midTitleColor));
        mTitleTextView.setText(mTitle);
        mTitleLayout.addView(mTitleTextView);

        return mTitleLayout;
    }

    public void setTitleText(String text) {
        if (null != mTitleTextView) {
            mTitleTextView.setText(text);
        }
    }

    public TextView getTitleTextView() {
        return mTitleTextView;
    }

    public void setTitleText(int resId) {
        if (null != mTitleTextView) {
            mTitleTextView.setText(getContext().getString(resId));
        }
    }

    public void showActionButton(boolean showAction) {
        if (null != mActionLayout) {
            if (showAction) {
                mActionLayout.setVisibility(View.VISIBLE);
            } else {
                mActionLayout.setVisibility(View.GONE);
            }
        }
    }

    public void setTitleDrawRight(int rightView) {
        if (null != mTitleLayout && mTitleTextView != null && rightView != 0) {
            mTitleTextView.setCompoundDrawablePadding(dp2px(5));
            Drawable drawable = getResources().getDrawable(rightView);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight()); //设置边界
            mTitleTextView.setCompoundDrawables(null, null, drawable, null);//画在右边
        }
    }

    public void setActionRight(int rightView) {
        if (null != mActionLayout && actionTv != null && rightView != 0) {
            Drawable drawable = getResources().getDrawable(rightView);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight()); //设置边界
            actionTv.setCompoundDrawables(null, null, drawable, null);//画在右边
        }
    }

    public void setActionLeft(int leftView) {
        if (null != mActionLayout && actionTv != null && leftView != 0) {
            Drawable drawable = getResources().getDrawable(leftView);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight()); //设置边界
            actionTv.setCompoundDrawables(drawable, null, null, null);//画在右边
        }
    }

    public void showActionImageButton(boolean showActionImage) {
        if (null != mActionImageLayout) {
            if (showActionImage) {
                mActionImageLayout.setVisibility(View.VISIBLE);
            } else {
                mActionImageLayout.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 右上角文字按钮设置
     */
    public void setActionText(String text) {
        if (showAction && null != actionTv) {
            actionTv.setText(text);
        }
    }

    /**
     * 右上角文字按钮设置
     */
    public void setActionText(int res) {
        if (showAction && null != actionTv) {
            actionTv.setText(getContext().getString(res));
        }
    }

    /**
     * 右上角图片按钮
     */
    public void setActionImage(int res) {
        if (showAction && null != actionTv) {
            actionImage.setImageResource(iconActionImage);
        }
    }

    public ImageView getActionImage() {
        return actionImage;
    }

    private class ViewClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            if (v == mBackLayout) {
                if (!homeBackClick) {
                    if (mListener != null) {
                        mListener.onBackClick();
                    }
                } else {
                    ((Activity) getContext()).onBackPressed();
                }
            } else if (v == mActionLayout) {
                if (mListener != null && actionTv.isEnabled()) {
                    mListener.onActionClick();
                }
            } else if (v == mActionImageLayout) {
                if (mListener != null) {
                    mListener.onActionImageClick();
                }
            }

        }
    }

    public void setTitleBarListener(TitleBarListener l) {
        mListener = l;
    }

    //显示返回按钮
    public void showTitleBarHomeIcon() {
        if (null != mBackLayout) {
            mBackLayout.setVisibility(VISIBLE);
        }
    }

    //隐藏返回按钮
    public void hideTitleBarHomeIcon() {
        if (null != mBackLayout) {
            mBackLayout.setVisibility(GONE);
        }
    }

    /**
     * 关闭按钮是否显示时，调整Title的宽度
     */
    protected void resetTitleBarTitleParams() {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        int margin;
        if ((null == mActionImageLayout || mActionImageLayout.getVisibility() == GONE) && (null == mActionLayout || mActionLayout.getVisibility() == GONE)) {
            margin = 44;
        } else {
            margin = 88;
        }
        lp.setMargins(dp2px(margin), 0, dp2px(margin), 0);
        mTitleLayout.setLayoutParams(lp);
    }

    /**
     * 强制设置Title的宽度，当返回键不显示时，左边最小的间隔为44dp
     */
    protected void resetTitleBarTitleParams(boolean b) {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        int margin = b ? 88 : 44;
        lp.setMargins(dp2px(margin), 0, dp2px(margin), 0);
        mTitleLayout.setLayoutParams(lp);
    }

    public interface TitleBarListener {
        void onBackClick();

        void onActionImageClick();

        void onActionClick();
    }

    private int dp2px(final float dpValue) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public void setHomeBackClick(boolean homeBackClick) {
        this.homeBackClick = homeBackClick;
    }

    public TextView getActionTextView() {
        return actionTv;
    }

    public RelativeLayout getActionLayout() {
        return mActionLayout;
    }
}
