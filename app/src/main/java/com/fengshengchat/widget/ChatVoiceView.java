package com.fengshengchat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fengshengchat.R;

public class ChatVoiceView extends LinearLayout {
    private TextView mDuration;
    private AnimationDrawable mVoiceIconAnimator;

    public ChatVoiceView(Context context) {
        super(context);
        init(context, R.layout.chat_voice_send_layout);
    }

    public ChatVoiceView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        int layoutRes = R.layout.chat_voice_send_layout;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ChatVoiceView);
        boolean isSend = typedArray.getBoolean(R.styleable.ChatVoiceView_isSend, true);
        typedArray.recycle();
        if(!isSend){
            layoutRes = R.layout.chat_voice_receive_layout;
        }
        init(context, layoutRes);
    }

    private void init(Context context, @LayoutRes int layoutRes){
        inflate(context, layoutRes, this);
        mDuration = findViewById(R.id.duration);
        ImageView voiceIcon = findViewById(R.id.voice_icon);
        Drawable drawable = voiceIcon.getDrawable();
        if(drawable instanceof AnimationDrawable){
            mVoiceIconAnimator = (AnimationDrawable) drawable;
        }
    }

    public void setReceiveMode(){
        init(getContext(), R.layout.chat_voice_receive_layout);
    }

    public void setDuration(int duration){
        if(duration < 0){
            duration = 0;
        }
        mDuration.setText(duration + "''");
        updateWidth(duration);
    }

    public void startVoiceAnimation(){
        if(null != mVoiceIconAnimator){
            mVoiceIconAnimator.start();
        }
    }

    public void stopVoiceAnimation(){
        if(null != mVoiceIconAnimator){
            mVoiceIconAnimator.stop();
        }
    }

    public void voiceAnimation(boolean start){
        if(start){
            startVoiceAnimation();
        }else{
            stopVoiceAnimation();
        }
    }

    private void updateWidth(int duration){
        final float r = 160F / 30; // 160dp / 30s;
        int widthDp = (int)(duration * r);
        if(widthDp < 20){
            widthDp = 20;
        }else if(widthDp > 160){
            widthDp = 160;
        }
        mDuration.setWidth(dp2px(widthDp));
    }

    private int dp2px(int dp){
        return (int)(getResources().getDisplayMetrics().density * dp);
    }
}
