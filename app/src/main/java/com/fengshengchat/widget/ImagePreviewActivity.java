package com.fengshengchat.widget;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.fengshengchat.R;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.base.utils.NetUtils;
import com.fengshengchat.base.utils.PermissionUtils;
import com.fengshengchat.file.FileConstant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import me.panpf.sketch.SketchImageView;

public class ImagePreviewActivity extends BaseActivity {
    private SketchImageView imageView;
    private ImageView defaultImage;
    private View progress;
    private View downloadBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        setContentView(R.layout.image_preview_layout);

        imageFile = null;
        defaultImage = findViewById(R.id.default_icon);
        downloadBtn = findViewById(R.id.download_icon);
        downloadBtn.setOnClickListener(v -> {
            requestPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, granted -> {
                if(granted){
                    if(null != imageFile){
                        try {
                            Observable.fromCallable((Callable<File>) () -> {
                                String fileName = "jike_" + System.currentTimeMillis() + "_pic.jpg";
                                File dest = new File(FileConstant.getFileExternalStorageImageAbsolutePath(), fileName);
                                copy(imageFile, dest);
                                return dest;
                            }).compose(RxSchedulersHelper.applyIO2MainSchedulers())
                                    .subscribe(dest -> {
                                        if(dest != null) {
                                            String tip = getString(R.string.save_file_to) + dest.getParent();
                                            Toast.makeText(getContext(), tip, Toast.LENGTH_LONG).show();
                                            downloadBtn.setVisibility(View.GONE);

                                            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                            Uri uri = Uri.fromFile(dest);
                                            intent.setData(uri);
                                            sendBroadcast(intent);
                                        }
                                    }, throwable -> {

                                    });
                        }catch(Exception e){
                            e.printStackTrace();
                            toast(R.string.error);
                        }
                    }
                }else{
                    PermissionUtils.tipPermissionDenied(ImagePreviewActivity.this, getString(R.string.read_write_sd_permission));
                }
            });
        });
        imageView = findViewById(R.id.intensify_image);
        imageView.setZoomEnabled(true);
        imageView.setOnClickListener(v -> {
            finishAfterTransition();
        });
        progress = findViewById(R.id.progress);

        init();
    }

    private void init(){
        Intent intent = getIntent();
        String imagePath = intent.getStringExtra("imagePath");
        String thumbImagePath = intent.getStringExtra("thumbImagePath");
        if(null == imagePath || "".equals(imagePath)){
            finish();
            overridePendingTransition(R.anim.enter_anim, R.anim.exit_anim);
            printe("imagePath is null");
            return;
        }

        File cacheFile = ImageUtils.getCacheFile(getContext(), thumbImagePath);
        if(null != cacheFile && cacheFile.exists() && cacheFile.isFile()){
            imageView.setImageURI(Uri.fromFile(cacheFile));
            runDelay(() -> previewImage(imagePath), 500);
        }else{
            previewImage(imagePath);
        }
    }

    private void previewImage(String imagePath){
        File cacheFile = ImageUtils.getCacheFile(getContext(), imagePath);
        if(null != cacheFile && cacheFile.exists() && cacheFile.isFile()){
            imageFile = cacheFile;
            try {
                Bitmap c = c(cacheFile.getAbsolutePath());
                if (null != c) {
                    imageView.setImageBitmap(c);
                } else {
                    imageView.setImageURI(Uri.fromFile(cacheFile));
                }
            }catch(Exception e) {
                imageView.setImageURI(Uri.fromFile(cacheFile));
            }
            downloadBtn.setVisibility(View.VISIBLE);
            return;
        }

        progress.setVisibility(View.VISIBLE);
        Disposable subscribe = Observable.fromCallable(() -> ImageUtils.downloadImage(getContext(), imagePath))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(file -> {
                    imageFile = file;
                    progress.setVisibility(View.GONE);
                    try {
                        Bitmap c = c(file.getAbsolutePath());
                        if(null != c) {
                            imageView.setImageBitmap(c);
                        }else{
                            imageView.setImageURI(Uri.fromFile(file));
                        }
                    }catch(Exception e){
                        loadImage(defaultImage, file.getAbsolutePath(), R.mipmap.chat_img_default);
                    }
                    downloadBtn.setVisibility(View.VISIBLE);
                }, throwable -> {
                    progress.setVisibility(View.GONE);
                    downloadBtn.setVisibility(View.GONE);
//                    imageView.setVisibility(View.INVISIBLE);
//                    defaultImage.setVisibility(View.VISIBLE);
//                    defaultImage.setOnClickListener(v -> finish());
                    toast(NetUtils.isConnected() ? R.string.error : R.string.no_net);
                });
    }

    private Bitmap c(String path){
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
            int degree = 0;
            if(ExifInterface.ORIENTATION_ROTATE_90 == orientation){
                degree = 90;
            }else if(ExifInterface.ORIENTATION_ROTATE_180 == orientation){
                degree = 180;
            }else if(ExifInterface.ORIENTATION_ROTATE_270 == orientation){
                degree = 270;
            }
            if(0 != degree){
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                Matrix matrix = new Matrix();
                matrix.postRotate(degree);
                return Bitmap.createBitmap(
                        bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private File imageFile;
    public void copy(File source, File target) {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(source);
            fileOutputStream = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fileInputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finishAfterTransition();
    }
}
