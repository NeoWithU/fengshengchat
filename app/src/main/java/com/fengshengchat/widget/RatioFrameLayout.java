package com.fengshengchat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.fengshengchat.R;


/**
 * 等比缩放的 Frame Layout
 * <p>
 * Created by Jinjia on 16/10/21.
 */
public class RatioFrameLayout extends FrameLayout {

    public static final float DEFAULT_RATIO = 1.0f;

    public RatioFrameLayout(Context context) {
        super(context);
        init(context, null);
    }

    public RatioFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RatioFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    protected float mRatio;

    public void setRatio(float ratio) {
        mRatio = ratio;
        requestLayout();
    }

    protected void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RatioFrameLayout);
            mRatio = a.getFloat(R.styleable.RatioFrameLayout_ratio, DEFAULT_RATIO);
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = (int) (measuredWidth * mRatio);

        int parentWidthMeasureSpec = MeasureSpec.makeMeasureSpec(measuredWidth, MeasureSpec.EXACTLY);
        int parentHeightMeasureSpec = MeasureSpec.makeMeasureSpec(measuredHeight, MeasureSpec.EXACTLY);
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                measureChildWithMargins(child, parentWidthMeasureSpec, 0, parentHeightMeasureSpec, 0);
            }
        }

        setMeasuredDimension(measuredWidth, measuredHeight);
    }

}
