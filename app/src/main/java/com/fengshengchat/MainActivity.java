package com.fengshengchat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.fengshengchat.account.activity.AccountCompleteActivity;
import com.fengshengchat.account.activity.LoginActivity;
import com.fengshengchat.base.BaseActivity;
import com.fengshengchat.base.BaseFragment;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.HttpRequestObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.mvp.IBasePresenter;
import com.fengshengchat.base.utils.DeviceUtils;
import com.fengshengchat.chat.model.ChatListTabEntity;
import com.fengshengchat.chat.view.ChatListFragment;
import com.fengshengchat.contacts.model.ContactsListTabEntity;
import com.fengshengchat.contacts.view.MainFriendFragment;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.helper.DataHelper;
import com.fengshengchat.mine.bean.MineTabEntity;
import com.fengshengchat.mine.view.MineFragment;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.push.HandlePushService;
import com.fengshengchat.push.NotifyUtils;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.utils.CheckUpdateUtils;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.info.Info;
import yiproto.yichat.msg.MsgOuterClass;

public class MainActivity extends BaseActivity<IBasePresenter> {
    private CommonTabLayout tabLayout;

    public static void toMainActivity(Activity activity) {
        if (null == activity)
            return;
        Intent intent = new Intent(activity, MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.enter_anim, R.anim.exit_anim);
        activity.finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean fromNotify = intent.getBooleanExtra("fromNotify", false);
        if(fromNotify){
            long uid = intent.getLongExtra("uid", 0);
            if(uid > 0 && null != mChatListFragment){
                mChatListFragment.startChat(uid);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentForWindow(this);
        setContentView(R.layout.activity_main);

        init();
        NotifyUtils.init(this);
        super.addPushObserver();

        checkUserIcon();
//        loadBasicData(); //Called by ChatListFragment when history ready
    }

    @Override
    protected void addPushObserver() {
//        super.addPushObserver();
    }

    @Override
    protected void removePushObserver() {
//        super.removePushObserver();
    }

    private void checkUserIcon() {
        String icon = UserManager.getUser().icon;
        if (null == icon || icon.length() <= 0) {
            Info.GetUserInfoReq.Builder body = Info.GetUserInfoReq
                    .newBuilder()
                    .addUserIdList(UserManager.getUser().uid);

            PBMessage pbMessage = new PBMessage()
                    .setCmd(Info.InfoYimCMD.GET_USER_INFO_REQ_CMD_VALUE)
                    .setBody(body);

            ApiManager.getInstance()
                    .post(new Request(pbMessage))
                    .map(msg -> Info.GetUserInfoRsp.parseFrom(msg.getBody()))
                    .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                    .subscribe(new HttpRequestDialogObserver<Info.GetUserInfoRsp>(getSupportFragmentManager()) {
                        @Override
                        public void onNext(Info.GetUserInfoRsp data) {
                            bindUserInfo(data);
                        }
                    });
        }
    }

    private void bindUserInfo(Info.GetUserInfoRsp data) {
        if (data.getResult() != Info.GetUserInfoRsp.GetUserInfoResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        if (data.getUserInfoListCount() == 0) {
            return;
        }

        Info.UserInfoDetail detail = data.getUserInfoList(0);
        String nickname = detail.getNickname();
        String smallAvatar = detail.getSmallAvatar();
        String bigAvatar = detail.getBigAvatar();
        UserManager.save(nickname, smallAvatar, bigAvatar);
    }

    private boolean checkAccountInfo() {
        boolean accountSetComplete = SPUtils.getInstance().getBoolean("accountSetComplete", false);
        if (!accountSetComplete) {
            startActivityForResult(new Intent(getContext(), AccountCompleteActivity.class), REQUEST_CODE);
        }
        return accountSetComplete;
    }

    private static final int REQUEST_CODE = 102;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                SPUtils.getInstance().put("accountSetComplete", true);
            } else {
                finish();
            }
        } else if (requestCode == IntentIntegrator.REQUEST_CODE) {
            mChatListFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        printe("-------onResume-------");
        NotifyUtils.clearNotify();
    }

    private void init() {
        initView();
    }

    @Override
    protected void onPusherReady() {
        super.onPusherReady();
        setChatListTitle(STATE_CONNECTING);
        PBMessage.initDeviceInfo(DeviceUtils.getDeviceInfo(getContext()));
        startSocket();
      //  checkAccountInfo();
    }

    private void startSocket() {
        HandlePushService.HandlePushServiceInterface pusher = getPusher();
        if (!pusher.isRunning()) {
            User user = UserManager.getUser();
            PBMessage pbMessage = new PBMessage();
            pbMessage.setUid(user.uid)
                    .setLoginInfoToken(user.token)
                    .setCmd(MsgOuterClass.YimCMD.VERIFY_CONN_REQ_CMD_VALUE);
            getPusher().start(pbMessage);
        } else {
            setChatListTitle(STATE_OK);
        }
    }

    private boolean handleInternalCmd(MsgOuterClass.Msg msg) {
        if (Constant.INTERNAL_CMD_TYPE != msg.getBigCmd()) {
            return false;
        }

        printi("---handle <internal> msg: " + msg.getCmd());
        switch (msg.getCmd()) {
            case Constant.INTERNAL_CMD_SOCKET_CONNECT_SUCCESS:
                setChatListTitle(STATE_OK);
                new CheckUpdateUtils().checkUpdate(this);
                return true;
            case Constant.INTERNAL_CMD_SOCKET_CONNECTING:
                setChatListTitle(STATE_CONNECTING);
                return true;
            case Constant.INTERNAL_CMD_SOCKET_CONNECT_FALIED:
                setChatListTitle(STATE_CONNECT_FAILED);
                return true;
        }
        return false;
    }

    @Override
    protected void onReceiveMessage(MsgOuterClass.Msg msg) {
        printi("---handle msg: " + msg.getCmd() + " code: " + msg.getCode());
        if (handleInternalCmd(msg)) {
            return;
        }

        switch (msg.getCmd()) {
            case MsgOuterClass.YimCMD.VERIFY_CONN_RSP_CMD_VALUE:
                if (BaseHandler.isNoError(msg.getCode())
                        || MsgOuterClass.YiChatErrCode.YICHAT_VERIFY_CONN_REPEAT_VALUE == msg.getCode()) {
                    return;
                } else if (MsgOuterClass.YiChatErrCode.YICHAT_VERIFY_CONN_ERROR_VALUE == msg.getCode() //100 : token invalid
                        || MsgOuterClass.YiChatErrCode.YICHAT_VERIFY_CONN_TOKEN_LOGIN_REPLACE_ERROR_VALUE == msg.getCode()) { //token replace means kick out
                    User user = UserManager.getUser();
                    user.uid = 0;
                    user.token = null;
                    user.expireTime = 0;
                    UserManager.clearUser();
                    LoginActivity.toLogin(this, msg.getErrMsg());
                    return;
                }
                break;

            case Friend.FriendYimCMD.NOTIFY_UPDATE_REQ_CMD_VALUE:
                if(BaseHandler.isNoError(msg.getCode())){
                    try {
                        Friend.NotifyUpdateReq notifyUpdateReq = Friend.NotifyUpdateReq.parseFrom(msg.getBody());
                        Friend.NotifyUpdateReq.FriendGreetingSystemMsg greetingMsg = notifyUpdateReq.getGreetingMsg();
                        if(0 == greetingMsg.getRequestUserId() || 0 == greetingMsg.getAgreeUserId()){
                            updateTabRedDot(1, true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }

        for (Fragment fragment : mFragmentList) {
            if (fragment instanceof BaseFragment) {
                if (((BaseFragment) fragment).handleMessage(msg))
                    break;
            }
        }
    }

    private ArrayList<CustomTabEntity> getTabs() {
        ArrayList<CustomTabEntity> list = new ArrayList<>();
        list.add(new ChatListTabEntity(getString(R.string.chat_list_tab)));
        list.add(new ContactsListTabEntity(getString(R.string.friend)));
        list.add(new MineTabEntity(getString(R.string.mine_tab)));
        return list;
    }

    private ChatListFragment mChatListFragment;
    private final ArrayList<Fragment> mFragmentList = new ArrayList<>();

    private ArrayList<Fragment> getFragments() {
        mChatListFragment = new ChatListFragment();
        mFragmentList.add(mChatListFragment);
        mFragmentList.add(new MainFriendFragment());
        mFragmentList.add(new MineFragment());
        return mFragmentList;
    }

    public static final int STATE_OK = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_CONNECT_FAILED = 3;

    private void setChatListTitle(int state) {
        if (STATE_OK == state) {
            User user = UserManager.getUser();
            if (null != user) {
                uploadJPushToken(user.uid);
            }
        }
        mChatListFragment.onSocketConnectStateChange(state);
    }

    private void uploadJPushToken(long uid) {
//        try {
//            String registrationID = JPushInterface.getRegistrationID(getContext());
//            printe("--- JPush registrationID: " + registrationID);
//            PBMessage pbMessage = new PBMessage();
//            pbMessage.setCmd(Login.LoginYimCMD.SET_REPORT_INFO_REQ_CMD_VALUE);
//            pbMessage.setBody(Login.SetReportInfoReq.newBuilder()
//                    .setReportInfo(Login.ReportInfo.newBuilder()
//                            .setPushToken(registrationID)
//                            .setUserId(uid)));
//
//            ApiManager.getInstance()
//                    .post(new Request(pbMessage))
//                    .map(message -> Login.SetReportInfoRsp.parseFrom(message.getBody()))
//                    .compose(RxSchedulersHelper.applyIO2MainSchedulers())
//                    .subscribe(new HttpRequestDialogObserver<Login.SetReportInfoRsp>(getSupportFragmentManager()) {
//                        public void onNext(Login.SetReportInfoRsp data) {
//                            if (data.getResult() != Login.SetReportInfoRsp.ReportInfoResult.OK_VALUE) {
//                                printe("----upload JPush failed----");
//                            } else {
//                                printe("----upload JPush OK----");
//                            }
//                        }
//
//                        protected void onError(String msg) {
//                            super.onError(msg);
//                        }
//                    });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void updateTabRedDot(int pos, boolean show) {
        if (show) {
            tabLayout.showDot(pos);
        } else {
            tabLayout.hideMsg(pos);
        }
    }

    private void initView() {
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setTabData(getTabs(), this, R.id.fragment_container, getFragments());
//        tabLayout.setOnTabSelectListener(new OnTabSelectListener() {
//            public void onTabSelect(int position) {
//                if (position > 0) {
//                    printe("------------需要真实看到新内容才消失------------");
//                    updateTabRedDot(position, false);
//                }
//            }
//
//            public void onTabReselect(int position) {
//            }
//        });
    }

    public void updateNewMessageCount(int count) {
        if (count <= 0) {
            tabLayout.hideMsg(0);
        } else {
            tabLayout.showMsg(0, count);
        }
    }

    @Override
    protected void onDestroy() {
        super.removePushObserver();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }

    @Override
    protected IBasePresenter createPresenter() {
        return null;
    }

    public void loadBasicData() {
        DataHelper.getBaseDataObservable()
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(ActivityEvent.DESTROY))
                .subscribe(getDataObservable());
    }

    public static final String KEY_FIRST_ENTER = "first_enter_";

    @NonNull
    private HttpRequestObserver<Boolean> getDataObservable() {
        long uid = UserManager.getUser().uid;
        if (SPUtils.getInstance().getBoolean(KEY_FIRST_ENTER + uid, true)) {
            return new HttpRequestDialogObserver<Boolean>(getSupportFragmentManager()) {
                @Override
                public void onSubscribe(Disposable d) {
                    super.onSubscribe(d);
                }

                @Override
                public void onNext(Boolean aBoolean) {
                    loadCompleted();
                    SPUtils.getInstance().put(KEY_FIRST_ENTER + uid, false);
                }
            };
        } else {
            return new HttpRequestObserver<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {
                    loadCompleted();
                }
            };
        }
    }

    private void loadCompleted() {
        LogUtils.e("基础数据加载成功");
        if (null != mChatListFragment) {
            mChatListFragment.onBaseInfoRefreshOK();
        }
    }
}
