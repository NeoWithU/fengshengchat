package com.fengshengchat.app.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.app.model.SearchFriend;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;

public class SearchFriendViewBinder extends BaseItemViewBinder<SearchFriend,
        SearchFriendViewBinder.ViewHolder> implements View.OnClickListener {

    public SearchFriendViewBinder(@NonNull OnClickListener<SearchFriend> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.search_friend_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull SearchFriend item) {
        holder.itemView.setOnClickListener(this);
        holder.itemView.setTag(item);
      //  ImageUtils.loadRadiusImage(holder.avatarIv, item.getFriend().avatar, R.drawable.image_default, AppConfig.IMAGE_RADIUS);
        ImageUtils.loadRadiusImage(holder.avatarIv, item.getFriend().avatar, R.mipmap.default_head_icon, AppConfig.IMAGE_RADIUS);

        bindNickname(holder, item);
        bindId(holder, item);
    }

    private void bindId(@NonNull ViewHolder holder, @NonNull SearchFriend item) {
        String id = "ID: " + item.getFriend().id;
        int start = id.indexOf(item.getKey());
        if (start == -1) {
            holder.idTv.setText(id);
            return;
        }

        ForegroundColorSpan highlight = new ForegroundColorSpan(0xFF7B4FF7);
        int end = start + item.getKey().length();
        SpannableStringBuilder builder = new SpannableStringBuilder(id);
        builder.setSpan(highlight, start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        holder.idTv.setText(builder);
    }

    private void bindNickname(@NonNull ViewHolder holder, @NonNull SearchFriend item) {
        String nickname = item.getFriend().nickname;
        int start = nickname.indexOf(item.getKey());
        if (start == -1) {
            holder.nicknameTv.setText(nickname);
            return;
        }

        ForegroundColorSpan highlight = new ForegroundColorSpan(0xFF7B4FF7);
        int end = start + item.getKey().length();
        SpannableStringBuilder builder = new SpannableStringBuilder(nickname);
        builder.setSpan(highlight, start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        holder.nicknameTv.setText(builder);
    }

    @Override
    public void onClick(View v) {
        SearchFriend friend = (SearchFriend) v.getTag();
        onClickListener.onItemClicked(friend);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView avatarIv;
        final TextView nicknameTv;
        final TextView idTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            avatarIv = itemView.findViewById(R.id.avatar_iv);
            nicknameTv = itemView.findViewById(R.id.nickname_tv);
            idTv = itemView.findViewById(R.id.id_tv);
        }
    }

}
