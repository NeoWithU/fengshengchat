package com.fengshengchat.app.view;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.app.model.SearchViewMore;
import com.fengshengchat.base.BKViewHolder;
import com.fengshengchat.base.BaseItemViewBinder;

import butterknife.BindView;

public class SearchViewMoreViewBinder extends BaseItemViewBinder<SearchViewMore, SearchViewMoreViewBinder.ViewHolder>
        implements View.OnClickListener {

    public SearchViewMoreViewBinder(@NonNull OnClickListener<SearchViewMore> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.search_view_more_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull SearchViewMore item) {
        holder.viewMoreTv.setText(item.getTitle());
        holder.itemView.setTag(item);
        holder.itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        SearchViewMore item = (SearchViewMore) v.getTag();
        onClickListener.onItemClicked(item);
    }

    public static class ViewHolder extends BKViewHolder {
        @BindView(R.id.view_more_tv)
        TextView viewMoreTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
