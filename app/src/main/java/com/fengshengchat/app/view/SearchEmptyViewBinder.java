package com.fengshengchat.app.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fengshengchat.R;
import com.fengshengchat.app.model.SearchEmpty;
import com.fengshengchat.base.BaseItemViewBinder;

public class SearchEmptyViewBinder extends BaseItemViewBinder<SearchEmpty, SearchEmptyViewBinder.ViewHolder> {
    @NonNull
    @Override
    protected SearchEmptyViewBinder.ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View inflate = inflater.inflate(R.layout.search_empty_item, parent, false);
        return new SearchEmptyViewBinder.ViewHolder(inflate);
    }

    @Override
    protected void onBindViewHolder(@NonNull SearchEmptyViewBinder.ViewHolder holder, @NonNull SearchEmpty item) {
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
