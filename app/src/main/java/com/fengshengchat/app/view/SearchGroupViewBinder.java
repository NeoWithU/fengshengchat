package com.fengshengchat.app.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.app.model.SearchGroupMember;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;

public class SearchGroupViewBinder extends BaseItemViewBinder<SearchGroupMember, SearchGroupViewBinder.ViewHolder> implements View.OnClickListener {

    public SearchGroupViewBinder(@NonNull OnClickListener<SearchGroupMember> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.search_group_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull SearchGroupMember item) {
        holder.itemView.setOnClickListener(this);
        holder.itemView.setTag(item);
        ImageUtils.loadRadiusImage(holder.avatarIv, item.avatar, R.drawable.group_ic_default, AppConfig.IMAGE_RADIUS);
        holder.groupNameTv.setText(item.groupName);
        bindNickname(holder, item);
    }

    private static final String idLabel = "(ID:";

    private void bindNickname(@NonNull ViewHolder holder, @NonNull SearchGroupMember item) {
        Context context = holder.nicknameTv.getContext();
        String contain = context.getString(R.string.search_contain);
        String nickname = item.nickname;
        SpannableStringBuilder builder = new SpannableStringBuilder(contain + nickname);

        int index = nickname.indexOf(item.key);
        if (index != -1) {
            int start = index + contain.length();
            int end = start + item.key.length();
            ForegroundColorSpan span = new ForegroundColorSpan(0xFF7B4FF7);
            builder.setSpan(span, start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        String id = String.valueOf(item.id);
        index = id.indexOf(item.key);
        if (index != -1) {
            int length = builder.length();
            int start = length + idLabel.length() + index;
            int end = start + item.key.length();

            builder.append(idLabel).append(id).append(")");
            ForegroundColorSpan span = new ForegroundColorSpan(0xFF7B4FF7);
            builder.setSpan(span, start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        } else {
            builder.append(idLabel).append(id).append(")");
        }

        holder.nicknameTv.setText(builder);
    }

    @Override
    public void onClick(View v) {
        SearchGroupMember friend = (SearchGroupMember) v.getTag();
        onClickListener.onItemClicked(friend);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView avatarIv;
        final TextView groupNameTv;
        final TextView nicknameTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            groupNameTv = itemView.findViewById(R.id.group_name_tv);
            avatarIv = itemView.findViewById(R.id.avatar_iv);
            nicknameTv = itemView.findViewById(R.id.nickname_tv);
        }
    }

}
