package com.fengshengchat.app.view;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.app.model.SearchSession;
import com.fengshengchat.base.BKViewHolder;
import com.fengshengchat.base.BaseItemViewBinder;

import butterknife.BindView;

public class SearchSectionViewBinder extends BaseItemViewBinder<SearchSession, SearchSectionViewBinder.ViewHolder> {

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.search_section_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull SearchSession item) {
        holder.titleTv.setText(item.getTitle());
    }

    static class ViewHolder extends BKViewHolder {
        @BindView(R.id.title_tv)
        TextView titleTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
