package com.fengshengchat.app.activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.blankj.utilcode.util.ToastUtils;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.dialog.MyQrcodeDialogFragment;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.utils.QrcodeUtils;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * 二维码 Activity
 */
public class QrcodeCaptureActivity extends BaseActivity {

    private static final String IMAGE_TYPE = "image/*";
    public static final int IMAGE_REQUEST_CODE = 1;

    public static final String EXTRA_CONTENT = "CONTENT";

    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindow();
        barcodeScannerView = initializeContent();
        capture = new MyCaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();
        ButterKnife.bind(this);
    }

    public static class MyCaptureManager extends CaptureManager {
        private final Activity activity;
        private final DecoratedBarcodeView barcodeView;

        MyCaptureManager(Activity activity, DecoratedBarcodeView barcodeView) {
            super(activity, barcodeView);
            this.activity = activity;
            this.barcodeView = barcodeView;
        }

        protected void returnResult(BarcodeResult rawResult) {
            String text = rawResult.toString();
            if (!TextUtils.isEmpty(text)) {
                long gid = Protocol.extractGid(text);
                long uid = Protocol.extractUid(text);
                if (gid == 0 && uid == UserManager.getUser().uid) {
                    ToastUtils.showShort(R.string.search_self_uid);
                    barcodeView.resume();
                    decode();
                    return;
                }
            }

            Intent intent = resultIntent(rawResult, null);
            activity.setResult(Activity.RESULT_OK, intent);
            closeAndFinish();
        }
    }

    /**
     * 初始化窗口
     */
    private void initWindow() {
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | layoutParams.flags;
    }

    protected DecoratedBarcodeView initializeContent() {
        setContentView(R.layout.qrcode_activity);
        return (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
        startTime();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        capture.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @OnClick(R.id.close_tv)
    public void close() {
        finish();
    }

    @OnClick(R.id.album_tv)
    public void openAlbum() {
        Intent intent = new Intent();
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType(IMAGE_TYPE);
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        startActivityForResult(intent, IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            handleParseResult(data);
        }
    }

    private void handleParseResult(@Nullable Intent data) {
        if (data == null) {
            return;
        }

        Uri uri = data.getData();
        if (uri == null) {
            return;
        }

        Observable.fromCallable(() -> {
            ContentResolver contentResolver = QrcodeCaptureActivity.this.getContentResolver();
            return MediaStore.Images.Media.getBitmap(contentResolver, uri);
        }).map(QrcodeUtils::decodeQRCode)
                .compose(this.bindUntilEvent(ActivityEvent.DESTROY))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(getParseResObservable());
    }

    @NonNull
    private HttpRequestDialogObserver<String> getParseResObservable() {
        return new HttpRequestDialogObserver<String>(getSupportFragmentManager()) {
            @Override
            public void onNext(String s) {
                if (s.length() == 0) {
                    toast("未发现二维码");
                    return;
                }

                long gid = Protocol.extractGid(s);
                long uid = Protocol.extractUid(s);
                if (gid == 0 && uid == UserManager.getUser().uid) {
                    toast(R.string.search_self_uid);
                    return;
                }

                Intent data = new Intent();
                data.putExtra(EXTRA_CONTENT, s);
                setResult(Activity.RESULT_OK, data);
                finish();
            }
        };
    }

    @OnClick(R.id.scan_tv)
    public void scan() {
        MyQrcodeDialogFragment fragment = MyQrcodeDialogFragment.newInstance();
        fragment.show(getSupportFragmentManager(), MyQrcodeDialogFragment.TAG);
    }

    private void startTime() {
        Observable.timer(7, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(ActivityEvent.PAUSE))
                .subscribe(new BaseObserver<Long>() {
                    @Override
                    public void onNext(Long aLong) {
                        if (getSupportFragmentManager().findFragmentByTag(MyQrcodeDialogFragment.TAG) != null) {
                            return;
                        }
                        toast("未识别到二维码，请换个角度试试");
                    }
                });
    }

}
