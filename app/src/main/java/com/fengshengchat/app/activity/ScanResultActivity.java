package com.fengshengchat.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.widget.TitleBarLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScanResultActivity extends BaseActivity {

    public static final String EXTRA_DATA = "DATA";

    @BindView(R.id.title_bar)
    TitleBarLayout titleBarLayout;
    @BindView(R.id.content_tv)
    TextView contentTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcode_scan_result_fragment);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        titleBarLayout.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                finish();
            }

            @Override
            public void onActionImageClick() {

            }

            @Override
            public void onActionClick() {

            }
        });
        ToolbarFragment.hackStatusBar(titleBarLayout);
        Intent intent = getIntent();
        String content = intent.getStringExtra(EXTRA_DATA);
        contentTv.setText(content);
        contentTv.setTextIsSelectable(true);
    }
}
