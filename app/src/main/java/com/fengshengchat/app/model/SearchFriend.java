package com.fengshengchat.app.model;

import com.fengshengchat.contacts.model.Friend;

import java.util.ArrayList;
import java.util.List;

public class SearchFriend {

    private final Friend friend;
    private final String key;

    public SearchFriend(Friend friend, String key) {
        this.friend = friend;
        this.key = key;
    }

    public static List<SearchFriend> convert(List<Friend> friendList, String key) {
        ArrayList<SearchFriend> list = new ArrayList<>();
        for (Friend friend : friendList) {
            SearchFriend item = new SearchFriend(friend, key);
            list.add(item);
        }
        return list;
    }

    public Friend getFriend() {
        return friend;
    }

    public String getKey() {
        return key;
    }
}
