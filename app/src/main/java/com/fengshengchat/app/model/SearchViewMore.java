package com.fengshengchat.app.model;

public class SearchViewMore {

    public static final int TYPE_CONTACTS = 1;
    public static final int TYPE_GROUP_CHAT = 2;

    private final int id;
    private final String title;

    public SearchViewMore(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

}
