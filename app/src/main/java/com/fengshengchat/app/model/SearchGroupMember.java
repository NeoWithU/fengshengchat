package com.fengshengchat.app.model;

import android.arch.persistence.room.Ignore;

/**
 * 搜索群成员
 */
public class SearchGroupMember {
    public long id;
    public String avatar;
    public String groupName;
    public String nickname;
    @Ignore
    public String key;
    public long groupId;
}
