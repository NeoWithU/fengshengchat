package com.fengshengchat.app.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.CleanUtils;
import com.fengshengchat.BuildConfig;
import com.fengshengchat.R;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.net.BaseObserver;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

/**
 * 其它设置
 */
public class OtherSettingFragment extends ToolbarFragment {

    public static final String TAG = "OtherSettingFragment";

    @BindView(R.id.version_code_tv)
    TextView versionCodeTv;

    public static OtherSettingFragment newInstance() {
        Bundle args = new Bundle();
        OtherSettingFragment fragment = new OtherSettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.other_setting_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        versionCodeTv.setText(BuildConfig.VERSION_NAME);
    }

    @OnClick(R.id.share_layout)
    public void share() {
        openFragment(ShareAppFragment.newInstance(), ShareAppFragment.TAG);
    }

    @OnClick(R.id.feedback_layout)
    public void feedback() {
    }

    @OnClick(R.id.clear_cache_layout)
    public void clearCacheLayout() {
        Observable.fromCallable(() -> CleanUtils.cleanInternalCache() && CleanUtils.cleanExternalCache())
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            toast("清除成功");
                        } else {
                            toast("清除失败");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        toast("清除失败");
                    }
                });
    }

}
