package com.fengshengchat.app.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.app.SearchFragment;
import com.fengshengchat.app.model.SearchDivider;
import com.fengshengchat.app.model.SearchEmpty;
import com.fengshengchat.app.model.SearchFriend;
import com.fengshengchat.app.model.SearchGroupMember;
import com.fengshengchat.app.model.SearchSession;
import com.fengshengchat.app.model.SearchViewMore;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.GroupMemberDao;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.user.UserManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class SearchPartFragment extends SearchFragment {

    public static final String TAG = "SearchPartFragment";

    public static SearchPartFragment newInstance() {
        Bundle args = new Bundle();
        SearchPartFragment fragment = new SearchPartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSubject();
    }

    private void initSubject() {
        subject = PublishSubject.create();
        subject.debounce(TIME_OUT, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(this::checkKeyWordEmpty)
                .filter(key -> !TextUtils.isEmpty(key))
                .observeOn(Schedulers.io())
                .flatMap((Function<String, ObservableSource<Pair<List<SearchFriend>, List<SearchGroupMember>>>>) this::getResultObservable)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<Pair<List<SearchFriend>, List<SearchGroupMember>>>() {
                    @Override
                    public void onNext(Pair<List<SearchFriend>, List<SearchGroupMember>> pair) {
                        bindUI(pair);
                    }
                });
    }

    private Observable<Pair<List<SearchFriend>, List<SearchGroupMember>>> getResultObservable(String key) {
        if (TextUtils.isEmpty(key)) {
            return Observable.just(new Pair<>(Collections.emptyList(), Collections.emptyList()));
        }
        return Observable.combineLatest(getSearchFriendObservable(key), getSearchGroupMemberObservable(key), Pair::new);
    }

    private void bindUI(Pair<List<SearchFriend>, List<SearchGroupMember>> pair) {
        items.clear();

        List<SearchFriend> friendList = pair.first;
        if (!friendList.isEmpty()) {
            items.add(new SearchSession(SearchSession.TYPE_CONTACTS, getString(R.string.search_contacts)));

            boolean isMore = friendList.size() > 3;
            if (isMore) {
                friendList.remove(friendList.size() - 1);
            }

            items.addAll(friendList);
            if (isMore) {
                items.add(new SearchViewMore(SearchViewMore.TYPE_CONTACTS, getString(R.string.search_view_more_contacts)));
            }
            items.add(new SearchDivider());
        }

        List<SearchGroupMember> groupMemberList = pair.second;
        if (!groupMemberList.isEmpty()) {
            items.add(new SearchSession(SearchSession.TYPE_GROUP, getString(R.string.search_group_chat)));

            boolean isMore = groupMemberList.size() > 3;
            if (isMore) {
                groupMemberList.remove(groupMemberList.size() - 1);
            }
            items.addAll(groupMemberList);

            if (isMore) {
                items.add(new SearchViewMore(SearchViewMore.TYPE_GROUP_CHAT, getString(R.string.search_view_more_group_chat)));
            }
            items.add(new SearchDivider());
        }

        boolean isEmpty = friendList.isEmpty() && groupMemberList.isEmpty();
        if (isEmpty) {
            items.add(SearchEmpty.INSTANCE);
        }

        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.cancel_tv)
    public void cancel() {
        getActivity().finish();
    }

    private Observable<List<SearchFriend>> getSearchFriendObservable(String key) {
        return Observable.just(key)
                .map(s -> {
                    List<Account> accountList;
                    if (TextUtils.isDigitsOnly(s)) {
                        accountList = DbManager.getDb().getAccountDao().searchByNicknameOrId(s);
                    } else {
                        accountList = DbManager.getDb().getAccountDao().searchByNickname(s);
                    }
                    List<Friend> friendList = Friend.convert(accountList);
                    return SearchFriend.convert(friendList, s);
                })
                .onErrorReturnItem(new ArrayList<>())
                .observeOn(Schedulers.io());
    }

    private Observable<List<SearchGroupMember>> getSearchGroupMemberObservable(String key) {
        return Observable.just(key)
                .map(s -> {
                    GroupMemberDao groupMemberDao = DbManager.getDb().getGroupMemberDao();
                    if (TextUtils.isDigitsOnly(s)) {
                        return groupMemberDao.searchByNicknameOrId(key, UserManager.getUser().uid);
                    } else {
                        return groupMemberDao.searchByNickname(key, UserManager.getUser().uid);
                    }
                })
                .doOnNext(list -> {
                    for (SearchGroupMember member : list) {
                        member.key = key;
                    }
                })
                .onErrorReturnItem(new ArrayList<>())
                .observeOn(Schedulers.io());
    }
}
