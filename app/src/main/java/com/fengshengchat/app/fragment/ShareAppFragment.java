package com.fengshengchat.app.fragment;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.utils.PhotoAlbumUtil;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ClipboardUtils;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.utils.QrcodeUtils;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

/**
 * 分享应用
 */
public class ShareAppFragment extends ToolbarFragment {

    public static final String TAG = "ShareAppFragment";

    @BindView(R.id.nickname_tv)
    TextView nicknameTv;
    @BindView(R.id.qrcode_iv)
    ImageView qrcodeIv;
    @BindView(R.id.url_tv)
    TextView urlTv;

    public static ShareAppFragment newInstance() {
        Bundle args = new Bundle();
        ShareAppFragment fragment = new ShareAppFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.sahre_app_fragment;
    }

  //  private static final String URL = "http://qr.feige.co/add_friend";
  //  private static final String URL = "http://fengsheng.helimai8.com/add_friend";
    private static final String URL = "http://www.xxxxxx.net/x.html";

    @OnClick(R.id.copy_btn)
    public void copy() {
        ClipboardUtils.copyText(URL);
        toast(R.string.user_copy_success);
    }

    @OnClick(R.id.save_qrcode_tv)
    public void saveQrcode() {
        new RxPermissions(this)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean granted) {
                        if (granted) {
                            saveToPhotoAlbum();
                        } else {
                            ToastUtils.showShort(R.string.search_need_write_storage_permission);
                        }
                    }
                });
    }

    /**
     * 将二维码保存到相册
     */
    private void saveToPhotoAlbum() {
        Observable.fromCallable(() -> ConvertUtils.view2Bitmap(qrcodeIv))
                .map(bitmap -> PhotoAlbumUtil.saveImageToGallery(getContext(), bitmap))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean success) {
                        if (success) {
                            ToastUtils.showShort(R.string.search_save_success);
                        } else {
                            ToastUtils.showShort(R.string.search_save_error);
                        }
                    }
                });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindUI(view);
    }

    private void bindUI(View view) {
        urlTv.setText(URL);

        User user = UserManager.getUser();
        if (user == null) {
            return;
        }
        view.post(() -> QrcodeUtils.showQrcode(qrcodeIv, Protocol.generateCardString(user.uid), user.icon, this, this));
        nicknameTv.setText(user.nickname);
    }

}
