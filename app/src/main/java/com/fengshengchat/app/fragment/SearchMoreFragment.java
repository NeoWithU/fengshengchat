package com.fengshengchat.app.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.app.SearchFragment;
import com.fengshengchat.app.model.SearchDivider;
import com.fengshengchat.app.model.SearchEmpty;
import com.fengshengchat.app.model.SearchFriend;
import com.fengshengchat.app.model.SearchGroupMember;
import com.fengshengchat.app.model.SearchSession;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.GroupMemberDao;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.user.UserManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class SearchMoreFragment extends SearchFragment {

    public static final String TAG = "SearchMoreFragment";

    private static final String EXTRA_KEY_WORD = "KEY_WORD";
    private static final String EXTRA_TYPE = "TYPE";

    public static final int TYPE_FRIEND = 1;
    public static final int TYPE_GROUP = 2;

    public static SearchMoreFragment newInstance(String key, int type) {
        Bundle args = new Bundle();
        args.putString(EXTRA_KEY_WORD, key);
        args.putInt(EXTRA_TYPE, type);
        SearchMoreFragment fragment = new SearchMoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String keyWord;
    private int type;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        keyWord = args.getString(EXTRA_KEY_WORD);
        type = args.getInt(EXTRA_TYPE);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initSubject();
        super.onViewCreated(view, savedInstanceState);
        searchEt.setText(keyWord);
    }

    private void initSubject() {
        subject = PublishSubject.create();
        if (type == TYPE_FRIEND) {
            subject.debounce(TIME_OUT, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(this::checkKeyWordEmpty)
                    .filter(key -> !TextUtils.isEmpty(key))
                    .observeOn(Schedulers.io())
                    .flatMap((Function<String, ObservableSource<List<SearchFriend>>>) this::getSearchFriendObservable)
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                    .subscribe(new BaseObserver<List<SearchFriend>>() {
                        @Override
                        public void onNext(List<SearchFriend> searchFriends) {
                            bindSearchFriendUi(searchFriends);
                        }
                    });
        } else {
            subject.debounce(TIME_OUT, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(this::checkKeyWordEmpty)
                    .filter(key -> !TextUtils.isEmpty(key))
                    .observeOn(Schedulers.io())
                    .flatMap((Function<String, ObservableSource<List<SearchGroupMember>>>) this::getSearchGroupMemberObservable)
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                    .subscribe(new BaseObserver<List<SearchGroupMember>>() {
                        @Override
                        public void onNext(List<SearchGroupMember> searchFriends) {
                            bindGroupMemberUi(searchFriends);
                        }
                    });
        }
    }

    private Observable<List<SearchFriend>> getSearchFriendObservable(String key) {
        if (TextUtils.isEmpty(key)) {
            return Observable.just(Collections.emptyList());
        }
        return Observable.just(key)
                .map(s -> {
                    List<Account> accountList;
                    if (TextUtils.isDigitsOnly(key)) {
                        accountList = DbManager.getDb().getAccountDao().searchAllByNicknameOrId(s);
                    } else {
                        accountList = DbManager.getDb().getAccountDao().searchAllByNickname(s);
                    }
                    List<Friend> friendList = Friend.convert(accountList);
                    return SearchFriend.convert(friendList, s);
                })
                .onErrorReturnItem(new ArrayList<>())
                .observeOn(Schedulers.io());
    }

    private Observable<List<SearchGroupMember>> getSearchGroupMemberObservable(String key) {
        if (TextUtils.isEmpty(key)) {
            return Observable.just(Collections.emptyList());
        }
        return Observable.just(key)
                .map(s -> {
                    GroupMemberDao groupMemberDao = DbManager.getDb().getGroupMemberDao();
                    if (TextUtils.isDigitsOnly(key)) {
                        return groupMemberDao.searchAllByNicknameOrId(key, UserManager.getUser().uid);
                    } else {
                        return groupMemberDao.searchAllByNickname(key, UserManager.getUser().uid);
                    }
                })
                .doOnNext(list -> {
                    for (SearchGroupMember member : list) {
                        member.key = key;
                    }
                })
                .onErrorReturnItem(new ArrayList<>())
                .observeOn(Schedulers.io());
    }

    protected void bindGroupMemberUi(List<SearchGroupMember> groupMemberList) {
        items.clear();
        if (groupMemberList.isEmpty()) {
            items.add(SearchEmpty.INSTANCE);
        } else {
            items.add(new SearchSession(SearchSession.TYPE_GROUP, getString(R.string.search_group_chat)));
            items.addAll(groupMemberList);
            items.add(new SearchDivider());
        }
        adapter.notifyDataSetChanged();
    }

    protected void bindSearchFriendUi(List<SearchFriend> friendList) {
        items.clear();
        if (friendList.isEmpty()) {
            items.add(SearchEmpty.INSTANCE);
        } else {
            items.add(new SearchSession(SearchSession.TYPE_CONTACTS, getString(R.string.search_contacts)));
            items.addAll(friendList);
            items.add(new SearchDivider());
        }
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.cancel_tv)
    public void cancel() {
        getFragmentManager().popBackStack();
    }


}
