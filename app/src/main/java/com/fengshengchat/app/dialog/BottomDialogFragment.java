package com.fengshengchat.app.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fengshengchat.R;
import com.fengshengchat.base.BFBottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.drakeet.multitype.MultiTypeAdapter;

public class BottomDialogFragment extends BFBottomSheetDialogFragment {

    public static final String TAG = "BottomDialogFragment";
    public static final String EXTRA_SELECTED_ITEM = "SELECTED_ITEM";
    public static final String EXTRA_POSITION = "POSITION";

    public static BottomDialogFragment newInstance(String[] items) {
        Bundle args = new Bundle();
        args.putStringArray(EXTRA_DATA, items);
        BottomDialogFragment fragment = new BottomDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.common_bottom_dialog;
    }

    private final List<String> items = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null) {
            return;
        }

        String[] array = args.getStringArray(EXTRA_DATA);
        items.addAll(Arrays.asList(array));
    }

    @BindView(R.id.recycler_view)
    public RecyclerView recyclerView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        Context context = getContext();
        if (context == null) {
            return;
        }

        DividerItemDecoration decor = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.common_divider);
        if (drawable != null) {
            decor.setDrawable(drawable);
        }

        MultiTypeAdapter adapter = new MultiTypeAdapter(items);
        adapter.register(String.class, new TextItemViewBinder(data -> {
            int position = items.indexOf(data);
            onClickPositiveButton(position, data);
            dismiss();
        }));

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(decor);
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.cancel_tv)
    public void clickCancelButton(View view) {
        dismiss();
    }

    private void onClickPositiveButton(int position, String item) {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_SELECTED_ITEM, item);
            intent.putExtra(EXTRA_POSITION, position);
            targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        }
    }
}
