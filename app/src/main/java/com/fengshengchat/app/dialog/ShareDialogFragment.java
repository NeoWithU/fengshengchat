package com.fengshengchat.app.dialog;

import android.os.Bundle;
import android.view.View;

import com.fengshengchat.R;
import com.fengshengchat.base.BFBottomSheetDialogFragment;
import com.fengshengchat.base.utils.ToastUtils;

import butterknife.OnClick;

/**
 * 分享对话框
 */
public class ShareDialogFragment extends BFBottomSheetDialogFragment {

    public static final String TAG = "ShareDialogFragment";

    public static ShareDialogFragment newInstance() {
        Bundle args = new Bundle();
        ShareDialogFragment fragment = new ShareDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_share_dialog;
    }

    @OnClick(R.id.share_to_wechat_tv)
    public void shareToWechat(View v) {
        ToastUtils.show("wechat");
        dismiss();
    }

    @OnClick(R.id.share_to_wechat_circle_tv)
    public void shareToWechatCircle(View v) {
        ToastUtils.show("wechat circle");
        dismiss();
    }

    @OnClick(R.id.share_to_qq_tv)
    public void shareToQQ(View v) {
        ToastUtils.show("qq");
        dismiss();
    }

    @OnClick(R.id.share_to_weibo_tv)
    public void shareToWeibo(View v) {
        ToastUtils.show("weibo");
        dismiss();
    }

    @OnClick(R.id.cancel_tv)
    public void cancel(View v) {
        dismiss();
    }


}
