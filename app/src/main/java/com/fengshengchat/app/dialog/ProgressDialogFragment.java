package com.fengshengchat.app.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

import com.fengshengchat.base.base.BaseDialogFragment;
import com.fengshengchat.base.view.LoadingDialog;

import java.lang.reflect.Field;


/**
 * Progress Dialog Fragment
 */
public class ProgressDialogFragment extends BaseDialogFragment {

    public static final String TAG = "ProgressDialogFragment";
    private static final String EXTRA_MESSAGE = "MESSAGE";

    public static ProgressDialogFragment newInstance() {
        return newInstance(null);
    }

    public static ProgressDialogFragment newInstance(@Nullable String message) {
        Bundle args = new Bundle();
        args.putString(EXTRA_MESSAGE, message);
        ProgressDialogFragment fragment = new ProgressDialogFragment();
        fragment.setCancelable(false);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString(EXTRA_MESSAGE);

//        ProgressDialog dialog = new ProgressDialog(getContext());
//        dialog.setMessage(message);
//        return dialog;
        LoadingDialog dialog = new LoadingDialog(getContext());
        if (!TextUtils.isEmpty(message)) {
            dialog.setMessage(message);
        }
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    public interface OnDismissListener {
        void onDismiss();
    }

    private OnDismissListener onDismissListener;

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            Class c = getClass();
            Field dismissed = c.getDeclaredField("mDismissed");
            Field shownByMe = c.getDeclaredField("mShownByMe");
            dismissed.setAccessible(true);
            shownByMe.setAccessible(true);
            dismissed.setBoolean(this, false);
            shownByMe.setBoolean(this, true);
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        }catch(Exception e){
            try {
                super.show(manager, tag);
            }catch(Exception e1){
                e1.printStackTrace();
            }
        }
    }
}
