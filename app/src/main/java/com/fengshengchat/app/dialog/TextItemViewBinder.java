package com.fengshengchat.app.dialog;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;

public class TextItemViewBinder extends BaseItemViewBinder<String, TextItemViewBinder.ViewHolder>
        implements View.OnClickListener {

    public TextItemViewBinder(@NonNull OnClickListener<String> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected TextItemViewBinder.ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.common_bottom_dialog_text_item, parent, false);
        return new TextItemViewBinder.ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull TextItemViewBinder.ViewHolder holder, @NonNull String item) {
        holder.textView.setText(item);
        holder.textView.setTag(item);
        holder.textView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String data = (String) v.getTag();
        onClickListener.onItemClicked(data);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView textView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_view);
        }
    }

}
