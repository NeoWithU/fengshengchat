package com.fengshengchat.app;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.app.fragment.SearchPartFragment;
import com.fengshengchat.base.base.BaseActivity;

/**
 * 搜索
 */
public class SearchActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        SearchPartFragment searchFragment = SearchPartFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, searchFragment, SearchPartFragment.TAG)
                .commit();
    }
}
