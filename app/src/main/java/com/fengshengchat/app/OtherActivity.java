package com.fengshengchat.app;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.app.fragment.OtherSettingFragment;
import com.fengshengchat.base.base.BaseActivity;

/**
 * 其它设置
 */
public class OtherActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        OtherSettingFragment fragment = OtherSettingFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, OtherSettingFragment.TAG)
                .commit();
    }

}
