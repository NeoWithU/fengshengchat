package com.fengshengchat.app;

public class AppConfig {

    /**
     * 图片文件大小
     */
    public static final int IMAGE_MAX_SIZE_BYTE = 2 * 1024 * 1024;  //  2M

    public static final int IMAGE_RADIUS = 5;

    public static final int PAGE_SIZE = 30;
}
