package com.fengshengchat.app;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.fengshengchat.R;
import com.fengshengchat.app.fragment.SearchMoreFragment;
import com.fengshengchat.app.model.SearchDivider;
import com.fengshengchat.app.model.SearchEmpty;
import com.fengshengchat.app.model.SearchFriend;
import com.fengshengchat.app.model.SearchGroupMember;
import com.fengshengchat.app.model.SearchSession;
import com.fengshengchat.app.model.SearchViewMore;
import com.fengshengchat.app.view.SearchDividerViewBinder;
import com.fengshengchat.app.view.SearchEmptyViewBinder;
import com.fengshengchat.app.view.SearchFriendViewBinder;
import com.fengshengchat.app.view.SearchGroupViewBinder;
import com.fengshengchat.app.view.SearchSectionViewBinder;
import com.fengshengchat.app.view.SearchViewMoreViewBinder;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.base.RecyclerViewDivider;
import com.fengshengchat.chat.view.ChatActivity;
import com.fengshengchat.contacts.model.Friend;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.subjects.PublishSubject;
import me.drakeet.multitype.MultiTypeAdapter;

/**
 * 搜索
 */
public class SearchFragment extends ToolbarFragment {

    @BindView(R.id.search_et)
    protected EditText searchEt;
    @BindView(R.id.recycler_view)
    protected RecyclerView recyclerView;

    protected List<Object> items;
    protected MultiTypeAdapter adapter;
    protected PublishSubject<String> subject;

    public static final int TIME_OUT = 200;

    @Override
    public int getLayoutId() {
        return R.layout.search_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSearchEt();
        initRecyclerView();
    }

    private void initSearchEt() {
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                subject.onNext(s.toString());
            }
        });
    }

    private void initRecyclerView() {
        items = new ArrayList<>();
        adapter = new MultiTypeAdapter(items);
        adapter.register(SearchFriend.class, new SearchFriendViewBinder(this::openUserInfo));
        adapter.register(SearchSession.class, new SearchSectionViewBinder());
        adapter.register(SearchViewMore.class, new SearchViewMoreViewBinder(this::openSearchMore));
        adapter.register(SearchDivider.class, new SearchDividerViewBinder());
        adapter.register(SearchGroupMember.class, new SearchGroupViewBinder(this::judgeFriendRelation));
        adapter.register(SearchEmpty.class, new SearchEmptyViewBinder());

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new RecyclerViewDivider(1, 0xFFEEEEEE));
    }

    private void openSearchMore(SearchViewMore item) {
        String keyWord = searchEt.getText().toString();
        SearchMoreFragment fragment;
        if (item.getId() == SearchViewMore.TYPE_CONTACTS) {
            fragment = SearchMoreFragment.newInstance(keyWord, SearchMoreFragment.TYPE_FRIEND);
        } else {
            fragment = SearchMoreFragment.newInstance(keyWord, SearchMoreFragment.TYPE_GROUP);
        }
        openFragment(fragment, SearchMoreFragment.TAG);
    }

    private void openUserInfo(SearchFriend item) {
        Friend friend = item.getFriend();
        ChatActivity.startSingleChat(getContext(), friend.id, friend.nickname);
    }

    private void judgeFriendRelation(SearchGroupMember item) {
        ChatActivity.startChat(getContext(), item.groupId, true);
    }

    protected void checkKeyWordEmpty(String key) {
        if (TextUtils.isEmpty(key)) {
            items.clear();
            adapter.notifyDataSetChanged();
        }
    }


}
