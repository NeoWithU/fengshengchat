package com.fengshengchat.account.dialog;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.utils.PhotoAlbumUtil;
import com.fengshengchat.base.BFDialogFragment;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.utils.QrcodeUtils;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

/**
 * 我的二维码
 */
public class MyQrcodeDialogFragment extends BFDialogFragment {

    public static final String TAG = "MyQrcodeDialogFragment";

    public static MyQrcodeDialogFragment newInstance() {
        Bundle args = new Bundle();
        MyQrcodeDialogFragment fragment = new MyQrcodeDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.search_my_qrcode_dialog_fragment;
    }

    @BindView(R.id.nickname_tv)
    TextView nicknameTv;
    @BindView(R.id.qrcode_iv)
    ImageView qrcodeIv;

    @OnClick(R.id.save_qrcode_tv)
    public void saveQrcode() {
        new RxPermissions(this)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean granted) {
                        if (granted) {
                            saveToPhotoAlbum();
                        } else {
                            ToastUtils.showShort(R.string.search_need_write_storage_permission);
                        }
                    }
                });
    }

    /**
     * 将二维码保存到相册
     */
    private void saveToPhotoAlbum() {
        Observable.fromCallable(() -> ConvertUtils.view2Bitmap(qrcodeIv))
                .map(bitmap -> PhotoAlbumUtil.saveImageToGallery(getContext(), bitmap))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean success) {
                        if (success) {
                            ToastUtils.showShort(R.string.search_save_success);
                        } else {
                            ToastUtils.showShort(R.string.search_save_error);
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        if (window != null) {
            int width = (int) (ScreenUtils.getScreenWidth() * 0.92f);
            window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
        }
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindUI(view);
    }

    private void bindUI(View view) {
        User user = UserManager.getUser();
        if (user == null) {
            return;
        }

        view.post(() -> QrcodeUtils.showQrcode(qrcodeIv, Protocol.generateCardString(user.uid), user.icon, MyQrcodeDialogFragment.this, MyQrcodeDialogFragment.this));
        nicknameTv.setText(user.nickname);
    }

}
