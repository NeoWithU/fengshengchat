package com.fengshengchat.account.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.base.BaseBottomSheetDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ConfirmDialogFragment extends BaseBottomSheetDialogFragment {

    public static final String TAG = "ConfirmDialogFragment";

    private static final String EXTRA_TITLE = "TITLE";
    private static final String EXTRA_ACTION = "ACTION";

    private Unbinder unbinder;

    private String title;
    private String action;

    @BindView(R.id.title_tv)
    TextView titleTv;
    @BindView(R.id.action_tv)
    TextView actionTv;

    public static ConfirmDialogFragment newInstance(String title, String action) {
        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putString(EXTRA_ACTION, action);
        ConfirmDialogFragment fragment = new ConfirmDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.confirm_dialog_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        title = args.getString(EXTRA_TITLE);
        action = args.getString(EXTRA_ACTION);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        titleTv.setText(title);
        actionTv.setText(action);
    }

    @OnClick(R.id.cancel_tv)
    public void clickCancelButton(View view) {
        dismiss();
    }

    @OnClick(R.id.action_tv)
    public void doAction() {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
        }
        dismiss();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
