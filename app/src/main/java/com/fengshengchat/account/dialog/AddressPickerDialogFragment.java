package com.fengshengchat.account.dialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.aigestudio.wheelpicker.WheelPicker;
import com.fengshengchat.R;
import com.fengshengchat.account.model.Province;
import com.fengshengchat.base.base.BaseBottomSheetDialogFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * 地址选择器
 */
public class AddressPickerDialogFragment extends BaseBottomSheetDialogFragment implements WheelPicker.OnItemSelectedListener {

    public static final String TAG = "AddressPickerDialogFragment";
    public static final String EXTRA_PROVINCE = "NATIONALITY";
    public static final String EXTRA_CITY = "PROVINCE";
    public static final String EXTRA_AREA = "CITY";

    private List<Province> provinceList;
    private Unbinder unbinder;

    @BindView(R.id.provincePicker)
    WheelPicker provincePicker;
    @BindView(R.id.cityPicker)
    WheelPicker cityPicker;
//    @BindView(R.id.areaPicker)
//    WheelPicker areaPicker;

    public static AddressPickerDialogFragment newInstance() {
        Bundle args = new Bundle();
        AddressPickerDialogFragment fragment = new AddressPickerDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * 可以影响到效率问题,数据没有通过 Bundle 传递
     */
    public void init(List<Province> provinceList) {
        this.provinceList = provinceList;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.dialog_address_picker;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        provincePicker.setOnItemSelectedListener(this);
        cityPicker.setOnItemSelectedListener(this);
//        areaPicker.setOnItemSelectedListener(this);

        selectedProvince = provinceList.get(0);
        provincePicker.setData(provinceList);
        bindProvince(0);
    }

    @Override
    public void onItemSelected(WheelPicker picker, Object data, int position) {
        switch (picker.getId()) {
            case R.id.provincePicker:
                bindProvince(position);
                break;

            case R.id.cityPicker:
                bindCity(position);
                break;

//            case R.id.areaPicker:
//                bindArea(position);
//                break;

            default:
                break;
        }
    }

    private Province selectedProvince;
    private Province.City selectedCity;
//    private Province.City.Area selectedArea;

    private void bindProvince(int position) {
        selectedProvince = provinceList.get(position);

        selectedCity = selectedProvince.cityList.get(0);
        cityPicker.setData(selectedProvince.cityList);
        cityPicker.setSelectedItemPosition(0);

//        selectedArea = selectedCity.areaList.get(0);
//        areaPicker.setData(selectedCity.areaList);
//        areaPicker.setSelectedItemPosition(0);
    }

    private void bindCity(int position) {
        selectedCity = selectedProvince.cityList.get(position);

//        selectedArea = selectedCity.areaList.get(0);
//        areaPicker.setData(selectedCity.areaList);
//        areaPicker.setSelectedItemPosition(0);
    }

    private void bindArea(int position) {
//        selectedArea = selectedCity.areaList.get(position);
    }

    @OnClick(R.id.negative_tv)
    public void close() {
        dismiss();
    }

    @OnClick(R.id.positive_tv)
    public void onClickPositiveButton() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_PROVINCE, selectedProvince.name);
        intent.putExtra(EXTRA_CITY, selectedCity.name);
//        intent.putExtra(EXTRA_AREA, selectedArea.name);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        dismiss();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
