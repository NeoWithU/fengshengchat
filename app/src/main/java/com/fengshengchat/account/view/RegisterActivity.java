package com.fengshengchat.account.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.fengshengchat.MainActivity;
import com.fengshengchat.R;
import com.fengshengchat.account.presenter.RegisterPresenter;
import com.fengshengchat.base.BaseActivity;

import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/11/29
 */
@Deprecated
public class RegisterActivity extends BaseActivity<RegisterPresenter> implements IRegisterView{

    private EditText mEtNick;
    private EditText mEtPhone;
    private EditText mEtPwd;
    private ImageView mIvSeePwd;
    private EditText mEtVerifyCode;
    private Button mBtnSendCode;
    private Button mBtnRegister;
    private Button mBtnLogin;

    private TextWatcher watcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mBtnRegister.setEnabled(canRegister());
        }
        public void afterTextChanged(Editable s) {}
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    @Override
    protected void onReceiveMessage(MsgOuterClass.Msg msg) {
        getPresenter().onReceiveMessage(msg);
    }

    public void init() {
        mEtNick = findViewById(R.id.etNick);
        mEtPwd = findViewById(R.id.etPwd);
        mEtPhone = findViewById(R.id.etPhone);
        mEtVerifyCode = findViewById(R.id.etVerifyCode);
        mIvSeePwd = findViewById(R.id.ivSeePwd);
        mBtnSendCode = findViewById(R.id.btnSendCode);
        mBtnRegister = findViewById(R.id.btnRegister);
        mBtnLogin = findViewById(R.id.btnLogin);

        mEtNick.addTextChangedListener(watcher);
        mEtPwd.addTextChangedListener(watcher);
        mEtPhone.addTextChangedListener(watcher);
        mEtVerifyCode.addTextChangedListener(watcher);

        mIvSeePwd.setOnClickListener(v -> {

            if (mEtPwd.getTransformationMethod() == HideReturnsTransformationMethod.getInstance()) {
                mEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            } else {
                mEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }

            mEtPwd.setSelection(mEtPwd.getText().toString().trim().length());
        });

        mBtnSendCode.setOnClickListener(v -> {
            if (mBtnSendCode.isEnabled()) {
                getPresenter().sendCode(mEtPhone.getText().toString());
            }
        });

        mBtnRegister.setOnClickListener(v -> {
            getPresenter().register(mEtPhone.getText().toString(), mEtPwd.getText().toString());
        });
        mBtnLogin.setOnClickListener(v -> {
            getPresenter().login(mEtPhone.getText().toString(), mEtPwd.getText().toString());
        });

    }

    @Override
    protected void onPusherReady() {
        super.onPusherReady();
    }

    private boolean canRegister() {
        int nickNameLength = mEtNick.getText().toString().trim().length();
        int pwdLength = mEtPwd.getText().toString().trim().length();
        int phoneLength = mEtPhone.getText().toString().trim().length();
        int codeLength = mEtVerifyCode.getText().toString().trim().length();
//        if (nickNameLength > 0 && pwdLength > 0 && phoneLength > 0 && codeLength > 0) {
//            return true;
//        }
//        return false;
        return phoneLength == 11;
    }

    protected RegisterPresenter createPresenter() {
        return new RegisterPresenter(this);
    }

    @Override
    public void setRegisterSuccess(CharSequence tip) {
        toast(tip);
    }

    @Override
    public void setRegisterFailed(CharSequence error) {
        onServerError(error);
    }

    @Override
    public void setLoginSuccess(CharSequence tip) {
        startActivity(new Intent(getContext(), MainActivity.class));
    }

    @Override
    public void setLoginFailed(CharSequence error) {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.tip)
                .setMessage(error)
                .setPositiveButton(R.string.confirm, (dialog, which) -> dismissLoading())
                .create()
                .show();
    }

    @Override
    public Context getActivity() {
        return this;
    }

}
