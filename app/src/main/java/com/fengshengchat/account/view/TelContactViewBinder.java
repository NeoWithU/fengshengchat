package com.fengshengchat.account.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.account.model.TelContact;
import com.fengshengchat.base.BaseItemViewBinder;

public class TelContactViewBinder extends BaseItemViewBinder<TelContact, TelContactViewBinder.ViewHolder> {

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.search_tel_contact_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull TelContact item) {
        holder.nameTv.setText(item.name);
        holder.telTv.setText(item.tel);
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        final TextView nameTv;
        final TextView telTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.name_tv);
            telTv = itemView.findViewById(R.id.tel_tv);
        }
    }

}
