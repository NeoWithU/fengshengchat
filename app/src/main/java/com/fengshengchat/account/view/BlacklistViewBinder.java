package com.fengshengchat.account.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.Friend;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 黑名单 ViewBinder
 */
public class BlacklistViewBinder extends BaseItemViewBinder<Friend, BlacklistViewBinder.ViewHolder> {

    public interface OnClickListener {
        void onClickLayout(Friend friend);

        void onClickRemoveButton(Friend friend);
    }

    private final OnClickListener onClickListener;

    public BlacklistViewBinder(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.user_blacklist_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull Friend item) {
        holder.nameTv.setText(item.nickname);
        holder.idTv.setText(holder.itemView.getResources().getString(R.string.user_yichat_num_format, String.valueOf(item.id)));
        holder.itemView.setOnClickListener(onItemLayoutClickListener);
        holder.itemView.setTag(item);
        holder.removeTv.setOnClickListener(onRemoveClickListener);
        holder.removeTv.setTag(item);
     //   ImageUtils.loadRadiusImage(holder.avatarIv, item.avatar, R.drawable.image_default_with_round, AppConfig.IMAGE_RADIUS);
        ImageUtils.loadRadiusImage(holder.avatarIv, item.avatar, R.mipmap.default_head_icon, AppConfig.IMAGE_RADIUS);
    }

    private final View.OnClickListener onItemLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Friend friend = (Friend) v.getTag();
            onClickListener.onClickLayout(friend);
        }
    };

    private final View.OnClickListener onRemoveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Friend friend = (Friend) v.getTag();
            onClickListener.onClickRemoveButton(friend);
        }
    };

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar_iv)
        ImageView avatarIv;
        @BindView(R.id.name_tv)
        TextView nameTv;
        @BindView(R.id.id_tv)
        TextView idTv;
        @BindView(R.id.remove_tv)
        TextView removeTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
