package com.fengshengchat.account.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fengshengchat.R;
import com.fengshengchat.account.model.EmptyBlacklist;
import com.fengshengchat.base.BaseItemViewBinder;

public class EmptyBlacklistViewBinder extends BaseItemViewBinder<EmptyBlacklist, EmptyBlacklistViewBinder.ViewHolder> {

    @NonNull
    @Override
    protected EmptyBlacklistViewBinder.ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View inflate = inflater.inflate(R.layout.blacklist_empty_item, parent, false);
        return new EmptyBlacklistViewBinder.ViewHolder(inflate);
    }

    @Override
    protected void onBindViewHolder(@NonNull EmptyBlacklistViewBinder.ViewHolder holder, @NonNull EmptyBlacklist item) {
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
