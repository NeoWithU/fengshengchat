package com.fengshengchat.account.view;

import com.fengshengchat.base.IBaseViewWithPush;

public interface IRegisterView extends IBaseViewWithPush {

    void setRegisterSuccess(CharSequence tip);
    void setRegisterFailed(CharSequence error);
    void setLoginSuccess(CharSequence tip);
    void setLoginFailed(CharSequence error);

}
