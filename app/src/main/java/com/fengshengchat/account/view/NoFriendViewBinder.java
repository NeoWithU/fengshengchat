package com.fengshengchat.account.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fengshengchat.R;
import com.fengshengchat.app.model.SearchEmpty;
import com.fengshengchat.base.BaseItemViewBinder;

public class NoFriendViewBinder extends BaseItemViewBinder<SearchEmpty, NoFriendViewBinder.ViewHolder> {
    @NonNull
    @Override
    protected NoFriendViewBinder.ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View inflate = inflater.inflate(R.layout.user_no_friend, parent, false);
        return new NoFriendViewBinder.ViewHolder(inflate);
    }

    @Override
    protected void onBindViewHolder(@NonNull NoFriendViewBinder.ViewHolder holder, @NonNull SearchEmpty item) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
