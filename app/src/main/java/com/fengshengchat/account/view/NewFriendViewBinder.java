package com.fengshengchat.account.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.fengshengchat.R;
import com.fengshengchat.account.model.FriendReqInfo;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import yiproto.yichat.friend.Friend;

public class NewFriendViewBinder extends BaseItemViewBinder<FriendReqInfo, NewFriendViewBinder.ViewHolder> {

    public interface OnClickListener {
        void onClickLayout(FriendReqInfo reqInfo, int status);

        void onClickAgreeButton(FriendReqInfo reqInfo);
    }

    private final OnClickListener onClickListener;

    public NewFriendViewBinder(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.new_friend_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull FriendReqInfo item) {
        holder.nameTv.setText(item.nickname);
        holder.idTv.setText(holder.itemView.getResources().getString(R.string.user_yichat_num_format, String.valueOf(item.id)));
        holder.timeTv.setText(item.time);
        holder.itemView.setOnClickListener(onItemLayoutClickListener);
        holder.itemView.setTag(item);
        if (item.agree == Friend.RequestUserInfo.RequestResult.REQUEST_UNHANDLE_VALUE) {
            holder.actionSwitcher.setDisplayedChild(0);
            holder.agreeTv.setOnClickListener(onAgreeClickListener);
            holder.agreeTv.setTag(item);
        } else {
            holder.actionSwitcher.setDisplayedChild(1);
            holder.agreeTv.setOnClickListener(null);
            holder.agreeTv.setTag(null);
            if (item.agree == Friend.RequestUserInfo.RequestResult.REQUEST_EXPIRE_VALUE) {
                holder.passedTv.setText(R.string.search_expire);
            } else if (item.agree == Friend.RequestUserInfo.RequestResult.REQUEST_AGREED_VALUE) {
                holder.passedTv.setText(R.string.search_passed);
            } else if (item.agree == Friend.RequestUserInfo.RequestResult.REQUEST_REFUSED_VALUE) {
                holder.passedTv.setText(R.string.search_refuse);
            }
        }
     //   ImageUtils.loadRadiusImage(holder.avatarIv, item.avatar, R.drawable.image_default_with_round, AppConfig.IMAGE_RADIUS);
        ImageUtils.loadRadiusImage(holder.avatarIv, item.avatar, R.mipmap.default_head_icon, AppConfig.IMAGE_RADIUS);
    }

    private final View.OnClickListener onItemLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FriendReqInfo info = (FriendReqInfo) v.getTag();
            onClickListener.onClickLayout(info, info.agree);
        }
    };

    private final View.OnClickListener onAgreeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FriendReqInfo info = (FriendReqInfo) v.getTag();
            onClickListener.onClickAgreeButton(info);
        }
    };

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar_iv)
        ImageView avatarIv;
        @BindView(R.id.name_tv)
        TextView nameTv;
        @BindView(R.id.id_tv)
        TextView idTv;
        @BindView(R.id.action_switcher)
        ViewSwitcher actionSwitcher;
        @BindView(R.id.agree_tv)
        TextView agreeTv;
        @BindView(R.id.passed_tv)
        TextView passedTv;
        @BindView(R.id.time_tv)
        TextView timeTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
