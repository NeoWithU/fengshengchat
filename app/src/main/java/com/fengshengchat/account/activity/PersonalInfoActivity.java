package com.fengshengchat.account.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.PersonalInfoFragment;
import com.fengshengchat.base.base.BaseActivity;

/**
 * 个人信息
 */
public class PersonalInfoActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, PersonalInfoFragment.newInstance(), PersonalInfoFragment.TAG)
                .commit();
    }

}
