package com.fengshengchat.account.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.LoginFragment;
import com.fengshengchat.account.fragment.Reg2Fragment;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.test.KeyboardUtil;

public class AccountCompleteActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentForWindow(this);
        setContentView(R.layout.fragment_activity);
        fixSoftKeyboard();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, Reg2Fragment.newInstance(), LoginFragment.TAG)
                .commit();
    }

    private void fixSoftKeyboard() {
        View view = ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
        new KeyboardUtil(this, view).enable();
    }


}
