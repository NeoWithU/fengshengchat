package com.fengshengchat.account.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.AccountFragment;
import com.fengshengchat.base.BaseActivity;
import com.fengshengchat.base.mvp.IBasePresenter;

import yiproto.yichat.msg.MsgOuterClass;

/**
 * 账号与安全
 */
public class AccountActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, AccountFragment.newInstance(), AccountFragment.TAG)
                .commit();
    }

    @Override
    protected IBasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void onReceiveMessage(MsgOuterClass.Msg message) {
    }

}
