package com.fengshengchat.account.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.LoginFragment;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.base.base.OnBackPressedListener;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.test.KeyboardUtil;

/**
 * 用户登录页面
 */
public class LoginActivity extends BaseActivity {
    private LoginFragment loginFragment;

    public static void toLogin(Activity activity, String reason){
        toLogin(activity, reason, true);
    }

    public static void toLogin(Activity activity, String reason, boolean force){
        Debug.e("LoginActivity", "to login reason: " + reason);
        new Throwable("--------TO LOGIN--------").printStackTrace();

        Intent intent = new Intent(activity, LoginActivity.class);
        intent.putExtra(LoginFragment.ENTER_REASON, reason);
        if(force) {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.enter_anim, R.anim.exit_anim);
        activity.finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentForWindow(this);
        setContentView(R.layout.fragment_activity);
//        fixSoftKeyboard();

        loginFragment = LoginFragment.newInstance();
        Bundle bundle = new Bundle();
        Intent intent = getIntent();
        bundle.putString(LoginFragment.ENTER_REASON, intent.getStringExtra(LoginFragment.ENTER_REASON));
        bundle.putString(LoginFragment.ENTER_REASON_TIP, intent.getStringExtra(LoginFragment.ENTER_REASON_TIP));
        loginFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, loginFragment, LoginFragment.TAG)
                .commit();
    }

    /**
     * 修复全屏模式下软键盘问题
     */
    private void fixSoftKeyboard() {
        View view = ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
        new KeyboardUtil(this, view).enable();
    }

    public void onRegisterSuccess(String account, String password) {
        loginFragment.onRegisterSuccess(account, password);
    }


    @Override
    public void onBackPressed() {
        Fragment fragment = getStackTopFragment();
        if (fragment instanceof OnBackPressedListener) {
            boolean b = ((OnBackPressedListener) fragment).onBackPressed();
            if (b) {
                return;
            }
        }
        super.onBackPressed();
    }

    private Fragment getStackTopFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment;
        int count = fm.getBackStackEntryCount();
        if (count == 0) {
            fragment = fm.findFragmentById(R.id.fragment_container);
        } else {
            String tag = fm.getBackStackEntryAt(count - 1).getName();
            fragment = fm.findFragmentByTag(tag);
        }
        return fragment;
    }

}
