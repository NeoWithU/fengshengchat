package com.fengshengchat.account.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.SearchFriendOrGroupFragment;
import com.fengshengchat.base.base.BaseActivity;

/**
 * 搜索好友或组
 */
public class SearchFriendOrGroupActivity extends BaseActivity {
    private SearchFriendOrGroupFragment searchFriendOrGroupFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        searchFriendOrGroupFragment = SearchFriendOrGroupFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, searchFriendOrGroupFragment, SearchFriendOrGroupFragment.TAG)
                .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(null != searchFriendOrGroupFragment){
            searchFriendOrGroupFragment.onActivityResult(requestCode, resultCode, data);
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
