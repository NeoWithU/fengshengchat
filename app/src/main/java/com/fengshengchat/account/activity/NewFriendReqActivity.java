package com.fengshengchat.account.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.NewFriendFragment;
import com.fengshengchat.base.base.BaseActivity;

/**
 * 新的好友请求
 */
public class NewFriendReqActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, NewFriendFragment.newInstance(), NewFriendFragment.TAG)
                .commit();
    }
}
