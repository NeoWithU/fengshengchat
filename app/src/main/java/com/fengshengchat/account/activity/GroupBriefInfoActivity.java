package com.fengshengchat.account.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.GroupInfoFragment;
import com.fengshengchat.base.base.BaseActivity;

public class GroupBriefInfoActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        long groupId = getIntent().getLongExtra("groupId", 0L);
        long userId = getIntent().getLongExtra("userId", 0L);
        if (0 != groupId) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, GroupInfoFragment.newInstance(groupId, userId), GroupInfoFragment.TAG)
                    .commit();
        } else {
            toast(R.string.group_not_exist);
            finish();
        }
    }
}
