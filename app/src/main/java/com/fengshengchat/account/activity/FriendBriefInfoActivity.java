package com.fengshengchat.account.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.FriendBriefInfoFragment;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.base.BaseActivity;
import com.fengshengchat.base.mvp.IBasePresenter;
import com.fengshengchat.chat.presenter.ChatPresenter;
import com.fengshengchat.chat.view.IChatView;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.Group;
import com.fengshengchat.push.HandlePushService;

import java.util.List;

import yiproto.yichat.msg.MsgOuterClass;

/**
 * 联系人信息
 */
public class FriendBriefInfoActivity extends BaseActivity {

    public static final String KEY_DATA = "DATA";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        Intent intent = getIntent();
        BriefData briefData = intent.getParcelableExtra(KEY_DATA);
        if (briefData == null) {
            toast(R.string.user_not_exist);
            finish();
        } else {
            FriendBriefInfoFragment fragment = FriendBriefInfoFragment.newInstance(briefData);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment, FriendBriefInfoFragment.TAG)
                    .commit();
        }
    }

    @Override
    protected IBasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void onReceiveMessage(MsgOuterClass.Msg message) {

    }

    public void sendCardMessage(long toUid, long beSendUid, String beSendName, String beSendIcon, String text){
        HandlePushService.HandlePushServiceInterface pusher = getPusher();
        if(null == pusher){
            return;
        }
        ChatPresenter chatPresenter = new ChatPresenter(toUid, false, 0, new IChatView() {
            public void onSendMessage(ChatRecord msg) {}
            public void onAddTip(ChatRecord msg) {}
            public void onSendMessageSuccess(long seq, long serverSeq) {}
            public void onSendMessageFailed(long seq, String tip) {}
            public void onSendMessageRetry(long seq) {}
            public void onReceiveMessage(ChatRecord msg) {}
            public void onReceiveMessage(List<ChatRecord> msg) {}
            public void onUpdateSingleChatHisInfo() {}
            public void onUpdateGroupChatHisInfo() {}
            public void onUpdateGroupInfo(Group group) {}
            public void onGettingHistory() {}
            public void onNewestBack(List<ChatRecord> list) {}
            public void onHistory(List<ChatRecord> list) {}
            public void onHistoryError(CharSequence tip) {}
            public void onDeleteChatRecordCompleted(long msgSeq) {}
            public void setShowGroupNotify(boolean show) {}
            public void onReceiveGroupNotify(CharSequence msg) {}
            public void onHandle(CharSequence tip) {}
            public void onCancel(CharSequence tip) {}
            public void onUpdateViewFinally() {}
            public void clearErrorState() {}
            public Context getContext() {
                return FriendBriefInfoActivity.this.getContext();
            }
            public Context getActivity() {
                return FriendBriefInfoActivity.this;
            }
            public void toast(CharSequence s) {}
            public void toast(int resId) {}
        });
        chatPresenter.setPusher(pusher);
        chatPresenter.sendCardMessage(beSendUid, beSendName, beSendIcon);
        if(null != text && text.length() > 0){
            runDelay(() -> chatPresenter.sendTextMessage(text), 300);
        }
    }

}
