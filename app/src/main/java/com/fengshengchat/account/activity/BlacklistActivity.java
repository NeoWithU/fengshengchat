package com.fengshengchat.account.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.account.fragment.BlacklistFragment;
import com.fengshengchat.base.base.BaseActivity;

/**
 * 联系人黑名单
 */
public class BlacklistActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, BlacklistFragment.newInstance(), BlacklistFragment.TAG)
                .commit();
    }

}
