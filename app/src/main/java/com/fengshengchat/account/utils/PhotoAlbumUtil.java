package com.fengshengchat.account.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;

import com.blankj.utilcode.util.ImageUtils;

import java.io.File;

public class PhotoAlbumUtil {

    public static boolean saveImageToGallery(Context context, Bitmap bmp) {
        String storePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
        File appDir = new File(storePath);
        if (!appDir.exists()) {
            if (!appDir.mkdir()) {
                return false;
            }
        }

        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        if (file.exists()) {
            return false;
        }

        if (!ImageUtils.save(bmp, file, Bitmap.CompressFormat.JPEG)) {
            return false;
        }

        Uri uri = Uri.fromFile(file);
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
        return true;
    }

}
