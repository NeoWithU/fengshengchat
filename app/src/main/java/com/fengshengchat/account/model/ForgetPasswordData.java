package com.fengshengchat.account.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ForgetPasswordData implements Parcelable {
    public String username;
    public String password;
    public int questionId;
    public String answer;

    public ForgetPasswordData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeString(this.password);
        dest.writeInt(this.questionId);
        dest.writeString(this.answer);
    }

    protected ForgetPasswordData(Parcel in) {
        this.username = in.readString();
        this.password = in.readString();
        this.questionId = in.readInt();
        this.answer = in.readString();
    }

    public static final Creator<ForgetPasswordData> CREATOR = new Creator<ForgetPasswordData>() {
        @Override
        public ForgetPasswordData createFromParcel(Parcel source) {
            return new ForgetPasswordData(source);
        }

        @Override
        public ForgetPasswordData[] newArray(int size) {
            return new ForgetPasswordData[size];
        }
    };
}
