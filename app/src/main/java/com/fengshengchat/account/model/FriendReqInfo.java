package com.fengshengchat.account.model;

public class FriendReqInfo {
    public long id;
    public String nickname;
    public String avatar;
    public int agree;
    public String time;
}
