package com.fengshengchat.account.model;

import android.support.annotation.NonNull;

import com.blankj.utilcode.util.ResourceUtils;

import java.util.ArrayList;
import java.util.List;

import io.keiji.plistparser.PListArray;
import io.keiji.plistparser.PListDict;
import io.keiji.plistparser.PListParser;
import io.reactivex.Observable;

public class Province {

    public String name;
    public List<City> cityList;

    public Province(String name, List<City> cityList) {
        this.name = name;
        this.cityList = cityList;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    public static class City {
        public String name;
        public List<Area> areaList;

        public City(String name, List<Area> areaList) {
            this.name = name;
            this.areaList = areaList;
        }

        @NonNull
        @Override
        public String toString() {
            return name;
        }

        public static class Area {
            public String name;

            public Area(String name) {
                this.name = name;
            }

            @NonNull
            @Override
            public String toString() {
                return name;
            }
        }
    }

    public static Observable<List<Province>> getDataSource() {
        return Observable.fromCallable(() -> {
            String is = ResourceUtils.readAssets2String("Address.plist");
            PListDict dict = PListParser.parse(is);
            List<Province> provinceList = new ArrayList<>(dict.keySet().size());
            for (String provinceName : dict.keySet()) {
                PListDict cityDict = dict.getPListArray(provinceName).getPListDict(0);
                List<City> cityList1 = new ArrayList<>(cityDict.size());
                Province p = new Province(provinceName, cityList1);
                provinceList.add(p);

                for (String cityName : cityDict.keySet()) {
                    PListArray areaObjectList = cityDict.getPListArray(cityName);
                    ArrayList<City.Area> areaList = new ArrayList<>(areaObjectList.size());
                    City city = new City(cityName, areaList);
                    cityList1.add(city);

                    for (int i = 0; i < areaObjectList.size(); i++) {
                        City.Area area = new City.Area(areaObjectList.getString(i));
                        areaList.add(area);
                    }
                }
            }
            return provinceList;
        });
    }
}
