package com.fengshengchat.account.model;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.Objects;

public class TelContact {
    public final String tel;
    public final String name;
    public final String namePinyin;

    public TelContact(String tel, String name) {
        this.tel = tel;
        this.name = name;
        namePinyin = Pinyin.toPinyin(name, "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TelContact that = (TelContact) o;
        return Objects.equals(tel, that.tel) &&
                Objects.equals(name, that.name) &&
                Objects.equals(namePinyin, that.namePinyin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tel, name, namePinyin);
    }
}
