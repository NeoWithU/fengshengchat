package com.fengshengchat.account.model;

import com.fengshengchat.net.bean.NetModel;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.net.bean.Response;

import io.reactivex.Observer;

/**
 * @author KRT
 * 2018/11/29
 */
public class RegisterModel extends NetModel<IRegister> {

    public RegisterModel(){
        super(IRegister.class);
    }

    public void checkPhoneAvailable(Request request, Observer<Response> callback){
        request(getAPI().checkPhoneAvailable(createJsonRequestBody(request)), callback);
    }

    public void sendCode(Request request, Observer<Response> callback){
        request(getAPI().sendCode(createJsonRequestBody(request)), callback);
    }

    public void verifyCode(Request request, Observer<Response> callback){
        request(getAPI().verifyCode(createJsonRequestBody(request)), callback);
    }

    public void register(Request request, Observer<Response> callback){
        request(getAPI().register(createJsonRequestBody(request)), callback);
    }

    public void login(Request request, Observer<Response> callback){
        request(getAPI().login(createJsonRequestBody(request)), callback);
    }

}
