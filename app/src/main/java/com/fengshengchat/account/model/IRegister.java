package com.fengshengchat.account.model;

import com.fengshengchat.net.bean.Response;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * @author KRT
 * 2018/11/29
 */
public interface IRegister {

    @POST("web_gateway")
    @Headers("Content-Type:application/json; charset=utf-8")
    Observable<Response> checkPhoneAvailable(@Body RequestBody body);

    @POST("web_gateway")
    @Headers("Content-Type:application/json; charset=utf-8")
    Observable<Response> sendCode(@Body RequestBody body);

    @POST("web_gateway")
    @Headers("Content-Type:application/json; charset=utf-8")
    Observable<Response> verifyCode(@Body RequestBody body);

    @POST("web_gateway")
    @Headers("Content-Type:application/json; charset=utf-8")
    Observable<Response> register(@Body RequestBody body);

    @POST("web_gateway")
    @Headers("Content-Type:application/json; charset=utf-8")
    Observable<Response> login(@Body RequestBody body);

}
