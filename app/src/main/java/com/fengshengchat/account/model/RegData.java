package com.fengshengchat.account.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RegData implements Parcelable {

    public static final int N = 3;

    public String username;
    public String password;
    public String nickname;
    public String avatarUrl;
    public int[] questionIdList = new int[N];
    public String[] answerList = new String[3];

    public RegData() {
        reset();
    }

    public void reset() {
        for (int i = 0; i < questionIdList.length; i++) {
            questionIdList[i] = -1;
            answerList[i] = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeString(this.password);
        dest.writeString(this.nickname);
        dest.writeString(this.avatarUrl);
        dest.writeIntArray(this.questionIdList);
        dest.writeStringArray(this.answerList);
    }

    protected RegData(Parcel in) {
        this.username = in.readString();
        this.password = in.readString();
        this.nickname = in.readString();
        this.avatarUrl = in.readString();
        this.questionIdList = in.createIntArray();
        this.answerList = in.createStringArray();
    }

    public static final Creator<RegData> CREATOR = new Creator<RegData>() {
        @Override
        public RegData createFromParcel(Parcel source) {
            return new RegData(source);
        }

        @Override
        public RegData[] newArray(int size) {
            return new RegData[size];
        }
    };
}
