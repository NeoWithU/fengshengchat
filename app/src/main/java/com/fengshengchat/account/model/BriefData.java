package com.fengshengchat.account.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BriefData implements Parcelable {

    /**
     * 好友
     */
    public static final int FRIEND = 1;

    /**
     * 非好友
     */
    public static final int NON_FRIEND = 2;

    /**
     * 申请成为好友
     */
    public static final int APPLY_BECOME_FRIEND = 3;

    /**
     * 有共同群组
     */
    public static final int HAVE_TOGETHER_GROUP = 4;

    /**
     * 不确定是否是好友关系
     */
    public static final int NOT_SURE_FRIEND = 5;

    /**
     * 自己
     */
    public static final int SELF = 6;

    public int relative;
    public long id;
    public long groupId;
    public String nickname;
    public String avatar;
    public String bigAvatar;
    public int gender;
    public boolean isBlacklist;
    public boolean isMarkStar;
    public int groupType;

    public BriefData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.relative);
        dest.writeLong(this.id);
        dest.writeLong(this.groupId);
        dest.writeString(this.nickname);
        dest.writeString(this.avatar);
        dest.writeString(this.bigAvatar);
        dest.writeInt(this.gender);
        dest.writeByte(this.isBlacklist ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isMarkStar ? (byte) 1 : (byte) 0);
        dest.writeInt(this.groupType);
    }

    protected BriefData(Parcel in) {
        this.relative = in.readInt();
        this.id = in.readLong();
        this.groupId = in.readLong();
        this.nickname = in.readString();
        this.avatar = in.readString();
        this.bigAvatar = in.readString();
        this.gender = in.readInt();
        this.isBlacklist = in.readByte() != 0;
        this.isMarkStar = in.readByte() != 0;
        this.groupType = in.readInt();
    }

    public static final Creator<BriefData> CREATOR = new Creator<BriefData>() {
        @Override
        public BriefData createFromParcel(Parcel source) {
            return new BriefData(source);
        }

        @Override
        public BriefData[] newArray(int size) {
            return new BriefData[size];
        }
    };
}
