package com.fengshengchat.account.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Objects;

public class Question implements Parcelable {
    public final int id;
    public final String content;

    public Question(int id, String content) {
        this.id = id;
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.content);
    }

    protected Question(Parcel in) {
        this.id = in.readInt();
        this.content = in.readString();
    }

    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel source) {
            return new Question(source);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return id == question.id && Objects.equals(content, question.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content);
    }

    @NonNull
    @Override
    public String toString() {
        return content;
    }
}
