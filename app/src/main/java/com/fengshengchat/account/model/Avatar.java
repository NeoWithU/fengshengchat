package com.fengshengchat.account.model;

public class Avatar {

    /**
     * RetCode : 0
     * RetMsg : http://192.168.1.222:8888/group1/M00/00/01/wKgB3lwXB4KAUXWMAAwSxdWOc98386.jpg
     */

    private String RetCode;
    private String RetMsg;

    public String getRetCode() {
        return RetCode;
    }

    public void setRetCode(String RetCode) {
        this.RetCode = RetCode;
    }

    public String getRetMsg() {
        return RetMsg;
    }

    public void setRetMsg(String RetMsg) {
        this.RetMsg = RetMsg;
    }
}
