package com.fengshengchat.account.presenter;

import android.support.annotation.StringRes;
import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.R;
import com.fengshengchat.account.model.RegisterModel;
import com.fengshengchat.account.view.IRegisterView;
import com.fengshengchat.base.mvp.BasePresenter;
import com.fengshengchat.net.DefaultObserverCallback;
import com.fengshengchat.net.MVPObserverCallback;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.net.bean.Response;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.test.login.model.LoginModel;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import yiproto.yichat.account.Account;
import yiproto.yichat.login.Login;
import yiproto.yichat.msg.MsgOuterClass;

public class RegisterPresenter extends BasePresenter<RegisterModel, IRegisterView> {

    public RegisterPresenter(IRegisterView registerView){
        super(new RegisterModel(), registerView);
    }

    private boolean isMobile(String phoneNumber) {
        String MOBILE_PHONE_PATTERN = "^((13[0-9])|(15[0-9])|(18[0-9])|(14[7])|(17[0|6|7|8]))\\d{8}$";
        Pattern p = Pattern.compile(MOBILE_PHONE_PATTERN);
        Matcher m = p.matcher(phoneNumber);
        return m.matches();
    }

    private CharSequence getString(@StringRes int res){
        return getView().getContext().getString(res);
    }

    public void sendCode(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            getView().onCancel(getString(R.string.phone_not_empty));
            return;
        }

        if (!isMobile(phoneNumber)) {
            getView().onCancel(getString(R.string.phone_format_error));
            return;
        }

        Request request = new Request();
        getModel().checkPhoneAvailable(request, new MVPObserverCallback<Response>(getView()) {
            public void onResponse(Response response) {
                sendCode();
            }
            public void onError(Throwable e) {
                getView().setRegisterFailed(getString(R.string.phone_not_available));
            }
        });
    }

    private void sendCode(){
    }

    public void register(String phoneNumber, String password) {
        getView().onHandle(getString(R.string.do_register));
        if (TextUtils.isEmpty(phoneNumber)) {
            getView().onCancel(getString(R.string.phone_not_empty));
            return;
        }

        if (TextUtils.isEmpty(password)) {
            getView().onCancel(getString(R.string.password_not_empty));
            return;
        }

        if (!isMobile(phoneNumber)) {
            getView().onCancel(getString(R.string.phone_format_error));
            return;
        }

        Request request = new Request();
        PBMessage message = new PBMessage();
        message.setCmd(Account.AccountYimCmd.ACCOUNT_REGIST_REQ_CMD_VALUE);

        Account.RegistReq.Builder registerReq = Account.RegistReq.newBuilder();
        registerReq.setAccount(phoneNumber);
        registerReq.setPasswd(Protocol.encodePassword(password));
        registerReq.setImei(message.getIMEI());
        registerReq.setMac(message.getMAC());
        registerReq.setPlatform(message.getPlatform());
        message.setBody(registerReq);
        request.setMessage(message.PB());

        getModel().register(request, new MVPObserverCallback<Response>(getView()){
            public void onResponse(Response response) {
                MsgOuterClass.Msg message = response.getMessage();
                if(Account.AccountYimCmd.ACCOUNT_REGIST_REQ_CMD_VALUE == message.getCmd()){
                    getView().setRegisterSuccess(message.getErrMsg());
                }else{
                    onError(null);
                }
            }
            public void onError(Throwable e) {
                getView().setRegisterFailed(getString(R.string.register_error));
            }
        });
    }

    /**
     * 自动登录
     */
    public void autoLogin(long uid, String token) {
        getView().onHandle("自动登录...");
        PBMessage pbMessage = new PBMessage();
        pbMessage.setUid(uid)
                .setCmd(Login.LoginYimCMD.AUTO_LOGIN_REQ_CMD_VALUE)
                .setLoginInfoToken(token)
                .setBody(Login.AutoLoginReq.newBuilder()
                        .setUid(uid)
                        .setToken(token)
                        .setPlatform(MsgOuterClass.ClientType.CLIENT_TYPE_ANDRIOD_VALUE));

        //  创建登录请求
        Request request = new Request();
        request.setMessage(pbMessage.PB());

        //  开始执行登录请求
        getModel().login(request, new MVPObserverCallback<Response>(getView()) {
            public void onResponse(Response response) {
                handleAutoLoginRes(response);
            }

            public void onError(Throwable e) {
                getView().setRegisterFailed(e.getMessage());
            }
        });
    }

    private void handleAutoLoginRes(Response response) {
        Disposable subscribe = Single.just(response.getMessage())
                .filter(msg -> msg != null)
                .map(msg -> Login.AutoLoginRsp.parseFrom(msg.getBody()))
                .subscribe(data -> {
                    if (data.getResult() != Login.AutoLoginRsp.AutoLoginResult.OK_VALUE) {
                        getView().setRegisterSuccess(data.getErrMsg());
                        return;
                    }

                    loginUser = UserManager.getUser();
                    loginUser.token = data.getToken();

                    PBMessage pbMessage = new PBMessage();
                    pbMessage.setUid(loginUser.uid);
                    pbMessage.setCmd(MsgOuterClass.YimCMD.VERIFY_CONN_REQ_CMD_VALUE);
                    pbMessage.setLoginInfoToken(loginUser.token);
                    getView().getPusher().start(pbMessage);
                }, throwable -> getView().setLoginFailed("登录失败"));
    }

    public void login(String phoneNumber, String password){
        getView().onHandle("登录中...");
        Request request = new Request();

        PBMessage message = new PBMessage();
        message.setCmd(Login.LoginYimCMD.LOGIN_REQ_CMD_VALUE);

        Login.LoginReq.Builder loginReq = Login.LoginReq.newBuilder();
        loginReq.setPasswd(Protocol.encodePassword(password));
        loginReq.setPlatform(message.getPlatform());
        loginReq.setDeviceInfo(message.getDeviceInfoPB());

        Login.AccountInfo.Builder accountInfo = Login.AccountInfo.newBuilder();
        accountInfo.setAccount(phoneNumber);
        loginReq.setAccountInfo(accountInfo);

        message.setBody(loginReq);

        request.setMessage(message.PB());
        new LoginModel().login(request, new DefaultObserverCallback<Response>() {
            public void onResponse(Response response) {
                MsgOuterClass.Msg message = response.getMessage();
                if(Login.LoginYimCMD.LOGIN_RSP_CMD_VALUE == message.getCmd()){
                    try {
                        Login.LoginRsp loginRsp = Login.LoginRsp.parseFrom(message.getBody());
                        loginUser = new User();
//                        loginUser.account = phoneNumber;
                        loginUser.uid = loginRsp.getUid();
                        loginUser.token = loginRsp.getToken();

                        PBMessage pbMessage = new PBMessage();
                        pbMessage.setUid(loginRsp.getUid());
                        pbMessage.setCmd(MsgOuterClass.YimCMD.VERIFY_CONN_REQ_CMD_VALUE);
                        pbMessage.setLoginInfoToken(loginRsp.getToken());
                        getView().getPusher().start(pbMessage);
                    } catch (InvalidProtocolBufferException e) {
                        e.printStackTrace();
                    }
                }
            }
            public void onError(Throwable e) {
                getView().setRegisterFailed(e.getMessage());
            }
            public void onComplete() {
                getView().clearErrorState();
            }

            public void onFinally() {
                getView().onUpdateViewFinally();
            }
        });
    }

    private User loginUser;
    public void onReceiveMessage(MsgOuterClass.Msg msg){
        switch(msg.getCmd()){
            case MsgOuterClass.YimCMD.VERIFY_CONN_RSP_CMD_VALUE:
                if(BaseHandler.isNoError(msg.getCode()) || 101 == msg.getCode()) { //101 : repeat verify
                    getView().setLoginSuccess("");
                    LogUtils.e("find-token", "register" + loginUser.token);
                    UserManager.saveUser(loginUser);
                }else{
                    getView().setLoginFailed(msg.getErrMsg());
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        getModel().onDestroy();
    }
}
