package com.fengshengchat.account.fragment;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.SPUtils;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.BuildConfig;
import com.fengshengchat.MainActivity;
import com.fengshengchat.R;
import com.fengshengchat.YiChatApplication;
import com.fengshengchat.base.BKFragment;
import com.fengshengchat.base.BaseActivity;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import yiproto.yichat.login.Login;

public class LoginFragment extends BKFragment {
    public static final String ENTER_REASON = "enterReason";
    public static final String ENTER_REASON_TIP = "enterReasonTip";
    public static final String REASON_KICK_OUT = "KickOut";
    public static final String REASON_NO_NET = "noNet";
    public static final String REASON_TOKEN_INVALID = "tokenInvalid";

    public static final String TAG = "LoginFragment";

    @BindView(R.id.username_msg_tv)
    TextView usernameMsgTv;
    @BindView(R.id.password_msg_tv)
    TextView passwordMsgTv;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void initView(View root) {
        super.initView(root);
        String name = UserManager.getLastLoginUserName();
        if (null != name) {
            usernameEt.setText(name);
            usernameEt.setSelection(name.length());
        }
        Bundle arguments = getArguments();
        if (null != arguments) {
            String reason = arguments.getString(ENTER_REASON);
            if (REASON_KICK_OUT.equals(reason)) {
                showTip(arguments.getString(ENTER_REASON_TIP));
            } else if (REASON_NO_NET.equals(reason)) {
                showTip(getString(R.string.no_net));
            } else if (REASON_TOKEN_INVALID.equals(reason)) {
                showTip(getString(R.string.token_invalid));
            } else if (null != reason && reason.length() > 0) {
                showTip(reason);
            }
        }
        logo = findViewById(R.id.logo);
        slogan = findViewById(R.id.slogan);
        int height = (int) (getResources().getDisplayMetrics().density * 56);
//        addLayoutListener(root, height);

        if (BuildConfig.DEBUG) {
            logo.setOnLongClickListener(v -> {
                PopupMenu popupMenu = new PopupMenu(getContext(), v);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 0:
                                Protocol.setEnv(Protocol.ENV_222);
                                break;
                            case 1:
                                Protocol.setEnv(Protocol.ENV_103);
                                break;
                            case 2:
                                Protocol.setEnv(Protocol.ENV_FG);
                                break;
                        }
                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                                .setTitle("提示")
                                .setMessage("应用需要重新启动以切换环境")
                                .setCancelable(false)
                                .create();
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.show();
                        logo.postDelayed(() -> {
                            exitApp(getContext());
                        }, 1500);
                        return true;
                    }
                });
                Menu menu = popupMenu.getMenu();
                CharSequence item_222 = "测试环境 222";
                CharSequence item_103 = "外网环境 103";
                CharSequence item_FG = "外网环境 FeiGe";
                SpannableString spannableString;
                switch (Protocol.getEnv()) {
                    case Protocol.ENV_222:
                        spannableString = new SpannableString(item_222);
                        spannableString.setSpan(new ForegroundColorSpan(0xFF0000FF), 0, item_222.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        item_222 = spannableString;
                        break;
                    case Protocol.ENV_103:
                        spannableString = new SpannableString(item_103);
                        spannableString.setSpan(new ForegroundColorSpan(0xFF0000FF), 0, item_103.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        item_103 = spannableString;
                        break;
                    case Protocol.ENV_FG:
                        spannableString = new SpannableString(item_FG);
                        spannableString.setSpan(new ForegroundColorSpan(0xFF0000FF), 0, item_FG.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        item_FG = spannableString;
                        break;
                }
                menu.add(0, 0, 0, item_222);
                menu.add(0, 1, 1, item_103);
                menu.add(0, 2, 2, item_FG);
                popupMenu.show();
                return true;
            });
        }
    }

    public static void exitApp(Context context) {
        ActivityManager.RunningAppProcessInfo processByName = YiChatApplication.getProcessByName(context,
                context.getPackageName() + ":push");
        if (null != processByName) {
            Debug.e("exitApp", "---push exit---");
            Process.killProcess(processByName.pid);
        }
        processByName = YiChatApplication.getProcessByName(context,
                context.getPackageName() + ":pushcore");
        if (null != processByName) {
            Debug.e("exitApp", "---push core exit---");
            Process.killProcess(processByName.pid);
        }
        Debug.e("exitApp", "---main exit---");
        Process.killProcess(Process.myPid());
        System.exit(0);
    }

    private void showTip(String tip) {
        new AlertDialog.Builder(getActivity())
                .setTitle(com.fengshengchat.base.R.string.tip)
                .setMessage(tip)
                .setPositiveButton(R.string.confirm, (dialog, which) -> {
                    FragmentActivity activity = getActivity();
                    if (activity instanceof BaseActivity) {
                        ((BaseActivity) activity).dismissLoading();
                    }
                })
                .create()
                .show();
    }

    @Override
    public int getLayoutId() {
        return R.layout.user_login_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @BindView(R.id.username_et)
    public EditText usernameEt;

    @BindView(R.id.pwd_et)
    public EditText pwdEt;

    @BindView(R.id.login_btn)
    public Button loginBtn;

    /**
     * 打开注册账号页面
     */
    @OnClick(R.id.reg_account_tv)
    public void openRegAccount() {
        Reg1Fragment fragment = Reg1Fragment.newInstance();
        fragment.setTargetFragment(this, REQ_CODE_SAVE_REG_DATA);

        FragmentManager fm = getFragmentManager();
        if (fm == null) {
            return;
        }

        Fragment regFragment = fm.findFragmentByTag(Reg1Fragment.TAG);
        if (regFragment == null) {
            fm.beginTransaction()
                    .hide(this)
                    .add(R.id.fragment_container, fragment, Reg1Fragment.TAG)
                    .commit();
        } else {
            fm.beginTransaction()
                    .hide(this)
                    .show(regFragment)
                    .commit();
        }
    }

//    /**
//     * 打开忘记密码页面
//     */
//    @OnClick(R.id.forget_pwd_tv)
//    public void openForgetPwd() {
//        openFragment(Forget1PasswordFragment.newInstance(), Forget1PasswordFragment.TAG);
//    }

    /**
     * 登录
     */
    @OnClick(R.id.login_btn)
    public void login() {
        String username = usernameEt.getText().toString();
        if (TextUtils.isEmpty(username)) {
            toast(R.string.user_username_hint);
            return;
        }

        if (!RegexUtils.isMatch("^[A-Za-z0-9]+$", username)) {
            toast(R.string.user_username_err_1);
            return;
        }

        if (username.length() < 6) {
            toast(R.string.user_username_err_2);
            return;
        }

        String password = pwdEt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            toast(R.string.user_pwd_hint);
            return;
        }

        if (!RegexUtils.isMatch("^[\\x00-\\xff]+$", password)) {
            toast(R.string.user_password_err_1);
            return;
        }

        if (password.length() < 6) {
            toast(R.string.user_password_err_2);
            return;
        }

        KeyboardUtils.hideSoftInput(getActivity());
        execLogin(username, password);
    }

    public void onRegisterSuccess(String account, String pwd) {
        usernameEt.setText(account);
        pwdEt.setText(pwd);
        login();
    }

    /**
     * 执行登录
     */
    private void execLogin(String account, String pwd) {
        PBMessage pbMessage = new PBMessage();
        pbMessage.setCmd(Login.LoginYimCMD.LOGIN_REQ_CMD_VALUE);
        pbMessage.setBody(Login.LoginReq.newBuilder()
                .setPasswd(Protocol.encodePassword(pwd))
                .setPlatform(PBMessage.getPlatform())
                .setDeviceInfo(PBMessage.getDeviceInfoPB())
                .setAccountInfo(Login.AccountInfo.newBuilder().setAccount(account)));

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> Login.LoginRsp.parseFrom(message.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Login.LoginRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Login.LoginRsp data) {
                        handleLoginRes(data, account);
                    }
                });
    }

    /**
     * 处理登录结果
     */
    private void handleLoginRes(Login.LoginRsp data, String account) {
        if (data.getResult() != Login.LoginRsp.LoginResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        User user = new User();
        user.uid = data.getUid();
        user.token = data.getToken();
        user.account = account;
        user.expireTime = data.getExpireTime();
        printUserInfo(user);
        SPUtils.getInstance().put("accountSetComplete", true);
        List<Login.LoginTask> loginTaskListList = data.getLoginTaskListList();
        if (null != loginTaskListList) {
            for (Login.LoginTask loginTask : loginTaskListList) {
                switch (loginTask.getTaskType()) {
                    case Login.LoginTaskType.TASK_SET_USER_INFO_VALUE:
                        SPUtils.getInstance().put("accountSetComplete", false);
                        break;
                }
            }
        }
        LogUtils.e("find-token", "login-fragment" + user.token);
        UserManager.saveUser(user);
        MainActivity.toMainActivity(getActivity());
    }

    private void printUserInfo(User user) {
        printi("-----uid: " + user.uid);
        printi("-----token: " + user.token);
        printi("-----expireTime: " + user.expireTime);
    }

    View logo;
    View slogan;

//    public void addLayoutListener(final View main, final int height) {
//        main.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
//            Rect rect = new Rect();
//            main.getWindowVisibleDisplayFrame(rect);
//            int mainInvisibleHeight = main.getRootView().getHeight() - rect.bottom;
//            if (mainInvisibleHeight > height) {
//                logo.setVisibility(View.GONE);
//                slogan.setVisibility(View.GONE);
//            } else {
//                logo.setVisibility(View.VISIBLE);
//                slogan.setVisibility(View.VISIBLE);
//            }
//        });
//    }

    private String regUsername;
    private String regPassword;

    public static final int REQ_CODE_SAVE_REG_DATA = 1;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_SAVE_REG_DATA && resultCode == Activity.RESULT_OK) {
            regUsername = data.getStringExtra(Reg1Fragment.EXTRA_USERNAME);
            regPassword = data.getStringExtra(Reg1Fragment.EXTRA_PASSWORD);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        usernameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                usernameMsgTv.setText(null);
                check();
            }
        });
        usernameEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                return;
            }

            if (usernameEt == null) {
                return;
            }

            checkUsername(usernameEt.getText().toString());
        });

        pwdEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                passwordMsgTv.setText(null);
                check();
            }
        });
        pwdEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                return;
            }

            if (pwdEt == null) {
                return;
            }

            checkPassword(pwdEt.getText().toString());
        });
    }

    private void checkUsername(String username) {
        if (TextUtils.isEmpty(username)) {
            usernameMsgTv.setText(R.string.user_username_err_3);
            return;
        }

        if (!RegexUtils.isMatch("^[A-Za-z0-9]+$", username)) {
            usernameMsgTv.setText(R.string.user_username_err_1);
            return;
        }

        if (username.length() < 6) {
            usernameMsgTv.setText(R.string.user_username_err_2);
            return;
        }

        usernameMsgTv.setText(null);
    }

    private void checkPassword(String password) {
        if (TextUtils.isEmpty(password)) {
            passwordMsgTv.setText(R.string.user_password_err_1);
            return;
        }

        if (!RegexUtils.isMatch("^[\\x00-\\xff]+$", password)) {
            passwordMsgTv.setText(R.string.user_password_err_1);
            return;
        }

        if (password.length() < 6) {
            passwordMsgTv.setText(R.string.user_password_err_2);
            return;
        }

        passwordMsgTv.setText(null);
    }

    private void check() {
        boolean notEmpty = usernameEt.length() != 0 && pwdEt.length() != 0;
        loginBtn.setEnabled(notEmpty);
    }

}
