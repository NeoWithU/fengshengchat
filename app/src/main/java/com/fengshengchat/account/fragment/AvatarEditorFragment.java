package com.fengshengchat.account.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.Pair;
import android.view.View;
import android.widget.Button;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.SDCardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.model.Avatar;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.BitmapUtils;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.TitleBarLayout;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * 头像编辑器
 */
public class AvatarEditorFragment extends ToolbarFragment {

    /**
     * 图片文件大小
     */
    public static final int IMAGE_MAX_SIZE_BYTE = 2 * 1024 * 1024;  //  2M

    public static final String TAG = "AvatarEditorFragment";

    public static final int PICK_IMAGE_FROM_GALLERY_CODE = 10001;
    public static final int TAKE_PHOTO_REQUEST_CODE = 10002;

    public static final String EXTRA_PICTURE_OBTAIN_MODE = "PICTURE_OBTAIN_MODE";

    public static final String EXTRA_SMALL_AVATAR = "SMALL_AVATAR";
    public static final String EXTRA_BIG_AVATAR = "BIG_AVATAR";

    public static final int TYPE_TAKE_PHOTO = 1;
    public static final int TYPE_PHOTO_ALBUM = 2;

    public static AvatarEditorFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_PICTURE_OBTAIN_MODE, type);
        AvatarEditorFragment fragment = new AvatarEditorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.user_avatar_editor_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    private int type;

    private void initData() {
        Bundle args = getArguments();
        if (args == null) {
            return;
        }

        type = args.getInt(EXTRA_PICTURE_OBTAIN_MODE);
    }

    @BindView(R.id.title_bar)
    public TitleBarLayout titleBar;

    @Override
    public void onViewCreated(@android.support.annotation.NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                getFragmentManager().popBackStack();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
                save();
            }
        });
    }

    @BindView(R.id.crop_view)
    public CropImageView cropView;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cropView.post(this::obtainPicture);
    }

    /**
     * 准备保存头像
     */
    public void save() {
        if (cropView.getImageBitmap() == null) {
            ToastUtils.showShort(R.string.user_pick_picture_empty);
            return;
        }

        requestSaveAvatar();
    }

    /**
     * 请求保存头像
     */
    private void requestSaveAvatar() {
        Observable.combineLatest(getSmallAvatarObservable(), getBigAvatarObservable(), Pair::new)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Pair<String, String>>(getFragmentManager()) {
                    @Override
                    public void onNext(Pair<String, String> pair) {
                        handleSaveRes(pair.first, pair.second);
                    }
                });
    }

    @SuppressWarnings("Convert2MethodRef")
    private Observable<String> getSmallAvatarObservable() {
        return Observable.fromCallable(() -> cropView.getCroppedBitmap())
                .filter(bitmap -> bitmap != null)
                .map(bitmap -> BitmapUtils.getResizedBitmap(bitmap, 180, 180))
                .map(bitmap -> ConvertUtils.bitmap2Bytes(bitmap, Bitmap.CompressFormat.JPEG))
                .filter(bytes -> bytes.length != 0)
                .flatMap((Function<byte[], ObservableSource<Avatar>>) this::getUploadObservable)
                .map(this::getAvatarUrl)
                .observeOn(Schedulers.io());
    }

    @SuppressWarnings("Convert2MethodRef")
    private Observable<String> getBigAvatarObservable() {
        return Observable.fromCallable(() -> cropView.getCroppedBitmap())
                .filter(bitmap -> bitmap != null)
                .map(bm -> BitmapUtils.compressImage(bm, IMAGE_MAX_SIZE_BYTE))
                .filter(bytes -> bytes.length != 0)
                .flatMap((Function<byte[], ObservableSource<Avatar>>) this::getUploadObservable)
                .map(this::getAvatarUrl)
                .observeOn(Schedulers.io());
    }

    private String getAvatarUrl(Avatar avatar) {
        if (!"0".equals(avatar.getRetCode())) {
            return "";
        }
        return avatar.getRetMsg();
    }

    /**
     * 处理保存结果
     */
    private void handleSaveRes(String smallAvatar, String bigAvatar) {
        Intent data = new Intent();
        data.putExtra(EXTRA_SMALL_AVATAR, smallAvatar);
        data.putExtra(EXTRA_BIG_AVATAR, bigAvatar);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
        getFragmentManager().popBackStack();
    }

    /**
     * 获取上传 Observable
     */
    private ObservableSource<Avatar> getUploadObservable(byte[] bytes) {
        MultipartBody.Part part1 = MultipartBody.Part.createFormData("file", System.currentTimeMillis() + ".jpg", RequestBody.create(MultipartBody.FORM, bytes));
        MultipartBody.Part part2 = MultipartBody.Part.createFormData("info", getUid() + ",logo");
        return ApiManager.getInstance()
                .getApiService()
                .uploadPicture(Protocol.getUploadUrl(false), part1, part2);
    }

    private static long getUid() {
        try {
            return UserManager.getUser().uid;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 开始选择
     */
    public void obtainPicture() {
        if (type == TYPE_TAKE_PHOTO) {
            requestTakePhoto();
        } else if (type == TYPE_PHOTO_ALBUM) {
            requestChoosePicture();
        }
    }

    /**
     * 请求选择图片
     */
    private void requestChoosePicture() {
        new RxPermissions(getActivity())
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(@NonNull Boolean aBoolean) {
                        if (aBoolean) {
                            startActivityForResult(createChooserIntent(), PICK_IMAGE_FROM_GALLERY_CODE);
                        } else {
                            ToastUtils.showShort(R.string.user_read_write_permission_denied);
                            getFragmentManager().popBackStack();
                        }
                    }
                });
    }

    /**
     * 选择照片选择器 Intent
     */
    private Intent createChooserIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        return Intent.createChooser(intent, getString(R.string.user_choose_avatar));
    }

    /**
     * 请求拍照
     */
    private void requestTakePhoto() {
        new RxPermissions(getActivity())
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(@NonNull Boolean aBoolean) {
                        if (aBoolean) {
                            startCaptureImage();
                        } else {
                            ToastUtils.showShort(R.string.user_permission_denied);
                            getFragmentManager().popBackStack();
                        }
                    }
                });
    }

    /**
     * 开始拍照
     */
    private void startCaptureImage() {
        File file = getCaptureImageFile();
        if (file == null) {
            return;
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authority = getContext().getPackageName() + ".fileprovider";
            uri = FileProvider.getUriForFile(getContext().getApplicationContext(), authority, file);
        } else {
            uri = Uri.fromFile(getCaptureImageFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, TAKE_PHOTO_REQUEST_CODE);
    }

    private static final String CAPTURE_TEMP_FILENAME = "temp.jpg";
    private File captureImageFile;

    /**
     * 获取拍照存储的文件路径
     */
    private File getCaptureImageFile() {
        if (!SDCardUtils.isSDCardEnable()) {
            ToastUtils.showShort(R.string.sdcard_not_available);
            return null;
        }

        Context context = getContext();
        if (context == null) {
            return null;
        }

        File dir = context.getExternalFilesDir(Environment.DIRECTORY_DCIM);
        if (dir == null) {
            return null;
        }

        if (!dir.exists()) {
            boolean b = dir.mkdirs();
            if (!b) {
                return null;
            }
        }

        if (captureImageFile == null) {
            captureImageFile = new File(dir, CAPTURE_TEMP_FILENAME);
        }
        return captureImageFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            getFragmentManager().popBackStack();
            return;
        }

        Uri uri;
        switch (requestCode) {
            case PICK_IMAGE_FROM_GALLERY_CODE:
                uri = data.getData();
                break;

            case TAKE_PHOTO_REQUEST_CODE:
                uri = Uri.fromFile(getCaptureImageFile());
                break;

            default:
                return;
        }

        chooseAvatarBtn.setVisibility(View.GONE);
        cropView.load(uri).execute(new LoadCallback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError(Throwable e) {
            }
        });
    }

    @BindView(R.id.choose_avatar_btn)
    public Button chooseAvatarBtn;

    @OnClick(R.id.choose_avatar_btn)
    public void chooseAvatar() {
        obtainPicture();
    }

}
