package com.fengshengchat.account.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.model.LengthFilter;
import com.fengshengchat.app.dialog.BottomDialogFragment;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.PickerDialogStringFragment;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.TitleBarLayout;

import butterknife.BindView;
import butterknife.OnClick;
import yiproto.yichat.info.Info;

/**
 * 注册页面：设置个人信息
 */
public class Reg2Fragment extends ToolbarFragment {

    public static final String TAG = "Reg2Fragment";

    public static Reg2Fragment newInstance() {
        return new Reg2Fragment();
    }

    @Override
    public int getLayoutId() {
        return R.layout.user_reg_2_fragment;
    }

    private String smallAvatar;
    private String bigAvatar;

    @BindView(R.id.avatar_iv)
    ImageView avatarIv;
    @BindView(R.id.nickname_et)
    EditText nicknameEt;
    @BindView(R.id.submit_btn)
    Button submitBtn;

    /**
     * 下一步
     */
    @OnClick(R.id.submit_btn)
    public void submit() {
        if (TextUtils.isEmpty(smallAvatar)) {
            toast(R.string.user_set_avatar_hint);
            return;
        }

        String nickname = nicknameEt.getText().toString();
        if (TextUtils.isEmpty(nickname)) {
            toast(R.string.user_set_nickname_hint);
            return;
        }

        if (nickname.trim().length() == 0) {
            toast(R.string.user_set_nickname_hint);
            return;
        }

        KeyboardUtils.hideSoftInput(getActivity());
//
//        Reg3Fragment fragment = Reg3Fragment.newInstance(regData);
//        openFragment(fragment, Reg3Fragment.TAG);
        requestModifyAvatar(nickname);
    }

    @Override
    public void initView(View root) {
        super.initView(root);
        ((TitleBarLayout) findViewById(R.id.title_bar)).setShowHome(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        nicknameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                updateSubmitBtn();
            }
        });
        nicknameEt.setFilters(new InputFilter[]{new LengthFilter(26)});
    }

    private void updateSubmitBtn() {
        boolean notEmpty = nicknameEt.length() != 0 && !TextUtils.isEmpty(smallAvatar);
        submitBtn.setEnabled(notEmpty);
    }

    /**
     * 选择头像
     */
    @OnClick(R.id.avatar_layout)
    public void chooseAvatarLayout() {
        Resources res = getResources();
        String[] array = res.getStringArray(R.array.user_picker_type_list);
//        ArrayList<String> list = new ArrayList<>(Arrays.asList(array));
//        PickerDialogStringFragment fragment = PickerDialogStringFragment.newInstance(list);
//        fragment.setTargetFragment(this, REQ_CODE_PICK_PHOTO_TYPE);
//        fragment.show(getFragmentManager(), PickerDialogStringFragment.TAG);
        BottomDialogFragment fragment = BottomDialogFragment.newInstance(array);
        fragment.setTargetFragment(this, REQ_CODE_PICK_PHOTO_TYPE);
        fragment.show(getFragmentManager(), PickerDialogStringFragment.TAG);
    }

    /**
     * 请求码：选择照片方式
     */
    public static final int REQ_CODE_PICK_PHOTO_TYPE = 1;
    /**
     * 请求码：获取头像地址
     */
    public static final int REQ_CODE_GET_AVATAR_URL = 2;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_PICK_PHOTO_TYPE:
                handlePickPhotoType(resultCode, data);
                break;

            case REQ_CODE_GET_AVATAR_URL:
                handleAvatarUrl(resultCode, data);
                break;

            default:
                break;
        }
    }

    /**
     * 处理选择照片方式
     */
    private void handlePickPhotoType(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        int position = data.getIntExtra(BottomDialogFragment.EXTRA_POSITION, -1);
        if (position == -1) {
            return;
        }

        int type = position + 1;
        AvatarEditorFragment fragment = AvatarEditorFragment.newInstance(type);
        fragment.setTargetFragment(this, REQ_CODE_GET_AVATAR_URL);
        openFragment(fragment, AvatarEditorFragment.TAG);
    }

    /**
     * 处理获取头像地址
     */
    private void handleAvatarUrl(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        smallAvatar = data.getStringExtra(AvatarEditorFragment.EXTRA_SMALL_AVATAR);
        bigAvatar = data.getStringExtra(AvatarEditorFragment.EXTRA_BIG_AVATAR);
        updateSubmitBtn();
        LogUtils.e("2头像-链接","--" + smallAvatar + "--"+bigAvatar);
        ImageUtils.loadImage(avatarIv, smallAvatar, R.drawable.user_ic_upload_avatar);
    }

    private void requestModifyAvatar(String nickname) {
        Info.UserInfoDetail.Builder detail = Info.UserInfoDetail
                .newBuilder()
                .setNickname(nickname)
                .setBigAvatar(smallAvatar)
                .setSmallAvatar(bigAvatar);

        Info.SetUserInfoReq.Builder body = Info.SetUserInfoReq
                .newBuilder()
                .setUserInfo(detail);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.SET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.SetUserInfoRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Info.SetUserInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Info.SetUserInfoRsp data) {
                        handleModifyAvatarRes(data, nickname);
                    }
                });
    }

    /**
     * 处理修改头像结果
     */
    private void handleModifyAvatarRes(Info.SetUserInfoRsp data, String nickname) {
        if (data.getResult() != Info.SetUserInfoRsp.SetUserInfoResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

   //     ImageUtils.loadRadiusImage(avatarIv, smallAvatar, R.drawable.image_default, 5);
        ImageUtils.loadRadiusImage(avatarIv, smallAvatar, R.mipmap.default_head_icon, 5);
        LogUtils.e("find-token", "reg" + UserManager.getUser().token);
        LogUtils.e("name-头像-链接",nickname + "--" + smallAvatar + "--"+bigAvatar);
        UserManager.save(nickname, smallAvatar, bigAvatar);
        try {
            SPUtils.getInstance().put("accountSetComplete", true);
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
