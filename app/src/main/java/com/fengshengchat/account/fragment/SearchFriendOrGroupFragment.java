package com.fengshengchat.account.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.dialog.MyQrcodeDialogFragment;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.app.activity.QrcodeCaptureActivity;
import com.fengshengchat.app.activity.ScanResultActivity;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.ClearEditText;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.group.Group;

/**
 * 搜索好友或群
 */
public class SearchFriendOrGroupFragment extends ToolbarFragment {

    public static final String TAG = "SearchFriendOrGroupFragment";

    public static SearchFriendOrGroupFragment newInstance() {
        Bundle args = new Bundle();
        SearchFriendOrGroupFragment fragment = new SearchFriendOrGroupFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.search_friend_or_group;
    }

    @BindView(R.id.search_et)
    ClearEditText searchEt;
    @BindView(R.id.search_prompt_layout)
    ViewGroup promptPanel;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (TextUtils.isEmpty(text)) {
                    showPromptPanel();
                } else {
                    showResultPanel();
                    searchFriendResultTv.setText(text);
                    searchGroupResultTv.setText(text);
                }
            }
        });
    }

    @OnClick(R.id.scan_layout)
    public void openQrCodeUI() {
        openQrcodeActivity(getActivity());
    }

    public static void openQrcodeActivity(FragmentActivity activity) {
        new RxPermissions(activity)
                .request(Manifest.permission.CAMERA)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (!aBoolean) {
                            ToastUtils.showShort("请前往设置开启相机权限");
                            return;
                        }

                        new IntentIntegrator(activity)
                                .setPrompt("将二维码放入框内")
                                .setOrientationLocked(true)
                                .setCaptureActivity(QrcodeCaptureActivity.class)
                                .initiateScan(); // 初始化扫描
                    }
                });
    }

    @OnClick(R.id.sms_invite_layout)
    public void openSmsInvite() {
        InvestPhoneContactsFragment fragment = InvestPhoneContactsFragment.newInstance();
        openFragment(fragment, InvestPhoneContactsFragment.TAG);
    }

    @OnClick(R.id.my_qrcode_tv)
    public void showMyQrcode() {
        MyQrcodeDialogFragment dialogFragment = MyQrcodeDialogFragment.newInstance();
        dialogFragment.show(getFragmentManager(), MyQrcodeDialogFragment.TAG);
    }

    private void showPromptPanel() {
        promptPanel.setVisibility(View.VISIBLE);
        getResultPanel().setVisibility(View.GONE);
    }

    private void showResultPanel() {
        promptPanel.setVisibility(View.GONE);
        getResultPanel().setVisibility(View.VISIBLE);
    }

    @Nullable
    private ViewGroup resultPanel;
    private TextView searchFriendResultTv;
    private TextView searchGroupResultTv;

    @NonNull
    private ViewGroup getResultPanel() {
        if (resultPanel != null) {
            return resultPanel;
        }

        ViewStub resultPanelViewStub = findViewById(R.id.result_panel_view_stub);
        resultPanel = (ViewGroup) resultPanelViewStub.inflate();

        searchFriendResultTv = resultPanel.findViewById(R.id.search_friend_result_tv);
        resultPanel.findViewById(R.id.search_friend_layout).setOnClickListener(v -> searchFriend());

        searchGroupResultTv = resultPanel.findViewById(R.id.search_group_result_tv);
        resultPanel.findViewById(R.id.search_group_layout).setOnClickListener(v -> searchGroup());
        return resultPanel;
    }

    /**
     * 搜索好友
     */
    private void searchFriend() {
        Editable editableText = searchEt.getText();
        if (editableText == null) {
            return;
        }

        String text = editableText.toString();
        if (TextUtils.isEmpty(text)) {
            return;
        }

        long friendId;
        try {
            friendId = Long.parseLong(text);
        } catch (Exception e) {
            toast(R.string.search_not_exist_friend);
            LogUtils.e(e);
            return;
        }

        if (UserManager.getUser().uid == friendId) {
            toast(R.string.search_not_allow_search_self);
            return;
        }

        requestSearchFriend(friendId);
    }

    /**
     * 请求搜索好友
     */
    private void requestSearchFriend(long friendId) {
        Friend.SearchFriendReq.Builder body = Friend.SearchFriendReq.newBuilder()
                .setTargetUserId(friendId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.SEARCH_FRIEND_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> Friend.SearchFriendRsp.parseFrom(message.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(data -> {
                    if (data.getResult() == Friend.SearchFriendRsp.Result.NOT_EXIST_VALUE) {
                        toast(R.string.search_not_exist_friend);
                    } else if (data.getResult() != Friend.SearchFriendRsp.Result.OK_VALUE) {
                        toast(data.getErrMsg());
                        return;
                    }

                    if (!data.hasUserInfo()) {
                        toast(R.string.search_not_exist_friend);
                    }
                })
                .filter(data -> data.getResult() == Friend.SearchFriendRsp.Result.OK_VALUE)
                .observeOn(Schedulers.io())
                .map(SearchFriendOrGroupFragment::getBriefData)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new HttpRequestDialogObserver<BriefData>(getFragmentManager()) {
                    @Override
                    public void onNext(BriefData briefData) {
                        KeyboardUtils.hideSoftInput(getActivity());
                        FriendBriefInfoFragment fragment = FriendBriefInfoFragment.newInstance(briefData);
                        openFragment(fragment, FriendBriefInfoFragment.TAG);
                    }
                });
    }

    @NonNull
    public static BriefData getBriefData(Friend.SearchFriendRsp data) {
        Friend.FriendUserInfo userInfo = data.getUserInfo();
        BriefData briefData = new BriefData();
        briefData.id = userInfo.getUserId();
        briefData.nickname = userInfo.getNickName();
        briefData.avatar = userInfo.getSmallAvatar();
        briefData.bigAvatar = userInfo.getBigAvatar();
        briefData.gender = userInfo.getSex();
        briefData.relative = userInfo.getIsFriend() == 1 ? BriefData.FRIEND : BriefData.NON_FRIEND;
        return briefData;
    }

    /**
     * 搜索群
     */
    private void searchGroup() {
        Editable editableText = searchEt.getText();
        if (editableText == null) {
            return;
        }

        String text = editableText.toString();
        if (TextUtils.isEmpty(text)) {
            return;
        }

        long groupId;
        try {
            groupId = Long.parseLong(text);
        } catch (Exception e) {
            LogUtils.e(e);
            toast(R.string.search_not_exist_group);
            return;
        }

        requestSearchGroup(groupId, 0);
    }

    /**
     * 请求搜索群
     */
    private void requestSearchGroup(long groupId, long userId) {
        Group.SearchGroupReq.Builder body = Group.SearchGroupReq
                .newBuilder()
                .addGroupIdList(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.SEARCH_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> Group.SearchGroupRsp.parseFrom(message.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Group.SearchGroupRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.SearchGroupRsp data) {
                        handleSearchGroupRes(data, groupId, userId);
                    }
                });
    }

    /**
     * 处理请求搜索好友结果
     */
    private void handleSearchGroupRes(Group.SearchGroupRsp data, long groupId, long userId) {
        if (data.getResult() != Group.SearchGroupRsp.SearchGroupResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        if (data.getGroupInfoListCount() == 0) {
            toast(R.string.search_not_exist_group);
            return;
        }

        openFragment(GroupInfoFragment.newInstance(groupId, userId), GroupInfoFragment.TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (null != intentResult && null != intentResult.getContents()) {
            String s = intentResult.getContents();
            handleQrcode(s);
            return;
        } else {
            if (IntentIntegrator.REQUEST_CODE == requestCode && resultCode == Activity.RESULT_OK) {
                String qrCode = data.getStringExtra(QrcodeCaptureActivity.EXTRA_CONTENT);
                if (!TextUtils.isEmpty(qrCode)) {
                    handleQrcode(qrCode);
                }
                return;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleQrcode(String s) {
        try {
            if (Protocol.isAuthUrl(s)) {
                Protocol.requestAuth(s);
                return;
            }

            if(Protocol.isWebLogin(s)){
                Protocol.requestWebLogin(s);
                return;
            }

            long id = Protocol.extractGid(s);
            if (0L != id) {
                requestSearchGroup(id, Protocol.extractUid(s));
            } else {
                id = Protocol.extractUid(s);
                if (0L != id) {
                    if (id == UserManager.getUser().uid) {
                        toast(R.string.search_self_uid);
                        return;
                    }
                    requestSearchFriend(id);
                } else {
                    Intent intent = new Intent(getContext(), ScanResultActivity.class);
                    intent.putExtra(ScanResultActivity.EXTRA_DATA, s);
                    startActivity(intent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
