package com.fengshengchat.account.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.hwangjr.rxbus.RxBus;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.MainActivity;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.dialog.ConfirmDialogFragment;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.HttpRequestObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.chat.view.ChatActivity;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.AppDatabase;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.GroupMember;
import com.fengshengchat.db.helper.AccountHelper;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.group.activity.SelectContactActivity;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.EmptyObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.ImagePreviewActivity;
import com.fengshengchat.widget.ItemLayout;
import com.fengshengchat.widget.TitleBarLayout;
import com.zaaach.toprightmenu.MenuItem;
import com.zaaach.toprightmenu.TopRightMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.group.Group;
import yiproto.yichat.info.Info;

/**
 * 朋友信息
 */
public class FriendBriefInfoFragment extends ToolbarFragment {

    public static final String TAG = "FriendBriefInfoFragment";

    public static final int REQ_CODE_DEL_FRIEND = 1;
    public static final int REQ_CODE_ADD_BLACKLIST = 2;
    public static final int SELECT_FRIEND_CARD = 3;

    private BriefData briefData;

    @BindView(R.id.title_bar)
    TitleBarLayout titleBar;
    @BindView(R.id.avatar_iv)
    ImageView avatarIv;
    @BindView(R.id.name_tv)
    TextView nameTv;
    @BindView(R.id.id_tv)
    TextView idTv;
    @BindView(R.id.gender_iv)
    ImageView genderIv;
    @BindView(R.id.set_star_layout)
    ItemLayout setStartLayout;
    @BindView(R.id.share_contact_btn)
    Button shareContactBtn;
    @BindView(R.id.voice_calls_btn)
    Button voiceCallsBtn;
    @BindView(R.id.submit_btn)
    Button submitButton;
    @BindView(R.id.address_layout)
    ItemLayout addressLayout;

    public static FriendBriefInfoFragment newInstance(BriefData briefData) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, briefData);
        FriendBriefInfoFragment fragment = new FriendBriefInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.search_friend_brief_info_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        briefData = args.getParcelable(EXTRA_DATA);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitleBar();
        bindBaseInfo(briefData.id, briefData.nickname, briefData.avatar, briefData.gender);
        bindFriendRelationUI(briefData.relative);
        if (briefData.relative == BriefData.FRIEND && briefData.isMarkStar) {
            setStartLayout.getSwitchCompat().setChecked(true);
        }
    }

    /**
     * 绑定基础信息
     */
    private void bindBaseInfo(long id, String nickname, String avatar, int gender) {
        nameTv.setText(nickname);
        idTv.setText(getString(R.string.user_yichat_num_format, String.valueOf(id)));
     //   ImageUtils.loadRadiusImage(avatarIv, avatar, R.drawable.image_default, AppConfig.IMAGE_RADIUS);
        ImageUtils.loadRadiusImage(avatarIv, avatar, R.mipmap.default_head_icon, AppConfig.IMAGE_RADIUS);
        if (gender == Info.SexType.SEX_MALE_VALUE) {
            genderIv.setImageResource(R.drawable.common_ic_man);
        } else if (gender == Info.SexType.SEX_FEMALE_VALUE) {
            genderIv.setImageResource(R.drawable.common_ic_woman);
        } else {
            genderIv.setImageBitmap(null);
        }
    }

    /**
     * 绑定好友关系信息
     */
    private void bindFriendRelationUI(int type) {
        if (!isSelf() && (type == BriefData.FRIEND || type == BriefData.APPLY_BECOME_FRIEND)) {
            setStartLayout.setVisibility(View.VISIBLE);
            SwitchCompat starSwitch = setStartLayout.getSwitchCompat();
            starSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (buttonView.isPressed()) {
                    requestSetStar(isChecked);
                }
            });

            if (type == BriefData.FRIEND) {
                submitButton.setText(R.string.search_send_msg);
                shareContactBtn.setVisibility(View.VISIBLE);
                voiceCallsBtn.setVisibility(View.VISIBLE);
            } else {
                submitButton.setText(R.string.search_agree_apply);
                shareContactBtn.setVisibility(View.GONE);
                voiceCallsBtn.setVisibility(View.GONE);
            }
            titleBar.getActionImage().setVisibility(View.VISIBLE);
        } else {
            submitButton.setText(R.string.search_add_contacts);
            setStartLayout.setVisibility(View.GONE);
            shareContactBtn.setVisibility(View.GONE);
            voiceCallsBtn.setVisibility(View.GONE);
            titleBar.getActionImage().setVisibility(View.GONE);
        }

        if (isSelf()) {
            submitButton.setVisibility(View.GONE);
        }

        //  娱乐群不显示
        boolean isHide = briefData.groupId != 0L
                && briefData.groupType == Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE
                && (briefData.relative != BriefData.SELF && briefData.relative != BriefData.FRIEND && !isSelf());
        idTv.setVisibility(isHide ? View.GONE : View.VISIBLE);
    }

    /**
     * 初始化 title bar
     */
    private void initTitleBar() {
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                onBack();
            }

            @Override
            public void onActionImageClick() {
                if (briefData.relative == BriefData.FRIEND) {
                    showMoreMenu(titleBar);
                }
            }

            @Override
            public void onActionClick() {
            }
        });
    }

    /**
     * 显示更多菜单
     */
    private void showMoreMenu(View more) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }

        List<MenuItem> menuItems = new ArrayList<>();
        if (briefData.isBlacklist) {
            menuItems.add(new MenuItem(R.drawable.search_ic_blacklist, getString(R.string.search_remove_blacklist)));
        } else {
            menuItems.add(new MenuItem(R.drawable.search_ic_blacklist, getString(R.string.search_add_blacklist)));
        }
        menuItems.add(new MenuItem(R.drawable.search_ic_del, getString(R.string.search_delete)));

        TopRightMenu moreMenu = new TopRightMenu(activity);
        moreMenu.showIcon(true)
                .dimBackground(true)
                .needAnimationStyle(true)
                .setWidth(dp2px(150))
                .setHeight(dp2px(44 * menuItems.size() + 10))
                .setAnimationStyle(R.style.TRM_ANIM_STYLE)
                .addMenuList(menuItems)
                .setOnMenuItemClickListener(this::onMoreItemClick);
        moreMenu.showAsDropDown(more, dp2px(330), 0);
    }

    /**
     * 点击菜单项
     */
    private void onMoreItemClick(int position) {
        switch (position) {
            case 0: { //  黑名单操作
                if (briefData.isBlacklist) {
                    String title = getString(R.string.search_remove_blacklist_title);
                    String action = getString(R.string.search_remove_blacklist);
                    ConfirmDialogFragment fragment = ConfirmDialogFragment.newInstance(title, action);
                    fragment.setTargetFragment(this, REQ_CODE_ADD_BLACKLIST);
                    fragment.show(getFragmentManager(), ConfirmDialogFragment.TAG);
                } else {
                    String title = getString(R.string.search_blacklist_title);
                    String action = getString(R.string.add_to_black_list);
                    ConfirmDialogFragment fragment = ConfirmDialogFragment.newInstance(title, action);
                    fragment.setTargetFragment(this, REQ_CODE_ADD_BLACKLIST);
                    fragment.show(getFragmentManager(), ConfirmDialogFragment.TAG);
                }
                break;
            }

            case 1: { //  删除好友
                String title = getString(R.string.search_del_friend_title, briefData.nickname);
                String action = getString(R.string.delete);
                ConfirmDialogFragment fragment = ConfirmDialogFragment.newInstance(title, action);
                fragment.setTargetFragment(this, REQ_CODE_DEL_FRIEND);
                fragment.show(getFragmentManager(), ConfirmDialogFragment.TAG);
                break;
            }

            case 3:
                break;
        }
    }

    /**
     * 点击头像
     */
    @OnClick(R.id.avatar_iv)
    public void onClickAvatar() {
        if (TextUtils.isEmpty(briefData.avatar)) {
            return;
        }

        Intent intent = new Intent(getContext(), ImagePreviewActivity.class);
        intent.putExtra("imagePath", TextUtils.isEmpty(briefData.bigAvatar) ? briefData.avatar : briefData.bigAvatar);
        intent.putExtra("thumbImagePath", briefData.avatar);
        if (Build.VERSION.SDK_INT >= 22) {
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), avatarIv, getString(R.string.share_element_tag));
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    /**
     * 点击提交按钮
     */
    @OnClick(R.id.submit_btn)
    public void submit() {
        if (briefData.relative == BriefData.FRIEND) {
            ChatActivity.startSingleChat(getContext(), briefData.id, nameTv.getText().toString());
            try {
                getActivity().finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (briefData.relative == BriefData.APPLY_BECOME_FRIEND) {
            agreeAddFriendRequest();
        } else {
            requestAddFriend();
        }
    }

    /**
     * 同意好友请求
     */
    private void agreeAddFriendRequest() {
        Friend.AgreeAddFriendReq.Builder body = Friend.AgreeAddFriendReq
                .newBuilder()
                .setTargetUserId(briefData.id);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.AGREE_ADD_FRIEND_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> Friend.AgreeAddFriendRsp.parseFrom(message.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Friend.AgreeAddFriendRsp>() {
                    @Override
                    public void onNext(Friend.AgreeAddFriendRsp data) {
                        handleAgreeFriendReq(data);
                    }
                });
    }

    /**
     * 处理同意好友请求
     */
    private void handleAgreeFriendReq(Friend.AgreeAddFriendRsp data) {
        int result = data.getResult();
        if (result != Friend.AgreeAddFriendRsp.AgreeAddFriendResult.OK_VALUE && result != Friend.AgreeAddFriendRsp.AgreeAddFriendResult.FRIEND_EXIST_VALUE) {
            ToastUtils.showShort(data.getErrMsg());
            return;
        }

        briefData.relative = BriefData.FRIEND;
        bindFriendRelationUI(briefData.relative);
        ChatActivity.startChat(getContext(), briefData.id, false);
    }

    /**
     * 请求添加为好友
     */
    private void requestAddFriend() {
//        Observable.combineLatest(getAddFriendObservable(), getIsDeletedStatusObservable(), Pair::new)
//                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
//                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
//                .subscribe(new HttpRequestDialogObserver<Pair<Friend.AddFriendRsp, Boolean>>(getFragmentManager()) {
//                    @Override
//                    public void onNext(Pair<Friend.AddFriendRsp, Boolean> pair) {
//                        Friend.AddFriendRsp data = pair.first;
//                        int result = data.getResult();
//                        if (result == Friend.AddFriendRsp.AddFriendResult.TARGET_IN_FRIEND_VALUE) {
//                            bindFriendRelationUI(BriefData.FRIEND);
//                        } else if (result != Friend.AddFriendRsp.AddFriendResult.OK_VALUE) {
//                            toast(data.getErrMsg());
//                        }
//
//                        if (pair.second) {
//                            ChatActivity.startChat(getContext(), briefData.id, false);
//                            finish();
//                        } else {
//                            toast(R.string.search_req_verify);
//                        }
//                    }
//                });
        getAddFriendObservable()
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Friend.AddFriendRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Friend.AddFriendRsp data) {
                        int result = data.getResult();
                        if (result != Friend.AddFriendRsp.AddFriendResult.OK_VALUE) {
                            toast(data.getErrMsg());
                            return;
                        }

                        if (data.getIsFriend() == 1) {
                            briefData.relative = BriefData.FRIEND;
                            bindFriendRelationUI(briefData.relative);
                            ChatActivity.startChat(getContext(), briefData.id, false);
                            finish();
                        } else {
                            toast(R.string.search_req_verify);
                        }
                    }
                });
    }

    /**
     * 该id是否是删除状态
     */
    private Observable<Boolean> getIsDeletedStatusObservable() {
        return getGetFriendInfoRspObservable()
                .map(data -> data.getResult() == 0 && data.getFriendListCount() == 1 && data.getFriendList(0).getIsFriend() == 1);
    }

    /**
     * 请求添加为好友
     */
    private Observable<Friend.AddFriendRsp> getAddFriendObservable() {
        Friend.AddFriendReq.Builder body = Friend.AddFriendReq
                .newBuilder()
                .setTargetUserId(briefData.id);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.ADD_FRIEND_REQ_CMD_VALUE)
                .setBody(body);

        return ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> Friend.AddFriendRsp.parseFrom(message.getBody()));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateData();
    }

    /**
     * 更新数据
     */
    private void updateData() {
        if (briefData.relative == BriefData.FRIEND) {
            requestUserInfoFromDb();
        } else if (briefData.relative == BriefData.HAVE_TOGETHER_GROUP) {
            requestUserInfo(getHaveTogetherGroupCallback());
        } else if (briefData.relative == BriefData.NOT_SURE_FRIEND) {
            requestFriendStatus();
        } else if (briefData.relative == BriefData.SELF) {
            requestUserAddress();
        }
    }

    /**
     * 请求用户信息
     */
    private void requestUserAddress() {
        Info.GetUserInfoReq.Builder body = Info.GetUserInfoReq
                .newBuilder()
                .addUserIdList(briefData.id);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.GET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.GetUserInfoRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestObserver<Info.GetUserInfoRsp>() {
                    @Override
                    public void onNext(Info.GetUserInfoRsp data) {
                        Info.UserInfoDetail detail = data.getUserInfoListList().get(0);
                        briefData.nickname = detail.getNickname();
                        briefData.avatar = detail.getSmallAvatar();
                        briefData.gender = detail.getSex();
                        briefData.bigAvatar = detail.getBigAvatar();
                        bindBaseInfo(briefData.id, briefData.nickname, briefData.avatar, briefData.gender);
                        addressLayout.getMsgTv().setText(Account.getAddress(detail.getProvince(), detail.getCity()));
                        updateGroupMemberData(detail.getNickname(), detail.getSmallAvatar(), detail.getBigAvatar());
                    }
                });
    }

    private void requestFriendStatus() {
        Observable.fromCallable(() -> {
            Account account = DbManager.getDb().getAccountDao().findById(briefData.id);
            return account == null ? new Account() : account;
        }).filter(account -> account.id != 0)
                .map(com.fengshengchat.contacts.model.Friend::new)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<com.fengshengchat.contacts.model.Friend>() {
                    boolean exist;

                    @Override
                    public void onNext(com.fengshengchat.contacts.model.Friend friend) {
                        exist = true;
                        briefData.relative = !friend.isDeleted ? BriefData.FRIEND : BriefData.NON_FRIEND;
                        bindUserInfo(friend);
                        bindFriendRelationUI(briefData.relative);
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        if (exist) {
                            requestUpdateUserInfo();
                        } else {
                            requestUserInfo(getHaveTogetherGroupCallback());
                        }
                    }
                });
    }

    /**
     * 请求更新好友信息
     */
    private void requestUpdateUserInfo() {
        getGetFriendInfoRspObservable()
                .filter(msg -> msg.getResult() == 0 && msg.getFriendListCount() == 1)
                .map(data -> {
                    Friend.FriendUserInfo userInfo = data.getFriendList(0);
                    Account account = AccountHelper.getAccount(userInfo);
                    DbManager.getDb().getAccountDao().update(account);
                    if (briefData.groupId != 0) {
                        updateGroupMemberData(userInfo.getNickName(), userInfo.getSmallAvatar(), userInfo.getBigAvatar());
                    }
                    return new com.fengshengchat.contacts.model.Friend(account);
                })
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .doOnNext(this::bindUserInfo)
                .subscribe(getUpdateFriendObserver());
    }

    @NonNull
    private BaseObserver<com.fengshengchat.contacts.model.Friend> getUpdateFriendObserver() {
        return new BaseObserver<com.fengshengchat.contacts.model.Friend>() {
            @Override
            public void onNext(com.fengshengchat.contacts.model.Friend friend) {
                RxBus.get().post(BusEvent.TAG_UPDATE_SINGLE_FRIEND, friend.id);
            }
        };
    }

    private Observable<Friend.GetFriendInfoRsp> getGetFriendInfoRspObservable() {
        Friend.GetFriendInfoReq.Builder body = Friend.GetFriendInfoReq
                .newBuilder()
                .addUserIdList(briefData.id);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.GET_FRIEND_INFO_REQ_CMD_VALUE)
                .setBody(body);

        return ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Friend.GetFriendInfoRsp.parseFrom(msg.getBody()));
    }

    @NonNull
    private BaseObserver<Info.GetUserInfoRsp> getHaveTogetherGroupCallback() {
        return new BaseObserver<Info.GetUserInfoRsp>() {
            @Override
            public void onNext(Info.GetUserInfoRsp data) {
                if (data.getResult() != 0) {
                    return;
                }

                if (data.getUserInfoListCount() == 0) {
                    return;
                }

                Info.UserInfoDetail detail = data.getUserInfoListList().get(0);
                briefData.nickname = detail.getNickname();
                briefData.avatar = detail.getSmallAvatar();
                briefData.gender = detail.getSex();
                briefData.bigAvatar = detail.getBigAvatar();
                bindBaseInfo(briefData.id, briefData.nickname, briefData.avatar, briefData.gender);
                addressLayout.getMsgTv().setText(Account.getAddress(detail.getProvince(), detail.getCity()));
                updateGroupMemberData(detail.getNickname(), detail.getSmallAvatar(), detail.getBigAvatar());
            }
        };
    }

    /**
     * 更新群成员数据库
     */
    private void updateGroupMemberData(String nickname, String avatar, String bigAvatar) {
        if (briefData.groupId == 0) {
            return;
        }

        Observable.fromCallable(() -> DbManager.getDb().getGroupMemberDao().find(briefData.groupId, briefData.id, Group.DeleteFlag.DELETE_UNDEFINED_VALUE))
                .doOnNext(groupMember -> {
                    groupMember.nickname = nickname;
                    groupMember.avatar = avatar;
                    groupMember.bigAvatar = bigAvatar;
                    DbManager.getDb().getGroupMemberDao().update(groupMember);
                }).compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(FriendBriefInfoFragment.this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<GroupMember>() {
                    @Override
                    public void onNext(GroupMember groupMember) {
                        RxBus.get().post(BusEvent.TAG_GROUP_MEMBER_LIST, briefData.groupId);
                    }
                });
    }

    /**
     * 从数据库中请求数据信息
     */
    private void requestUserInfoFromDb() {
        Observable.fromCallable(() -> DbManager.getDb().getAccountDao().findById(briefData.id))
                .map(com.fengshengchat.contacts.model.Friend::new)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(this::bindUserInfo)
                .observeOn(Schedulers.io())
                .flatMap((Function<com.fengshengchat.contacts.model.Friend, ObservableSource<Friend.GetFriendInfoRsp>>) friend -> getGetFriendInfoRspObservable())
                .filter(msg -> msg.getResult() == 0 && msg.getFriendListCount() == 1)
                .map(data -> {
                    Friend.FriendUserInfo userInfo = data.getFriendList(0);
                    Account account = AccountHelper.getAccount(userInfo);
                    DbManager.getDb().getAccountDao().update(account);
                    return new com.fengshengchat.contacts.model.Friend(account);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(this::bindUserInfo)
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(getUpdateFriendObserver());
    }

    private void bindUserInfo(com.fengshengchat.contacts.model.Friend friend) {
        briefData.isBlacklist = friend.isBlacklist;
        briefData.isMarkStar = friend.isMarkStar;
        briefData.avatar = friend.avatar;
        briefData.bigAvatar = friend.bigAvatar;
        briefData.gender = friend.gender;
        briefData.nickname = friend.nickname;
        bindBaseInfo(friend.id, friend.nickname, friend.avatar, friend.gender);
        setStartLayout.getSwitchCompat().setChecked(friend.isMarkStar);
        addressLayout.getMsgTv().setText(friend.getAddress());
    }

    /**
     * 请求用户信息
     */
    private void requestUserInfo(Observer<Info.GetUserInfoRsp> callback) {
        getGetUserInfoRspObservable()
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(callback);
    }

    private Observable<Info.GetUserInfoRsp> getGetUserInfoRspObservable() {
        Info.GetUserInfoReq.Builder body = Info.GetUserInfoReq
                .newBuilder()
                .addUserIdList(briefData.id);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.GET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        return ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.GetUserInfoRsp.parseFrom(msg.getBody()));
    }

    @OnClick(R.id.share_contact_btn)
    public void shareContact() {
        Intent intent = new Intent(getContext(), SelectContactActivity.class);
        intent.putExtra(SelectContactActivity.ENTER_KEY, SelectContactActivity.ENTER_SELECT_CONTACT_CARD);
        intent.putExtra("isShareCard", false);
        intent.putExtra("sendToName", briefData.nickname);
        intent.putExtra("sendToIcon", briefData.avatar);
        startActivityForResult(intent, SELECT_FRIEND_CARD);
    }

    /**
     * 请求设置标星
     */
    private void requestSetStar(boolean star) {
        int starValue = star
                ? Friend.StarredType.STARRED_TYPE_STARRED_VALUE
                : Friend.StarredType.STARRED_TYPE_NON_STARRED_VALUE;

        Friend.SetContactTypeReq.Builder body = Friend.SetContactTypeReq
                .newBuilder()
                .setUserId(briefData.id)
                .setStarredType(starValue);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.SET_CONTACT_TYPE_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Friend.SetContactTypeRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Friend.SetContactTypeRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Friend.SetContactTypeRsp data) {
                        bindSetStar(data, star);
                    }
                });
    }

    private void bindSetStar(Friend.SetContactTypeRsp data, boolean star) {
        if (data.getResult() != Friend.SetContactTypeRsp.Result.OK_VALUE) {
            setStartLayout.getSwitchCompat().setChecked(!star);
            toast(data.getErrMsg());
        }

        //  更新数据库状态
        Observable.fromCallable((Callable<Object>) () -> DbManager.getDb().getAccountDao().updateSetStarState(briefData.id, star))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new EmptyObserver());
    }

    /**
     * 设置加入黑名单
     */
    private void requestAddBlacklist(boolean isAdd) {
        int value = isAdd
                ? Friend.BlackType.BLACK_TYPE_BLACK_VALUE
                : Friend.BlackType.BLACK_TYPE_NON_BLACK_VALUE;

        Friend.SetContactTypeReq.Builder body = Friend.SetContactTypeReq
                .newBuilder()
                .setUserId(briefData.id)
                .setBlackType(value);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.SET_CONTACT_TYPE_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Friend.SetContactTypeRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Friend.SetContactTypeRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Friend.SetContactTypeRsp data) {
                        bindAddBlacklistUI(data, isAdd);
                    }
                });
    }

    /**
     * 绑定加入黑名单 UI
     */
    private void bindAddBlacklistUI(Friend.SetContactTypeRsp data, boolean isAdd) {
        if (data.getResult() != Friend.SetContactTypeRsp.Result.OK_VALUE) {
            toast(data.getErrMsg());
        }

        if (isAdd) {
            toast(R.string.search_add_blacklist_success);
        } else {
            toast(R.string.search_remove_blacklist_success);
        }
        briefData.isBlacklist = !briefData.isBlacklist;

        //  更新数据库
        Observable.fromCallable((Callable<Object>) () -> DbManager.getDb().getAccountDao().updateBlacklistState(briefData.id, briefData.isBlacklist))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new EmptyObserver());
    }

    /**
     * 请求删除好友
     */
    private void requestDelFriend() {
        Friend.DelFriendReq.Builder body = Friend.DelFriendReq
                .newBuilder()
                .setTargetUserId(briefData.id);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.DEL_FRIEND_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Friend.DelFriendRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(this::bindDelFriendUI)
                .filter(data -> data.getResult() == Friend.DelFriendRsp.DelFriendResult.OK_VALUE)
                .observeOn(Schedulers.io())
                .map(delFriendRsp -> deleteFriendAndSession())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Boolean>(getFragmentManager()) {
                    boolean success;

                    @Override
                    public void onNext(Boolean aBoolean) {
                        success = aBoolean;
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        if (success) {
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                });
    }

    /**
     * 绑定删除好友 UI
     */
    private void bindDelFriendUI(Friend.DelFriendRsp data) {
        if (data.getResult() != Friend.DelFriendRsp.DelFriendResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        briefData.relative = BriefData.NON_FRIEND;
        bindFriendRelationUI(briefData.relative);
    }

    /**
     * 删除好友数据并删除
     */
    private boolean deleteFriendAndSession() {
        boolean success = true;
        AppDatabase db = DbManager.getDb();
        db.beginTransaction();
        try {
            db.getChatListDao().deleteChatItem(briefData.id);
            db.getRecordDao().deleteAllChatRecord(briefData.id);
            db.getAccountDao().updateDeleteFriendState(briefData.id, true);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            db.endTransaction();
        }

        if (success) {
            RxBus.get().post(BusEvent.TAG_DELETED_FRIEND, briefData.id);
        }
        return success;
    }

    private boolean isSelf() {
        return briefData.id == UserManager.getUser().uid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQ_CODE_DEL_FRIEND:
                requestDelFriend();
                break;

            case REQ_CODE_ADD_BLACKLIST:
                requestAddBlacklist(!briefData.isBlacklist);
                break;

            case SELECT_FRIEND_CARD:
                sendCard(data);
                break;

            default:
                break;
        }
    }

    private void sendCard(Intent data) {
        if (null == data) {
            return;
        }

        long uid = data.getLongExtra("uid", 0);
        if (uid <= 0) {
            return;
        }
        String textMessage = data.getStringExtra("textMessage");
        FragmentActivity activity = getActivity();
        if (activity instanceof FriendBriefInfoActivity) {
            ((FriendBriefInfoActivity) activity)
                    .sendCardMessage(uid, briefData.id, briefData.nickname, briefData.avatar, textMessage);
        }
    }

    @OnClick(R.id.voice_calls_btn)
    public void voiceCalls() {
        // TODO: 2019/2/15 发起语音通话
    }

}
