package com.fengshengchat.account.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.fengshengchat.R;
import com.fengshengchat.account.model.ForgetPasswordData;
import com.fengshengchat.base.ToolbarFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 忘记密码步骤一：输入用户名
 */
public class Forget1PasswordFragment extends ToolbarFragment {

    public static final String TAG = "Forget1PasswordFragment";

    public static final String EXTRA_DATA = "DATA";

    public static Forget1PasswordFragment newInstance() {
        Bundle args = new Bundle();
        Forget1PasswordFragment fragment = new Forget1PasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_forget1_password;
    }

    @BindView(R.id.username_et)
    EditText usernameEt;

    @OnClick(R.id.submit_btn)
    public void submit() {
        String username = usernameEt.getText().toString();
        if (TextUtils.isEmpty(username)) {
            toast(R.string.user_username_hint);
            return;
        }

        ForgetPasswordData data = new ForgetPasswordData();
        data.username = username;
        Forget2PasswordFragment fragment = Forget2PasswordFragment.newInstance(data);
        openFragment(fragment, Forget2PasswordFragment.TAG);
    }

}
