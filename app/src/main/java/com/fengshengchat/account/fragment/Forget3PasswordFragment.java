package com.fengshengchat.account.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;

import com.fengshengchat.R;
import com.fengshengchat.account.model.ForgetPasswordData;
import com.fengshengchat.base.ToolbarFragment;

import butterknife.BindView;
import butterknife.OnClick;
import yiproto.yichat.account.Account;

/**
 * 忘记密码问题步骤三：设置新密码
 */
public class Forget3PasswordFragment extends ToolbarFragment {

    public static final String TAG = "Forget3PasswordFragment";

    public static Forget3PasswordFragment newInstance(ForgetPasswordData data) {
        Bundle args = new Bundle();
        args.putParcelable(Forget1PasswordFragment.EXTRA_DATA, data);
        Forget3PasswordFragment fragment = new Forget3PasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_forget3_password;
    }

    private ForgetPasswordData data;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        data = args.getParcelable(Forget1PasswordFragment.EXTRA_DATA);
    }

    @BindView(R.id.password_et)
    EditText passwordEt;

    @BindView(R.id.affirm_password_et)
    EditText affirmPasswordEt;

    @OnClick(R.id.submit_btn)
    public void submit() {
        String password = passwordEt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            toast(R.string.user_affirm_password_hint);
            return;
        }

        if (password.length() < 6) {
            toast(R.string.user_pwd_too_short);
            return;
        }

        String affirmPassword = affirmPasswordEt.getText().toString();
        if (TextUtils.isEmpty(affirmPassword)) {
            toast(R.string.user_affirm_password_hint);
            return;
        }

        if (!password.equals(affirmPassword)) {
            toast(R.string.user_pwd_not_equal);
            return;
        }

        data.password = password;
        requestResetPassword();
    }

    /**
     * 请求重置密码
     */
    private void requestResetPassword() {
//        Account.PasswordQuestion passwordQuestion = Account.PasswordQuestion
//                .newBuilder()
//                .setQuestionId(data.questionId)
//                .setAnswer(Protocol.encodePassword(data.answer))
//                .build();
//
//        Account.RegistReq.Builder body = Account.RegistReq
//                .newBuilder()
//                .setAccount(data.username)
//                .setPasswd(Protocol.encodePassword(data.password))
//                .addQuestionList(passwordQuestion);
//
//        PBMessage pbMessage = new PBMessage()
//                .setCmd(Account.AccountYimCmd.RESET_PASSWORD_REQ_CMD_VALUE)
//                .setBody(body);
//
//        ApiManager.getInstance()
//                .post(new Request(pbMessage))
//                .map(msg -> Account.ResetPasswordRsp.parseFrom(msg.getBody()))
//                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
//                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
//                .subscribe(new HttpRequestDialogObserver<Account.ResetPasswordRsp>(getFragmentManager()) {
//                    @Override
//                    public void onNext(Account.ResetPasswordRsp data) {
//                        handleResetPasswordRes(data);
//                    }
//                });
    }

    /**
     * 处理重置密码结果
     */
    private void handleResetPasswordRes(Account.ResetPasswordRsp data) {
        if (data.getResult() != Account.ResetPasswordRsp.ResetPasswordResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        toast(R.string.user_reset_password_success);
        getFragmentManager().popBackStack(Forget1PasswordFragment.TAG, 1);
    }

}
