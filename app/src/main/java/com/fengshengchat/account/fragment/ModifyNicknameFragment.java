package com.fengshengchat.account.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.utils.RegUtils;
import com.fengshengchat.widget.TitleBarLayout;

import butterknife.BindView;
import yiproto.yichat.info.Info;

/**
 * 修改昵称
 */
public class ModifyNicknameFragment extends ToolbarFragment {

    public static final String TAG = "ModifyNicknameFragment";

    @BindView(R.id.nickname_et)
    EditText nicknameEt;

    public static ModifyNicknameFragment newInstance(String nickname) {
        Bundle args = new Bundle();
        args.putString(EXTRA_DATA, nickname);
        ModifyNicknameFragment fragment = new ModifyNicknameFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.modify_nickname_fragment;
    }

    private String nickname;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        nickname = args.getString(EXTRA_DATA);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        nicknameEt.setText(nickname);
        initTitleBar();
    }

    @BindView(R.id.title_bar)
    TitleBarLayout titleBar;

    private void initTitleBar() {
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                finish();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
                String nickname = nicknameEt.getText().toString().trim();
                if (TextUtils.isEmpty(nickname)) {
                    toast(R.string.user_nickname_hint);
                    return;
                }

                if (RegUtils.stringLength(nickname) > 26) {
                    toast("昵称限制最多26个字符");
                    return;
                }

                requestModifyNickname(nickname);
            }
        });
    }

    /**
     * 请求修改昵称
     */
    private void requestModifyNickname(String nickname) {
        Info.UserInfoDetail.Builder detail = Info.UserInfoDetail
                .newBuilder()
                .setNickname(nickname);

        Info.SetUserInfoReq.Builder body = Info.SetUserInfoReq
                .newBuilder()
                .setUserInfo(detail);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.SET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.SetUserInfoRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Info.SetUserInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Info.SetUserInfoRsp data) {
                        handleModifyNicknameRes(data);
                    }
                });
    }

    /**
     * 处理修改昵称结果
     */
    private void handleModifyNicknameRes(Info.SetUserInfoRsp data) {
        if (data.getResult() != Info.SetUserInfoRsp.SetUserInfoResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        Fragment targetFragment = getTargetFragment();
        int targetRequestCode = getTargetRequestCode();
        Intent intent = new Intent();
        intent.putExtra(EXTRA_DATA, nicknameEt.getText().toString());
        targetFragment.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent);
        getFragmentManager().popBackStack();
    }

}
