package com.fengshengchat.account.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.dialog.AddressPickerDialogFragment;
import com.fengshengchat.account.model.Province;
import com.fengshengchat.account.utils.PhotoAlbumUtil;
import com.fengshengchat.app.dialog.BottomDialogFragment;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.PickerDialogStringFragment;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ClipboardUtils;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.db.Account;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.utils.QrcodeUtils;
import com.fengshengchat.widget.ItemLayout;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import yiproto.yichat.info.Info;

/**
 * 个人信息
 */
public class PersonalInfoFragment extends ToolbarFragment {

    public static final String TAG = "PersonalInfoFragment";

    /**
     * 请求码：修改昵称
     */
    public static final int REQ_CODE_MODIFY_NICKNAME = 1;
    /**
     * 请求码：选择性别
     */
    public static final int REQ_CODE_SELECT_GENDER = 2;
    /**
     * 请求码：选择照片方式
     */
    public static final int REQ_CODE_PICK_PHOTO_TYPE = 3;
    /**
     * 请求码：获取头像地址
     */
    public static final int REQ_CODE_GET_AVATAR_URL = 4;
    /**
     * 请求码：获取地区
     */
    public static final int REQ_CODE_GET_ADDRESS = 5;

    @BindView(R.id.avatar_iv)
    ImageView avatarIv;
    @BindView(R.id.name_tv)
    TextView nameTv;
    @BindView(R.id.id_tv)
    TextView idTv;
    @BindView(R.id.qrcode_iv)
    ImageView qrcodeIv;
    @BindView(R.id.nickname_layout)
    ItemLayout nicknameLayout;
    @BindView(R.id.gender_layout)
    ItemLayout genderLayout;
    @BindView(R.id.location_layout)
    ItemLayout locationLayout;
    @BindView(R.id.title_bar)
    TitleBarLayout titleBar;

    public static PersonalInfoFragment newInstance() {
        Bundle args = new Bundle();
        PersonalInfoFragment fragment = new PersonalInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.personal_info_fragment;
    }

    @OnClick(R.id.nickname_layout)
    public void modifyNickname() {
        String nickname = nicknameLayout.getMsgTv().getText().toString();
        ModifyNicknameFragment fragment = ModifyNicknameFragment.newInstance(nickname);
        fragment.setTargetFragment(this, REQ_CODE_MODIFY_NICKNAME);
        openFragment(fragment, ModifyNicknameFragment.TAG);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        User user = UserManager.getUser();
        idTv.setText(user.getUid());
        nameTv.setText(user.getNickname());
        nicknameLayout.getMsgTv().setText(user.getNickname());
        ImageUtils.loadRadiusImage(avatarIv, user.icon, R.drawable.image_default_with_round, 5);
        QrcodeUtils.showQrcode(qrcodeIv, generateCard(user.uid), user.icon, this, this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestUserInfoDetail();
    }

    /**
     * 请求用户信息
     */
    private void requestUserInfoDetail() {
        Info.GetUserInfoReq.Builder body = Info.GetUserInfoReq
                .newBuilder()
                .addUserIdList(UserManager.getUser().uid);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.GET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.GetUserInfoRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Info.GetUserInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Info.GetUserInfoRsp data) {
                        bindUserDetailInfo(data);
                    }
                });
    }

    /**
     * 绑定用户信息
     */
    private void bindUserDetailInfo(Info.GetUserInfoRsp data) {
        if (data.getResult() != Info.GetUserInfoRsp.GetUserInfoResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        if (data.getUserInfoListCount() == 0) {
            return;
        }

        Info.UserInfoDetail detail = data.getUserInfoList(0);
     //   ImageUtils.loadRadiusImage(avatarIv, detail.getSmallAvatar(), R.drawable.image_default_with_round, 5);
        ImageUtils.loadRadiusImage(avatarIv, detail.getSmallAvatar(), R.mipmap.default_head_icon, 5);
        QrcodeUtils.showQrcode(qrcodeIv, generateCard(detail.getUserId()), detail.getSmallAvatar(), this, this);
        nameTv.setText(detail.getNickname());
        nicknameLayout.getMsgTv().setText(detail.getNickname());

        if (detail.getSex() == Info.SexType.SEX_MALE_VALUE) {
            genderLayout.getMsgTv().setText(R.string.user_male);
        } else if (detail.getSex() == Info.SexType.SEX_FEMALE_VALUE) {
            genderLayout.getMsgTv().setText(R.string.user_female);
        }

        locationLayout.getMsgTv().setText(Account.getAddress(detail.getProvince(), detail.getCity()));
    }

    private String generateCard(long uid) {
        return Protocol.generateCardString(uid);
    }

    /**
     * 保存二维码
     */
    @OnClick(R.id.save_qrcode_tv)
    public void saveQrcode() {
        new RxPermissions(this)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean granted) {
                        if (granted) {
                            saveToPhotoAlbum();
                        } else {
                            ToastUtils.showShort(R.string.search_need_write_storage_permission);
                        }
                    }
                });
    }

    /**
     * 将二维码保存到相册
     */
    private void saveToPhotoAlbum() {
        Observable.fromCallable(() -> ConvertUtils.view2Bitmap(qrcodeIv))
//                .map(bitmap -> PhotoAlbumUtil.saveToPhotoAlbum(getContext(), bitmap, "yi-chat-id-qrcode", "qrcode"))
                .map(bitmap -> PhotoAlbumUtil.saveImageToGallery(getContext(), bitmap))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean success) {
                        if (success) {
                            ToastUtils.showShort(R.string.search_save_success);
                        } else {
                            ToastUtils.showShort(R.string.search_save_error);
                        }
                    }
                });
    }

    @OnClick(R.id.gender_layout)
    public void choiceGender() {
        Resources res = getResources();
        String[] genderList = res.getStringArray(R.array.gender_list);
//        ArrayList<String> items = new ArrayList<>(Arrays.asList(genderList));
//        PickerDialogStringFragment fragment = PickerDialogStringFragment.newInstance(items);
//        fragment.setTargetFragment(this, REQ_CODE_SELECT_GENDER);
//        fragment.show(getFragmentManager(), PickerDialogStringFragment.TAG);

        BottomDialogFragment fragment = BottomDialogFragment.newInstance(genderList);
        fragment.setTargetFragment(this, REQ_CODE_SELECT_GENDER);
        fragment.show(getFragmentManager(), PickerDialogStringFragment.TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQ_CODE_MODIFY_NICKNAME:
                String nickname = data.getStringExtra(EXTRA_DATA);
                nicknameLayout.getMsgTv().setText(nickname);
                nameTv.setText(nickname);
                LogUtils.e("find-token", "personal info" + UserManager.getUser().token);
                UserManager.saveNickname(nickname);
                break;

            case REQ_CODE_SELECT_GENDER:
                int position = data.getIntExtra(BottomDialogFragment.EXTRA_POSITION, 0);
                String gender = data.getStringExtra(BottomDialogFragment.EXTRA_SELECTED_ITEM);
                int genderInt = position == 0 ? Info.SexType.SEX_MALE_VALUE : Info.SexType.SEX_FEMALE_VALUE;
                requestModifyGender(genderInt, gender);
                break;

            case REQ_CODE_PICK_PHOTO_TYPE:
                handlePickPhotoType(resultCode, data);
                break;

            case REQ_CODE_GET_AVATAR_URL:
                handleAvatarUrl(resultCode, data);
                break;

            case REQ_CODE_GET_ADDRESS:
                String province = data.getStringExtra(AddressPickerDialogFragment.EXTRA_PROVINCE);
                String city = data.getStringExtra(AddressPickerDialogFragment.EXTRA_CITY);
                locationLayout.getMsgTv().setText(Account.getAddress(province, city));
                requestModifyAddress(province, city);
                break;

            default:
                break;
        }
    }

    /**
     * 修改地址
     */
    private void requestModifyAddress(String province, String city) {
        Info.UserInfoDetail.Builder detail = Info.UserInfoDetail
                .newBuilder()
                .setProvince(province)
                .setCity(city);

        Info.SetUserInfoReq.Builder body = Info.SetUserInfoReq
                .newBuilder()
                .setUserInfo(detail);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.SET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.SetUserInfoRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Info.SetUserInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Info.SetUserInfoRsp data) {
                        if (data.getResult() != 0) {
                            toast(data.getErrMsg());
                        }
                    }
                });
    }

    /**
     * 请求修改昵称
     */
    private void requestModifyGender(int sexType, String gender) {
        Info.UserInfoDetail.Builder detail = Info.UserInfoDetail
                .newBuilder()
                .setSex(sexType);

        Info.SetUserInfoReq.Builder body = Info.SetUserInfoReq
                .newBuilder()
                .setUserInfo(detail);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.SET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.SetUserInfoRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Info.SetUserInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Info.SetUserInfoRsp data) {
                        handleModifyNicknameRes(data, gender);
                    }
                });
    }

    /**
     * 处理修改昵称结果
     */
    private void handleModifyNicknameRes(Info.SetUserInfoRsp data, String gender) {
        if (data.getResult() != Info.SetUserInfoRsp.SetUserInfoResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        genderLayout.getMsgTv().setText(gender);
    }

    /**
     * 复制飞鸽号
     */
    @OnClick(R.id.id_tv)
    public void copyId() {
        ClipboardUtils.copyText(idTv.getText().toString());
        toast(R.string.user_copy_success);
    }

    /**
     * 更换头像
     */
    @OnClick(R.id.avatar_iv)
    public void openAvatarEditor() {
        Resources res = getResources();
        String[] array = res.getStringArray(R.array.user_picker_type_list);
        BottomDialogFragment fragment = BottomDialogFragment.newInstance(array);
        fragment.setTargetFragment(this, REQ_CODE_PICK_PHOTO_TYPE);
        fragment.show(getFragmentManager(), PickerDialogStringFragment.TAG);
    }

    /**
     * 处理选择照片方式
     */
    private void handlePickPhotoType(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        int position = data.getIntExtra(BottomDialogFragment.EXTRA_POSITION, -1);
        if (position == -1) {
            return;
        }

        int type = position + 1;
        AvatarEditorFragment fragment = AvatarEditorFragment.newInstance(type);
        fragment.setTargetFragment(this, REQ_CODE_GET_AVATAR_URL);
        openFragment(fragment, AvatarEditorFragment.TAG);
    }

    /**
     * 处理获取头像地址
     */
    private void handleAvatarUrl(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        String smallAvatar = data.getStringExtra(AvatarEditorFragment.EXTRA_SMALL_AVATAR);
        String bigAvatar = data.getStringExtra(AvatarEditorFragment.EXTRA_BIG_AVATAR);
        requestModifyAvatar(smallAvatar, bigAvatar);
    }

    /**
     * 请求头像
     */
    private void requestModifyAvatar(String smallAvatar, String bigAvatar) {
        Info.UserInfoDetail.Builder detail = Info.UserInfoDetail
                .newBuilder()
                .setBigAvatar(smallAvatar)
                .setSmallAvatar(bigAvatar);

        Info.SetUserInfoReq.Builder body = Info.SetUserInfoReq
                .newBuilder()
                .setUserInfo(detail);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.SET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.SetUserInfoRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Info.SetUserInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Info.SetUserInfoRsp data) {
                        handleModifyAvatarRes(data, smallAvatar, bigAvatar);
                    }
                });
    }

    /**
     * 处理修改头像结果
     */
    private void handleModifyAvatarRes(Info.SetUserInfoRsp data, String smallAvatar, String bigAvatar) {
        if (data.getResult() != Info.SetUserInfoRsp.SetUserInfoResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

     //   ImageUtils.loadRadiusImage(avatarIv, smallAvatar, R.drawable.image_default, 5);
        ImageUtils.loadRadiusImage(avatarIv, smallAvatar, R.mipmap.default_head_icon, 50);
        QrcodeUtils.showQrcode(qrcodeIv, generateCard(UserManager.getUser().uid), smallAvatar, this, this);
        UserManager.saveIcon(smallAvatar, bigAvatar);
    }

    @OnClick(R.id.location_layout)
    public void pickerAddress() {
        Province.getDataSource()
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<List<Province>>() {
                    @Override
                    public void onNext(List<Province> provinces) {
                        AddressPickerDialogFragment dialog = AddressPickerDialogFragment.newInstance();
                        dialog.setTargetFragment(PersonalInfoFragment.this, REQ_CODE_GET_ADDRESS);
                        dialog.init(provinces);
                        dialog.show(getFragmentManager(), AddressPickerDialogFragment.TAG);
                    }
                });
    }
}
