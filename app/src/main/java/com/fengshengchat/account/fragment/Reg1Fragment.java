package com.fengshengchat.account.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.bumptech.glide.Glide;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.LoginActivity;
import com.fengshengchat.base.BKFragment;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.base.OnBackPressedListener;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;

import butterknife.BindView;
import butterknife.OnClick;
import yiproto.yichat.account.Account;
import yiproto.yichat.msg.MsgOuterClass;

/**
 * 注册页面：填写账号信息
 */
public class Reg1Fragment extends BKFragment implements OnBackPressedListener {

    public static final String EXTRA_REG_DATA = "REG_DATA";

    public static final String TAG = "Reg1Fragment";
    private static final String PREFIX = "data:image/png;base64,";
    public static final String EXTRA_USERNAME = "USERNAME";
    public static final String EXTRA_PASSWORD = "PASSWORD";

    //    public static Reg1Fragment newInstance(String username, String password) {
    public static Reg1Fragment newInstance() {
        Bundle args = new Bundle();
//        args.putString(EXTRA_USERNAME, username);
//        args.putString(EXTRA_PASSWORD, password);
        Reg1Fragment fragment = new Reg1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.user_reg_1_fragment;
    }

    @BindView(R.id.content_layout)
    ViewGroup contentLayout;
    @BindView(R.id.username_et)
    EditText usernameEt;
    @BindView(R.id.pwd_et)
    EditText pwdEt;
    @BindView(R.id.re_pwd_et)
    EditText rePwdEt;
    @BindView(R.id.verify_code_iv)
    ImageView verifyCodeIv;
    @BindView(R.id.verify_code_et)
    EditText verifyCodeEt;

    @BindView(R.id.username_msg_tv)
    TextView usernameMsgTv;
    @BindView(R.id.password_msg_tv)
    TextView passwordMsgTv;
    @BindView(R.id.re_password_msg_tv)
    TextView rePasswordMsgTv;
    @BindView(R.id.verify_code_msg_tv)
    TextView verifyCodeMsgTv;

    View logo;
    View slogan;
    private Flags flags;

    @Override
    public void initView(View root) {
        super.initView(root);
        int height = (int) (getResources().getDisplayMetrics().density * 56);
        logo = findViewById(R.id.logo);
        slogan = findViewById(R.id.slogan);
//        addLayoutListener(root, height);

//        Bundle args = getArguments();
//        if (args == null) {
//            return;
//        }
//        String username = args.getString(EXTRA_USERNAME);
//        String password = args.getString(EXTRA_PASSWORD);
//        usernameEt.setText(username);
//        pwdEt.setText(password);
    }

    @OnClick(R.id.reg_btn)
    public void register() {
        String username = usernameEt.getText().toString();
        if (TextUtils.isEmpty(username)) {
            toast(R.string.user_username_err_3);
            return;
        }

        if (!RegexUtils.isMatch("^[A-Za-z0-9]+$", username)) {
            toast(R.string.user_username_err_1);
            return;
        }

        if (username.length() < 6) {
            toast(R.string.user_username_err_2);
            return;
        }

        String password = pwdEt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            toast(R.string.user_password_err_1);
            return;
        }

        if (!RegexUtils.isMatch("^[\\x00-\\xff]+$", password)) {
            toast(R.string.user_password_err_1);
            return;
        }

        if (password.length() < 6) {
            toast(R.string.user_password_err_2);
            return;
        }

        String rePassword = rePwdEt.getText().toString();
        if (TextUtils.isEmpty(rePassword)) {
            toast(R.string.user_re_pwd_hint);
            return;
        }

        if (!password.equals(rePassword)) {
            toast(R.string.user_pwd_not_equal);
            return;
        }

        String verifyCode = verifyCodeEt.getText().toString();
        if (TextUtils.isEmpty(verifyCode)) {
            toast(R.string.user_auth_code_hint);
            return;
        }

        KeyboardUtils.hideSoftInput(getActivity());
//        Reg2Fragment reg2Fragment = Reg2Fragment.newInstance(regData);
//        openFragment(reg2Fragment, Reg2Fragment.TAG);
        requestReg(username, password, verifyCode);
    }

    private void requestReg(String username, String pwd, String verifyCode) {
        Account.RegistReq.Builder registerReq = Account.RegistReq
                .newBuilder()
                .setAccount(username)
                .setPlatform(MsgOuterClass.ClientType.CLIENT_TYPE_ANDRIOD_VALUE)
                .setVerifyCode(verifyCode)
                .setPasswd(Protocol.encodePassword(pwd));

        PBMessage message = new PBMessage()
                .setCmd(Account.AccountYimCmd.ACCOUNT_REGIST_REQ_CMD_VALUE)
                .setBody(registerReq);

        Request request = new Request(message);

        ApiManager.getInstance()
                .post(request)
                .map(msg -> Account.RegistRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Account.RegistRsp>(getFragmentManager()) {
                    public void onNext(Account.RegistRsp data) {
                        handleRegRes(data);
                    }
                });
    }

    private void handleRegRes(Account.RegistRsp data) {
        if (data.getResult() != Account.RegistRsp.RegistResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        toast("注册成功");
//        openLoginFragment();
        FragmentActivity activity = getActivity();
        if (activity instanceof LoginActivity) {
            String username = usernameEt.getText().toString();
            String password = pwdEt.getText().toString();
            ((LoginActivity) activity).onRegisterSuccess(username, password);
        }
    }

    @OnClick(R.id.open_login_tv)
    public void openLoginFragment() {
//        getFragmentManager().popBackStack();
        onBackPressed();
    }

    public void addLayoutListener(final View main, final int height) {
        main.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect rect = new Rect();
            main.getWindowVisibleDisplayFrame(rect);
            int mainInvisibleHeight = main.getRootView().getHeight() - rect.bottom;
            if (mainInvisibleHeight > height) {
                logo.setVisibility(View.GONE);
                slogan.setVisibility(View.GONE);
            } else {
                logo.setVisibility(View.VISIBLE);
                slogan.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestVerifyCode();
    }

    /**
     * 请求加载验证码
     */
    @OnClick(R.id.verify_code_iv)
    public void requestVerifyCode() {
        Account.GetVerifyCodeReq.Builder body = Account.GetVerifyCodeReq
                .newBuilder()
                .setWidth(ConvertUtils.dp2px(72))
                .setHeight(ConvertUtils.dp2px(30));

        PBMessage pbMessage = new PBMessage()
                .setCmd(Account.AccountYimCmd.GET_VERIFY_CODE_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Account.GetVerifyCodeRsp.parseFrom(msg.getBody()))
                .filter(data -> data.getResult() == 0)
                .map(this::getVerifyCodeBytes)
                .filter(bytes -> bytes.length != 0)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<byte[]>() {
                    @Override
                    public void onNext(byte[] bytes) {
                        Glide.with(Reg1Fragment.this)
                                .load(bytes)
                                .placeholder(R.drawable.image_default)
                                .into(verifyCodeIv);
                    }
                });
    }

    private byte[] getVerifyCodeBytes(Account.GetVerifyCodeRsp data) {
        String verifyCode = data.getVerifyCode();
        if (TextUtils.isEmpty(verifyCode)) {
            return new byte[0];
        }

        String newVerifyCode;
        if (verifyCode.startsWith(PREFIX)) {
            newVerifyCode = verifyCode.substring(PREFIX.length());
        } else {
            newVerifyCode = verifyCode;
        }

        return Base64.decode(newVerifyCode, Base64.DEFAULT);
    }

    @Override
    public boolean onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm == null) {
            return true;
        }

        if (isHidden()) {
            getActivity().finish();
        }

        Fragment loginFragment = fm.findFragmentByTag(LoginFragment.TAG);
        if (loginFragment == null) {
            return true;
        }

        fm.beginTransaction().hide(this).show(loginFragment).commit();
        return true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initEditText();
    }

    private void initEditText() {
        flags = new Flags();
        usernameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                usernameMsgTv.setText(null);
                checkUsername(s.toString());
                check();
            }
        });

        pwdEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                passwordMsgTv.setText(null);
                checkPassword(s.toString());
                check();
            }
        });

        rePwdEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                rePasswordMsgTv.setText(null);
                checkRePassword(pwdEt.getText().toString(), s.toString());
                check();
            }
        });

        verifyCodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                verifyCodeMsgTv.setText(null);
                checkVerifyCode(s.toString());
                check();
            }
        });
    }

    private void checkPassword(String password) {
        if (TextUtils.isEmpty(password)) {
            passwordMsgTv.setText(R.string.user_password_err_1);
            flags.password = false;
            return;
        }

        if (!RegexUtils.isMatch("^[\\x00-\\xff]+$", password)) {
            passwordMsgTv.setText(R.string.user_password_err_1);
            flags.password = false;
            return;
        }

        if (password.length() < 6) {
            passwordMsgTv.setText(R.string.user_password_err_2);
            flags.password = false;
            return;
        }

        passwordMsgTv.setText(null);
        flags.password = true;

        String rePassword = rePwdEt.getText().toString();
        if (TextUtils.isEmpty(rePassword)) {
            return;
        }

        if (!password.equals(rePassword)) {
            rePasswordMsgTv.setText(R.string.user_pwd_not_equal);
            flags.rePassword = false;
        }
    }

    private void checkUsername(String username) {
        if (TextUtils.isEmpty(username)) {
            usernameMsgTv.setText(R.string.user_username_err_3);
            flags.username = false;
            return;
        }

        if (!RegexUtils.isMatch("^[A-Za-z0-9]+$", username)) {
            usernameMsgTv.setText(R.string.user_username_err_1);
            flags.username = false;
            return;
        }

        if (username.length() < 6) {
            usernameMsgTv.setText(R.string.user_username_err_2);
            flags.username = false;
            return;
        }

        usernameMsgTv.setText(null);
        flags.username = true;
    }

    private void checkRePassword(String password, String rePassword) {
        if (TextUtils.isEmpty(rePassword)) {
            rePasswordMsgTv.setText(R.string.user_re_pwd_hint);
            flags.rePassword = false;
            return;
        }

        if (!rePassword.equals(password)) {
            rePasswordMsgTv.setText(R.string.user_pwd_not_equal);
            flags.rePassword = false;
            return;
        }

        flags.rePassword = true;
        rePasswordMsgTv.setText(null);
    }

    private void checkVerifyCode(String verifyCode) {
        if (TextUtils.isEmpty(verifyCode)) {
            verifyCodeMsgTv.setText(R.string.user_auth_code_hint);
            flags.verifyCode = false;
            return;
        }

        verifyCodeMsgTv.setText(null);
        flags.verifyCode = true;
    }

    @BindView(R.id.reg_btn)
    public Button regBtn;

    private void check() {
        regBtn.setEnabled(flags.isEnabled());
    }

    private static final class Flags {
        boolean username;
        boolean password;
        boolean rePassword;
        boolean verifyCode;

        boolean isEnabled() {
            return username && password && rePassword && verifyCode;
        }
    }

}

