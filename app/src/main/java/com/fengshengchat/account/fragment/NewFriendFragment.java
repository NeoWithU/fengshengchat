package com.fengshengchat.account.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.account.model.FriendReqInfo;
import com.fengshengchat.account.view.NewFriendViewBinder;
import com.fengshengchat.base.BKFragment;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.base.RecyclerViewDivider;
import com.fengshengchat.base.utils.TimeUtils;
import com.fengshengchat.db.helper.Task;
import com.fengshengchat.db.helper.TaskHelper;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;
import me.drakeet.multitype.MultiTypeAdapter;
import yiproto.yichat.friend.Friend;

/**
 * 新的朋友
 */
public class NewFriendFragment extends BKFragment {

    public static final String TAG = "NewFriendFragment";
    private boolean isWorking;

    public static NewFriendFragment newInstance() {
        Bundle args = new Bundle();
        NewFriendFragment fragment = new NewFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.new_friend_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private MultiTypeAdapter adapter;
    private List<Object> items = new ArrayList<>();

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private void initView() {
        adapter = new MultiTypeAdapter(items);
        adapter.register(FriendReqInfo.class, new NewFriendViewBinder(new NewFriendViewBinder.OnClickListener() {
            @Override
            public void onClickLayout(FriendReqInfo reqInfo, int status) {
                BriefData briefData = new BriefData();
                briefData.id = reqInfo.id;

                if (status == Friend.RequestUserInfo.RequestResult.REQUEST_UNHANDLE_VALUE) {
                    briefData.relative = BriefData.APPLY_BECOME_FRIEND;
                } else {
                    briefData.relative = BriefData.NOT_SURE_FRIEND;
                }

                briefData.nickname = reqInfo.nickname;
                briefData.avatar = reqInfo.avatar;
                Intent intent = new Intent(getContext(), FriendBriefInfoActivity.class);
                intent.putExtra(FriendBriefInfoActivity.KEY_DATA, briefData);
                startActivity(intent);
            }

            @Override
            public void onClickAgreeButton(FriendReqInfo reqInfo) {
                agreeAddFriendRequest(reqInfo);
            }
        }));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new RecyclerViewDivider(1, 0xFFEEEEEE));
        recyclerView.setAdapter(adapter);
    }

    /**
     * 同意好友请求
     */
    private void agreeAddFriendRequest(FriendReqInfo reqInfo) {
        Friend.AgreeAddFriendReq.Builder body = Friend.AgreeAddFriendReq
                .newBuilder()
                .setTargetUserId(reqInfo.id);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.AGREE_ADD_FRIEND_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> Friend.AgreeAddFriendRsp.parseFrom(message.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Friend.AgreeAddFriendRsp>() {
                    @Override
                    public void onNext(Friend.AgreeAddFriendRsp data) {
                        handleAgreeFriendReq(data, reqInfo);
                    }
                });
    }

    private void handleAgreeFriendReq(Friend.AgreeAddFriendRsp data, FriendReqInfo reqInfo) {
        int result = data.getResult();
        if (result == Friend.AgreeAddFriendRsp.AgreeAddFriendResult.OK_VALUE
                || result == Friend.AgreeAddFriendRsp.AgreeAddFriendResult.FRIEND_EXIST_VALUE) {
            reqInfo.agree = Friend.RequestUserInfo.RequestResult.REQUEST_AGREED_VALUE;
            adapter.notifyDataSetChanged();
            TaskHelper.getInstance().post(Task.TASK_UPDATE_FRIEND);
            toast("添加好友成功");
            return;
        }

        toast(data.getErrMsg());
    }

    /**
     * 获取好友请求列表
     */
    private void getFriendRequestList() {
        if (isWorking) {
            return;
        }

        yiproto.yichat.friend.Friend.GetFriendRequestListReq.Builder body = yiproto.yichat.friend.Friend.GetFriendRequestListReq
                .newBuilder()
                .setStart(0)
                .setNum(100);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.GET_FRIEND_REQUEST_LIST_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(message -> yiproto.yichat.friend.Friend.GetFriendRequestListRsp.parseFrom(message.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<yiproto.yichat.friend.Friend.GetFriendRequestListRsp>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        super.onSubscribe(d);
                        isWorking = true;
                    }

                    @Override
                    public void onNext(yiproto.yichat.friend.Friend.GetFriendRequestListRsp data) {
                        handleFriendRes(data);
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        isWorking = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        isWorking = false;
                    }
                });
    }

    private void handleFriendRes(Friend.GetFriendRequestListRsp data) {
        if (data.getResult() != Friend.GetFriendRequestListRsp.GetFriendRequestListResult.OK_VALUE) {
            ToastUtils.showShort(data.getErrMsg());
            return;
        }

        items.clear();
        List<Friend.RequestUserInfo> requestListList = data.getRequestListList();
        for (Friend.RequestUserInfo info : requestListList) {
            FriendReqInfo friendReqInfo = new FriendReqInfo();
            friendReqInfo.id = info.getUserId();
            friendReqInfo.nickname = info.getNickName();
            friendReqInfo.avatar = info.getLogo();
            friendReqInfo.agree = info.getResult();
            friendReqInfo.time = TimeUtils.getFriendlyTime(Utils.getApp(), info.getTime() * 1000);
            items.add(friendReqInfo);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        getFriendRequestList();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getFriendRequestList();
        }
    }
}
