package com.fengshengchat.account.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.account.model.ForgetPasswordData;
import com.fengshengchat.account.model.Question;
import com.fengshengchat.base.PickerDialogFragment;
import com.fengshengchat.base.PickerDialogStringFragment;
import com.fengshengchat.base.ToolbarFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 忘记密码步骤二：输入密保问题
 */
public class Forget2PasswordFragment extends ToolbarFragment {

    public static final String TAG = "Forget2PasswordFragment";

    public static Forget2PasswordFragment newInstance(ForgetPasswordData data) {
        Bundle args = new Bundle();
        args.putParcelable(Forget1PasswordFragment.EXTRA_DATA, data);
        Forget2PasswordFragment fragment = new Forget2PasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_forget2_password;
    }

    private ForgetPasswordData data;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        data = args.getParcelable(Forget1PasswordFragment.EXTRA_DATA);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestPasswordQuestion();
    }

    /**
     * 请求密保问题
     */
    private void requestPasswordQuestion() {
//        Account.GetPasswordQuestionReq.Builder body = Account.GetPasswordQuestionReq.newBuilder();
//
//        PBMessage pbMessage = new PBMessage()
//                .setCmd(Account.AccountYimCmd.GET_PASSWORD_QUESTION_REQ_CMD_VALUE)
//                .setBody(body);
//
//        ApiManager.getInstance()
//                .post(new Request(pbMessage))
//                .map(msg -> Account.GetPasswordQuestionRsp.parseFrom(msg.getBody()))
//                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
//                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
//                .subscribe(new HttpRequestDialogObserver<Account.GetPasswordQuestionRsp>(getFragmentManager()) {
//                    @Override
//                    public void onNext(Account.GetPasswordQuestionRsp data) {
//                        handlePasswordQuestion(data);
//                    }
//                });
    }

    private List<Question> questionList = new ArrayList<>();

    /**
     * 绑定密保问题
     */
//    private void handlePasswordQuestion(Account.GetPasswordQuestionRsp data) {
//        if (data.getResult() != Account.GetPasswordQuestionRsp.GetPasswordQuestionResult.OK_VALUE) {
//            toast(data.getErrMsg());
//            return;
//        }
//
//        for (Account.PasswordQuestion passwordQuestion : data.getQuestionListList()) {
//            Question question = new Question(passwordQuestion.getQuestionId(), passwordQuestion.getQuestion());
//            questionList.add(question);
//        }
//    }

    public static final int REQ_CODE_CHOOSE_QUESTION = 1;

    @OnClick(R.id.question_tv)
    public void chooseQuestion() {
        ArrayList<Question> surplusQuestionList = new ArrayList<>(questionList);
        if (data.questionId != -1) {
            for (Question question : questionList) {
                if (data.questionId == question.id) {
                    break;
                }
            }
        }

        PickerDialogFragment dialogFragment = PickerDialogFragment.newInstance(surplusQuestionList);
        dialogFragment.setTargetFragment(this, REQ_CODE_CHOOSE_QUESTION);
        dialogFragment.show(getFragmentManager(), PickerDialogStringFragment.TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_CHOOSE_QUESTION:
                handleChooseQuestion(resultCode, data);
                break;

            default:
                break;
        }
    }

    /**
     * 绑定密码问题
     */
    private void handleChooseQuestion(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        Question question = data.getParcelableExtra(PickerDialogFragment.EXTRA_SELECTED_ITEM);
        this.data.questionId = question.id;
        questionTv.setText(question.content);
    }

    @BindView(R.id.question_tv)
    TextView questionTv;

    @BindView(R.id.answer_et)
    EditText answerEt;

    @OnClick(R.id.submit_btn)
    public void submit() {
        String answer = answerEt.getText().toString();
        if (TextUtils.isEmpty(answer)) {
            toast(R.string.user_password_question_answer);
            return;
        }

        data.answer = answer;
        Forget3PasswordFragment fragment = Forget3PasswordFragment.newInstance(data);
        openFragment(fragment, Forget3PasswordFragment.TAG);
    }

}
