package com.fengshengchat.account.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.account.model.EmptyBlacklist;
import com.fengshengchat.account.view.BlacklistViewBinder;
import com.fengshengchat.account.view.EmptyBlacklistViewBinder;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.base.RecyclerViewDivider;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.EmptyHttpRequestDialogObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.drakeet.multitype.MultiTypeAdapter;

/**
 * 联系人黑名单
 */
public class BlacklistFragment extends ToolbarFragment {

    public static final String TAG = "BlacklistFragment";

    public static BlacklistFragment newInstance() {
        Bundle args = new Bundle();
        BlacklistFragment fragment = new BlacklistFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.blacklist_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private MultiTypeAdapter adapter;
    private List<Object> items = new ArrayList<>();

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private void initView() {
        adapter = new MultiTypeAdapter(items);
        adapter.register(Friend.class, new BlacklistViewBinder(new BlacklistViewBinder.OnClickListener() {
            @Override
            public void onClickLayout(Friend friend) {
                startChat(friend);
            }

            @Override
            public void onClickRemoveButton(Friend friend) {
                requestRemoveBlacklist(friend);
            }
        }));
        adapter.register(EmptyBlacklist.class, new EmptyBlacklistViewBinder());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new RecyclerViewDivider(1, 0xFFEEEEEE));
        recyclerView.setAdapter(adapter);
    }

    private void startChat(Friend friend) {
        Intent intent = new Intent(getContext(), FriendBriefInfoActivity.class);
        BriefData briefData = new BriefData();
        briefData.id = friend.id;
        briefData.relative = BriefData.FRIEND;
        briefData.nickname = friend.nickname;
        briefData.avatar = friend.avatar;
        briefData.bigAvatar = friend.bigAvatar;
        briefData.gender = friend.gender;
        briefData.isMarkStar = friend.isMarkStar;
        intent.putExtra(FriendBriefInfoActivity.KEY_DATA, briefData);
        startActivity(intent);
    }

    private void getBlacklistData() {
        Observable.fromCallable(() -> DbManager.getDb().getAccountDao().getBlacklist(true))
                .map(Friend::convert)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<List<Friend>>() {
                    @Override
                    public void onNext(List<Friend> friendList) {
                        bindFriendList(friendList);
                    }
                });
    }

    private void bindFriendList(List<Friend> friendList) {
        items.clear();
        items.addAll(friendList);

        if (items.isEmpty()) {
            items.add(EmptyBlacklist.INSTANCE);
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * 设置加入黑名单
     */
    private void requestRemoveBlacklist(Friend friend) {
        yiproto.yichat.friend.Friend.SetContactTypeReq.Builder body = yiproto.yichat.friend.Friend.SetContactTypeReq
                .newBuilder()
                .setUserId(friend.id)
                .setBlackType(yiproto.yichat.friend.Friend.BlackType.BLACK_TYPE_NON_BLACK_VALUE);

        PBMessage pbMessage = new PBMessage()
                .setCmd(yiproto.yichat.friend.Friend.FriendYimCMD.SET_CONTACT_TYPE_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> yiproto.yichat.friend.Friend.SetContactTypeRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .doOnNext(data -> bindRemoveBlacklist(data, friend))
                .filter(data -> data.getResult() == yiproto.yichat.friend.Friend.SetContactTypeRsp.Result.OK_VALUE)
                .observeOn(Schedulers.io())
                .doOnNext(data -> updateBlacklistStatus(friend.id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new EmptyHttpRequestDialogObserver(getFragmentManager()));
    }

    private void bindRemoveBlacklist(yiproto.yichat.friend.Friend.SetContactTypeRsp data, Friend friend) {
        if (data.getResult() != yiproto.yichat.friend.Friend.SetContactTypeRsp.Result.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        int position = items.indexOf(friend);
        items.remove(friend);
        if (items.isEmpty()) {
            items.add(EmptyBlacklist.INSTANCE);
            adapter.notifyDataSetChanged();
        } else {
            adapter.notifyItemRemoved(position);
        }
    }

    private void updateBlacklistStatus(long id) {
        DbManager.getDb().getAccountDao().updateBlacklistState(id, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getBlacklistData();
    }
}
