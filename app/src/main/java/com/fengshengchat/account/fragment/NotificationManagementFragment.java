package com.fengshengchat.account.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.View;

import com.fengshengchat.R;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.push.NotifyUtils;
import com.fengshengchat.widget.ItemLayout;

import butterknife.BindView;

/**
 * 通知管理
 */
public class NotificationManagementFragment extends ToolbarFragment {

    public static final String TAG = "NotificationManagementFragment";

    @BindView(R.id.message_notification_layout)
    ItemLayout messageNotificationLayout;
    @BindView(R.id.sound_layout)
    ItemLayout soundLayout;
    @BindView(R.id.shake_layout)
    ItemLayout shakeLayout;

    public static NotificationManagementFragment newInstance() {
        Bundle args = new Bundle();
        NotificationManagementFragment fragment = new NotificationManagementFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.notiflication_management_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SwitchCompat soundSwitch = soundLayout.getSwitchCompat();
        SwitchCompat shakeSwitch = shakeLayout.getSwitchCompat();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            findViewById(R.id.hint1).setVisibility(View.VISIBLE);
            findViewById(R.id.hint2).setVisibility(View.INVISIBLE);
            findViewById(R.id.sound_layout).setVisibility(View.INVISIBLE);
            findViewById(R.id.shake_layout).setVisibility(View.INVISIBLE);
        }else{
            findViewById(R.id.hint1).setVisibility(View.GONE);
            soundSwitch.setChecked(NotifyUtils.isEnableSound());
            soundSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                NotifyUtils.setEnableSound(isChecked);
            });

            shakeSwitch.setChecked(NotifyUtils.isEnableVibrate());
            shakeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                NotifyUtils.setEnableVibrate(isChecked);
            });
        }

        SwitchCompat msgNotificationSwitch = messageNotificationLayout.getSwitchCompat();
        boolean enableNotify = NotifyUtils.isEnableNotify();
        msgNotificationSwitch.setChecked(enableNotify);
        msgNotificationSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            NotifyUtils.setEnableNotify(isChecked);
            soundSwitch.setEnabled(isChecked);
            shakeSwitch.setEnabled(isChecked);
        });
        if (!enableNotify) {
            soundSwitch.setEnabled(false);
            shakeSwitch.setEnabled(false);
        }
        msgNotificationSwitch.setEnabled(enableNotify);
    }

}
