package com.fengshengchat.account.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.LoginActivity;
import com.fengshengchat.base.BaseActivity;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ClipboardUtils;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.ItemLayout;

import butterknife.BindView;
import butterknife.OnClick;
import yiproto.yichat.login.Login;

/**
 * 账号与安全
 */
public class AccountFragment extends ToolbarFragment {

    public static final String TAG = "AccountFragment";

    @BindView(R.id.id_layout)
    ItemLayout idLayout;
    @BindView(R.id.account_layout)
    ItemLayout accountLayout;

    public static AccountFragment newInstance() {
        Bundle args = new Bundle();
        AccountFragment fragment = new AccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.account_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        User user = UserManager.getUser();
        if (user == null) {
            return;
        }

        idLayout.getMsgTv().setText(user.getUid());
        idLayout.getMsgTv().setOnClickListener(v -> {
            ClipboardUtils.copyText(user.getUid());
            toast(R.string.user_copy_success);
        });
        accountLayout.getMsgTv().setText(user.account);
    }

    @OnClick(R.id.logout_btn)
    public void logout() {
        User user = UserManager.getUser();
        Login.LogoutReq.Builder logoutReq = Login.LogoutReq.newBuilder()
                .setPlatform(PBMessage.getPlatform())
                .setUid(user.uid)
                .setToken(user.getToken());

        PBMessage pbMessage = new PBMessage();
        pbMessage.setCmd(Login.LoginYimCMD.LOGOUT_REQ_CMD_VALUE)
                .setBody(logoutReq);
        ApiManager.getInstance().post(new Request(pbMessage))
                .map(msg -> Login.LogoutRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Login.LogoutRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Login.LogoutRsp data) {
                        if (data.getResult() != Login.LogoutRsp.ReportInfoResult.OK_VALUE) {
                            toast(data.getErrMsg());
                            return;
                        }
                        toLogin();
                    }
                });
    }

    private void toLogin() {
        FragmentActivity activity = getActivity();
        if (activity instanceof BaseActivity) {
//            User user = UserManager.getUser();
//            user.uid = 0L;
//            user.token = null;
//            user.expireTime = 0;
//            UserManager.saveUser(user);
            logout((BaseActivity) activity);
        } else {
            toast(R.string.error);
        }
    }

    public static void logout(BaseActivity activity) {
        UserManager.clearUser();
        activity.getPusher().close();
        LoginActivity.toLogin(activity, "");
    }

    @OnClick(R.id.reset_password_layout)
    public void resetPassword() {
        openFragment(ResetPasswordFragment.newInstance(), ResetPasswordFragment.TAG);
    }

}
