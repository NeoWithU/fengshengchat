package com.fengshengchat.account.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.utils.SoftKeyboardStateWatcher;
import com.fengshengchat.base.BaseActivity;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.UserManager;

import butterknife.BindView;
import butterknife.OnClick;
import yiproto.yichat.account.Account;

/**
 * 重置密码
 */
public class ResetPasswordFragment extends ToolbarFragment {

    public static final String TAG = "ResetPasswordFragment";

    @BindView(R.id.content_layout)
    ViewGroup contentLayout;

    @BindView(R.id.old_password_et)
    EditText oldPasswordEt;

    @BindView(R.id.password_et)
    EditText passwordEt;

    @BindView(R.id.affirm_password_et)
    EditText affirmPasswordEt;

    @BindView(R.id.submit_btn)
    Button submitBtn;

    public static ResetPasswordFragment newInstance() {
        Bundle args = new Bundle();
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.reset_password_fragment;
    }

    @OnClick(R.id.submit_btn)
    public void submit() {
        String oldPassword = oldPasswordEt.getText().toString();
        if (TextUtils.isEmpty(oldPassword)) {
            toast(R.string.user_old_password_hint);
            return;
        }

        String password = passwordEt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            toast(R.string.user_new_password_hint);
            return;
        }

        if (password.length() < 6) {
            toast(R.string.user_pwd_too_short);
            return;
        }

        String affirmPassword = affirmPasswordEt.getText().toString();
        if (TextUtils.isEmpty(affirmPassword)) {
            toast(R.string.user_affirm_password_hint_3);
            return;
        }

        if (!password.equals(affirmPassword)) {
            toast(R.string.user_login_pwd_not_equal);
            return;
        }

        if (oldPassword.equals(password)) {
            toast(R.string.user_old_pwd_equal_new_pwd);
            return;
        }

        requestResetPassword(Protocol.encodePassword(oldPassword), Protocol.encodePassword(password));
    }

    /**
     * 请求重置密码
     */
    private void requestResetPassword(String oldPassword, String newPassword) {
        Account.ResetPasswordReq.Builder body = Account.ResetPasswordReq
                .newBuilder()
                .setAccount(UserManager.getUser().account)
                .setOldPasswd(oldPassword)
                .setPasswd(newPassword);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Account.AccountYimCmd.RESET_PASSWORD_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Account.ResetPasswordRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Account.ResetPasswordRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Account.ResetPasswordRsp data) {
                        handleResetPasswordRes(data);
                    }
                });
    }

    /**
     * 处理重置密码结果
     */
    private void handleResetPasswordRes(Account.ResetPasswordRsp data) {
        int result = data.getResult();
        if (result != Account.ResetPasswordRsp.ResetPasswordResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        toast(R.string.user_reset_password_success);
        AccountFragment.logout((BaseActivity) getActivity());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        new SoftKeyboardStateWatcher(contentLayout, getContext()).addSoftKeyboardStateListener(new SoftKeyboardStateWatcher.SoftKeyboardStateListener() {
            @Override
            public void onSoftKeyboardOpened(int keyboardHeightInPx) {
            }

            @Override
            public void onSoftKeyboardClosed() {
                if (affirmPasswordEt == null) {
                    return;
                }

                if (!affirmPasswordEt.hasFocus()) {
                    return;
                }

                checkPassword();
            }
        });
        oldPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateSubmitBtnStatus();
            }
        });
        passwordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateSubmitBtnStatus();
            }
        });
        affirmPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateSubmitBtnStatus();
            }
        });
        affirmPasswordEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                checkPassword();
            }
        });
    }

    private void checkPassword() {
        if (passwordEt == null) {
            return;
        }

        String password = passwordEt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            return;
        }

        if (affirmPasswordEt == null) {
            return;
        }

        String affirmPassword = affirmPasswordEt.getText().toString();
        if (TextUtils.isEmpty(affirmPassword)) {
            return;
        }

        if (!TextUtils.equals(password, affirmPassword)) {
            toast(R.string.user_login_pwd_not_equal);
        }
    }

    private void updateSubmitBtnStatus() {
        boolean enabled = oldPasswordEt.length() != 0 && passwordEt.length() != 0 && affirmPasswordEt.length() != 0;
        submitBtn.setEnabled(enabled);
    }
}
