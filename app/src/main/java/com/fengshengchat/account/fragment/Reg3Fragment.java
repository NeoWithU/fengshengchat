package com.fengshengchat.account.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.LoginActivity;
import com.fengshengchat.account.model.Question;
import com.fengshengchat.account.model.RegData;
import com.fengshengchat.base.PickerDialogFragment;
import com.fengshengchat.base.PickerDialogStringFragment;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.protocol.Protocol;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindViews;
import butterknife.OnClick;
import yiproto.yichat.account.Account;

/**
 * 注册页面：设置密保问题
 */
public class Reg3Fragment extends ToolbarFragment implements View.OnClickListener {

    public static final String TAG = "Reg3Fragment";

    public static Reg3Fragment newInstance(RegData regData) {
        Bundle args = new Bundle();
        args.putParcelable(Reg1Fragment.EXTRA_REG_DATA, regData);
        Reg3Fragment fragment = new Reg3Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.user_reg_3_fragment;
    }

    private RegData regData;

    private List<Question> questionList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        regData = args.getParcelable(Reg1Fragment.EXTRA_REG_DATA);
    }

    @BindViews({R.id.q1_tv, R.id.q2_tv, R.id.q3_tv})
    TextView[] questionTvs;

    @BindViews({R.id.a1_et, R.id.a2_et, R.id.a3_et})
    EditText[] answerEts;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        for (int i = 0; i < questionTvs.length; i++) {
            questionTvs[i].setTag(i);
            questionTvs[i].setOnClickListener(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestPasswordQuestion();
    }

    /**
     * 请求密保问题
     */
    private void requestPasswordQuestion() {
//        Account.GetPasswordQuestionReq.Builder body = Account.GetPasswordQuestionReq.newBuilder();
//
//        PBMessage pbMessage = new PBMessage()
//                .setCmd(Account.AccountYimCmd.GET_PASSWORD_QUESTION_REQ_CMD_VALUE)
//                .setBody(body);
//
//        ApiManager.getInstance()
//                .post(new Request(pbMessage))
//                .map(msg -> Account.GetPasswordQuestionRsp.parseFrom(msg.getBody()))
//                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
//                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
//                .subscribe(new HttpRequestDialogObserver<Account.GetPasswordQuestionRsp>(getFragmentManager()) {
//                    @Override
//                    public void onNext(Account.GetPasswordQuestionRsp data) {
//                        handlePasswordQuestion(data);
//                    }
//                });
    }

//    /**
//     * 绑定密保问题
//     */
//    private void handlePasswordQuestion(Account.GetPasswordQuestionRsp data) {
//        if (data.getResult() != Account.GetPasswordQuestionRsp.GetPasswordQuestionResult.OK_VALUE) {
//            toast(data.getErrMsg());
//            return;
//        }
//
//        for (Account.PasswordQuestion passwordQuestion : data.getQuestionListList()) {
//            Question question = new Question(passwordQuestion.getQuestionId(), passwordQuestion.getQuestion());
//            questionList.add(question);
//        }
//    }

    @OnClick(R.id.submit_btn)
    public void submit() {
        for (int i = 0; i < RegData.N; i++) {
            int selectedQuestion = regData.questionIdList[i];
            if (selectedQuestion == -1) {
                toast(getString(R.string.user_security_question_hint, i + 1));
                return;
            }

            EditText answerEt = answerEts[i];
            String text = answerEt.getText().toString();
            if (TextUtils.isEmpty(text)) {
                toast(getString(R.string.user_security_question_answer_hint, i + 1));
                return;
            }
            regData.answerList[i] = Protocol.encodePassword(text);
        }

        requestRegister();
    }

    /**
     * 请求注册
     */
    public void requestRegister() {
//        //  用户名和密码
//        Account.RegistReq.Builder body = Account.RegistReq.newBuilder()
//                .setAccount(regData.username)
//                .setPasswd(Protocol.encodePassword(regData.password))
//                .setImei(PBMessage.getIMEI())
//                .setMac(PBMessage.getMAC())
//                .setPlatform(PBMessage.getPlatform());
//
//        //  昵称
//        Account.RegistReq.UserInfo userInfo = Account.RegistReq
//                .UserInfo
//                .newBuilder()
//                .setAvatar(regData.avatarUrl)
//                .setNickname(regData.nickname)
//                .build();
//        body.setUserInfo(userInfo);
//
//        //  安全问题
//        for (int i = 0; i < regData.questionIdList.length; i++) {
//            Account.PasswordQuestion passwordQuestion = Account.PasswordQuestion
//                    .newBuilder()
//                    .setQuestionId(regData.questionIdList[i])
//                    .setAnswer(regData.answerList[i])
//                    .build();
//            body.addQuestionList(i, passwordQuestion);
//        }
//
//        PBMessage pbMessage = new PBMessage()
//                .setCmd(Account.AccountYimCmd.ACCOUNT_REGIST_REQ_CMD_VALUE)
//                .setBody(body);
//
//        ApiManager.getInstance()
//                .post(new Request(pbMessage))
//                .map(msg -> Account.RegistRsp.parseFrom(msg.getBody()))
//                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
//                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
//                .subscribe(new BaseObserver<Account.RegistRsp>() {
//                    @Override
//                    public void onNext(Account.RegistRsp data) {
//                        handleRegRes(data);
//                    }
//                });
    }

    /**
     * 处理注册结果
     */
    private void handleRegRes(Account.RegistRsp data) {
      if (data.getResult() != Account.RegistRsp.RegistResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        KeyboardUtils.hideSoftInput(getActivity());
        toast(R.string.user_reg_success);

        FragmentActivity activity = getActivity();
        if (activity instanceof LoginActivity) {
            ((LoginActivity) activity).onRegisterSuccess(regData.username, regData.password);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode >= 1 && requestCode <= questionTvs.length) {
            int index = requestCode - 1;
            Question question = data.getParcelableExtra(PickerDialogFragment.EXTRA_SELECTED_ITEM);
            questionTvs[index].setText(question.content);
            regData.questionIdList[index] = question.id;
        }
    }

    @Override
    public void onClick(View v) {
        ArrayList<Question> surplusQuestionList = new ArrayList<>();

        for (Question question : questionList) {
            boolean existed = false;
            for (int id : regData.questionIdList) {
                if (question.id == id) {
                    existed = true;
                    break;
                }
            }

            if (!existed) {
                surplusQuestionList.add(question);
            }
        }

        int index = (int) v.getTag();
        int reqCode = index + 1;
        PickerDialogFragment dialogFragment = PickerDialogFragment.newInstance(surplusQuestionList);
        dialogFragment.setTargetFragment(this, reqCode);
        dialogFragment.show(getFragmentManager(), PickerDialogStringFragment.TAG);
    }

}
