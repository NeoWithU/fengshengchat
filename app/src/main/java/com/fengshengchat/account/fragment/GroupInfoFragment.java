package com.fengshengchat.account.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.chat.view.ChatActivity;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.ItemLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.group.Group;

/**
 * 群组详情
 */
public class GroupInfoFragment extends ToolbarFragment {

    public static final String TAG = "GroupInfoFragment";
    private static final String EXTRA_USER_ID = "USER_ID";

    private long groupId;
    private long userId;
    private boolean isJoined;
    private boolean needInvestConfirm;
    private GInfo info;

    @BindView(R.id.content_layout)
    ViewGroup contentLayout;
    @BindView(R.id.submit_btn)
    Button submitBtn;
    MemberUserInfo owner, admin;

    public static GroupInfoFragment newInstance(long groupId, long userId) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_DATA, groupId);
        args.putLong(EXTRA_USER_ID, userId);
        GroupInfoFragment fragment = new GroupInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_group_info;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }

        groupId = args.getLong(EXTRA_DATA);
        userId = args.getLong(EXTRA_USER_ID);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestJoinedStatus();
    }

    private void requestJoinedStatus() {
        Observable.fromCallable(() -> DbManager.getDb().getGroupMemberDao().find(groupId, UserManager.getUser().uid, Group.DeleteFlag.DELETE_UNDEFINED_VALUE) != null)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean aBoolean) {
                        isJoined = aBoolean;
                        if (isJoined) {
                            submitBtn.setText(R.string.search_enter_group);
                            requestGroupInfoFromDb();
                        } else {
                            requestGroupInfo();
                        }
                    }
                });
    }

    private void requestGroupInfoFromDb() {
        Observable.combineLatest(getGroupObservable(), getGroupMemberCountObservable(), Pair::new)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(pair -> {
                    com.fengshengchat.db.Group group = pair.first;
                    owner = new MemberUserInfo(group.ownerId, group.ownerName, group.avatar);
                    Integer count = pair.second;
                    needInvestConfirm = group.needInvestConfirm;
                    bindGroupInfoRes(group.name, group.avatar, group.notice, group.ownerName, count, group.type);
                })
                .subscribe(new BaseObserver<Pair<com.fengshengchat.db.Group, Integer>>() {
                    @Override
                    public void onNext(Pair<com.fengshengchat.db.Group, Integer> pair) {
                        requestGroupMember();
                    }
                });
    }

    private Observable<com.fengshengchat.db.Group> getGroupObservable() {
        return Observable.fromCallable(() -> DbManager.getDb().getGroupDao().findById(groupId))
                .observeOn(Schedulers.io());
    }

    private Observable<Integer> getGroupMemberCountObservable() {
        return Observable.fromCallable(() -> DbManager.getDb().getGroupMemberDao().getGroupMemberCount(groupId))
                .observeOn(Schedulers.io());
    }

    /**
     * 请求群信息
     */
    private void requestGroupInfo() {
        Group.GetGroupInfoReq.Builder body = Group.GetGroupInfoReq
                .newBuilder()
                .addGroupIdList(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.GET_GROUP_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.GetGroupInfoRsp.parseFrom(msg.getBody()))
                .filter(data -> data.getResult() == Group.GetGroupInfoRsp.GetGroupInfoResult.OK_VALUE && data.getGroupInfoListCount() != 0)
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(getGroupInfoObserver());
    }

    @NonNull
    private BaseObserver<Group.GetGroupInfoRsp> getGroupInfoObserver() {
        return new BaseObserver<Group.GetGroupInfoRsp>() {
            @Override
            public void onNext(Group.GetGroupInfoRsp data) {
                Group.GroupInfo groupInfo = data.getGroupInfoList(0);

                Group.GroupMemberInfo ownerInfo = groupInfo.getGroupOwnerInfo();

                isJoined = groupInfo.getIsMember() == 1;
                if (isJoined) {
                    submitBtn.setText(R.string.search_enter_group);
                } else {
                    submitBtn.setText(R.string.search_joint_group);
                }

                owner = new MemberUserInfo(ownerInfo.getUserId(), ownerInfo.getGroupNickname(), ownerInfo.getSmallAvatar());

                if (groupInfo.getAdminListCount() != 0) {
                    Group.GroupMemberInfo adminInfo = groupInfo.getAdminList(0);
                    admin = new MemberUserInfo(adminInfo.getUserId(), adminInfo.getGroupNickname(), adminInfo.getSmallAvatar());
                }

                needInvestConfirm = groupInfo.getInviteConfirm() == Group.GroupInfo.InviteConfirmType.INVITE_CONFIRM_NEED_CONFIRM_VALUE;
                bindGroupInfoRes(groupInfo.getGroupName(),
                        groupInfo.getGroupLogo(),
                        groupInfo.getGroupNotice(),
                        groupInfo.getGroupOwnerInfo().getGroupNickname(),
                        groupInfo.getGroupMemberCount(),
                        groupInfo.getGroupType());
            }

            @Override
            public void onComplete() {
                super.onComplete();
                requestGroupMember();
            }
        };
    }

    @BindView(R.id.name_layout)
    ItemLayout nameLayout;
    @BindView(R.id.id_layout)
    ItemLayout idLayout;
    @BindView(R.id.notice_layout)
    ItemLayout noticeLayout;
    @BindView(R.id.owner_layout)
    ItemLayout ownerLayout;
    @BindView(R.id.member_layout)
    ItemLayout memberLayout;
    @BindView(R.id.member_container_layout)
    LinearLayout memberContainerLayout;
    @BindView(R.id.user_num_tv)
    TextView userNumTv;

    /**
     * 处理群组信息
     */
    private void bindGroupInfoRes(String groupName, String avatar, String notice, String groupOwnerName, int count, int type) {
        info = new GInfo(groupName, avatar, count, type);
        nameLayout.getMsgTv().setText(groupName);
        idLayout.getMsgTv().setText(String.valueOf(groupId));
        noticeLayout.getMsgTv().setText(notice);
        ownerLayout.getMsgTv().setText(groupOwnerName);
        bindGroupMember(count);
    }

    //  群成员
    private void bindGroupMember(int count) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        String memberCount = String.valueOf(count);
        builder.append("等");
        builder.append(memberCount);
        builder.append("位用户");
        ForegroundColorSpan span = new ForegroundColorSpan(0xFFFF4956);
        builder.setSpan(span, 1, memberCount.length() + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        userNumTv.setText(builder);
    }

    @OnClick(R.id.submit_btn)
    public void submit() {
        if (isJoined) {
            ChatActivity.startChat(getContext(), groupId, true);
        } else {
            requestJoinGroup();
        }
    }

    private void requestJoinGroup() {
        Group.RequestJoinGroupReq.Builder body = Group.RequestJoinGroupReq
                .newBuilder()
                .setInviterUserId(userId)
                .setGroupId(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.REQUEST_JOIN_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.RequestJoinGroupRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Group.RequestJoinGroupRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.RequestJoinGroupRsp data) {
                        handleRequestJoinGroupRes(data);
                    }
                });
    }

    /**
     * 处理请求添加好友请求结果
     */
    private void handleRequestJoinGroupRes(Group.RequestJoinGroupRsp data) {
        if (data.getResult() != Group.RequestJoinGroupRsp.RequestJoinGroupResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        if (needInvestConfirm) {
            toast(R.string.search_req_verify);
        } else {
            isJoined = true;
            submitBtn.setText(R.string.search_enter_group);
            // TODO: 2019/1/18 名称

            if (info == null) {
                ChatActivity.startChat(getContext(), groupId, true);
            } else {
                ChatActivity.startChat(getContext(), groupId, info.name, info.icon, true, info.type, info.memberCount);
            }
        }
    }

    private void requestGroupMember() {
        Group.GetIncrementGroupMemberListReq.Builder body = Group.GetIncrementGroupMemberListReq
                .newBuilder()
                .setUpdateTime(0)
                .setGroupId(groupId)
                .setStart(0)
                .setNum(3);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.GET_INCREMENT_GROUP_MEMBER_LIST_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.GetIncrementGroupMemberListRsp.parseFrom(msg.getBody()))
                .filter(data -> data.getResult() == Group.GetGroupMemberInfoRsp.GetGroupMemberInfoResult.OK_VALUE)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Group.GetIncrementGroupMemberListRsp>() {
                    @Override
                    public void onNext(Group.GetIncrementGroupMemberListRsp data) {
                        bindGroupMember(data);
                    }
                });
    }

    private void bindGroupMember(Group.GetIncrementGroupMemberListRsp data) {
        ArrayList<MemberUserInfo> memberUserInfoArrayList = new ArrayList<>(3);
        memberUserInfoArrayList.add(owner);
        if (admin != null) {
            memberUserInfoArrayList.add(admin);
        }

        for (Group.MemberInfo memberInfo : data.getMemberInfoListList()) {
            long userId = memberInfo.getUserId();
            if (userId == owner.id) {
                owner.avatar = memberInfo.getSmallAvatar();
                continue;
            } else if (admin != null && admin.id == userId) {
                continue;
            }

            if (memberUserInfoArrayList.size() < 3) {
                MemberUserInfo memberUserInfo = new MemberUserInfo(memberInfo.getUserId(),
                        memberInfo.getGroupNickname(), memberInfo.getSmallAvatar());
                memberUserInfoArrayList.add(memberUserInfo);
            }
        }

        LayoutInflater inflater = LayoutInflater.from(getContext());
        for (MemberUserInfo memberInfo : memberUserInfoArrayList) {
            View view = inflater.inflate(R.layout.search_group_member_item, memberContainerLayout, false);
            ImageView avatarIv = view.findViewById(R.id.avatar_iv);
            TextView nameTv = view.findViewById(R.id.name_tv);
            ImageUtils.loadImage(avatarIv, memberInfo.avatar, R.drawable.image_default);
            nameTv.setText(memberInfo.nickname);
            memberContainerLayout.addView(view);
        }
    }

    public static class MemberUserInfo {
        private long id;
        private String nickname;
        private String avatar;

        public MemberUserInfo(long id, String nickname, String avatar) {
            this.id = id;
            this.nickname = nickname;
            this.avatar = avatar;
        }
    }

    public static class GInfo {
        public String name;
        public String icon;
        public int memberCount;
        public int type;

        public GInfo(String name, String icon, int memberCount, int type) {
            this.name = name;
            this.icon = icon;
            this.memberCount = memberCount;
            this.type = type;
        }
    }
}
