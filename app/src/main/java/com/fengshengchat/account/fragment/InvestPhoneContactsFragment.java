package com.fengshengchat.account.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gjiazhe.wavesidebar.WaveSideBar;
import com.fengshengchat.R;
import com.fengshengchat.account.model.TelContact;
import com.fengshengchat.account.view.TelContactViewBinder;
import com.fengshengchat.base.ToolbarFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import me.drakeet.multitype.MultiTypeAdapter;

/**
 * 邀请手机联系人
 */
public class InvestPhoneContactsFragment extends ToolbarFragment {

    public static final String TAG = "InvestPhoneContactsFragment";

    public static InvestPhoneContactsFragment newInstance() {
        Bundle args = new Bundle();
        InvestPhoneContactsFragment fragment = new InvestPhoneContactsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_invest_phone_contacts;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTestData();
        initView();
        initSlideBar();
    }

    private int[] sectionCount;

    private void initTestData() {
        items.add(new TelContact("123456", "一部"));
        items.add(new TelContact("123456", "一快"));
        items.add(new TelContact("03456", "王总"));
        items.add(new TelContact("000056", "韩少"));
        items.add(new TelContact("723456", "张少"));
        items.add(new TelContact("123456", "张教师用书"));
        items.add(new TelContact("123456", "高高"));
        items.add(new TelContact("123456", "一部"));
        items.add(new TelContact("123456", "一快"));
        items.add(new TelContact("03456", "王总"));
        items.add(new TelContact("000056", "韩少"));
        items.add(new TelContact("723456", "张少"));
        items.add(new TelContact("123456", "张教师用书"));
        items.add(new TelContact("123456", "高高"));

        Collections.sort(items, (o1, o2) -> o1.namePinyin.compareTo(o2.namePinyin));

        sectionCount = new int[26];
        int index = 0;
        char c = 0;
        int cnt = 0;
        // TODO: 2018/12/12 考虑没有名字的情况
        for (TelContact item : items) {
            char ch = item.namePinyin.charAt(0);
            if (c == 0) {
                c = ch;
                cnt = 0;
            } else if (c != ch) {
                c = ch;
                sectionCount[index++] = cnt;
                cnt = 1;
            }
        }
    }

    @BindView(R.id.recycler_view)
    public RecyclerView recyclerView;

    private final List<TelContact> items = new ArrayList<>();

    private LinearLayoutManager linearLayoutManager;

    private void initView() {
        MultiTypeAdapter adapter = new MultiTypeAdapter(items);
        adapter.register(TelContact.class, new TelContactViewBinder());

        linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    @BindView(R.id.side_bar)
    WaveSideBar waveSideBar;

    private void initSlideBar() {
//        waveSideBar.setOnSelectIndexItemListener(index -> {
//            if (STAR.equals(index)) {
//                linearLayoutManager.scrollToPosition(0);
//                return;
//            }
//
//            int offset = index.charAt(0) - 'A';
//            int position = offset + friendOps.size();
//            for (int i = 0; i < offset; i++) {
//                position += sections.get(i).count;
//            }
//            linearLayoutManager.scrollToPositionWithOffset(position, 0);
//        });
    }
}
