package com.fengshengchat.user;

import android.support.annotation.NonNull;

import com.blankj.utilcode.util.SPUtils;
import com.fengshengchat.utils.ExSpUtils;

/**
 * @author KRT
 * 2018/12/5
 */
public class UserManager {

    private static final String KEY_USER = "user";

    private volatile static User user;

    public synchronized static void saveUser(@NonNull User user) {
        UserManager.user = user;
        save();
    }

    public static void save() {
        SPUtils.getInstance().put("lastLoginName", user.account);
        ExSpUtils.saveObject(KEY_USER, user);
    }

    public static void saveNickname(String nickname) {
        user.nickname = nickname;
        save();
    }

    public static void saveIcon(String smallAvatar, String bigAvatar) {
        user.icon = smallAvatar;
        user.bigIcon = bigAvatar;
        save();
    }

    public static void save(String nickname, String smallAvatar, String bigAvatar) {
        user.nickname = nickname;
        user.icon = smallAvatar;
        user.bigIcon = bigAvatar;
        save();
    }

    public synchronized static User getUser() {
        if (null == user) {
            user = ExSpUtils.getObject(KEY_USER, User.class);
        }
        return user;
    }

    public static boolean isEmpty() {
        return getUser() == null;
    }

    public static void clearUser() {
        user = null;
        ExSpUtils.remove(KEY_USER);
    }

    public static String getLastLoginUserName() {
        return SPUtils.getInstance().getString("lastLoginName");
    }

}
