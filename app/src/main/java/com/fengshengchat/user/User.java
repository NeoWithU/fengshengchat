package com.fengshengchat.user;

/**
 * @author KRT
 * 2018/12/5
 */
public class User {
    public long uid;
    public String token;
    public String nickname;
    public String account;
    public String icon;
    public String bigIcon;
    public long expireTime;
    public String province;
    public String city;
    public String area;

    public String getUid() {
        return String.valueOf(uid);
    }

    public String getToken() {
        return token;
    }

    public String getNickname() {
        return nickname;
    }

    public String getAccount() {
        return account;
    }

    public String getIcon() {
        return icon;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public synchronized void setToken(String token) {
        this.token = token;
    }

    public String getBigIcon() {
        return bigIcon;
    }
}
