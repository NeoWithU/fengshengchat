package com.fengshengchat.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import me.drakeet.multitype.ItemViewBinder;

/**
 * Base Item View Binder
 * <p>
 */
public abstract class BaseItemViewBinder<DATA, VH extends RecyclerView.ViewHolder> extends ItemViewBinder<DATA, VH> {

    /**
     * Item 点击监听器
     */
    public interface OnClickListener<DATA> {
        void onItemClicked(DATA item);
    }

    /**
     * 移除 Item 监听器
     */
    public interface OnRemovedListener<DATA> {
        void onItemRemoved(DATA item);
    }

    /**
     * 长按 Item 监听器
     */
    public interface OnLongClickedListener<DATA> {
        void onItemLongClicked(DATA item);
    }

    public BaseItemViewBinder() {
    }

    protected OnClickListener<DATA> onClickListener;

    public BaseItemViewBinder(@NonNull OnClickListener<DATA> onClickListener) {
        this.onClickListener = onClickListener;
    }

}
