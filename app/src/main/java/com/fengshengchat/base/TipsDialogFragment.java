package com.fengshengchat.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.Utils;
import com.fengshengchat.R;
import com.fengshengchat.base.base.BaseDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 提示对话框
 */
public class TipsDialogFragment extends BaseDialogFragment {

    public static final String TAG = "ConfirmDialogFragment";

    public static final String EXTRA_MESSAGE = "MESSAGE";
    public static final String EXTRA_POSITIVE_TITLE = "POSITIVE_TITLE";
    public static final String EXTRA_NEGATIVE_TITLE = "NEGATIVE_TITLE";

    public static final int POSITIVE_BUTTON = 1;
    public static final int NEGATIVE_BUTTON = 2;

    public interface Callback {
        void onClickButton(int whichButton);
    }

    public static TipsDialogFragment newInstance(@StringRes int messageResId) {
        return newInstance(Utils.getApp().getString(messageResId));
    }

    public static TipsDialogFragment newInstance(@NonNull String message) {
        return newInstance(message, Utils.getApp().getString(R.string.sure), Utils.getApp().getString(R.string.cancel));
    }

    public static TipsDialogFragment newInstance(@StringRes int messageResId,
                                                 @StringRes int positiveTitleResId,
                                                 @StringRes int negativeTitleResId) {
        return newInstance(
                Utils.getApp().getString(messageResId),
                Utils.getApp().getString(positiveTitleResId),
                Utils.getApp().getString(negativeTitleResId)
        );
    }

    public static TipsDialogFragment newInstance(@NonNull String message,
                                                 @Nullable String positiveTitle,
                                                 @Nullable String negativeTitle) {
        Bundle args = new Bundle();
        args.putString(EXTRA_MESSAGE, message);
        args.putString(EXTRA_POSITIVE_TITLE, positiveTitle);
        args.putString(EXTRA_NEGATIVE_TITLE, negativeTitle);
        TipsDialogFragment fragment = new TipsDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String message;
    private String positiveTitle;
    private String negativeTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }

        message = args.getString(EXTRA_MESSAGE);
        positiveTitle = args.getString(EXTRA_POSITIVE_TITLE);
        negativeTitle = args.getString(EXTRA_NEGATIVE_TITLE);
    }

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.common_tips_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @BindView(R.id.msg_tv)
    public TextView msgTv;

    @BindView(R.id.positive_tv)
    public TextView positiveTv;

    @BindView(R.id.negative_tv)
    public TextView negativeTv;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        msgTv.setText(message);
        positiveTv.setText(positiveTitle);
        negativeTv.setText(negativeTitle);
    }

    @OnClick(R.id.positive_tv)
    public void clickPositiveButton() {
        clickButton(POSITIVE_BUTTON);
    }

    @OnClick(R.id.negative_tv)
    public void clickNegativeButton() {
        clickButton(NEGATIVE_BUTTON);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        if (window != null) {
            int width = (int) (ScreenUtils.getScreenWidth() * 0.92f);
            window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
        }
        super.onResume();
    }

    private void clickButton(int whichButton) {
        FragmentActivity activity = getActivity();
        if (activity instanceof Callback) {
            ((Callback) activity).onClickButton(whichButton);
        }
        dismiss();
    }

}
