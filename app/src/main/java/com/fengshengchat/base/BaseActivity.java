package com.fengshengchat.base;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.fengshengchat.account.activity.LoginActivity;
import com.fengshengchat.account.fragment.LoginFragment;
import com.fengshengchat.base.mvp.BaseMVPActivity;
import com.fengshengchat.base.mvp.IBasePresenter;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.MsgWrapper;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.push.HandlePushService;
import com.fengshengchat.push.IPushObserver;

import java.util.List;

import yiproto.yichat.msg.MsgOuterClass;

public abstract class BaseActivity<T extends IBasePresenter> extends BaseMVPActivity<T> {
    private IPushObserver mPushObserver;
    private ServiceConnector mService;
    private HandlePushService.HandlePushServiceInterface mPusher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindPushService();
    }

    @Override
    protected void onDestroy() {
        clearErrorState();
        super.onDestroy();
        unbindService(mService);
        mService = null;
    }

    @Override
    protected void onStop() {
        removePushObserver();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        addPushObserver();
    }

    protected void addPushObserver(){
        if(null != mPusher) {
            mPusher.addObserver(mPushObserver);
        }
    }

    protected void removePushObserver(){
        if(null != mPusher) {
            mPusher.removeObserver(mPushObserver);
        }
    }

    private void bindPushService(){
        mPushObserver = new PushObserver();
        mService = new ServiceConnector();
        Intent intent = new Intent(getContext(), HandlePushService.class);
        bindService(intent, mService, Context.BIND_AUTO_CREATE);
    }

    public void push(PBMessage message){
        if(null != mPusher){
            mPusher.push(message);
        }
    }

    @CallSuper
    protected void onPusherReady(){
        mPusher.addObserver(mPushObserver);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if(null != fragments){
            for(Fragment fragment : fragments){
                if(fragment instanceof BaseFragment){
                    ((BaseFragment) fragment).onPusherReady();
                }
            }
        }
    }

    protected abstract void onReceiveMessage(MsgOuterClass.Msg message);

    public HandlePushService.HandlePushServiceInterface getPusher(){
        return mPusher;
    }

    private class PushObserver implements IPushObserver{
        public void onReceive(MsgWrapper msgWrapper) {
            if(!mUniversalHandler.handleMessage(msgWrapper)){
                onReceiveMessage(msgWrapper.inMsg);
            }
        }
    }

    private Runnable checkPusherReadyRunnable = new Runnable() {
        public void run() {
            printi("-------checkPusherReady------- " + mPusher.isReady());
            if(mPusher.isReady()){
                onPusherReady();
            }else{
                checkPusherReady();
            }
        }
    };
    private void checkPusherReady(){
        runDelay(checkPusherReadyRunnable, 320);
    }

    private class ServiceConnector implements ServiceConnection {
        public void onServiceConnected(ComponentName name, IBinder service) {
            if(service instanceof HandlePushService.HandlePushServiceInterface) {
                printi("handle push service connected");
                mPusher = (HandlePushService.HandlePushServiceInterface) service;
                checkPusherReady();
            }else{
                printe("service connected failed");
            }
        }
        public void onServiceDisconnected(ComponentName name) {
            printi("service disconnected");
        }
    }

    private UniversalHandler mUniversalHandler = new UniversalHandler();
    private class UniversalHandler extends BaseHandler {
        @Override
        protected boolean canHandle(int bigCmd) {
            //TODO
            return true;
        }

        @Override
        public boolean handle(MsgWrapper msgWrapper) {
            switch(msgWrapper.inMsg.getCmd()){
                default:
                    return false;
                case MsgOuterClass.YimCMD.CONN_KICK_OUT_CMD_VALUE: toLogin(msgWrapper.inMsg.getErrMsg());
                    break;
            }
            super.handle(msgWrapper);
            return true;
        }

        private void toLogin(String tip){
            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(LoginFragment.ENTER_REASON, LoginFragment.REASON_KICK_OUT);
            intent.putExtra(LoginFragment.ENTER_REASON_TIP, tip);
            startActivity(intent);
        }
    }
}
