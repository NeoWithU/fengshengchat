package com.fengshengchat.base;


import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * RxJava Helper
 */
public final class RxSchedulersHelper {

    private static final ObservableTransformer sIO2MainSchedulersTransformer =
            (ObservableTransformer<Object, Object>) observable -> observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

    @SuppressWarnings("unchecked")
    public static <T> ObservableTransformer<T, T> applyIO2MainSchedulers() {
        return (ObservableTransformer<T, T>) sIO2MainSchedulersTransformer;
    }

    private static final ObservableTransformer sIOSchedulersTransformer =
            (ObservableTransformer<Object, Object>) observable -> observable.subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io());

    @SuppressWarnings("unchecked")
    public static <T> ObservableTransformer<T, T> applyIOSchedulers() {
        return (ObservableTransformer<T, T>) sIOSchedulersTransformer;
    }
}
