package com.fengshengchat.base;

import android.text.TextUtils;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.google.gson.JsonSyntaxException;
import com.fengshengchat.R;
import com.fengshengchat.net.BaseObserver;

import java.io.EOFException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;


/**
 * Http 请求 Observer
 */
public abstract class HttpRequestObserver<T> extends BaseObserver<T> {

    protected int httpCode;

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        if (!NetworkUtils.isConnected()) {
            httpCode = HttpCode.NETWORK_UNAVAILABLE;
            onError(Utils.getApp().getString(R.string.network_available));
        } else if (e instanceof ConnectException) {
            httpCode = HttpCode.CONNECT_EXCEPTION;
            onError(Utils.getApp().getString(R.string.connect_exception));
        } else if (e instanceof JsonSyntaxException || e instanceof EOFException) {
            httpCode = HttpCode.JSON_SYNTAX_EXCEPTION;
            onError(Utils.getApp().getString(R.string.json_syntax_exception));
        } else if (e instanceof SocketTimeoutException || e instanceof UnknownHostException) {
            httpCode = HttpCode.TIMEOUT_EXCEPTION;
            onError(Utils.getApp().getString(R.string.timeout_exception));
        } else if (e instanceof IOException) {
            httpCode = HttpCode.NETWORK_UNAVAILABLE;
            onError(Utils.getApp().getString(R.string.network_available));
        } else {
            httpCode = HttpCode.UNKNOWN_ERROR;
            onError(e.getMessage());
        }
    }

    protected void onError(String msg) {
        if (TextUtils.isEmpty(msg)) {
            return;
        }

        ToastUtils.showShort(msg);
    }

}
