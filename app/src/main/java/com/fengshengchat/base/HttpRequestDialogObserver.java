package com.fengshengchat.base;

import android.support.v4.app.FragmentManager;

import com.fengshengchat.app.dialog.ProgressDialogFragment;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Http Request Dialog Observer
 */
public abstract class HttpRequestDialogObserver<T> extends HttpRequestObserver<T> {

    @NonNull
    private final FragmentManager fragmentManager;

    public HttpRequestDialogObserver(@NonNull FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    protected ProgressDialogFragment dialogFragment;

    @Override
    public void onSubscribe(@NonNull Disposable d) {
        super.onSubscribe(d);
        dialogFragment = ProgressDialogFragment.newInstance();
        dialogFragment.show(fragmentManager, ProgressDialogFragment.TAG);
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        dismissDialog();
    }

    @Override
    public void onComplete() {
        super.onComplete();
        dismissDialog();
    }

    public void dismissDialog() {
        if (dialogFragment == null) {
            return;
        }

        if (dialogFragment.isResumed()) {
            dialogFragment.dismiss();
        } else {
            dialogFragment.dismissAllowingStateLoss();
        }
    }

}
