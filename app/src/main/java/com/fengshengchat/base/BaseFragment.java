package com.fengshengchat.base;

import android.support.annotation.CallSuper;
import android.support.v4.app.FragmentActivity;

import com.fengshengchat.base.mvp.BaseMVPFragment;
import com.fengshengchat.base.mvp.IBasePresenter;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.push.HandlePushService;

import yiproto.yichat.msg.MsgOuterClass;

public abstract class BaseFragment<T extends IBasePresenter> extends BaseMVPFragment<T> {
    private HandlePushService.HandlePushServiceInterface mPusher;

    protected HandlePushService.HandlePushServiceInterface getPusher(){
        if(null == mPusher){
            FragmentActivity activity = getActivity();
            if(activity instanceof BaseActivity){
                mPusher = ((BaseActivity) activity).getPusher();
            }
        }
        return mPusher;
    }

    public void push(PBMessage message){
        HandlePushService.HandlePushServiceInterface pusher = getPusher();
        if(null != pusher){
            pusher.push(message);
        }
    }

    @CallSuper
    public void onPusherReady(){
        printi("--- onPusherReady ---");
    }

    public abstract boolean handleMessage(MsgOuterClass.Msg message);

}
