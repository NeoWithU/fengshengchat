package com.fengshengchat.base;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.BarUtils;
import com.fengshengchat.R;
import com.fengshengchat.widget.TitleBarLayout;


/**
 * Toolbar Fragment
 * <p>
 * Created by Jinjia on 2017/11/20.
 */
public abstract class ToolbarFragment extends BKFragment {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hackStatusBar(view);
        initToolbar(view);
    }

    public static void hackStatusBar(@NonNull View rootView) {
        if (!(rootView instanceof ViewGroup)) {
            return;
        }

        ViewGroup viewGroup = (ViewGroup) rootView;
        if (viewGroup.getChildCount() == 0) {
            return;
        }

        View offsetView = new View(rootView.getContext());
        offsetView.setBackgroundColor(Color.WHITE);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, BarUtils.getStatusBarHeight());
        offsetView.setLayoutParams(lp);
        viewGroup.addView(offsetView, 0);

        if (!(viewGroup instanceof LinearLayout)) {
            BarUtils.addMarginTopEqualStatusBarHeight(viewGroup.getChildAt(1));
        }
    }

    public static void hackStatusBar(@NonNull TitleBarLayout titleBar) {
        int statusBarHeight = BarUtils.getStatusBarHeight();
        titleBar.setPadding(titleBar.getLeft(), titleBar.getTop() + statusBarHeight, titleBar.getRight(), titleBar.getBottom());
        ViewGroup.LayoutParams params = titleBar.getLayoutParams();
        params.height += statusBarHeight;
        titleBar.setLayoutParams(params);
    }

    private void initToolbar(View view) {
        TitleBarLayout titleBarLayout = view.findViewById(R.id.title_bar);
        if (titleBarLayout != null) {
            titleBarLayout.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
                @Override
                public void onBackClick() {
                    onBack();
                }

                @Override
                public void onActionImageClick() {
                }

                @Override
                public void onActionClick() {
                }
            });
        }
    }

    protected void onBack() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            getActivity().finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

}
