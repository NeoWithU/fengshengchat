package com.fengshengchat.base;

/**
 * Http Code
 * <p>
 * Created by Jinjia on 2017/11/13.
 */
public interface HttpCode {

    /**
     * 成功
     */
    int SUCCESS = 0;

    /**
     * 未知错误
     */
    int UNKNOWN_ERROR = Integer.MAX_VALUE - 1;
    /**
     * 网络不可用
     */
    int NETWORK_UNAVAILABLE = Integer.MAX_VALUE - 2;
    /**
     * Socket 超时异常
     */
    int TIMEOUT_EXCEPTION = Integer.MAX_VALUE - 3;
    /**
     * 链接异常
     */
    int CONNECT_EXCEPTION = Integer.MAX_VALUE - 4;
    /**
     * Json 解析异常
     */
    int JSON_SYNTAX_EXCEPTION = Integer.MAX_VALUE - 5;
    /**
     * 数据data为空
     */
    int RESULT_NO_DATA = Integer.MAX_VALUE - 6;

    /**
     *  非法 token
     */
    int TOKEN_INVALID = 22;
    int TOKEN_INVALID_2 = 33;
}
