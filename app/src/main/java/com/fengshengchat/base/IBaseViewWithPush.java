package com.fengshengchat.base;

import com.fengshengchat.base.mvp.IBaseView;
import com.fengshengchat.push.HandlePushService;

public interface IBaseViewWithPush extends IBaseView {
    HandlePushService.HandlePushServiceInterface getPusher();
}
