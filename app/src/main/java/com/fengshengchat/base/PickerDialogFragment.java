package com.fengshengchat.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aigestudio.wheelpicker.WheelPicker;
import com.fengshengchat.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Picker Dialog Fragment
 */
public class PickerDialogFragment extends BottomSheetDialogFragment implements
        WheelPicker.OnItemSelectedListener, View.OnClickListener {

    public static final String TAG = "PickerDialogFragment";
    public static final String EXTRA_SELECTED_ITEM = "SELECTED_ITEM";
    public static final String EXTRA_POSITION = "POSITION";

    public static PickerDialogFragment newInstance(ArrayList<? extends Parcelable> items) {
        return newInstance(items, 0);
    }

    public static PickerDialogFragment newInstance(ArrayList<? extends Parcelable> items, int position) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_SELECTED_ITEM, items);
        args.putInt(EXTRA_POSITION, position);
        PickerDialogFragment fragment = new PickerDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private List<? extends Parcelable> mItems;

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mItems = (List<? extends Parcelable>) args.getSerializable(EXTRA_SELECTED_ITEM);
        mPosition = args.getInt(EXTRA_POSITION, 0);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.common_wheel_picker_dialog, container, false);
    }

    private Parcelable mSelectedItem;
    private int mPosition;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        WheelPicker wheelPicker = view.findViewById(R.id.wheel_picker);
        wheelPicker.setData(mItems);
        wheelPicker.setOnItemSelectedListener(this);
        wheelPicker.setSelectedItemPosition(mPosition);

        mSelectedItem = mItems.get(mPosition);

        view.findViewById(R.id.negative_tv).setOnClickListener(this);
        view.findViewById(R.id.positive_tv).setOnClickListener(this);
    }

    @Override
    public void onItemSelected(WheelPicker picker, Object data, int position) {
        mPosition = position;
        mSelectedItem = mItems.get(position);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.negative_tv) {
            dismiss();
        } else if (id == R.id.positive_tv) {
            onClickPositiveButton();
            dismiss();
        }
    }

    private void onClickPositiveButton() {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_SELECTED_ITEM, mSelectedItem);
            intent.putExtra(EXTRA_POSITION, mPosition);
            targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        }
    }
}
