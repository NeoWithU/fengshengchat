package com.fengshengchat.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.KeyboardUtils;
import com.fengshengchat.R;
import com.fengshengchat.base.base.BaseFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BKFragment extends BaseFragment {

    public static final String EXTRA_DATA = "data";

    @Nullable
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            unbinder = ButterKnife.bind(this, view);
        }
        return view;
    }

    @Override
    public void initView(View root) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    public void openFragment(Fragment fragment, String tag) {
        getFragmentManager().beginTransaction()
                .hide(this)
                .add(R.id.fragment_container, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    public void showDialogFragment(DialogFragment dialogFragment, String tag) {
        dialogFragment.show(getFragmentManager(), tag);
    }

    protected void onNewIntent(@Nullable Bundle args) {
    }

    public void openFragmentForSingleTask(@NonNull String tag) {
        openFragmentForSingleTask(tag, null);
    }

    public void openFragmentForSingleTask(@NonNull String tag, @Nullable Bundle args) {
        FragmentManager fm = getFragmentManager();
        BKFragment fragmentByTag = (BKFragment) fm.findFragmentByTag(tag);
        if (fragmentByTag != null) {
            fm.popBackStack(tag, 0);
            fragmentByTag.onNewIntent(args);
        }
    }

    public void finish() {
        KeyboardUtils.hideSoftInput(getActivity());
        getFragmentManager().popBackStack();
    }

}
