package com.fengshengchat;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Process;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.crashlytics.android.Crashlytics;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.utils.Utilities;
import com.fengshengchat.protocol.Protocol;

import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * @author KRT
 * 2018/11/29
 */
public class YiChatApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        LogUtils.getConfig().setLog2FileSwitch(Debug.isLog2File());
        if (getPackageName().equals(getCurrentProcessName())) {
            Debug.e("YiChatApplication", "----------YiChatApplication init-----------");
//            initUncaughtExceptionHandler();
//            initJPush();
        }
        Utilities.init(this);
        Utils.init(this);
        Protocol.initEnv(this);
//        initBugly();
        initFabric();
    }

    private void initJPush() {
//        JPushInterface.setDebugMode(true);
//        JPushInterface.init(this);
    }

    private void initUncaughtExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            Debug.e("YiChatApp", "---[" + Thread.currentThread().getName() + "]---");
            e.printStackTrace();
        });
    }

    public String getCurrentProcessName() {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return "";
        }

        int pid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningApps) {
            if (processInfo.pid == pid) {
                return processInfo.processName;
            }
        }
        return "";
    }

    public static ActivityManager.RunningAppProcessInfo getProcessByName(Context context, String name) {
        if (null == name || name.length() <= 0) {
            return null;
        }

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }

        for (ActivityManager.RunningAppProcessInfo processInfo : runningApps) {
            if (name.equals(processInfo.processName)) {
                return processInfo;
            }
        }
        return null;
    }

    //  全局设定 SmartRefreshLayout Header 和 Footer 构建器
    static {
        //  设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator((context, layout) -> {
            layout.setPrimaryColorsId(R.color.white, R.color.refresh_layout_text_color); //  全局设置主题颜色
            return new ClassicsHeader(context);//.setTimeFormat(new DynamicTimeFormat("更新于 %s"));
        });

        //  设置全局的Footer构建器
//        SmartRefreshLayout.setDefaultRefreshFooterCreator(new DefaultRefreshFooterCreator() {
//            @NonNull
//            @Override
//            public RefreshFooter createRefreshFooter(@NonNull Context context, @NonNull RefreshLayout layout) {
//                return new ClassicsFooter(context).setDrawableSize(20);
//            }
//        });
    }


    /**
     * Bugly app id
     */
//    private static final String BUGLY_APP_ID = "538e09090d";

    /**
     * 设置 Bugly
     */
//    private void initBugly() {
//        //  只在主进程上报，以节省流量、内存等资源
//        LogUtils.e("is main thread: " + getPackageName().equals(getCurrentProcessName()));
//        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(this);
//        strategy.setUploadProcess(getPackageName().equals(getCurrentProcessName()));
//        CrashReport.initCrashReport(this, BUGLY_APP_ID, BuildConfig.DEBUG);
//    }
    private void initFabric() {
        Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .build();
        Fabric.with(fabric);
    }


}
