package com.fengshengchat.protocol;

import java.util.List;

import yiproto.yichat.msg.MsgOuterClass;

public class MsgWrapper {
    public MsgOuterClass.Msg inMsg;
    public MsgOuterClass.Msg outMsg;
    public List<MsgOuterClass.Msg> outMsgs;
    public long extRefreshGroupInfo;
}
