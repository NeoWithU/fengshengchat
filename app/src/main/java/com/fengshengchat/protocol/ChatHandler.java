package com.fengshengchat.protocol;

import android.util.LongSparseArray;

import com.google.protobuf.InvalidProtocolBufferException;
import com.hwangjr.rxbus.RxBus;
import com.fengshengchat.chat.model.ChatModel;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.DbChatList;
import com.fengshengchat.db.DbChatRecord;
import com.fengshengchat.db.DbFriend;
import com.fengshengchat.db.Group;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.push.NotifyUtils;
import com.fengshengchat.test.RefreshTEST;
import com.fengshengchat.user.UserManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import yiproto.yichat.chat.Chat;
import yiproto.yichat.groupchat.Groupchat;
import yiproto.yichat.msg.MsgOuterClass;
import yiproto.yichat.sportfulgroup.Sportfulgroup;

public class ChatHandler extends BaseHandler {

    @Override
    protected boolean canHandle(int bigCmd) {
        //TODO
        return true;//bigCmd == MsgOuterClass.BigCmd.BIG_CMD_FRIEND_VALUE
//                || bigCmd == MsgOuterClass.BigCmd.BIG_CMD_GROUP_VALUE;
    }

    @Override
    public boolean handle(MsgWrapper msgWrapper) {
        switch(msgWrapper.inMsg.getCmd()){
            default:
                return false;

                //Common
            case MsgOuterClass.YimCMD.TAG_SESSION_MSG_READ_RSP_CMD_VALUE:
                sendSingleAlreadyReadResponse(msgWrapper.inMsg);
                break;
            case MsgOuterClass.YimCMD.TAG_GROUP_NEW_MSG_READ_RSP_CMD_VALUE:
                sendGroupAlreadyReadResponse(msgWrapper.inMsg);
                break;

                //Single
            case MsgOuterClass.YimCMD.SEND_CHAT_RSP_CMD_VALUE:
                handleSendSingleChatResponse(msgWrapper.inMsg);
                break;
            case MsgOuterClass.YimCMD.CHAT_MSG_PUSH_CMD_VALUE:
                handleReceiveSingleChatMessage(msgWrapper.inMsg);
                break;
            case MsgOuterClass.YimCMD.GET_SESSION_NEW_MSG_INFO_RSP_CMD_VALUE:
                handlePullSingleNewChatListResponse(msgWrapper);
                break;
            case MsgOuterClass.YimCMD.GET_SESSION_NEW_MSG_RSP_CMD_VALUE:
                handlePullSingleNewMessageResponse(msgWrapper);
                break;

                //Group
            case MsgOuterClass.YimCMD.SEND_GROUP_CHAT_RSP_CMD_VALUE:
                handleSendGroupMessageResponse(msgWrapper.inMsg);
                break;
            case MsgOuterClass.YimCMD.GROUP_CHAT_MSG_PUSH_CMD_VALUE:
                handleReceiveGroupMessageResponse(msgWrapper.inMsg);
                break;
            case MsgOuterClass.YimCMD.GET_GROUP_SESSION_NEW_MSG_INFO_RSP_CMD_VALUE:
                handlePullGroupChatListResponse(msgWrapper);
                break;
            case MsgOuterClass.YimCMD.GET_GROUP_NEW_MSG_RSP_CMD_VALUE:
                handlePullGroupNewMessageResponse(msgWrapper);
                break;
            case MsgOuterClass.YimCMD.GROUP_SYSTEM_BROADCAST_CMD_VALUE:
                handleGroupBroadcast(msgWrapper);
                break;

            case MsgOuterClass.YimCMD.SPORTFULGROUP_CHAT_MSG_PUSH_CMD_VALUE:
                onReceiveFunGroupMessage(msgWrapper.inMsg);
                break;
        }
        super.handle(msgWrapper);
        return true;
    }

    private void onReceiveFunGroupMessage(MsgOuterClass.Msg msg) {
        if(BaseHandler.isNoError(msg.getCode())) {
            try {
                Sportfulgroup.SportfulGroupChat groupChatMsg = Sportfulgroup.SportfulGroupChat.parseFrom(msg.getBody());
                int showType = groupChatMsg.getShowType();
                boolean a = Sportfulgroup.SHOW_TYPE.SHOW_TYPE_NORMAL_SYSTEM_VALUE == showType
                        || Sportfulgroup.SHOW_TYPE.SHOW_TYPE_BUSINESS_SYSTEM_VALUE == showType;
                DbChatList.updateChatItemKeepStateAndName(a ? 0 : 1,
                        DataTypeConverter.convertChatListBean(groupChatMsg));
                printi("--- onReceiveFunGroupMessage success --- ");
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printi("--- onReceiveFunGroupMessage failed --- " + msg.getErrMsg());
    }

    public static boolean needHandle(Groupchat.GroupSystemBroadcast groupBroadcast){
        Groupchat.OperatorInfo operator = groupBroadcast.getOperator();
        long uid = UserManager.getUser().uid;

        if(operator.getOperatorId() == uid){
            return true;
        }

        Group groupInfo = DbFriend.getGroupInfo(groupBroadcast.getGroupId());
        long adminId = 0;
        try {
            adminId = groupInfo.getAdminId();
        }catch(Exception e){
            e.printStackTrace();
        }
        if(uid == adminId || uid == groupInfo.ownerId){
            return true;
        }

        List<Groupchat.TargetInfo> targetListList = groupBroadcast.getTargetListList();
        if(null != targetListList){
            for(Groupchat.TargetInfo info : targetListList){
                if(info.getTargetId() == uid){
                    return true;
                }
            }
        }
        return false;
    }

    private void handleGroupBroadcast(MsgWrapper msgWrapper){
        if (isNoError(msgWrapper.inMsg.getCode())) {
            try {
                Groupchat.GroupSystemBroadcast groupBroadcast = Groupchat.GroupSystemBroadcast.parseFrom(msgWrapper.inMsg.getBody());
                printe("---------group broadcast: " + groupBroadcast);
                if(Groupchat.GROUP_SYS_MSG_TYPE.INITIATIVE_OUT_VALUE == groupBroadcast.getMsgType()
                        || Groupchat.GROUP_SYS_MSG_TYPE.KICT_OUT_VALUE == groupBroadcast.getMsgType()){
                    msgWrapper.outMsg = generateRefreshGroupInfoInternalMsg(groupBroadcast.getGroupId());
                    if(!needHandle(groupBroadcast)){
                        return;
                    }
                }else if(Groupchat.GROUP_SYS_MSG_TYPE.DEL_GROUP_VALUE == groupBroadcast.getMsgType()){
                    RxBus.get().post(BusEvent.TAG_DELETED_GROUP, groupBroadcast.getGroupId());
                    return;
                }else if(Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_ANNOUNCEMENT_OPEN_VALUE == groupBroadcast.getMsgType()
                        || Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_ANNOUNCEMENT_OFF_VALUE == groupBroadcast.getMsgType()){
                    return;
                }
                RefreshTEST.testPoint("群广播");
                DbChatRecord.saveReceiveGroupBroadcastRecord(groupBroadcast);
//                DbChatList.updateChatItemKeepStateAndNameAndContent(0, DataTypeConverter.convertChatListBean(groupBroadcast));
                DbChatList.updateChatItemKeepStateAndName(0, DataTypeConverter.convertChatListBean(groupBroadcast));
                msgWrapper.outMsg = generateRefreshGroupInfoInternalMsg(groupBroadcast.getGroupId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private MsgOuterClass.Msg generateRefreshGroupInfoInternalMsg(long gid){
        return MsgOuterClass.Msg.newBuilder()
                .setBigCmd(Constant.INTERNAL_CMD_TYPE)
                .setCmd(Constant.INTERNAL_CMD_REFRESH_GROUP_INFO)
                .setUid(gid)
                .build();
    }

    private void handleSendSingleChatResponse(MsgOuterClass.Msg msg){
        if(isNoError(msg.getCode())) {
            try {
                Chat.SendChatRsp sendChatRsp = Chat.SendChatRsp.parseFrom(msg.getBody());
                DbChatRecord.updateSendResultState(msg.getSeq(), sendChatRsp.getMsgSeq(), Constant.MESSAGE_STATE_OK);
                printi("--- send chat OK ---");
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printe("--- send chat failed: " + msg.getErrMsg());
        try {
            Chat.SendChatRsp sendChatRsp = Chat.SendChatRsp.parseFrom(msg.getBody());
            saveTip(sendChatRsp.getToUid(), msg.getErrMsg(), sendChatRsp.getSendTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        DbChatRecord.updateSendResultState(msg.getSeq(), msg.getSeq(), Constant.MESSAGE_STATE_FAILED);
    }

    private void saveTip(long hisUid, String tipstr, long time){
        ChatRecord tip = new ChatRecord();
        tip.messageType = Constant.MESSAGE_TYPE_TIP;
        tip.content = tipstr;
        com.fengshengchat.db.User his = new com.fengshengchat.db.User();
        his.uid = hisUid;
        tip.his = his;
        new ChatModel().saveMessage(tip, false);
        ChatListBean chatItem = DbChatList.getChatItem(hisUid);
        if(null != chatItem) {
            chatItem.lastMessage = tipstr;
            chatItem.time = time;
            DbChatList.updateChatItem(chatItem);
        }
    }

    private void handleReceiveSingleChatMessage(MsgOuterClass.Msg msg){
        if(isNoError(msg.getCode())){
            printi("--- Receive chat message --- " + msg);
            try {
                Chat.ChatMsg chatMsg = Chat.ChatMsg.parseFrom(msg.getBody());

                //TODO
                if(isPullingNewMessage)
                    ;



                DbChatRecord.saveReceiveSingleChatRecord(chatMsg);
                DbChatList.updateChatItemKeepStateAndName(1, DataTypeConverter.convertChatListBean(chatMsg));
                long fromUid = chatMsg.getFromUid();
                if(fromUid != UserManager.getUser().uid) {
                    Account friend = DbFriend.getFriend(fromUid);
                    if(null != friend && !friend.notDisturb) {
                        notifyNewMessage(fromUid, friend.nickname, chatMsg.getMsg());
                    }
                }
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printe("--- Receive chat failed: " + msg.getErrMsg());
    }

    private void handleSendGroupMessageResponse(MsgOuterClass.Msg msg){
        if(isNoError(msg.getCode())) {
            try {
                Groupchat.SendGroupChatRsp sendGroupChatRsp = Groupchat.SendGroupChatRsp.parseFrom(msg.getBody());
                DbChatRecord.updateSendResultState(msg.getSeq(), sendGroupChatRsp.getMsgSeq(), Constant.MESSAGE_STATE_OK);
                printi("--- send group chat OK ---");
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printe("--- send group chat failed: " + msg.getErrMsg());
        try {
            Groupchat.SendGroupChatRsp sendGroupChatRsp = Groupchat.SendGroupChatRsp.parseFrom(msg.getBody());
            saveTip(sendGroupChatRsp.getGroupId(), msg.getErrMsg(), sendGroupChatRsp.getSendTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        DbChatRecord.updateSendResultState(msg.getSeq(), msg.getSeq(), Constant.MESSAGE_STATE_FAILED);
    }

    private void handleReceiveGroupMessageResponse(MsgOuterClass.Msg msg){
        if(isNoError(msg.getCode())) {
            try {
                Groupchat.GroupChatMsg groupChatMsg = Groupchat.GroupChatMsg.parseFrom(msg.getBody());

                if(isPullingNewMessage)
                    ;


                DbChatRecord.saveReceiveGroupChatRecord(groupChatMsg);
                DbChatList.updateChatItemKeepStateAndName(1,
                        DataTypeConverter.convertChatListBean(groupChatMsg));
                printi("--- receive GROUP message --- " + groupChatMsg.getMsgContent());
                long fromUid = groupChatMsg.getUid();
                if(fromUid != UserManager.getUser().uid){
                    Group groupInfo = DbFriend.getGroupInfo(groupChatMsg.getGroupId());
                    if(null != groupInfo && !groupInfo.notDisturb) {
                        notifyNewMessage(groupInfo.id, groupInfo.name, groupChatMsg.getMsgContent());
                    }
                }
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printe("--- receive GROUP message failed: " + msg.getErrMsg());
    }

    private void handlePullSingleNewChatListResponse(MsgWrapper msgWrapper){
        if (BaseHandler.isNoError(msgWrapper.inMsg.getCode())) {
            LongSparseArray<Long> needPull = extractSingleNeedPullItem(msgWrapper.inMsg);
            DbChatList.updateLastSequence(needPull);

            List<ChatListBean> needPullItems = new ArrayList<>();
            ChatListBean chatItem;
            for(int i = 0; i < needPull.size(); i++){
                chatItem = DbChatList.getChatItem(needPull.keyAt(i));
                if(null == chatItem) {
                    chatItem = new ChatListBean();
                    chatItem.hisUid = needPull.keyAt(i);
                    chatItem.lastReceiveSequence = needPull.valueAt(i);
                }
                needPullItems.add(chatItem);
            }
            packPullSingleChatMessageRequests(needPullItems, msgWrapper);
        }
    }

    private void handlePullGroupChatListResponse(MsgWrapper msgWrapper){
        if (BaseHandler.isNoError(msgWrapper.inMsg.getCode())) {
            LongSparseArray<Long> needPull = extractGroupNeedPullItem(msgWrapper.inMsg);
            DbChatList.updateLastSequence(needPull);

            List<ChatListBean> needPullItems = new ArrayList<>();
            ChatListBean chatItem;
            for(int i = 0; i < needPull.size(); i++){
                chatItem = DbChatList.getChatItem(needPull.keyAt(i));
                if(null == chatItem) {
                    chatItem = new ChatListBean();
                    chatItem.hisUid = needPull.keyAt(i);
                    chatItem.lastReceiveSequence = needPull.valueAt(i);
                    DbChatList.insertEmptyGroupItem(chatItem);
                }
                needPullItems.add(chatItem);
            }
            packPullGroupChatMessageRequests(needPullItems, msgWrapper);
        }
    }

    private void packPullSingleChatMessageRequests(List<ChatListBean> needPullItems, MsgWrapper msgWrapper){
        List<MsgOuterClass.Msg> outMsgs = new ArrayList<>();
        for(ChatListBean bean : needPullItems){
            printe("--- pull singleChatMessage --- req: " + bean);
            if(!isPulledAll(bean)) {
                outMsgs.add(generatePullSingleChatMessageRequest(bean));
            }
        }
        msgWrapper.outMsgs = outMsgs;
        printe("--- pull single ChatNewMessages --- task: " + outMsgs.size());
    }

    private void packPullGroupChatMessageRequests(List<ChatListBean> needPullItems, MsgWrapper msgWrapper){
        List<MsgOuterClass.Msg> outMsgs = new ArrayList<>();
        for(ChatListBean bean : needPullItems){
            printe("--- pull group ChatMessage --- req: " + bean);
            if(!isPulledAll(bean)) {
                outMsgs.add(generatePullGroupChatMessageRequest(bean));
            }
        }
        msgWrapper.outMsgs = outMsgs;
        printe("--- pull group ChatNewMessages --- task: " + outMsgs.size());
    }

    private MsgOuterClass.Msg generatePullSingleChatMessageRequest(ChatListBean bean){
        return new PBMessage()
                .setCmd(MsgOuterClass.YimCMD.GET_SESSION_NEW_MSG_REQ_CMD_VALUE)
                .setBody(Chat.GetSessNewMsgReq.newBuilder()
                        .setFromUid(bean.hisUid)
                        .setUid(UserManager.getUser().uid)
                        .setCurSeq(bean.alreadyPullSequence)
                        .setMaxSeq(bean.lastReceiveSequence)
                        .setIndex(bean.resultCount))
                .getMsg();
    }

    private MsgOuterClass.Msg generatePullGroupChatMessageRequest(ChatListBean bean){
        return new PBMessage()
                .setCmd(MsgOuterClass.YimCMD.GET_GROUP_NEW_MSG_REQ_CMD_VALUE)
                .setBody(Groupchat.GetGroupNewMsgReq.newBuilder()
                        .setGroupId(bean.hisUid)
                        .setUid(UserManager.getUser().uid)
                        .setGroupType(bean.groupType)
                        .setCurSeq(bean.alreadyPullSequence)
                        .setMaxSeq(bean.lastReceiveSequence)
                        .setIndex(bean.resultCount))
                .getMsg();
    }

    private LongSparseArray<Long> extractSingleNeedPullItem(MsgOuterClass.Msg message){
        LongSparseArray<Long> map = new LongSparseArray<>();
        try {
            Chat.GetSessNewMsgInfoRsp chatRsp = Chat.GetSessNewMsgInfoRsp.parseFrom(message.getBody());
            Iterator<Chat.SessNewInfo> iterator = chatRsp.getSessListMap().values().iterator();
            List<Chat.ChatMsg> listList;
            Chat.ChatMsg chatMsg;
            while (iterator.hasNext()) {
                Chat.SessNewInfo next = iterator.next();
                if(next.getCnt() > 0) { // new message count
                    printe("---New single message: " + next.getCnt());
                    listList = next.getListList();
                    if (listList.size() > 0) {
                        chatMsg = listList.get(listList.size() - 1);
                        map.put(chatMsg.getFromUid(), chatMsg.getMsgSeq());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    private LongSparseArray<Long> extractGroupNeedPullItem(MsgOuterClass.Msg message){
        LongSparseArray<Long> map = new LongSparseArray<>();
        try {
            Groupchat.GetGroupSessNewInfoRsp groupRsp = Groupchat.GetGroupSessNewInfoRsp.parseFrom(message.getBody());
            Iterator<Groupchat.GroupSessNewInfo> iterator = groupRsp.getListMap().values().iterator();
            List<Groupchat.GroupChatMsg> list;
            Groupchat.GroupChatMsg groupMsg;
            while (iterator.hasNext()) {
                Groupchat.GroupSessNewInfo next = iterator.next();
                if(next.getCnt() > 0) { // new message count
                    printe("---New group message: " + next.getCnt());
                    list = next.getMsgListList();
                    if (list.size() > 0) {
                        groupMsg = list.get(list.size() - 1);
                        map.put(groupMsg.getGroupId(), groupMsg.getMsgSeq());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    private boolean isPullingNewMessage = false;
    private void handlePullSingleNewMessageResponse(MsgWrapper msgWrapper){
        printe("---------------pull single NewMessageResponse-----------");
        if(BaseHandler.isNoError(msgWrapper.inMsg.getCode())){
            try {
                Chat.GetSessNewMsgRsp newMsgList = Chat.GetSessNewMsgRsp.parseFrom(msgWrapper.inMsg.getBody());
                List<Chat.ChatMsg> listList = newMsgList.getListList();

                printe("---pull single back: " + (null != listList ? listList.size() : 0));
                if(null != listList && listList.size() > 0) {
                    DbChatRecord.saveReceiveSingleChatRecord(listList);
                    ChatListBean chatListBean = DataTypeConverter.convertChatListBean(listList.get(listList.size() - 1));
                    DbChatList.updateOnPullBack(listList.size(), chatListBean);

                    printe("---pull single back index: " + newMsgList.getIndex());
                    if(newMsgList.getIndex() > 0) {
                        chatListBean = DbChatList.getChatItem(chatListBean.hisUid);
                        if(!isPulledAll(chatListBean)) {
                            msgWrapper.outMsg = generatePullSingleChatMessageRequest(chatListBean);
                        }
                    }
                }
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
    }

    private void handlePullGroupNewMessageResponse(MsgWrapper msgWrapper){
        printe("---------------pull group NewMessageResponse-----------");
        if(BaseHandler.isNoError(msgWrapper.inMsg.getCode())){
            try {
                Groupchat.GetGroupNewMsgRsp groupNewMsgRsp = Groupchat.GetGroupNewMsgRsp.parseFrom(msgWrapper.inMsg.getBody());
                List<Groupchat.GroupChatMsg> listList = groupNewMsgRsp.getListList();

                printe("---pull group back: " + (null != listList ? listList.size() : 0));
                if(null != listList && listList.size() > 0) {
                    DbChatRecord.saveReceiveGroupChatRecord(listList);
                    ChatListBean chatListBean = DataTypeConverter.convertChatListBean(listList.get(listList.size() - 1));
                    DbChatList.updateGroupOnPullBack(listList);
                    checkGroupBroadcast(msgWrapper, listList);

                    printe("--- pull group back index: " + groupNewMsgRsp.getIndex());
                    if(groupNewMsgRsp.getIndex() > 0) {
                        chatListBean = DbChatList.getChatItem(chatListBean.hisUid);
                        if(!isPulledAll(chatListBean)) {
                            msgWrapper.outMsg = generatePullGroupChatMessageRequest(chatListBean);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void checkGroupBroadcast(MsgWrapper msgWrapper, List<Groupchat.GroupChatMsg> list){
        if(null != list && list.size() > 0){
            for(Groupchat.GroupChatMsg msg : list){
                if(Chat.MsgType.MSGTYPE_GROUP_SYSTEM_VALUE == msg.getMsgType()){
                    msgWrapper.extRefreshGroupInfo = msg.getGroupId();
                    break;
                }
            }
        }
    }

    private boolean isPulledAll(ChatListBean chatListBean){
        return chatListBean.alreadyPullSequence == chatListBean.lastReceiveSequence;
    }

    private void sendSingleAlreadyReadResponse(MsgOuterClass.Msg message){
        if(BaseHandler.isNoError(message.getCode())){
            try {
                Chat.TagSessNewReadRsp tagRsp = Chat.TagSessNewReadRsp.parseFrom(message.getBody());
//                DbChatList.updateAlreadyReadSequence(tagRsp.getFromUid(), tagRsp.getMaxSeq());
                printi("--- Tag already read seq: " + tagRsp.getMaxSeq());
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printe("--- Tag already read failed: " + message.getErrMsg());
    }

    private void sendGroupAlreadyReadResponse(MsgOuterClass.Msg message){
        if(BaseHandler.isNoError(message.getCode())){
            try {
                Groupchat.TagGroupMsgReadRsp tagGroupMsgReadRsp = Groupchat.TagGroupMsgReadRsp.parseFrom(message.getBody());
//                DbChatList.updateAlreadyReadSequence(tagRsp.getFromUid(), tagRsp.getMaxSeq());
                printi("--- Tag group already read seq: " + tagGroupMsgReadRsp.getMaxSeq());
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        printe("--- Tag group already read failed: " + message.getErrMsg());
    }

    private void notifyNewMessage(long uid, String title, String content){
        NotifyUtils.notifyNewMessage(uid, title, Protocol.decodeBase64ToString(content));
    }
}
