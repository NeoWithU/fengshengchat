package com.fengshengchat.protocol;

public interface IMessageHandler {
    boolean handleMessage(MsgWrapper msgWrapper);
}
