package com.fengshengchat.protocol;

public abstract class AbsEnv {
    public String getHttpSchema(){
        return "http://";
    }

    public abstract String getDomain();

    public String getSocketUrl(){
        return getDomain();
    }

    public int getSocketPort(){
        return 8089;
    }

    public String getAuthUrl(String url){
        try {
            return getHttpSchema() + (url.split("://")[1]).split("/")[0] + "/";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getSilentAuthUrl(){
        return getHttpSchema() + getDomain() + ":8081/";
    }

    public String getBusinessUrl(){
        return getHttpSchema() + getDomain() + ":9090/";
    }

    public String getUploadUrl(boolean isMultiFile){
        if(isMultiFile){
            return getHttpSchema() + getDomain() + ":9300/multiupload";
        }else{
            return getHttpSchema() + getDomain() + ":9300/upload";
        }
    }

    public String getQRUrl(){
      //  return "http://qr.feige.co/";
        return "http://fengsheng.helimai8.com/";
    }
}
