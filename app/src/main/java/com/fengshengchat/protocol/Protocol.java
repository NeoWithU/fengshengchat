package com.fengshengchat.protocol;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Base64;

import com.blankj.utilcode.util.SPUtils;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.R;
import com.fengshengchat.base.HttpRequestObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.utils.NetUtils;
import com.fengshengchat.base.utils.TimeUtils;
import com.fengshengchat.base.utils.ToastUtils;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.RetrofitWrapper;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;

import org.whispersystems.curve25519.Curve25519;
import org.whispersystems.curve25519.Curve25519KeyPair;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import yiproto.yichat.login.Login;
import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/11/22
 */
public class Protocol {
    public static final int ENCRYPT_VERSION_NONE = 1;
    public static final int ENCRYPT_VERSION_FIXED_KEY = 2;
    public static final int ENCRYPT_VERSION_DH = 3;
    public static final int ENCRYPT_VERSION = ENCRYPT_VERSION_DH;

    public static final int RETRY_MAX_COUNT = 2;
    public static final int NET_CONNECT_TIMEOUT = 5; //s
    public static final int NET_READ_TIMEOUT = 5; //s
    public static final int NET_WRITE_TIMEOUT = 5; //s

    public static final int ENV_222 = 0;
    public static final int ENV_103 = 1;
    public static final int ENV_FG = 2;
    private static final int DEFAULT_ENV = ENV_103;
    private static int mEnv = DEFAULT_ENV;
    private static AbsEnv envInstance;

    public static final int MAX_INPUT_LENGTH = 1000;

    private static final int BASE64_FLAG = Base64.NO_WRAP;

    public static void initEnv(Context context){
        mEnv = SPUtils.getInstance().getInt("env", -1);
        Debug.e("test", "-------1------env: " + mEnv);
        if(-1 == mEnv) {
            try {
                ApplicationInfo appInfo = context.getPackageManager()
                        .getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                mEnv = appInfo.metaData.getInt("env", DEFAULT_ENV);
                setEnv(mEnv);
                Debug.e("test", "-------2------env: " + mEnv);
            } catch (Exception e) {
                e.printStackTrace();
                mEnv = DEFAULT_ENV;
            }
        }
        switch(mEnv){
            default:
            case ENV_103: envInstance = new ENV_103();
                break;
            case ENV_222: envInstance = new ENV_222();
                break;
            case ENV_FG: envInstance = new ENV_FG();
                break;
        }
        Debug.e("test", "-------3------env: " + mEnv);
    }

    public static void setEnv(int env){
        mEnv = env;
        SPUtils.getInstance().put("env", mEnv);
    }

    public static int getEnv(){
        return mEnv;
    }

    public static String getDomain(){
        return envInstance.getDomain();
    }

    public static String getSocketUrl(){
        return envInstance.getSocketUrl();
    }

    public static int getSocketPort(){
        return envInstance.getSocketPort();
    }

    public static String getBusinessUrl(){
        return envInstance.getBusinessUrl();
    }

    public static String getQRUrl(){
        return envInstance.getQRUrl();
    }

    public static String getUploadUrl(boolean isMultiFile) {
        return envInstance.getUploadUrl(isMultiFile);
    }

    public static ByteString convert2ByteString(byte[] src) {
        return ByteString.copyFrom(src);
    }

    public static ByteString encodeBody(byte[] input) {
        return convert2ByteString(input);
    }

    public static String encodeSHA256(String src) {
        return byte2Hex(digest("SHA-256", src));
    }

    public static String encodePassword(String src) {
        return encodeBase64ToString(encodeSHA256(src));
    }

    public static String encodeBase64ToString(String src){
        if(null == src)
            return null;
        return encodeBase64ToString(src.getBytes());
    }

    public static String encodeBase64ToString(byte[] src){
        if(null == src)
            return null;
        try {
            return Base64.encodeToString(src, BASE64_FLAG);
        }catch(Exception e){
            e.printStackTrace();
            return new String(src);
        }
    }

    public static byte[] encodeBase64(byte[] src){
        if(null == src)
            return null;
        try {
            return Base64.encode(src, BASE64_FLAG);
        }catch(Exception e){
            e.printStackTrace();
            return src;
        }
    }

    public static String decodeBase64ToString(String src){
        if(null == src)
            return null;
        return new String(decodeBase64(src.getBytes()));
    }

    public static byte[] decodeBase64(byte[] src){
        if(null == src)
            return null;
        try {
            return Base64.decode(src, BASE64_FLAG);
        }catch(Exception e){
            e.printStackTrace();
            return src;
        }
    }

    public static byte[] encodeWithYiMethod(byte[] src){
        switch(ENCRYPT_VERSION){
            default:
            case ENCRYPT_VERSION_NONE:
                return src;
            case ENCRYPT_VERSION_FIXED_KEY:
                return encodeAESWithFixedKey(src);
            case ENCRYPT_VERSION_DH:
                return encodeWithDH(src);
        }
    }

    public static byte[] decodeWithYiMethod(byte[] src){
        try {
            switch(ENCRYPT_VERSION){
                default:
                case ENCRYPT_VERSION_NONE:
                    return src;
                case ENCRYPT_VERSION_FIXED_KEY:
                    return decodeAESWithFixedKey(src);
                case ENCRYPT_VERSION_DH:
                    return decodeWithDH(src);
            }
        }catch(Exception e){
            e.printStackTrace();
            return src;
        }
    }

    private static final String PRIVATE_KEY = "sdfjnxmyiyisebtskkj66871#@%jituy";
    private static final byte[] PRIVATE_KEY_BYTES = PRIVATE_KEY.getBytes();
    private static final byte[] IV_PARAMETER_BYTES = PRIVATE_KEY.substring(0, 16).getBytes();
    private static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS7Padding";

    private static boolean useDHKey = false;
    private static byte[] curvePrivateKey;
    private static byte[] privateKeyDH;
    private static byte[] IVParameterDH;
    public static void generatePrivateKeyWithDH(byte[] publicKeyFromServer){
        byte[] sharedSecretBytes = Curve25519.getInstance(Curve25519.BEST)
                .calculateAgreement(publicKeyFromServer, curvePrivateKey);

        String sharedSecret = encodeBase64ToString(sharedSecretBytes);
        if(sharedSecret.length() > PRIVATE_KEY.length()){
            sharedSecret = sharedSecret.substring(0, PRIVATE_KEY.length());
        }

        privateKeyDH = sharedSecret.getBytes();
        IVParameterDH = sharedSecret.substring(0, 16).getBytes();
        curvePrivateKey = null;
        useDHKey = true;

        Debug.e("Protocol", "-------final key: " + new String(privateKeyDH));
    }

    public static String getPrivateKey(){
        if(useDHKey) {
            return new String(privateKeyDH);
        }else{
            return PRIVATE_KEY;
        }
    }

    public static void updateDHPrivateKeyForMain(String key){
        privateKeyDH = key.getBytes();
        IVParameterDH = key.substring(0, 16).getBytes();
        curvePrivateKey = null;
        useDHKey = true;
    }

    public static String getPublicKeyWithDH(){
        Curve25519KeyPair curve25519KeyPair = Curve25519.getInstance(Curve25519.BEST).generateKeyPair();
        curvePrivateKey = curve25519KeyPair.getPrivateKey();
        return encodeBase64ToString(curve25519KeyPair.getPublicKey());
    }

    public static void resetPrivateKeyDH(){
        useDHKey = false;
        Debug.e("Protocol", "---resetPrivateKeyDH--- " + Thread.currentThread().getName());
    }

    private static synchronized byte[] getPrivateKeyBytes(){
        Debug.e("Protocol", "---getPrivateKeyBytes--- " + useDHKey);
        if(useDHKey){
            return privateKeyDH;
        }else{
            return PRIVATE_KEY_BYTES;
        }
    }

    private static synchronized byte[] getIvParameterBytes(){
        if(useDHKey){
            return IVParameterDH;
        }else{
            return IV_PARAMETER_BYTES;
        }
    }

    private static Cipher getEncryptCipher() throws Exception {
        SecretKeySpec aes = new SecretKeySpec(getPrivateKeyBytes(), "AES");
        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, aes, new IvParameterSpec(getIvParameterBytes()));
        return cipher;
    }

    private static Cipher getDecryptCipher() throws Exception {
        SecretKeySpec aes = new SecretKeySpec(getPrivateKeyBytes(), "AES");
        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, aes, new IvParameterSpec(getIvParameterBytes()));
        return cipher;
    }

    public static byte[] encodeWithDH(byte[] src){
        return encodeAESWithFixedKey(src);
    }

    public static byte[] decodeWithDH(byte[] src){
        return decodeAESWithFixedKey(src);
    }

    public static byte[] encodeAESWithFixedKey(byte[] src){
        try {
            Cipher cipher = getEncryptCipher();
            Debug.e("Protocol", "--- encode: " + getPrivateKey());
            return encodeBase64ToString(cipher.doFinal(src)).getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return src;
    }

    public static byte[] decodeAESWithFixedKey(byte[] src){
        try {
            Cipher cipher = getDecryptCipher();
            Debug.e("Protocol", "--- decode: " + getPrivateKey());
            return cipher.doFinal(decodeBase64(src));
        }catch(Exception e){
            e.printStackTrace();
        }
        return src;
    }

    public static String byte2Hex(byte[] bytes) {
        if(null == bytes)
            return null;

        StringBuilder stringBuilder = new StringBuilder();
        String temp;
        for(byte b : bytes){
            temp = Integer.toHexString(b & 0xFF);
            if(1 == temp.length()){
                stringBuilder.append("0");
            }
            stringBuilder.append(temp);
        }
        return stringBuilder.toString();
    }

    public static byte[] digest(String algorithm, String src){
        if(null == src)
            return null;

        try{
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            digest.update(src.getBytes());
            return digest.digest();
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String generateCardString(long uid){
        return getQRUrl() + "add_friend?c_time=" + TimeUtils.getTimeSeconds() + "&user_id=" + uid;
    }

    public static String generateGroupCardString(long gid, long uid){
        return getQRUrl() + "add_group?c_time="
                + TimeUtils.getTimeSeconds() + "&group_id=" + gid
                + "&user_id=" + uid;
    }

    public static long extractUid(String content){
//        if(null == content || content.length() <= 0){
//            return 0L;
//        }else{
//            try {
//                return Long.parseLong(content.split("&user_id=")[1]);
//            }catch(Exception e){
//                e.printStackTrace();
//                return 0L;
//            }
//        }
        try {
            URL domain = new URL(content);
            Map<String, String> map = splitQuery(domain);
            String v = map.get("user_id");
            return getLongValue(v);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static long extractGid(String content){
//        if(null == content || content.length() <= 0){
//            return 0L;
//        }else{
//            try {
//                return Long.parseLong(content.split("&group_id=")[1].split("&user_id=")[0]);
//            }catch(Exception e){
//                e.printStackTrace();
//                return 0L;
//            }
//        }
        try {
            URL domain = new URL(content);
            Map<String, String> map = splitQuery(domain);
            String v = map.get("group_id");
            return getLongValue(v);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static long getLongValue(String v) {
        if (TextUtils.isEmpty(v)) {
            return 0;
        }

        try {
            return Long.parseLong(v);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static Map<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            if (idx == -1) {
                continue;
            }

            String k = URLDecoder.decode(pair.substring(0, idx), "UTF-8");
            String v = URLDecoder.decode(pair.substring(idx + 1), "UTF-8");
            query_pairs.put(k, v);
        }
        return query_pairs;
    }

    @Deprecated
    public static String generateAuthInfo(String url, long uid, String token){
//        if(Protocol.isYiDomainUrl(url) && Protocol.isYiDomainAuthUrl(url)){
//            String[] split = url.split("\\?");
//            if(split.length > 1) {
//                StringBuilder stringBuilder = new StringBuilder();
//                stringBuilder.append(Protocol.getAuthUrl(url));
//                stringBuilder.append("?");
//                stringBuilder.append(split[1]);
//                stringBuilder.append("&uid=");
//                stringBuilder.append(uid);
//                stringBuilder.append("&token=");
//                stringBuilder.append(token);
//                stringBuilder.append("&client_type=");
//                stringBuilder.append(PBMessage.getPlatform());
//                return stringBuilder.toString();
//            }
//        }
        return null;//url;
    }

    private static final String YI_DOMAIN_SILENT_PRE_AUTH_URL = "/qrconnect/web_silent";
    public static final String YI_DOMAIN_SILENT_URL = "user/auth/web_silent";
    public static final String YI_DOMAIN_AUTH_URL = "user/auth/web_auth";
    public static String getSilentAuthUrl(){
        return envInstance.getSilentAuthUrl();
    }

    public static String getAuthUrl(String url){
        return envInstance.getAuthUrl(url);
    }

    public static boolean isYiDomainSilentUrl(String url){
        if(null == url || url.length() <= 0)
            return false;

        try {
            return url.split("\\?")[0].endsWith(YI_DOMAIN_SILENT_PRE_AUTH_URL);
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isYiDomainAuthUrl(String url){
        if(null == url || url.length() <= 0)
            return false;

        try {
            return url.split("\\?")[0].endsWith(YI_DOMAIN_AUTH_URL);
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

//    http://yiyiadmin.tpddns.cn:8081/qrconnect/web_silent?appid=100003&seq=1000031004
    public static boolean isYiDomainUrl(String url){
        if(null == url || url.length() <= 0)
            return false;

        List<String> domains = yiDomainUrl();
        for(String domain : domains){
            if(url.startsWith(domain)){
                return true;
            }
        }
        return false;
    }

    private static List<String> yiDomainUrl(){
        List<String> list = new ArrayList<>();
        list.add("http://" + getDomain());
//        list.add("http://" + DOMAIN_103);
        list.add("https://" + getDomain());
//        list.add("https://" + DOMAIN_103);
        list.add("yychat://" + getDomain());
//        list.add("yychat://" + DOMAIN_103);
        return list;
    }

    public static Map<String, String> getSilentAuthParams(long gid, String url, User user){
        Map<String, String> params = new HashMap<>();
        try {
            String[] split = url.split("\\?");
            String[] s = split[1].split("&");
            String[] temp = s[0].split("=");
            params.put(temp[0], temp[1]);
            temp = s[1].split("=");
            params.put(temp[0], temp[1]);
            params.put("uid", user.uid + "");
            params.put("token", user.token);
            params.put("group_id", gid + "");
            params.put("client_type", PBMessage.getPlatform() + "");
        }catch(Exception e){
            e.printStackTrace();
        }
        return params;
    }

    public static Map<String, String> getAuthParams(String url, User user){
        Map<String, String> params = new HashMap<>();
        try {
            String[] s = url.split("\\?");
            String[] temp = s[1].split("=");
            params.put(temp[0], temp[1]);
            params.put("uid", user.uid + "");
            params.put("token", user.token);
            params.put("client_type", PBMessage.getPlatform() + "");
        }catch(Exception e){
            e.printStackTrace();
        }
        return params;
    }

    public static boolean isSilentUrl(String url){
        if(null == url || url.length() <= 0){
            return false;
        }

        return Protocol.isYiDomainUrl(url) && Protocol.isYiDomainSilentUrl(url);
    }

    public static boolean isSilentAuthUrl(String url){
        if(null == url || url.length() <= 0){
            return false;
        }

        return Protocol.isYiDomainUrl(url) && Protocol.isYiDomainSilentUrl(url);
    }

    public static boolean isAuthUrl(String url){
        if(null == url || url.length() <= 0){
            return false;
        }

        return Protocol.isYiDomainUrl(url) && Protocol.isYiDomainAuthUrl(url);
    }

    public static void requestSilentAuth(long gid, String url){
        if(Protocol.isSilentAuthUrl(url)){
            RetrofitWrapper.getRetrofit(Protocol.getSilentAuthUrl())
                    .create(Silent.class)
                    .slientAuth(Protocol.getSilentAuthParams(gid, url, UserManager.getUser()))
                    .enqueue(new Callback<AuthResponse>() {
                        public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
//                            try {
//                                ToastUtils.show(response.body().err_msg);
//                            }catch(Exception e){
//                                e.printStackTrace();
//                            }
                        }
                        public void onFailure(Call<AuthResponse> call, Throwable t) {
//                            ToastUtils.show(R.string.auth_failed);
                        }
                    });
        }
    }

    public static void requestAuth(String url){
        if(Protocol.isAuthUrl(url)){
            RetrofitWrapper.getRetrofit(Protocol.getAuthUrl(url))
                    .create(Auth.class)
                    .auth(Protocol.getAuthParams(url, UserManager.getUser()))
                    .enqueue(new Callback<AuthResponse>() {
                        public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
//                            try {
//                                ToastUtils.show(response.body().err_msg);
//                            }catch(Exception e){
//                                e.printStackTrace();
//                            }
                        }
                        public void onFailure(Call<AuthResponse> call, Throwable t) {
//                            ToastUtils.show(R.string.auth_failed);
                        }
                    });
        }
    }

    private interface Silent{
        @FormUrlEncoded
        @POST(Protocol.YI_DOMAIN_SILENT_URL)
        Call<AuthResponse> slientAuth(@FieldMap Map<String, String> params);
    }

    private interface Auth{
        @FormUrlEncoded
        @POST(Protocol.YI_DOMAIN_AUTH_URL)
        Call<AuthResponse> auth(@FieldMap Map<String, String> params);
    }

    public class AuthResponse {
        public String code;
        public String err_msg;
    }

    public static boolean isWebLogin(String s){
        if(null == s || s.length() <= 0){
            return false;
        }

        String[] split = s.split("w_login_id=");
        if(split.length > 1){
            try {
                Long.parseLong(split[1]);
                return true;
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }

    public static void requestWebLogin(String s){
        if(null == s || s.length() <= 0){
            return;
        }

        String[] split = s.split("w_login_id=");
        if(split.length > 1){
            try {
                long did = Long.parseLong(split[1]);
                ApiManager.getInstance()
                        .post(new Request(new PBMessage()
                                .setCmd(Login.LoginYimCMD.SET_WEB_LOGIN_INFO_REQ_CMD_VALUE)
                                .setBody(Login.SetWebLoginInfoReq.newBuilder()
                                        .setLoginId(did))))
                        .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                        .subscribe(new HttpRequestObserver<MsgOuterClass.Msg>() {
                            public void onNext(MsgOuterClass.Msg msg) {
                                if(BaseHandler.isNoError(msg.getCode())){
                                    try {
                                        Login.SetWebLoginInfoRsp setWebLoginInfoRsp = Login.SetWebLoginInfoRsp.parseFrom(msg.getBody());
                                        if(setWebLoginInfoRsp.getResult() == Login.SetWebLoginInfoRsp.Result.OK_VALUE){
                                            ToastUtils.show(R.string.login_success);
                                        }else{
                                            ToastUtils.show(setWebLoginInfoRsp.getErrMsg());
                                        }
                                        return;
                                    } catch (InvalidProtocolBufferException e) {
                                        e.printStackTrace();
                                    }
                                }
                                ToastUtils.show(R.string.error);
                            }
                            public void onError(Throwable e) {
                                ToastUtils.show(NetUtils.isConnected() ? R.string.error : R.string.no_net);
                            }
                        });
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}