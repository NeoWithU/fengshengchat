package com.fengshengchat.protocol;

public class ENV_FG extends AbsEnv {

    public String getHttpSchema(){
        return "http://";
    }

    public String getDomain(){
        return "";
    }

    public String getSocketUrl(){
       // return "sock.feige.co";
        return "fengsheng.helimai8.com";
    }

    public int getSocketPort(){
        return 8089;
    }

    public String getAuthUrl(String url){
        try {
            return getHttpSchema() + (url.split("://")[1]).split("/")[0] + "/";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getSilentAuthUrl(){
       // return getHttpSchema() + "auth.feige.co" + ":8081/";
        return getHttpSchema() + "fengsheng.helimai8.com" + ":8081/";
    }

    public String getBusinessUrl(){
     //   return getHttpSchema() + "srv.feige.co" + ":9090/";
        return getHttpSchema() + "fengsheng.helimai8.com" + ":9090/";
    }

    public String getUploadUrl(boolean isMultiFile){
        if(isMultiFile){
         //   return getHttpSchema() + "up.feige.co" + ":9300/multiupload";
            return getHttpSchema() + "fengsheng.helimai8.com" + ":9300/multiupload";
        }else{
          //  return getHttpSchema() + "up.feige.co" + ":9300/upload";
            return getHttpSchema() + "fengsheng.helimai8.com" + ":9300/upload";
        }
    }

    public String getQRUrl(){
      //  return getHttpSchema() + "qr.feige.co";
        return getHttpSchema() + "fengsheng.helimai8.com";
    }

}
