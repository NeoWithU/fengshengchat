package com.fengshengchat.protocol;

import com.google.gson.Gson;
import com.fengshengchat.chat.bean.ImageContentBean;
import com.fengshengchat.chat.bean.VoiceContentBean;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.User;
import com.fengshengchat.user.UserManager;

import java.util.ArrayList;
import java.util.List;

import yiproto.yichat.chat.Chat;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.groupchat.Groupchat;
import yiproto.yichat.sportfulgroup.Sportfulgroup;

public class DataTypeConverter {

    public static User convertUserHe(Chat.ChatMsg chatMsg){
        User he = new User();
        he.uid = chatMsg.getFromUid();
//        he.nickName = "uid " + he.uid;
//        he.account =
//        he.icon =
        return he;
    }

    public static User convertUserHe(Groupchat.GroupChatMsg  groupChatMsg){
        User he = new User();
        he.uid = groupChatMsg.getGroupId();
        he.sendUid = groupChatMsg.getUid();
//        he.nickName = "uid " + he.uid;
//        he.account =
//        he.icon =
        return he;
    }

    public static User convertUserHe(Groupchat.GroupSystemBroadcast groupBroadcast){
        User he = new User();
        he.uid = groupBroadcast.getGroupId();
//        he.sendUid = groupBroadcast.getOperator().getOperatorId();
//        he.nickName = "uid " + he.uid;
//        he.account =
//        he.icon =
        return he;
    }

    public static User convertUserHe(Sportfulgroup.SportfulGroupChat funGroup){
        User he = new User();
        he.uid = funGroup.getGroupId();
        he.sendUid = funGroup.getUid();
//        he.nickName = "uid " + he.uid;
//        he.account =
//        he.icon =
        return he;
    }

    public static User getUserMe(){
        User me = new User();
        com.fengshengchat.user.User user = UserManager.getUser();
        if(null != user) {
            me.uid = user.uid;
        }
        return me;
    }

    private static String decodeBase64(String src){
        return Protocol.decodeBase64ToString(src);
    }

    public static ChatRecord convertChatRecord(Groupchat.GroupSystemBroadcast groupBroadcast) {
        ChatRecord chatRecord = new ChatRecord();
        chatRecord.chatType = Constant.CHAT_TYPE_GROUP;
        chatRecord.messageType = Chat.MsgType.MSGTYPE_GROUP_SYSTEM_VALUE;
        chatRecord.messageSequence = groupBroadcast.getMsgSeq();
        chatRecord.time = groupBroadcast.getSendTime();
        chatRecord.messageState = Constant.MESSAGE_STATE_OK;
        chatRecord.isSendMessage = false;
//        chatRecord.setMy(getUserMe());
        chatRecord.setHis(convertUserHe(groupBroadcast));
//        extractMediaContent(chatRecord, msg.getMsg());
        chatRecord.content = convertContent(groupBroadcast);

        return chatRecord;
    }

    public static ChatRecord convertChatRecord(boolean isSend, Sportfulgroup.SportfulGroupChat funGroup) {
        ChatRecord chatRecord = new ChatRecord();
        chatRecord.chatType = Constant.CHAT_TYPE_GROUP;
        chatRecord.groupType = Constant.CHAT_TYPE_FUN_GROUP;
        chatRecord.messageType = funGroup.getMsgType();
        chatRecord.messageSequence = funGroup.getMsgSeq();
        chatRecord.time = funGroup.getSendTime();
        chatRecord.messageState = Constant.MESSAGE_STATE_OK;
        chatRecord.isSendMessage = isSend;
        chatRecord.showType = funGroup.getShowType();
        chatRecord.setMy(getUserMe());
        chatRecord.setHis(convertUserHe(funGroup));
        extractMediaContent(chatRecord, funGroup.getMsgContent());

        return chatRecord;
    }

    private static String convertContent(Groupchat.GroupSystemBroadcast groupBroadcast){
        switch(groupBroadcast.getMsgType()){
            case Groupchat.GROUP_SYS_MSG_TYPE.INVITE_ENTER_VALUE:
                break;
            case Groupchat.GROUP_SYS_MSG_TYPE.ACCEPT_ENTER_VALUE:
                break;
            case Groupchat.GROUP_SYS_MSG_TYPE.INITIATIVE_OUT_VALUE:
                break;
            case Groupchat.GROUP_SYS_MSG_TYPE.KICT_OUT_VALUE:
//                try {
//                    List<Groupchat.TargetInfo> targetListList = groupBroadcast.getTargetListList();
//                    long uid = UserManager.getUser().uid;
//                    for(Groupchat.TargetInfo info : targetListList){
//                        if(info.getTargetId() == uid){
//                            return "你已被\"" + groupBroadcast.getOperator().getOperatorNick() + "\"移除了群聊";
//                        }
//                    }
//                }catch(Exception e){
//                    e.printStackTrace();
//                }
                break;
            case Groupchat.GROUP_SYS_MSG_TYPE.UPDATE_GROUP_NAME_VALUE:
                break;
        }
        return Protocol.decodeBase64ToString(groupBroadcast.getMsgContent());
    }

    public static ChatRecord convertChatRecord(int chatType, boolean isSend, Chat.ChatMsg msg){
        ChatRecord chatRecord = new ChatRecord();
        chatRecord.chatType = chatType;
        chatRecord.messageType = msg.getMsgType();
        chatRecord.messageSequence = msg.getMsgSeq();
        chatRecord.time = msg.getSendTime();
        chatRecord.messageState = Constant.MESSAGE_STATE_OK;
        chatRecord.isSendMessage = isSend;
        chatRecord.setMy(getUserMe());
        chatRecord.setHis(convertUserHe(msg));
        extractMediaContent(chatRecord, msg.getMsg());

        return chatRecord;
    }

    private static void extractMediaContent(ChatRecord chatRecord, String content){
        content = decodeBase64(content);

        switch(chatRecord.messageType){
            case Chat.MsgType.MSGTYPE_TEXT_VALUE:
            case Chat.MsgType.MSGTYPE_CARD_VALUE:
                chatRecord.content = content;
                break;
            case Chat.MsgType.MSGTYPE_IMAGE_VALUE:
                chatRecord.rawContent = content;
                try {
                    ImageContentBean imageContentBean = new Gson().fromJson(content, ImageContentBean.class);
                    chatRecord.content = imageContentBean.thumb_info.uri;
                }catch(Exception e){
                    e.printStackTrace();
                    chatRecord.content = "";
                }
                break;
            case Chat.MsgType.MSGTYPE_VOICE_VALUE:
                chatRecord.rawContent = content;
                try {
                    VoiceContentBean voiceContentBean = new Gson().fromJson(content, VoiceContentBean.class);
                    chatRecord.content = voiceContentBean.uri;
                    chatRecord.duration = Integer.parseInt(voiceContentBean.media_info.media_time);
                }catch(Exception e){
                    e.printStackTrace();
                    chatRecord.content = "";
                }
                break;
            case Chat.MsgType.MSGTYPE_GROUP_SYSTEM_VALUE:
                chatRecord.content = content;
                break;
        }
    }

    public static List<ChatRecord> convertSingleChatRecords(List<Chat.ChatMsg> list){
        List<ChatRecord> res = new ArrayList<>();
        if(null == list)
            return res;

        int chatType = Constant.CHAT_TYPE_SINGLE;
        com.fengshengchat.user.User user = UserManager.getUser();
        long myUid = null != user ? user.uid : 0L;
        for(Chat.ChatMsg msg : list){
            res.add(convertChatRecord(chatType, msg.getFromUid() == myUid, msg));
        }
        return res;
    }

    public static List<ChatRecord> convertGroupChatRecords(List<Groupchat.GroupChatMsg> list){
        List<ChatRecord> res = new ArrayList<>();
        if(null == list)
            return res;

        int chatType = Constant.CHAT_TYPE_GROUP;
        com.fengshengchat.user.User user = UserManager.getUser();
        long myUid = null != user ? user.uid : 0L;
        for(Groupchat.GroupChatMsg msg : list){
            res.add(convertChatRecord(chatType, msg.getUid() == myUid, msg));
        }
        return res;
    }

    public static List<ChatRecord> convertChatRecords(List<Sportfulgroup.SportfulGroupChat> list){
        List<ChatRecord> res = new ArrayList<>();
        if(null == list)
            return res;

        com.fengshengchat.user.User user = UserManager.getUser();
        long myUid = null != user ? user.uid : 0L;
        for(Sportfulgroup.SportfulGroupChat groupChat : list){
            res.add(convertChatRecord(groupChat.getUid() == myUid, groupChat));
        }
        return res;
    }

    public static ChatRecord convertChatRecord(int chatType, boolean isSend, Groupchat.GroupChatMsg msg){
        ChatRecord chatRecord = new ChatRecord();
        chatRecord.chatType = chatType;
        chatRecord.messageType = msg.getMsgType();
        chatRecord.messageSequence = msg.getMsgSeq();
        chatRecord.time = msg.getSendTime();
        chatRecord.messageState = Constant.MESSAGE_STATE_OK;
        chatRecord.isSendMessage = isSend;
        chatRecord.setMy(getUserMe());
        chatRecord.setHis(convertUserHe(msg));
        extractMediaContent(chatRecord, msg.getMsgContent());

        return chatRecord;
    }

    public static ChatListBean convertChatListBean(Chat.ChatMsg msg){
        ChatListBean chatListBean = new ChatListBean();
        chatListBean.hisUid = msg.getFromUid();
        chatListBean.chatType = Constant.CHAT_TYPE_SINGLE;
        chatListBean.messageType = msg.getMsgType();
        chatListBean.lastMessage = decodeBase64(msg.getMsg());
        chatListBean.time = msg.getSendTime();
        chatListBean.alreadyPullSequence = msg.getMsgSeq();
        chatListBean.lastReceiveSequence = msg.getMsgSeq();
//        chatListBean.icon = msg.get
//        chatListBean.groupNickName = msg.get

//        chatListBean.resultCount //no use
//        chatListBean.unreadCount //update outside
//        chatListBean.isPin //keep
//        chatListBean.disableNotify //keep
        return chatListBean;
    }

    public static ChatListBean convertChatListBean(Sportfulgroup.SportfulGroupChat groupChatMsg){
        ChatListBean chatListBean = new ChatListBean();
        chatListBean.hisUid = groupChatMsg.getGroupId();
        chatListBean.chatType = Constant.CHAT_TYPE_GROUP;
        chatListBean.messageType = groupChatMsg.getMsgType();
        chatListBean.groupType = yiproto.yichat.group.Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE;//groupChatMsg.getGroupType();
        chatListBean.lastMessage = decodeBase64(groupChatMsg.getMsgContent());
        chatListBean.time = groupChatMsg.getSendTime();
        chatListBean.alreadyPullSequence = groupChatMsg.getMsgSeq();
        chatListBean.lastReceiveSequence = groupChatMsg.getMsgSeq();
//        chatListBean.icon = msg.get
//        chatListBean.groupNickName = msg.get

//        chatListBean.resultCount //no use
//        chatListBean.unreadCount //update outside
//        chatListBean.isPin //keep
//        chatListBean.disableNotify //keep
        chatListBean.ext_1 = groupChatMsg.getUid() + "";
        return chatListBean;
    }

    public static List<ChatListBean> convertChatListBeans(List<Chat.ChatMsg> list){
        List<ChatListBean> res = new ArrayList<>();
        if(null == list)
            return res;

        for(Chat.ChatMsg msg : list){
            res.add(convertChatListBean(msg));
        }
        return res;
    }

    public static ChatListBean convertChatListBean(Groupchat.GroupSystemBroadcast groupBroadcast){
        ChatListBean chatListBean = new ChatListBean();
        chatListBean.hisUid = groupBroadcast.getGroupId();
        chatListBean.chatType = Constant.CHAT_TYPE_GROUP;
        chatListBean.messageType = Chat.MsgType.MSGTYPE_GROUP_SYSTEM_VALUE;
        chatListBean.lastMessage = convertContent(groupBroadcast);
        chatListBean.time = groupBroadcast.getSendTime();
        chatListBean.alreadyPullSequence = groupBroadcast.getMsgSeq();
        chatListBean.lastReceiveSequence = groupBroadcast.getMsgSeq();
        chatListBean.groupType = groupBroadcast.getGroupType();
//        chatListBean.icon = msg.get
//        chatListBean.groupNickName = msg.get

//        chatListBean.resultCount //no use
//        chatListBean.unreadCount //update outside
//        chatListBean.isPin //keep
//        chatListBean.disableNotify //keep
        return chatListBean;
    }

    public static ChatListBean convertChatListBean(Groupchat.GroupChatMsg groupChatMsg){
        ChatListBean chatListBean = new ChatListBean();
        chatListBean.hisUid = groupChatMsg.getGroupId();
        chatListBean.chatType = Constant.CHAT_TYPE_GROUP;
        chatListBean.messageType = groupChatMsg.getMsgType();
        chatListBean.lastMessage = decodeBase64(groupChatMsg.getMsgContent());
        chatListBean.time = groupChatMsg.getSendTime();
        chatListBean.alreadyPullSequence = groupChatMsg.getMsgSeq();
        chatListBean.lastReceiveSequence = groupChatMsg.getMsgSeq();
//        chatListBean.icon = msg.get
//        chatListBean.groupNickName = msg.get

//        chatListBean.resultCount //no use
//        chatListBean.unreadCount //update outside
//        chatListBean.isPin //keep
//        chatListBean.disableNotify //keep
        chatListBean.ext_1 = groupChatMsg.getUid() + "";
        return chatListBean;
    }

    public static ChatListBean convertChatRecord(ChatListBean prevBean, ChatRecord chatRecord){
        ChatListBean chatListBean = new ChatListBean();
        chatListBean.icon = chatRecord.his.icon;
        chatListBean.chatType = chatRecord.chatType;
        chatListBean.messageType = chatRecord.messageType;
        chatListBean.hisUid = chatRecord.his.uid;
        chatListBean.name = chatRecord.his.nickName;
        chatListBean.lastMessage = chatRecord.content;
        chatListBean.time = chatRecord.time;
        if(null != prevBean) {
            chatListBean.groupType = prevBean.groupType;
            chatListBean.alreadyPullSequence = prevBean.alreadyPullSequence;
            chatListBean.lastReceiveSequence = prevBean.lastReceiveSequence;
        }

//        chatListBean.resultCount //no use
//        chatListBean.unreadCount //update outside
//        chatListBean.isPin //keep
//        chatListBean.disableNotify //keep
        return chatListBean;
    }

    public static ChatRecord convertChatRecord(Friend.NotifyUpdateReq.FriendGreetingSystemMsg greetingMsg){
        ChatRecord chatRecord = new ChatRecord();
        chatRecord.chatType = Constant.CHAT_TYPE_SINGLE;
        chatRecord.messageType = Chat.MsgType.MSGTYPE_TEXT_VALUE;
        chatRecord.messageSequence = 0;
        chatRecord.time = greetingMsg.getSendTime();
        chatRecord.messageState = Constant.MESSAGE_STATE_OK;
        chatRecord.isSendMessage = false;
        User userMe = getUserMe();
        chatRecord.setMy(userMe);
        User he = new User();
        if(userMe.uid == greetingMsg.getRequestUserId()){
            he.uid = greetingMsg.getAgreeUserId();
            he.nickName = greetingMsg.getAgreeNickname();
            he.icon = greetingMsg.getAgreeSmallAvatar();
        }else{
            he.uid = greetingMsg.getRequestUserId();
            he.nickName = greetingMsg.getRequestNickname();
            he.icon = greetingMsg.getRequestSmallAvatar();
        }
        chatRecord.setHis(he);
        extractMediaContent(chatRecord, greetingMsg.getText());

        return chatRecord;
    }

    public static ChatListBean generateChatListBean(Friend.NotifyUpdateReq.FriendGreetingSystemMsg greetingMsg){
        ChatListBean chatListBean = new ChatListBean();
        chatListBean.chatType = Constant.CHAT_TYPE_SINGLE;
        chatListBean.messageType = Chat.MsgType.MSGTYPE_TEXT_VALUE;
        chatListBean.lastMessage = decodeBase64(greetingMsg.getText());
        chatListBean.time = greetingMsg.getSendTime();
        chatListBean.alreadyPullSequence = 0;
        chatListBean.lastReceiveSequence = 0;

        if(UserManager.getUser().uid == greetingMsg.getRequestUserId()) {
            chatListBean.unreadCount = 1;
            chatListBean.hisUid = greetingMsg.getAgreeUserId();
            chatListBean.name = greetingMsg.getAgreeNickname();
            chatListBean.icon = greetingMsg.getAgreeSmallAvatar();
        }else{
            chatListBean.unreadCount = 0;
            chatListBean.hisUid = greetingMsg.getRequestUserId();
            chatListBean.name = greetingMsg.getRequestNickname();
            chatListBean.icon = greetingMsg.getRequestSmallAvatar();
        }
        return chatListBean;
    }

}
