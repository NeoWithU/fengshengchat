package com.fengshengchat.protocol;

import com.blankj.utilcode.util.SPUtils;
import com.google.protobuf.GeneratedMessageV3;
import com.fengshengchat.base.bean.DeviceInfo;
import com.fengshengchat.base.utils.TimeUtils;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;

import yiproto.yichat.login.Login;
import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/12/5
 */
public class PBMessage {
    private static long mMessageSequence = 0;

    private MsgOuterClass.Msg.Builder mMessage;
    private GeneratedMessageV3.Builder mBodyBuilder;
    private static String IMEI = null;
    private static String MAC = null;
    private static String IP = null;
    private static String deviceID = null;
    private static DeviceInfo mDeviceInfo;

    public PBMessage() {
        initMessageSequence();
        mMessage = MsgOuterClass.Msg.newBuilder();

        MsgOuterClass.LoginInfo.Builder loginInfo = MsgOuterClass.LoginInfo.newBuilder();
        loginInfo.setMac(getMAC());
        loginInfo.setImei(getIMEI());
        loginInfo.setClientIp(getIP());
        loginInfo.setDeviceId(getDeviceID());
        loginInfo.setClientType(getPlatform());

        User user = UserManager.getUser();
        if (user != null) {
            loginInfo.setToken(null == user.token ? "" : user.token);
            mMessage.setUid(user.uid);
        }
        mMessage.setLoginInfo(loginInfo);
        mMessage.setSeq((int)generateMessageSequence());
        mMessage.setTimestamp(TimeUtils.getTimeSeconds());
    }

    public PBMessage setCmd(int cmd) {
        mMessage.setCmd(cmd);
        return this;
    }

    public PBMessage setBody(GeneratedMessageV3.Builder bodyBuilder) {
        mBodyBuilder = bodyBuilder;
        mMessage.setBody(Protocol.encodeBody(bodyBuilder.build().toByteArray()));
        return this;
    }

    public GeneratedMessageV3.Builder getBodyBuilder(){
        return mBodyBuilder;
    }

    @Deprecated
    public PBMessage setUid(long uid) {
        mMessage.setUid(uid);
        return this;
    }

    @Deprecated
    public PBMessage setLoginInfoToken(String token) {
        mMessage.getLoginInfoBuilder().setToken(token);
        return this;
    }

    public boolean isSameMessageSequence(int sequence) {
        return sequence == mMessageSequence;
    }

    public int getMessageSequence() {
        return mMessage.getSeq();
    }

    private void initMessageSequence(){
        if(0 == mMessageSequence){
            mMessageSequence = SPUtils.getInstance().getLong("messageSequence", 0L);
        }
    }

    private long generateMessageSequence() {
        mMessageSequence++;
        SPUtils.getInstance().put("messageSequence", mMessageSequence);
        return mMessageSequence;
    }

    public byte[] PB() {
        return mMessage.build().toByteArray();
    }

    public MsgOuterClass.Msg getMsg(){
        return mMessage.build();
    }

    public MsgOuterClass.Msg.Builder getMsgBuilder(){
        return mMessage;
    }

    public static String getIMEI() {
        if (null == mDeviceInfo) {
            return "IMEI";
        }
        return mDeviceInfo.getIMEI();
    }

    public static String getMAC() {
        if (null == mDeviceInfo) {
            return "MAC";
        }
        return mDeviceInfo.getMAC();
    }

    public static String getIP() {
        if (null == mDeviceInfo) {
            return "IP";
        }
        return mDeviceInfo.getIP();
    }

    public static String getDeviceID() {
        if (null == mDeviceInfo) {
            return "UUID";
        }
        return mDeviceInfo.getUUID();
    }

    public static void initDeviceInfo(DeviceInfo deviceInfo){
        mDeviceInfo = deviceInfo;
    }

    public static void refreshDeviceInfo(DeviceInfo deviceInfo){
        mDeviceInfo = deviceInfo;
    }

    public static int getPlatform() {
        return MsgOuterClass.ClientType.CLIENT_TYPE_ANDRIOD_VALUE;
    }

    public static Login.DeviceInfo getDeviceInfoPB() {
        Login.DeviceInfo.Builder deviceInfo = Login.DeviceInfo.newBuilder();
        deviceInfo.setMac(getMAC());
        deviceInfo.setImei(getIMEI());
//        deviceInfo.setUdid();
        return deviceInfo.build();
    }

}
