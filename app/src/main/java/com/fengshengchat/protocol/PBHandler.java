package com.fengshengchat.protocol;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * @author KRT
 * 2018/11/28
 */
public class PBHandler {
    private final List<IMessageHandler> mHandlers = new ArrayList<>();
    private static final PBHandler mPBHandler = new PBHandler();

    private PBHandler(){
        registerHandler(new PingPongHandler());
        registerHandler(new FriendHandler());
        registerHandler(new ChatHandler());
    }

    public static PBHandler getInstance(){
        return mPBHandler;
    }

    private void registerHandler(IMessageHandler handler){
        if(null != handler) {
            mHandlers.add(handler);
        }
    }

    public void clearHandler(){
        mHandlers.clear();
    }

    public void handleMessage(Context context, MsgWrapper msgWrapper){
        if(null == msgWrapper || null == msgWrapper.inMsg)
            return;

        for(IMessageHandler handler : mHandlers){
            if(handler.handleMessage(msgWrapper)) {
                break;
            }
        }
    }

}
