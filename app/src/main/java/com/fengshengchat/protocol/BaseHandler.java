package com.fengshengchat.protocol;

import com.fengshengchat.base.debug.Debug;

public abstract class BaseHandler implements IMessageHandler {
    private final String TAG = this.getClass().getSimpleName();

    protected abstract boolean canHandle(int bigCmd);

    protected void printi(String msg){
        Debug.i(TAG, msg);
    }

    protected void printe(String msg){
        Debug.e(TAG, msg);
    }

    public static boolean isNoError(int errorCode){
        return 0 == errorCode;
    }

    public final boolean handleMessage(MsgWrapper msgWrapper){
        if(canHandle(msgWrapper.inMsg.getBigCmd())){
            return handle(msgWrapper);
        }
        return false;
    }

    protected boolean handle(MsgWrapper msgWrapper) {
        printi("--- " + TAG + " handle cmd: " + msgWrapper.inMsg.getCmd() + "  errCode: " + msgWrapper.inMsg.getCode());
        return false;
    }
}
