package com.fengshengchat.protocol;

import yiproto.yichat.msg.MsgOuterClass;

public class PingPongHandler extends BaseHandler {
    @Override
    protected boolean canHandle(int bigCmd) {
        //TODO
        return true;
    }

    @Override
    public boolean handle(MsgWrapper msgWrapper) {
        switch (msgWrapper.inMsg.getCmd()) {
            default:
                return false;
            case MsgOuterClass.YimCMD.CONN_KICK_OUT_CMD_VALUE:
                break;
        }
        super.handle(msgWrapper);
        return true;
    }
}
