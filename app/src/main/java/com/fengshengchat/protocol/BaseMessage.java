package com.fengshengchat.protocol;

import android.os.Parcel;

import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.push.IPCMessage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import yiproto.yichat.msg.MsgOuterClass;
import yiproto.yichat.version_info.VersionInfoOuterClass;

/**
 * @author KRT
 * 2018/11/26
 */
public abstract class BaseMessage extends IPCMessage{
    private static final int PACKAGE_LENGTH = 2;
    private static final int START_CODE_LENGTH = 1;
    private static final int VERSION_INFO_LENGTH = 2;
//    private static final int PB_VERSION_INFO_LENGTH = 1;
    private static final int MESSAGE_MAX_LENGTH = 1024 * 64; //byte
    private static final int END_CODE_LENGTH = 1;

    private static final int PREFIX_LENGTH = PACKAGE_LENGTH + START_CODE_LENGTH + VERSION_INFO_LENGTH;
    private static final int PREFIX_OFFSET_LENGTH = START_CODE_LENGTH + VERSION_INFO_LENGTH;

    private byte[] content;

    public BaseMessage(){
        super(Parcel.obtain());
    }

    public void setData(byte[] data){
        content = data;
        super.setData(getData());
    }

    public byte[] getData() {
        return packData();
    }

    private byte[] packData(){
        if(null == content){
            return null;
        }

        try {
            byte[] data = content;
            if(data.length > MESSAGE_MAX_LENGTH){
                Debug.e("BaseMessage", "--- Data byte size > MESSAGE_MAX_LENGTH ---");
                return null;
            }

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            fillPrefix(buffer);
            fillData(buffer, data);
            fillSuffix(buffer);
            buffer.flush();

            byte[] bytes = buffer.toByteArray();
            buffer.reset(); //reuse
            byte[] res = countData(buffer, bytes);
            buffer.reset();
            buffer.close();
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] countData(ByteArrayOutputStream buffer, byte[] data) throws IOException {
        buffer.write(encodeIntegerBigEndian(data.length, PACKAGE_LENGTH));
        buffer.write(data);
        return buffer.toByteArray();
    }

    private byte[] decryptData(byte[] d){
        return d;
    }

    private void fillData(ByteArrayOutputStream buffer, byte[] data) throws IOException {
        buffer.write(Protocol.encodeWithYiMethod(data));
    }

    private void fillPrefix(ByteArrayOutputStream buffer) throws IOException {
        byte[] versionWithPB = getVersionWithPB();
        buffer.write(encodeIntegerBigEndian(0x02, START_CODE_LENGTH));
        buffer.write(encodeIntegerBigEndian(versionWithPB.length, VERSION_INFO_LENGTH));
        buffer.write(versionWithPB); //version content
    }

    private void fillSuffix(ByteArrayOutputStream buffer) throws IOException {
        buffer.write(new byte[]{0x03}); //end code
    }

    private byte[] getVersionWithPB(){
        VersionInfoOuterClass.VersionInfo.Builder builder = VersionInfoOuterClass.VersionInfo.newBuilder();
        builder.setClientVersion(1);
        builder.setEncryptVersion(Protocol.ENCRYPT_VERSION);
        return builder.build().toByteArray();
    }

    private byte[] encodeIntegerBigEndian(int integerValue, int size){
        final int integerSize = Integer.SIZE / Byte.SIZE;
        byte[] code = new byte[size];
        for(int i = 0; i < size && i < integerSize; i++){
            code[i] = (byte)(integerValue >> (size - i - 1) * Byte.SIZE);
        }
        return code;
    }

    public static int getPackSize(byte[] data){
        if(null == data || data.length < 2)
            return 0;
        return ((data[0] << 8) & 0x0000FF00) | (data[1] & 0x000000FF);
    }

    public static int getPackSize(byte packFirstByte, byte packSecondByte){
        return ((packFirstByte << 8) & 0x0000FF00) | (packSecondByte & 0x000000FF);
    }

    private static byte[] concatByte(byte[] one, byte[] two){
        if(null == one && null == two)
            return null;
        if(null == one)
            return two;
        if(null == two)
            return one;

        byte[] container = Arrays.copyOf(one, one.length + two.length);
        System.arraycopy(two, 0, container, one.length, two.length);
        return container;
    }

    private static void parsePrint(String prefix, Object content){
        if(Debug.isEnableDebug()) {
            Debug.e("<" + Thread.currentThread().getName() + "> BaseMessage parse",
                    "[" + prefix + content + "]");
        }
    }

    public static void clearUncompletedCache(){
        mDataBeanForMainThread.clear();
        mDataBeanForOtherThread.clear();
        parsePrint("clearUncompletedCache()", "");
    }

    private static final DataBean mDataBeanForMainThread = new DataBean();
    public static List<MsgOuterClass.Msg> getMessagesForMainProcess(byte[] rawData){
        mDataBeanForMainThread.rawData = rawData;
        return getMessages(mDataBeanForMainThread);
    }

    private static final DataBean mDataBeanForOtherThread = new DataBean();
    public static List<MsgOuterClass.Msg> getMessages(byte[] rawData){
        mDataBeanForOtherThread.rawData = rawData;
        return getMessages(mDataBeanForOtherThread);
    }

    private static List<MsgOuterClass.Msg> getMessages(DataBean dataBean){
        List<MsgOuterClass.Msg> res = new ArrayList<>();

        if(null == dataBean.rawData || dataBean.rawData.length <= 0){
            return res;
        }

        if(null != dataBean.uncompletedData){
//            parsePrint("Has uncompleted data size: ", dataBean.uncompletedData.length);
            dataBean.rawData = concatByte(dataBean.uncompletedData, dataBean.rawData);
//            parsePrint("After concat size: ", dataBean.rawData.length);
        }
        dataBean.uncompletedData = null;

        int offset = 0;
        int onePackLength;
        final int rawDataLength = dataBean.rawData.length;
        byte[] onePackByteData;
        MsgOuterClass.Msg message;
        try {
            do {
                onePackLength = getPackSize(dataBean.rawData[offset], dataBean.rawData[offset + 1]);
                parsePrint("byte[low]: ", dataBean.rawData[offset] + " byte[high]: " + dataBean.rawData[offset + 1]);
                parsePrint("One pack length: ", onePackLength + " + " + PACKAGE_LENGTH);
                parsePrint("Remain data length: ", (rawDataLength - offset));
                if(onePackLength + 2 > rawDataLength - offset){
                    dataBean.uncompletedData = Arrays.copyOfRange(dataBean.rawData, offset, rawDataLength);
                    parsePrint("Save uncompleted data: ", dataBean.uncompletedData.length);
                    return res;
                }
                onePackByteData = Arrays.copyOfRange(dataBean.rawData, offset, offset + onePackLength + PACKAGE_LENGTH);
                message = getMessage(onePackByteData);
                if (null != message) {
                    res.add(message);
                }

                offset += onePackLength + PACKAGE_LENGTH;
            } while (offset < rawDataLength && offset + 1 < rawDataLength);
        }catch(Exception e){
            e.printStackTrace();
        }
//        parsePrint("Parse completed, message count: ", res.size());
        return res;
    }

    private static MsgOuterClass.Msg getMessage(byte[] onePack) throws InvalidProtocolBufferException {
        try {
            return MsgOuterClass.Msg.parseFrom(getPackBody(onePack));
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] getPackBody(byte[] src){
        final int versionSize = src[3] << 8 | src[4];
        byte[] bytes = Arrays.copyOfRange(src, PREFIX_LENGTH + versionSize, src.length - END_CODE_LENGTH);
        return Protocol.decodeWithYiMethod(bytes);
    }

    private static class DataBean{
        public byte[] uncompletedData;
        public byte[] rawData;

        public void clear(){
            uncompletedData = null;
            rawData = null;
        }
    }

}
