package com.fengshengchat.protocol;

import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.db.DbChatList;
import com.fengshengchat.db.DbChatRecord;
import com.fengshengchat.db.helper.Task;
import com.fengshengchat.db.helper.TaskHelper;
import com.fengshengchat.push.NotifyUtils;
import com.fengshengchat.user.UserManager;

import yiproto.yichat.friend.Friend;
import yiproto.yichat.msg.MsgOuterClass;

public class FriendHandler extends BaseHandler {
    @Override
    protected boolean canHandle(int bigCmd) {
        return true;
    }

    @Override
    protected boolean handle(MsgWrapper msgWrapper) {
        switch(msgWrapper.inMsg.getCmd()){
            default:
                return false;

            case Friend.FriendYimCMD.NOTIFY_UPDATE_REQ_CMD_VALUE:
                handleBeRequestedAddFriend(msgWrapper.inMsg);
                return false;

            case MsgOuterClass.YimCMD.TAG_GROUP_NEW_MSG_READ_RSP_CMD_VALUE:
                printe("-------delete this------");
                break;
        }
        super.handle(msgWrapper);
        return true;
    }

    private void handleBeRequestedAddFriend(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                Friend.NotifyUpdateReq notifyUpdateReq = Friend.NotifyUpdateReq.parseFrom(msg.getBody());
                printe("------add friend: " + notifyUpdateReq);
                TaskHelper.getInstance().post(Task.TASK_UPDATE_FRIEND);
                onAddFriend(msg);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
    }

    public void onAddFriend(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                Friend.NotifyUpdateReq notifyUpdateReq = Friend.NotifyUpdateReq.parseFrom(msg.getBody());
                Friend.NotifyUpdateReq.FriendGreetingSystemMsg greetingMsg = notifyUpdateReq.getGreetingMsg();
                if(0 == greetingMsg.getRequestUserId() || 0 == greetingMsg.getAgreeUserId()){
                    NotifyUtils.notifyAddFriend(null, false);
                    return;
                }

                long uid = UserManager.getUser().uid;
                if(greetingMsg.getRequestUserId() == uid){
                    NotifyUtils.notifyAddFriend(greetingMsg.getAgreeNickname(), true);
                }
                DbChatRecord.saveAddFriend(greetingMsg);
                DbChatList.updateChatItem(1, DataTypeConverter.generateChatListBean(greetingMsg));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
