package com.fengshengchat.fun;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.db.DbFriend;
import com.fengshengchat.db.Group;
import com.fengshengchat.group.fragment.GroupNoticeEditorFragment;
import com.fengshengchat.group.model.GroupData;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class FunGroupNotifyActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_fun_group_notify_layout);
//
//        TitleBarLayout titleBarLayout = findViewById(R.id.title_bar);
//        titleBarLayout.setTitleText(R.string.tip);
//        titleBarLayout.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
//            public void onBackClick() {
//                finish();
//            }
//            public void onActionImageClick() {}
//            public void onActionClick() {}
//        });
//        String content = getIntent().getStringExtra("content");
//        ((TextView)findViewById(R.id.notify)).setText(content);

        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        Intent intent = getIntent();
        long groupId = intent.getLongExtra("UID", 0);
        String content = intent.getStringExtra("content");
        Disposable subscribe = Observable.fromCallable(new Callable<Group>() {
            @Override
            public Group call() throws Exception {
                Group groupInfo = DbFriend.getGroupInfo(groupId);
                if(null != groupInfo){
                    groupInfo.notice = content;
                }
                return groupInfo;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Group>() {
                    @Override
                    public void accept(Group o) throws Exception {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container,
                                        GroupNoticeEditorFragment.newInstance(GroupData.getGroupData(o)), GroupNoticeEditorFragment.TAG)
                                .commit();
                    }
                }, new Consumer<Throwable>() {
                    public void accept(Throwable throwable) throws Exception {
                        toast(throwable.getMessage());
                    }
                });


    }
}
