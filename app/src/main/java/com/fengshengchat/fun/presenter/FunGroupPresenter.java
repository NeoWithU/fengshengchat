package com.fengshengchat.fun.presenter;

import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.base.utils.TimeUtils;
import com.fengshengchat.chat.presenter.ChatPresenter;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.fun.IFunGroupView;
import com.fengshengchat.fun.model.FunGroupModel;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.DataTypeConverter;
import com.fengshengchat.protocol.PBMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.group.Group;
import yiproto.yichat.groupchat.Groupchat;
import yiproto.yichat.msg.MsgOuterClass;
import yiproto.yichat.sportfulgroup.Sportfulgroup;

public class FunGroupPresenter extends ChatPresenter {
    public FunGroupPresenter(long toUid, IFunGroupView view) {
        super(new FunGroupModel(), toUid, true, Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE, view);
    }

    @Override
    protected PBMessage generatePBMessage(long uid, boolean isGroup, int groupType, int msgType, String content) {
        if(Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE == groupType) {
            content = encodeMessageContent(content);
            return new PBMessage()
                    .setCmd(MsgOuterClass.YimCMD.SEND_SPORTFULGROUP_CHAT_REQ_CMD_VALUE)
                    .setBody(Sportfulgroup.SendSportfulGroupReq.newBuilder()
                            .setGroupId(uid)
                            .setGroupType(groupType)
                            .setMsgContent(content)
                            .setUid(getModel().getUid())
                            .setMsgType(msgType));
        }else{
            return super.generatePBMessage(uid, isGroup, groupType, msgType, content);
        }
    }

    //Just on send, no care result
    protected void onGenerateSendMessage(long uid, boolean isGroup, int groupType, int sequence, int type, String content, String rawContent, int duration){
        if(Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE == groupType) {
            ChatRecord chatRecord = new ChatRecord();
            chatRecord.content = content;
            chatRecord.rawContent = rawContent;
            chatRecord.chatType = Constant.CHAT_TYPE_GROUP;
            chatRecord.groupType = groupType;
            chatRecord.messageType = type;
            chatRecord.time = TimeUtils.getTimeSeconds();
            chatRecord.isSendMessage = true;
            chatRecord.messageSequence = sequence;
            chatRecord.messageState = Constant.MESSAGE_STATE_SENDING;
            chatRecord.duration = duration;
            chatRecord.setMy(new com.fengshengchat.db.User(self.uid));
            com.fengshengchat.db.User he = new com.fengshengchat.db.User(uid);
            he.nickName = groupName;
            he.icon = groupIcon;
            chatRecord.setHis(he);

            if (uid == mToUid) {
                getView().onSendMessage(chatRecord);
            }
            getFunGroupModel().updateChatList(chatRecord);
        }else{
            super.onGenerateSendMessage(uid, isGroup, groupType, sequence, type, content, rawContent, duration);
        }
    }

    private FunGroupModel getFunGroupModel(){
        return (FunGroupModel) getModel();
    }

    private IFunGroupView getFunGroupView(){
        return (IFunGroupView) getView();
    }

    @Override
    public void resend(ChatRecord chatRecord) {
        super.resend(chatRecord);
    }

    @Override
    public boolean handleMessage(MsgOuterClass.Msg message) {
        if(Constant.INTERNAL_CMD_TYPE == message.getBigCmd()){
            return handleInternalCmd(message);
        }

        printi("handleMessage -> " + message.getCmd() + " errCode: " + message.getCode() + "  errMsg: " + message.getErrMsg());
        switch (message.getCmd()) {
            default:
                break;
            case MsgOuterClass.YimCMD.SEND_SPORTFULGROUP_CHAT_RSP_CMD_VALUE:
                sendMessageResponse(message);
                return true;
            case MsgOuterClass.YimCMD.SPORTFULGROUP_CHAT_MSG_PUSH_CMD_VALUE:
                onReceiveMessage(message);
                return true;
            case MsgOuterClass.YimCMD.GET_SPORTFULGROUP_RECORD_RSP_CMD_VALUE:
                onHistoryBack(message);
                return true;
            case MsgOuterClass.YimCMD.GROUP_SYSTEM_BROADCAST_CMD_VALUE:
                //just refresh by HandlePushService
                if (BaseHandler.isNoError(message.getCode())) {
                    try {
                        Groupchat.GroupSystemBroadcast groupBroadcast = Groupchat.GroupSystemBroadcast.parseFrom(message.getBody());
                        printe("---------group: " + groupBroadcast);

                        if(mToUid != groupBroadcast.getGroupId()){
                            return true;
                        }

                        if(preHandleGroupBroadcast(groupBroadcast)){
                            return true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return true;
        }
        return super.handleMessage(message);
    }

    private long oldestMessageSeq = 0;
    private void onHistoryBack(MsgOuterClass.Msg msg){
        if(BaseHandler.isNoError(msg.getCode())) {
            try {
                Sportfulgroup.GetMsgRecordRsp groupChatMsg = Sportfulgroup.GetMsgRecordRsp.parseFrom(msg.getBody());
                List<Sportfulgroup.SportfulGroupChat> listList = groupChatMsg.getListList();
                List<ChatRecord> chatRecords = DataTypeConverter.convertChatRecords(listList);
                if(isPullNewest){
                    getFunGroupView().onNewestBack(chatRecords);
                }else {
                    getView().onHistory(chatRecords);
                }
                isPullNewest = false;
                if(null != chatRecords && chatRecords.size() > 0) {
                    oldestMessageSeq = chatRecords.get(chatRecords.size() - 1).getMessageSequence();
                }
                getFunGroupView().setHasMoreHistory(1 != groupChatMsg.getEnd());
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }else if(MsgOuterClass.YiChatErrCode.YICHAT_SEND_GROUP_CHAT_NOT_IN_VALUE == msg.getCode()){
            getFunGroupView().onNotInGroup(msg.getErrMsg());
            List<ChatRecord> chatRecords = new ArrayList<>();
            ChatRecord chatRecord = new ChatRecord();
            chatRecord.chatType = Constant.CHAT_TYPE_GROUP;
            chatRecord.groupType = Constant.CHAT_TYPE_FUN_GROUP;
            chatRecord.messageState = Constant.MESSAGE_STATE_OK;
            chatRecord.showType = Sportfulgroup.SHOW_TYPE.SHOW_TYPE_NORMAL_SYSTEM_VALUE;
            chatRecord.content = msg.getErrMsg();

            chatRecords.add(chatRecord);
            getView().onHistory(chatRecords);
            getFunGroupView().setHasMoreHistory(false);

            Disposable subscribe = Observable.fromCallable((Callable<Object>) () -> {
                ChatListBean chatItem = DbManager.getChatListDao().getChatItem(mToUid);
                if(null != chatItem) {
                    chatItem.lastMessage = msg.getErrMsg();
                    DbManager.getChatListDao().updateChatItem(chatItem);
                }
                return "";
            }).subscribeOn(Schedulers.io())
                    .subscribe(o -> {
                    }, throwable -> {
                    });
            return;
        }
        getView().onHistoryError("");
    }

    private void onReceiveMessage(MsgOuterClass.Msg msg) {
        if(BaseHandler.isNoError(msg.getCode())) {
            try {
                ChatRecord chatRecord = null;
                Sportfulgroup.SportfulGroupChat groupChatMsg = Sportfulgroup.SportfulGroupChat.parseFrom(msg.getBody());
                if(groupChatMsg.getGroupId() == mToUid){
                    chatRecord = DataTypeConverter.convertChatRecord(
                            groupChatMsg.getUid() == getModel().getUid(), groupChatMsg);
                }

                if(null != chatRecord) {
                    if(Sportfulgroup.SHOW_TYPE.SHOW_TYPE_MARQUEE_VALUE == chatRecord.showType){
                        getFunGroupView().onReceiveGroupNotify(chatRecord.content);
                    }else {
                        getView().onReceiveMessage(chatRecord);
                        getModel().updateAlreadyReadSequence(chatRecord.his.uid, chatRecord.messageSequence);
//                        sendAlreadyRead(chatRecord.his.uid, chatRecord.messageSequence);
                    }
                }
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessageResponse(MsgOuterClass.Msg message){
        if (BaseHandler.isNoError(message.getCode())) {
            try {
                Sportfulgroup.SendSportfulGroupRsp sendChatRsp = Sportfulgroup.SendSportfulGroupRsp.parseFrom(message.getBody());
                getView().onSendMessageSuccess(message.getSeq(), sendChatRsp.getMsgSeq());
                sendAlreadyRead(mToUid, sendChatRsp.getMsgSeq());
                return;
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }else if(MsgOuterClass.YiChatErrCode.YICHAT_SEND_GROUP_CHAT_NOT_EXIST_VALUE == message.getCode()
                || MsgOuterClass.YiChatErrCode.YICHAT_SEND_GROUP_CHAT_NOT_IN_VALUE == message.getCode()){
            try {
                Sportfulgroup.SendSportfulGroupRsp sendChatRsp = Sportfulgroup.SendSportfulGroupRsp.parseFrom(message.getBody());
                addTip(sendChatRsp.getGroupId(), message.getErrMsg());
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        getView().onSendMessageFailed(message.getSeq(), message.getErrMsg());
    }

    @Override
    public void getHistoryMessage(int offset) {
        if(null != getPusher()){
            getHistoryMessageFromServer(oldestMessageSeq);
        }
    }

    private boolean isPullNewest = false;
    public void getNewestMessage(){
        if(null != getPusher()){
            isPullNewest = true;
            getHistoryMessageFromServer(0);
        }
    }

    public void getHistoryMessageFromServer(long msgSeq){
        getView().onGettingHistory();
        PBMessage pbMessage = new PBMessage()
                .setCmd(MsgOuterClass.YimCMD.GET_SPORTFULGROUP_RECORD_REQ_CMD_VALUE)
                .setBody(Sportfulgroup.GetMsgRecordReq.newBuilder()
                        .setGroupId(mToUid)
                        .setUid(getModel().getUid())
                        .setSeqIndex(msgSeq)
                );
        getPusher().push(pbMessage);
    }

    public void getGroupInfoFromServer(long gid){
        getModel().getGroupInfoFromServer(gid, msg -> {
            if(BaseHandler.isNoError(msg.getCode())){
                Group.GetGroupInfoRsp getGroupInfoRsp = Group.GetGroupInfoRsp.parseFrom(msg.getBody());
                if(Group.GetGroupInfoRsp.GetGroupInfoResult.OK_VALUE == getGroupInfoRsp.getResult()){
                    List<Group.GroupInfo> groupInfoListList = getGroupInfoRsp.getGroupInfoListList();
                    if(null != groupInfoListList && groupInfoListList.size() > 0){
                        Group.GroupInfo groupInfo = groupInfoListList.get(0);
                        if(Group.GroupInfo.NoticeType.NOTICE_ON_TOP_VALUE == groupInfo.getOnTopNotice()){
                            getFunGroupView().onReceiveGroupNotify(groupInfo.getGroupNotice());
                        }
                    }
                }
            }
        });
    }
}
