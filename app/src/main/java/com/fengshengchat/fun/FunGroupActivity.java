package com.fengshengchat.fun;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.fengshengchat.R;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.base.ViewHolder;
import com.fengshengchat.base.view.ChatInputView;
import com.fengshengchat.chat.presenter.ChatPresenter;
import com.fengshengchat.chat.view.ChatActivity;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.fun.presenter.FunGroupPresenter;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.widget.YiWebView;

import java.lang.reflect.Field;
import java.util.List;

import io.reactivex.functions.Consumer;
import yiproto.yichat.fungroup.Fungroup;
import yiproto.yichat.group.Group;
import yiproto.yichat.msg.MsgOuterClass;
import yiproto.yichat.sportfulgroup.Sportfulgroup;

public class FunGroupActivity extends ChatActivity implements IFunGroupView{
    private YiWebView mYiWebView;
    private YiWebView mPlayGameWebView;
    private View mPlayGameRoot;
    private ImageView mFloatView;
    private RadioGroup radioGroup;
    private ViewGroup mFloatRoot;
    private ImageView playGameBtn;
    private TextView mRbGame;

    public static void startFunGroupChat(Context context, long hisUid, String name){
        Intent intent = new Intent(context, FunGroupActivity.class);
        intent.putExtra("who", hisUid);
        intent.putExtra("name", name);
        intent.putExtra("isGroup", true);
        context.startActivity(intent);
    }

    @Override
    protected int getContentLayout() {
        return R.layout.activity_fun_group_chat_layout;
    }

    @Override
    protected ChatAdapter getAdapter() {
        return new FunGroupChatAdapter();
    }

    @Override
    protected ChatPresenter createPresenter() {
        hisUid = getIntent().getLongExtra("who", 0);
        isGroup = true;
        groupType = Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE;
        return new FunGroupPresenter(hisUid, this);
    }

    @Override
    protected void initView() {
        super.initView();
        mPlayGameRoot = findViewById(R.id.play_game_root);
        mFloatView = findViewById(R.id.float_btn);
        mFloatRoot = findViewById(R.id.float_btn_root);
        playGameBtn = findViewById(R.id.play_game);
        mRbGame = findViewById(R.id.rb_game);

        MyListener l = new MyListener();
        playGameBtn.setOnClickListener(l);
        findViewById(R.id.space_view).setOnClickListener(l);

        initWebView();

        getFunGroupInfo(null);
    }

    @Override
    protected void onShowPanelTypeChange(int showType) {
        printi("-----show panel : " + showType);
        ViewGroup.LayoutParams layoutParams = playGameBtn.getLayoutParams();
        if(layoutParams instanceof FrameLayout.LayoutParams){
            ((FrameLayout.LayoutParams) layoutParams).gravity = ChatInputView.SHOW_TYPE_PANEL == showType
                    ? Gravity.TOP : Gravity.CENTER_VERTICAL;
        }
        super.onShowPanelTypeChange(showType);
    }

    @Override
    public void onNewestBack(List<ChatRecord> list){
        if(null != list && list.size() > 0){
            offset = 0;
            mAdapter.clearData();
            onHistory(list);
        }
    }

    @Override
    protected void onReceiveMessage(MsgOuterClass.Msg message) {
        if (Constant.INTERNAL_CMD_TYPE == message.getBigCmd()
                && Constant.INTERNAL_CMD_SOCKET_CONNECT_SUCCESS == message.getCmd()){
            ChatPresenter presenter = getPresenter();
            if(presenter instanceof FunGroupPresenter){
                noMoreData = false;
                ((FunGroupPresenter) presenter).getNewestMessage();
            }
        }else {
            super.onReceiveMessage(message);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void getFunGroupInfo(Consumer<List<Fungroup.SetFunInfo>> runnable){
        PBMessage pbMessage = new PBMessage();
        pbMessage.setCmd(Fungroup.FunGroupCMD.GETCONFIG_FUN_GROUP_REQ_CMD_VALUE)
                .setBody(Fungroup.QueryFunGroupReq.newBuilder()
                        .setGroupid(hisUid));

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<MsgOuterClass.Msg>(getSupportFragmentManager()) {
                    public void onNext(MsgOuterClass.Msg msg) {
                        onFunGroupInfoBack(msg, runnable);
                    }
                    public void onError(Throwable e) {
                        super.onError(e);
                        if(null != runnable){
                            try {
                                runnable.accept(null);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });
    }

    private boolean enableFunGroup = false;
    private List<Fungroup.SetFunInfo> clistList = null;
    private void onFunGroupInfoBack(MsgOuterClass.Msg msg, Consumer<List<Fungroup.SetFunInfo>> runnable){
        if(null != msg && BaseHandler.isNoError(msg.getCode())) {
            try {
                Fungroup.GetConfigFunGroupRsp rsp = Fungroup.GetConfigFunGroupRsp.parseFrom(msg.getBody());
                enableFunGroup = 1 == rsp.getStatus();
                clistList = rsp.getClistList();
                printi("娱乐群信息 : " + clistList);
                initTitleButton();
                initBottomButton();
                initSideButton();
                if(null != runnable){
                    runnable.accept(clistList);
                }
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        printe("onFunGroupInfoBack error: " + msg);
        if(null != runnable){
            try {
                runnable.accept(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initTitleButton(){
        if(null != clistList){
            for(Fungroup.SetFunInfo info : clistList){
                if(1 == info.getPos()){
                    mRbGame.setText(info.getDesc());
                    mRbGame.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    }

    private void initBottomButton(){
        if(null != clistList) {
            for (Fungroup.SetFunInfo info : clistList) {
                if (2 == info.getPos()) {
                    loadImage(playGameBtn, info.getPicurl(), R.mipmap.play_game);
                    playGameBtn.setVisibility(View.VISIBLE);
                    initPlayButton();
                    break;
                }
            }
        }
    }

    private void initWebView(){
        mPlayGameWebView = findViewById(R.id.play_game_web_view);
        mPlayGameWebView.setGid(hisUid);

        mYiWebView = findViewById(R.id.yi_web_view);
        mYiWebView.setGid(hisUid);
        radioGroup = findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                    if(!enableFunGroup){
                        if(R.id.rb_game == checkedId){
                            group.check(R.id.rb_chat);
                            tipDisableFunGroup();
                        }
                        return;
                    }

                    if(R.id.rb_game == checkedId){
                        hideInputView();
                        if(null == clistList){
                            getFunGroupInfo(setFunGroupInfo -> enterGame(getTopJumpUrl()));
                        }else {
                            enterGame(getTopJumpUrl());
                        }
                    }
                    mYiWebView.setVisibility(R.id.rb_game == checkedId ? View.VISIBLE : View.GONE);
                    mFloatRoot.setVisibility(R.id.rb_game == checkedId || mFloatRoot.getChildCount() <= 0
                            ? View.GONE : View.VISIBLE);
                }
        );
    }

    private void enterGame(String topJumpUrl){
        printe("--------enterGame-------- " + topJumpUrl);
        if (null != topJumpUrl && topJumpUrl.length() > 0) {
            if(null == mYiWebView.getTag()) {
                mYiWebView.start(this, topJumpUrl); //mYiWebView.start(this, "http://192.168.1.222:8081/robottest");
                mYiWebView.setTag(topJumpUrl);
            }
        }else{
            mYiWebView.showErrorView(true);
        }
    }

    @Override
    protected void onDestroy() {
        mYiWebView.destroy();
        mPlayGameWebView.destroy();
        super.onDestroy();
    }

    private void initPlayButton(){
        try {
            Class clz = mInputView.getClass();
            Field mVoice = clz.getDeclaredField("mVoice");
            mVoice.setAccessible(true);
            Field mLayoutParams = mVoice.getType().getDeclaredField("mLayoutParams");
            mLayoutParams.setAccessible(true);
            Object voiceInstance = mVoice.get(mInputView);
            Object layoutParamsInstance = mLayoutParams.get(voiceInstance);
            if(layoutParamsInstance instanceof LinearLayout.LayoutParams){
                ((LinearLayout.LayoutParams)layoutParamsInstance)
                        .setMargins(dp2px(38), 0, 0, 0);
                if(voiceInstance instanceof View){
                    ((View) voiceInstance).setLayoutParams((ViewGroup.LayoutParams) layoutParamsInstance);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPusherReady() {
        super.onPusherReady();
        ((FunGroupPresenter)getPresenter()).getHistoryMessageFromServer(offset);
        ((FunGroupPresenter)getPresenter()).getGroupInfoFromServer(hisUid);
    }

    private class MyListener implements View.OnClickListener{
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.space_view: mPlayGameRoot.setVisibility(View.GONE);
                    break;
                case R.id.play_game: onPlayGameClick();
                    break;
            }
        }
    }

    private String getTopJumpUrl(){
        if(null != clistList){
            for(Fungroup.SetFunInfo info : clistList){
                if(1 == info.getPos()){
                    return info.getJumpurl();
                }
            }
        }
        return null;
    }

    private String getBottomJumpUrl(){
        if(null != clistList){
            for(Fungroup.SetFunInfo info : clistList){
                if(2 == info.getPos()){
                    return info.getJumpurl();
                }
            }
        }
        return null;
    }

    private void initSideButton(){
        if(null != clistList) {
            for(Fungroup.SetFunInfo info : clistList){
                if(3 == info.getPos() && 1 == info.getType()){
                    mFloatRoot.addView(generateSideButtonIcon(info.getPicurl(), info.getJumpurl(), info.getDesc()),
                            new LinearLayout.LayoutParams(dp2px(65), dp2px(70)));
                    String desc = info.getDesc();
                    if(null != desc && desc.length() > 0) {
                        mFloatRoot.addView(generateSideButtonText(info.getDesc()),
                                new LinearLayout.LayoutParams(dp2px(65), ViewGroup.LayoutParams.WRAP_CONTENT));
                    }
                }
            }
            if(R.id.rb_game != radioGroup.getCheckedRadioButtonId()) {
                mFloatRoot.setVisibility(mFloatRoot.getChildCount() > 0 ? View.VISIBLE : View.GONE);
            }
        }
    }

    private View generateSideButtonIcon(String iconUrl, String jumpUrl, String desc){
        ImageView imageView = new ImageView(getContext());
        imageView.setPadding(dp2px(15), dp2px(25), dp2px(15), dp2px(10));
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setOnClickListener(v -> onInviteClick(jumpUrl, desc));
        loadImage(imageView, iconUrl, R.mipmap.invitation_entry);
        return imageView;
    }

    private View generateSideButtonText(String text){
        TextView textView = new TextView(getContext());
        textView.setText(text);
        textView.setTextSize(12);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        return textView;
    }

    private void onInviteClick(String url, String desc){
        hideInputView();
        if(!enableFunGroup){
            tipDisableFunGroup();
            return;
        }

        Intent intent = new Intent(getContext(), FunGroupInviteActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("gid", hisUid);
        try {
            intent.putExtra("title", desc);
        }catch(Exception e){
            e.printStackTrace();
        }
        startActivity(intent);
    }

    private void onPlayGameClick(){
        hideInputView();
        if(null == clistList){
            getFunGroupInfo(setFunGroupInfo -> enterBottomGame(getBottomJumpUrl()));
        }else if(enableFunGroup){
            mPlayGameRoot.setVisibility(View.VISIBLE);
            enterBottomGame(getBottomJumpUrl());
        }else{
            tipDisableFunGroup();
        }
    }

    private void tipDisableFunGroup(){
        if(notInGroup){
            toast(notInGroupString);
        }else {
            toast(R.string.disable_fun_group);
        }
    }

    private void enterBottomGame(String jumpurl){
        printe("--------enterBottomGame-------- " + jumpurl);
        if(null != jumpurl && jumpurl.length() > 0) {
            if(null == mPlayGameWebView.getTag()) { //avoid repeat load
                mPlayGameWebView.start(FunGroupActivity.this, jumpurl);
                mPlayGameWebView.setTag(jumpurl);
            }
        }else{
            mPlayGameWebView.showErrorView(true);
        }
    }

    @Override
    public void setHasMoreHistory(boolean has) {
        noMoreData = !has;
        if(noMoreData) {
            super.onNoMoreData();
        }
    }

    private String notInGroupString = null;
    private boolean notInGroup = false;
    @Override
    public void onNotInGroup(String msg) {
        notInGroup = true;
        notInGroupString = msg;
        enableFunGroup = false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(mYiWebView.handleKeyEvent(keyCode, event)) {
            return true;
        }else if(View.VISIBLE == mYiWebView.getVisibility()){
            radioGroup.check(R.id.rb_chat);
            return true;
        }else if(mPlayGameWebView.handleKeyEvent(keyCode, event)){
            return true;
        }else if(View.VISIBLE == mPlayGameRoot.getVisibility()){
            mPlayGameRoot.setVisibility(View.GONE);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onNoMoreData() {}

    private class FunGroupChatAdapter extends ChatAdapter {
        protected static final int VIEW_TYPE_FUN_GROUP_NORMAL_SYSTEM = 20;
        protected static final int VIEW_TYPE_FUN_GROUP_BUSINESS_SYSTEM = 21;

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            ViewHolder viewHolder = super.onCreateViewHolder(parent, viewType);
            switch(viewType){
                default:
                    return viewHolder;

                case VIEW_TYPE_FUN_GROUP_NORMAL_SYSTEM:
                case VIEW_TYPE_FUN_GROUP_BUSINESS_SYSTEM:
                    return initFunGroupViewHolder(viewHolder);
            }
        }

        private ViewHolder initFunGroupViewHolder(ViewHolder viewHolder){
            return viewHolder;
        }

        protected void onBindData(ViewHolder holder, int position, ChatRecord data) {
            switch(getViewType(position, data)){
                default:
                    super.onBindData(holder, position, data);
                    break;

                case VIEW_TYPE_FUN_GROUP_NORMAL_SYSTEM:
                    bindFunGroupNormalData(holder, position, data);
                    break;
                case VIEW_TYPE_FUN_GROUP_BUSINESS_SYSTEM:
                    bindFunGroupBusinessData(holder, position, data);
                    break;
            }
        }

        private void bindFunGroupNormalData(ViewHolder holder, int position, ChatRecord data){
            onBindTag(holder, position, data);
            holder.setText(R.id.content, data.content);
        }

        private void bindFunGroupBusinessData(ViewHolder holder, int position, ChatRecord data){
            onBindTag(holder, position, data);
            holder.setText(R.id.content, data.content);
        }

        @Override
        protected int getViewType(int position, ChatRecord data) {
            switch(data.showType){
                case Sportfulgroup.SHOW_TYPE.SHOW_TYPE_NORMAL_VALUE:
                    return super.getViewType(position, data);
                case Sportfulgroup.SHOW_TYPE.SHOW_TYPE_NORMAL_SYSTEM_VALUE:
                    return VIEW_TYPE_FUN_GROUP_NORMAL_SYSTEM;
                case Sportfulgroup.SHOW_TYPE.SHOW_TYPE_BUSINESS_SYSTEM_VALUE:
                    return VIEW_TYPE_FUN_GROUP_BUSINESS_SYSTEM;
            }
            return VIEW_TYPE_NONE;
        }

        protected int getLayoutId(int viewType) {
            switch (viewType) {
                default:
                    return super.getLayoutId(viewType);

                case VIEW_TYPE_FUN_GROUP_NORMAL_SYSTEM:
                    return R.layout.fun_group_normal_system_info;
                case VIEW_TYPE_FUN_GROUP_BUSINESS_SYSTEM:
                    return R.layout.fun_group_business_system_info;
            }
        }
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_GROUP)})
    public void updatedDeletedGroup(Long groupId) {
        super.updatedDeletedGroup(groupId);
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_FRIEND)})
    public void updateDeleteFriend(Long uid){
        super.updateDeleteFriend(uid);
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_CHAT_RECORD)})
    public void updateDeleteChatRecord(Long uid){
        super.updateDeleteChatRecord(uid);
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_GROUP_INFO)})
    public void groupInfoFromServer(Long gid) {
        super.groupInfoFromServer(gid);
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_SINGLE_FRIEND)})
    public void friendInfoFromServer(Long uid) {
        super.friendInfoFromServer(uid);
    }
}
