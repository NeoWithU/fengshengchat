package com.fengshengchat.fun;

import com.fengshengchat.chat.view.IChatView;

public interface IFunGroupView extends IChatView {
    void setHasMoreHistory(boolean has);
    void onNotInGroup(String m);
}
