package com.fengshengchat.fun.model;

import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.chat.model.ChatModel;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.ChatRecord;
import com.fengshengchat.db.DbChatList;
import com.fengshengchat.protocol.DataTypeConverter;

import java.util.concurrent.Callable;

public class FunGroupModel extends ChatModel {

    public void updateChatList(ChatRecord chatRecord){
        asyncTask((Callable<Object>) () -> {
                ChatListBean chatItem = DbChatList.getChatItem(chatRecord.his.uid);
                DbChatList.updateChatItemKeepState(0,
                        DataTypeConverter.convertChatRecord(chatItem, chatRecord));
            Debug.i("FunGroupModel", "--- update chat list --- ");
            return "";
        });
    }

}
