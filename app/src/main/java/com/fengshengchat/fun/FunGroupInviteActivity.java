package com.fengshengchat.fun;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

import com.fengshengchat.R;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.widget.TitleBarLayout;
import com.fengshengchat.widget.YiWebView;

public class FunGroupInviteActivity extends BaseActivity {
    private YiWebView mYiWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fun_group_invite_layout);

        TitleBarLayout titleBar = findViewById(R.id.title_bar);
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            public void onBackClick() {
                finish();
            }
            public void onActionImageClick() {}
            public void onActionClick() {}
        });
        mYiWebView = findViewById(R.id.yi_web_view);
//        mYiWebView.setOnTitleListener(titleBar::setTitleText);
        try {
            String url = getIntent().getStringExtra("url");
            titleBar.setTitleText(getIntent().getStringExtra("title"));
            mYiWebView.setGid(getIntent().getLongExtra("gid", 0L));
            mYiWebView.start(this, url);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.tip)
                .setMessage(R.string.confirm_close_activity)
                .setPositiveButton(R.string.confirm, (dialog, which) -> finish())
                .setNegativeButton(R.string.cancel, (dialog, which) -> {})
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        mYiWebView.destroy();
        super.onDestroy();
    }
}
