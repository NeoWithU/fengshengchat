package com.fengshengchat.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.allenliu.versionchecklib.callback.APKDownloadListener;
import com.allenliu.versionchecklib.v2.AllenVersionChecker;
import com.allenliu.versionchecklib.v2.builder.UIData;
import com.allenliu.versionchecklib.v2.callback.CustomDownloadingDialogListener;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;
import com.fengshengchat.BuildConfig;
import com.fengshengchat.R;
import com.fengshengchat.account.fragment.LoginFragment;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;

import java.io.File;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import io.reactivex.android.schedulers.AndroidSchedulers;
import yiproto.yichat.config.Config;

public class CheckUpdateUtils {

    public void checkUpdate(RxAppCompatActivity activity) {
        Config.UpgradeReq.Builder body = Config.UpgradeReq
                .newBuilder()
                .setPlatform(Config.Platform.PLATFORM_ANDROID)
                .setDevice(Build.BOARD)
                .setSystem(String.valueOf(Build.VERSION.SDK_INT))
                .setVersion(BuildConfig.VERSION_NAME);

        PBMessage pbMessage = new PBMessage();
        pbMessage.setCmd(Config.ConfigCMD.QUERY_UPGRADE_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(data -> Config.UpgradeRsp.parseFrom(data.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Config.UpgradeRsp>() {
                    @Override
                    public void onNext(Config.UpgradeRsp upgradeRsp) {
                        handle(upgradeRsp, activity);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }

    private void handle(Config.UpgradeRsp data, RxAppCompatActivity activity) {
        int result = data.getResult();
        if (result == Config.UpgradeResult.UPGRADE_NONEED_VALUE) {
            return;
        }

        boolean isForce = result == Config.UpgradeResult.UPGRADE_MUST_VALUE;
        UIData uiData = UIData.create()
                .setDownloadUrl(data.getDownurl());

        AllenVersionChecker
                .getInstance()
                .downloadOnly(uiData)
//                .setForceRedownload(true)
                .setShowNotification(false)
                .setCustomVersionDialogListener((context, versionBundle) -> new BaseDialog(context, isForce))
                .setOnCancelListener(() -> {
                    if (isForce) {
//                        activity.finish();
                        LoginFragment.exitApp(activity.getApplicationContext());
                    }
                })
                .setCustomDownloadingDialogListener(new CustomDownloadingDialogListener() {
                    @Override
                    public Dialog getCustomDownloadingDialog(Context context, int progress, UIData versionBundle) {
                        return new ProgressDialog(context);
                    }

                    @Override
                    public void updateUI(Dialog dialog, int progress, UIData versionBundle) {
                        TextView tvProgress = dialog.findViewById(R.id.tv_progress);
                        ProgressBar progressBar = dialog.findViewById(R.id.pb);
                        progressBar.setProgress(progress);
                        tvProgress.setText(progress + "%");
                    }
                })
                .setApkDownloadListener(new APKDownloadListener() {
                    @Override
                    public void onDownloading(int progress) {

                    }

                    @Override
                    public void onDownloadSuccess(File file) {
                        if (isForce) {
                            delayFinish(activity);
                        }
                    }

                    @Override
                    public void onDownloadFail() {

                    }
                })
                .executeMission(activity);
    }

    private void delayFinish(RxAppCompatActivity activity) {
        io.reactivex.Observable.timer(100, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<Long>() {
                    @Override
                    public void onNext(Long aLong) {
                        LoginFragment.exitApp(activity.getApplicationContext());
                    }
                });
    }

    private static class BaseDialog extends Dialog {

        public BaseDialog(Context context, boolean isForce) {
            super(context, R.style.BaseDialog);
            setContentView(R.layout.custom_upgrade_dialog_layout);
            setCanceledOnTouchOutside(false);
            setCancelable(isForce);

            TextView cancelTv = findViewById(R.id.versionchecklib_version_dialog_cancel);
            TextView upgradeTv = findViewById(R.id.versionchecklib_version_dialog_commit);
            if (isForce) {
                cancelTv.setText("退出");
                upgradeTv.setText("升级");
            } else {
                cancelTv.setText("取消");
                upgradeTv.setText("升级");
            }
        }
    }

    private static class ProgressDialog extends Dialog {

        public ProgressDialog(@NonNull Context context) {
            super(context, R.style.BaseDialog);
            setContentView(R.layout.custom_download_layout);
            setCanceledOnTouchOutside(false);
            setCancelable(false);
        }

    }
}
