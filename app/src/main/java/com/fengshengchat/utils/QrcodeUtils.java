package com.fengshengchat.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.blankj.utilcode.util.ConvertUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.net.BaseObserver;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class QrcodeUtils {

    private QrcodeUtils() {
    }

    public static Bitmap generateQrcodeBitmap(@NonNull String content, int width, int height) throws WriterException {
        HashMap<EncodeHintType, Object> hintMap = new HashMap<>();
        hintMap.put(EncodeHintType.MARGIN, 0);
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        return new BarcodeEncoder().encodeBitmap(content, BarcodeFormat.QR_CODE, width, height, hintMap);
    }

    public static Bitmap generateQrcodeBitmapWithLogo(String content, int width, int height, Bitmap logo) throws WriterException {
        Bitmap qrcode = generateQrcodeBitmap(content, width, height);
        if (logo == null) {
            return qrcode;
        }
        return addLogo(qrcode, logo);
    }

    /**
     * 在二维码中间添加Logo图案
     */
    public static Bitmap addLogo(@NonNull Bitmap src, @NonNull Bitmap logo) {
        int srcWidth = src.getWidth();
        int srcHeight = src.getHeight();
        if (srcWidth == 0 || srcHeight == 0) {
            return null;
        }

        int logoWidth = logo.getWidth();
        int logoHeight = logo.getHeight();
        if (logoWidth == 0 || logoHeight == 0) {
            return src;
        }

        //  logo大小为二维码整体大小的1/4
        float scaleFactor = srcWidth / 4.0f / logoWidth;
        Bitmap bitmap = Bitmap.createBitmap(srcWidth, srcHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(src, 0, 0, null);
        canvas.scale(scaleFactor, scaleFactor, srcWidth / 2, srcHeight / 2);
        canvas.drawBitmap(logo, (srcWidth - logoWidth) / 2, (srcHeight - logoHeight) / 2, null);
        canvas.save();
        canvas.restore();
        return bitmap;
    }

    public static void showQrcode(ImageView imageView, String content, String logoUrl, Fragment fragment, LifecycleProvider<FragmentEvent> provider) {
        Glide.with(fragment)
                .load(logoUrl)
                .transform(new ImageUtils.GlideRoundTransform(imageView.getContext(), 15))
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        showQrcode(imageView, content, resource, provider);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        showQrcode(imageView, content, null, provider);
                    }

                    @Override
                    public void onLoadStarted(Drawable placeholder) {
                        super.onLoadStarted(placeholder);
                    }
                });
    }

    private static void showQrcode(ImageView imageView, String id, Drawable drawable, LifecycleProvider<FragmentEvent> provider) {
        int width = imageView.getMeasuredWidth();
        int height = imageView.getMeasuredHeight();
        Observable.fromCallable(() -> {
            Bitmap logo = drawable != null ? ConvertUtils.drawable2Bitmap(drawable) : null;
            return QrcodeUtils.generateQrcodeBitmapWithLogo(id, width, height, logo);
        }).compose(provider.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Bitmap>() {
                    @Override
                    public void onNext(Bitmap bitmap) {
                        imageView.setImageBitmap(bitmap);
                    }
                });
    }

    private static final Map<DecodeHintType, Object> HINTS = new EnumMap<>(DecodeHintType.class);

    public static String decodeQRCode(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        RGBLuminanceSource source = new RGBLuminanceSource(width, height, pixels);
        HybridBinarizer binarizer = new HybridBinarizer(source);
        BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);
        Result result;
        try {
            result = new MultiFormatReader().decode(binaryBitmap, HINTS);
            return result.getText();
        } catch (NotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

}
