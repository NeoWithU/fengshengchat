package com.fengshengchat.utils.bean;

public class UploadResponse {
    public String RetCode = "0";
    public String RetMsg = "";

    public String getCode(){
        return RetCode;
    }

    public String getMsg(){
        return RetMsg;
    }

    public boolean isOK(){
        return "0".equals(RetCode);
    }
}
