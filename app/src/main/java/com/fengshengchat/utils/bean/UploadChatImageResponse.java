package com.fengshengchat.utils.bean;

public class UploadChatImageResponse extends UploadResponse{
    private String File1;
    private String File2;

    public String getFile1() {
        return File1;
    }

    public void setFile1(String file1) {
        File1 = file1;
    }

    public String getFile2() {
        return File2;
    }

    public void setFile2(String file2) {
        File2 = file2;
    }
}
