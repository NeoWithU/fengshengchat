package com.fengshengchat.utils;

import com.google.gson.Gson;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.utils.bean.UploadChatImageResponse;
import com.fengshengchat.utils.bean.UploadResponse;

import java.io.File;
import java.io.IOException;

import io.github.lizhangqu.coreprogress.ProgressHelper;
import io.github.lizhangqu.coreprogress.ProgressUIListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UploadUtils {

    public interface UploadResponseCallback{
        void onFailure(Call call, Exception e);
        void onResponse(Call call, UploadResponse response) throws Exception;
    }

    public interface UploadChatImageResponseCallback extends UploadResponseCallback{
        void onResponse(Call call, UploadChatImageResponse response) throws Exception;
    }

    private static String getUrl(){
        return Protocol.getUploadUrl(false);
    }

    public static void uploadFile(String filePath, UploadResponseCallback responseCallback){
        uploadFileWithProgress(getUrl(), new File(filePath), null, responseCallback);
    }

    public static void uploadFile(File file, UploadResponseCallback responseCallback){
        uploadFileWithProgress(getUrl(), file, null, responseCallback);
    }

    public static void uploadFile(String url, String filePath, UploadResponseCallback responseCallback){
        uploadFileWithProgress(url, new File(filePath), null, responseCallback);
    }

    public static void uploadFile(String url, File file, UploadResponseCallback responseCallback){
        uploadFileWithProgress(url, file, null, responseCallback);
    }

    public static void uploadFileWithProgress(String url, String filePath,
                                              ProgressUIListener progressUIListener,
                                              UploadResponseCallback responseCallback){
        uploadFileWithProgress(url, new File(filePath), progressUIListener, responseCallback);
    }

    public static void uploadFileWithProgress(String url, File file,
                                              ProgressUIListener progressUIListener,
                                              UploadResponseCallback responseCallback){
        MultipartBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("info", getUid() + ",msg")
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), file))
                .build();

        RequestBody requestBodyWrapper = null == progressUIListener
                ? requestBody : ProgressHelper.withProgress(requestBody, progressUIListener);

        Request request = new Request.Builder()
                .url(url)
                .post(requestBodyWrapper)
                .build();

        new OkHttpClient().newCall(request)
        .enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                if(null != responseCallback){
                    responseCallback.onFailure(call, e);
                }
            }
            public void onResponse(Call call, Response response) throws IOException {
                if(null != responseCallback){
                    try{
                        UploadResponse uploadResponse = new Gson()
                                .fromJson(response.body().string(), UploadResponse.class);
                        responseCallback.onResponse(call, uploadResponse);
                    }catch(Exception e){
                        e.printStackTrace();
                        responseCallback.onFailure(call, e);
                    }
                }
            }
        });
    }

    private static long getUid(){
        try {
            return UserManager.getUser().uid;
        }catch(Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    public static void uploadChatImage(File thumb, File file,
                                              ProgressUIListener progressUIListener,
                                       UploadChatImageResponseCallback responseCallback){
        MultipartBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("info", getUid() + ",msg")
                .addFormDataPart("file1", thumb.getName(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), thumb))
                .addFormDataPart("file2", file.getName(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), file))
                .build();

        RequestBody requestBodyWrapper = null == progressUIListener
                ? requestBody : ProgressHelper.withProgress(requestBody, progressUIListener);

        Request request = new Request.Builder()
                .url(Protocol.getUploadUrl(true))
                .post(requestBodyWrapper)
                .build();

        new OkHttpClient().newCall(request)
                .enqueue(new Callback() {
                    public void onFailure(Call call, IOException e) {
                        if(null != responseCallback){
                            responseCallback.onFailure(call, e);
                        }
                    }
                    public void onResponse(Call call, Response response) throws IOException {
                        if(null != responseCallback){
                            try{
                                UploadChatImageResponse uploadResponse = new Gson()
                                        .fromJson(response.body().string(), UploadChatImageResponse.class);
                                responseCallback.onResponse(call, uploadResponse);
                            }catch(Exception e){
                                e.printStackTrace();
                                responseCallback.onFailure(call, e);
                            }
                        }
                    }
                });
    }

}
