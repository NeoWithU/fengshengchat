package com.fengshengchat.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.google.gson.Gson;
import com.fengshengchat.user.User;

/**
 * Sp Utils 扩展
 * <p>
 * Created by Jinjia on 2017/12/8.
 */
public class ExSpUtils {

    public synchronized static void saveObject(@NonNull String key, @NonNull Object object) {
        if (object instanceof User) {
            LogUtils.e("my-user: " + ((User) object).token);
        }
        Gson gson = new Gson();
        String json = gson.toJson(object);
        SPUtils.getInstance().put(key, json, true);
    }

    @Nullable
    public static <T> T getObject(@NonNull String key, @NonNull Class<T> clazz) {
        String json = SPUtils.getInstance().getString(key);
        if (TextUtils.isEmpty(json)) {
            return null;
        }
        return new Gson().fromJson(json, clazz);
    }

    public synchronized static void remove(@NonNull String key) {
        LogUtils.e("my-user: removed");
        SPUtils.getInstance().remove(key, true);
    }

}
