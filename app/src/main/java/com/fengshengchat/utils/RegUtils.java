package com.fengshengchat.utils;

public class RegUtils {
    private static final String REG_CHINESE = "[\u4e00-\u9fa5]";

    public static int stringLength(String value) {
        int length = 0;
        for (int i = 0; i < value.length(); i++) {
            String temp = value.substring(i, i + 1);
            if (temp.matches(REG_CHINESE)) {
                length += 2;
            } else {
                length += 1;
            }
        }
        return length;
    }
}
