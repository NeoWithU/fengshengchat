package com.fengshengchat.group.model;

import java.util.List;

public class GroupMergeData {
    public List<GroupMemberData> groupMembers;
    public GroupData groupData;

    public GroupMergeData(List<GroupMemberData> groupMembers, GroupData groupData) {
        this.groupMembers = groupMembers;
        this.groupData = groupData;
    }
}
