package com.fengshengchat.group.model;

import com.fengshengchat.contacts.model.Friend;

import java.util.ArrayList;
import java.util.List;

public class SelectedFriendList {
    private final List<Friend> items = new ArrayList<>();

    public List<Friend> getItems() {
        return items;
    }

    public void add(Friend friend) {
        items.add(friend);
    }

    public void remove(Friend friend) {
        items.remove(friend);
    }
}
