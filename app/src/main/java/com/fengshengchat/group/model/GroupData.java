package com.fengshengchat.group.model;

import android.os.Parcel;
import android.support.annotation.NonNull;

import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.Group;
import com.fengshengchat.user.UserManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * 群组数据
 */
public class GroupData extends Group {

    public GroupData() {
    }

    public GroupData(long id) {
        super(id);
    }

    public static Observable<List<GroupData>> getGroupListObservable() {
        return Observable.fromCallable(() -> DbManager.getDb().getGroupDao().findAll(UserManager.getUser().uid))
                .map(GroupData::convert);
    }

    public static Observable<GroupData> getGroupObservable(long id) {
        return Observable.fromCallable(() -> DbManager.getDb().getGroupDao().findById(id))
                .map(GroupData::getGroupData)
                .observeOn(Schedulers.io());
    }

    private static List<GroupData> convert(List<Group> list) {
        ArrayList<GroupData> items = new ArrayList<>(list.size());
        for (Group group : list) {
            GroupData item = getGroupData(group);
            items.add(item);
        }
        return items;
    }

    @NonNull
    public static GroupData getGroupData(Group group) {
        GroupData item = new GroupData();
        item.id = group.id;
        item.name = group.name;
        item.ownerId = group.ownerId;
        item.avatar = group.avatar;
        item.notice = group.notice;
        item.type = group.type;
        item.memberCount = group.memberCount;
        item.adminIds = group.adminIds;
        item.isPinNotice = group.isPinNotice;
        item.needInvestConfirm = group.needInvestConfirm;
        item.updatedTime = group.updatedTime;
        item.isPinSession = group.isPinSession;
        item.notDisturb = group.notDisturb;
        item.unhandledInviteCount = group.unhandledInviteCount;
        item.ownerName = group.ownerName;
        item.noticeUpdatedTime = group.noticeUpdatedTime;
        return item;
    }

    public boolean isAdmin() {
        if (UserManager.isEmpty()) {
            return false;
        }

        long uid = UserManager.getUser().uid;
        return uid == ownerId || uid == getAdminId();
    }

    public boolean isOwner() {
        if (UserManager.isEmpty()) {
            return false;
        }
        return UserManager.getUser().uid == ownerId;
    }

    /**
     * 是否是普通群
     */
    public boolean isGeneralGroup() {
        return type == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    protected GroupData(Parcel in) {
        super(in);
    }

    public static final Creator<GroupData> CREATOR = new Creator<GroupData>() {
        @Override
        public GroupData createFromParcel(Parcel source) {
            return new GroupData(source);
        }

        @Override
        public GroupData[] newArray(int size) {
            return new GroupData[size];
        }
    };
}
