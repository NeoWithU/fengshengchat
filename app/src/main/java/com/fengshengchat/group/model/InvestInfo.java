package com.fengshengchat.group.model;

/**
 * 邀请信息
 */
public class InvestInfo {
    /**
     * 被邀请人 id
     */
    public long id;
    /**
     * 被邀请人昵称 
     */
    public String nickname;
    /**
     * 邀请人id
     */
    public long inviterId;
    /**
     * 邀请人昵称
     */
    public String inviterNickname;
    
    public long updateTime;
    public int state;
}
