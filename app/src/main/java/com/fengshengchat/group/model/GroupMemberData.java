package com.fengshengchat.group.model;

import android.os.Parcel;

import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.GroupMember;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class GroupMemberData extends GroupMember {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    public GroupMemberData() {
    }

    private GroupMemberData(Parcel in) {
        super(in);
    }

    public static final Creator<GroupMemberData> CREATOR = new Creator<GroupMemberData>() {
        @Override
        public GroupMemberData createFromParcel(Parcel source) {
            return new GroupMemberData(source);
        }

        @Override
        public GroupMemberData[] newArray(int size) {
            return new GroupMemberData[size];
        }
    };

    public static Observable<List<GroupMemberData>> getMemberListObservable(long groupId) {
        return Observable.fromCallable(() -> DbManager.getDb().getGroupMemberDao().findAll(groupId))
                .map(GroupMemberData::convert)
                .observeOn(Schedulers.io());
    }

    private static List<GroupMemberData> convert(List<GroupMember> list) {
        ArrayList<GroupMemberData> items = new ArrayList<>();
        for (GroupMember member : list) {
            GroupMemberData item = new GroupMemberData();
            item.id = member.id;
            item.groupId = member.groupId;
            item.nickname = member.nickname;
            item.avatar = member.avatar;
            item.bigAvatar = member.bigAvatar;
            item.userId = member.userId;
            item.updatedTime = member.updatedTime;
            item.enterGroupTime = member.enterGroupTime;
            item.isDeleted = member.isDeleted;
            items.add(item);
        }
        return items;
    }

}
