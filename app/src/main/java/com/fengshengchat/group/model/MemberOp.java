package com.fengshengchat.group.model;

import com.fengshengchat.R;

public class MemberOp {
    /**
     * 增加群成员
     */
    public static final int OP_ADD_MEMBER = 1;
    /**
     * 删除群成员
     */
    public static final int OP_DEL_MEMBER = 2;

    public final int opType;
    public final int iconResId;

    private MemberOp(int opType, int iconResId) {
        this.opType = opType;
        this.iconResId = iconResId;
    }

    public static final MemberOp ADD_OP = new MemberOp(OP_ADD_MEMBER, R.drawable.group_ic_add);
    public static final MemberOp DEL_OP = new MemberOp(OP_DEL_MEMBER, R.drawable.group_ic_del);

}
