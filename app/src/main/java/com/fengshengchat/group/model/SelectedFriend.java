package com.fengshengchat.group.model;

/**
 * 已经选择的联系人
 */
public class SelectedFriend {
    public long id;
    public String avatarUrl;

    public SelectedFriend(long id, String avatarUrl) {
        this.id = id;
        this.avatarUrl = avatarUrl;
    }
}
