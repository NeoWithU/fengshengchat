package com.fengshengchat.group.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.utils.PhotoAlbumUtil;
import com.fengshengchat.app.dialog.BottomDialogFragment;
import com.fengshengchat.base.PickerDialogStringFragment;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.protocol.Protocol;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import io.reactivex.Observable;

/**
 * 群二维码名片
 */
public class GroupQrcodeFragment extends ToolbarFragment {

    public static final String TAG = "GroupQrcodeFragment";

    private static final String EXTRA_GROUP_ID = "GROUP_ID";
    private static final String EXTRA_NAME = "NAME";
    private static final String EXTRA_AVATAR_URL = "AVATAR_URL";

    public static final int REQ_CODE_SAVE_QR_CODE = 1;

    private long groupId;
    private String name;
    private String avatarUrl;

    @BindView(R.id.title_bar)
    TitleBarLayout titleBarLayout;
    @BindView(R.id.avatar_iv)
    ImageView avatarIv;
    @BindView(R.id.name_tv)
    TextView nameTv;
    @BindView(R.id.qrcode_iv)
    ImageView qrcodeIv;

    public static GroupQrcodeFragment newInstance(long groupId, String name, String avatarUrl) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_GROUP_ID, groupId);
        args.putString(EXTRA_NAME, name);
        args.putString(EXTRA_AVATAR_URL, avatarUrl);
        GroupQrcodeFragment fragment = new GroupQrcodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.group_qrcode_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }

        groupId = args.getLong(EXTRA_GROUP_ID);
        name = args.getString(EXTRA_NAME);
        avatarUrl = args.getString(EXTRA_AVATAR_URL);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        titleBarLayout.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                onBack();
            }

            @Override
            public void onActionImageClick() {
                openMoreDialog();
            }

            @Override
            public void onActionClick() {
            }
        });
        ImageUtils.loadImage(avatarIv, avatarUrl, R.drawable.image_default);
        nameTv.setText(name);
        qrcodeIv.post(this::generateQrCode);
    }

    private void openMoreDialog() {
        String[] save = new String[]{
                getString(R.string.save)
        };
        BottomDialogFragment fragment = BottomDialogFragment.newInstance(save);
        fragment.setTargetFragment(this, REQ_CODE_SAVE_QR_CODE);
        fragment.show(getFragmentManager(), PickerDialogStringFragment.TAG);
    }

    /**
     * 生成二维码
     */
    private void generateQrCode() {
        int width = qrcodeIv.getMeasuredWidth();
        int height = qrcodeIv.getMeasuredHeight();

        Observable.fromCallable(() -> {
            Map<EncodeHintType, Object> params = new HashMap<>();
            params.put(EncodeHintType.MARGIN, 0);
            params.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            return new BarcodeEncoder().encodeBitmap(Protocol.generateGroupCardString(groupId, UserManager.getUser().uid), BarcodeFormat.QR_CODE, width, height, params);
        }).compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Bitmap>() {
                    @Override
                    public void onNext(Bitmap bitmap) {
                        qrcodeIv.setImageBitmap(bitmap);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQ_CODE_SAVE_QR_CODE:
                saveQrcode();
                break;

            default:
                break;
        }
    }

    public void saveQrcode() {
        new RxPermissions(this)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean granted) {
                        if (granted) {
                            saveToPhotoAlbum();
                        } else {
                            ToastUtils.showShort(R.string.search_need_write_storage_permission);
                        }
                    }
                });
    }

    /**
     * 将二维码保存到相册
     */
    private void saveToPhotoAlbum() {
        Observable.fromCallable(() -> ConvertUtils.view2Bitmap(qrcodeIv))
                .map(bitmap -> PhotoAlbumUtil.saveImageToGallery(getContext(), bitmap))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean success) {
                        if (success) {
                            ToastUtils.showShort(R.string.search_save_success);
                        } else {
                            ToastUtils.showShort(R.string.search_save_error);
                        }
                    }
                });
    }
}
