package com.fengshengchat.group.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.group.model.GroupData;
import com.fengshengchat.net.EmptyHttpRequestDialogObserver;
import com.fengshengchat.widget.TitleBarLayout;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.group.Group;

/**
 * 群公告
 */
public class GroupNoticeEditorFragment extends ToolbarFragment {

    public static final String TAG = "GroupNoticeEditorFragment";
    public static final String EXTRA_CONTENT = "CONTENT";
    public static final String EXTRA_TIME = "TIME";

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE);

    private GroupData groupData;
    private boolean isPreviewed;
    private boolean isDirty;

    @BindView(R.id.view_switcher)
    ViewSwitcher viewSwitcher;
    @BindView(R.id.title_bar)
    TitleBarLayout titleBar;
    @BindView(R.id.avatar_iv)
    ImageView avatarIv;
    @BindView(R.id.nickname_tv)
    TextView nicknameTv;
    @BindView(R.id.updated_time_tv)
    TextView updatedTimeTv;
    @BindView(R.id.content_tv)
    TextView contentTv;
    @BindView(R.id.content_et)
    EditText contentEt;

    public static GroupNoticeEditorFragment newInstance(GroupData groupData) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, groupData);
        GroupNoticeEditorFragment fragment = new GroupNoticeEditorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.group_notice_editor_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        groupData = args.getParcelable(EXTRA_DATA);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitleBar();
        initView();
    }

    private void initTitleBar() {
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                onBack();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
                if (isPreviewed) {
                    setPreviewed(false);
                } else {
                    requestSetGroupInfo();
                }
            }
        });
    }

    private void setPreviewed(boolean isPreviewed) {
        this.isPreviewed = isPreviewed;
        TextView actionTextView = titleBar.getActionTextView();
        if (isPreviewed) {
            actionTextView.setText(R.string.group_edit);
            viewSwitcher.setDisplayedChild(1);
        } else {
            actionTextView.setText(R.string.complete);
            viewSwitcher.setDisplayedChild(0);
        }
    }

    /**
     * 请求设置群信息
     */
    private void requestSetGroupInfo() {
        String content = contentEt.getText().toString();

        Group.GroupInfo groupInfo = Group.GroupInfo
                .newBuilder()
                .setGroupId(groupData.id)
                .setGroupNotice(content)
                .build();

        GroupInfoFragment.getSetGroupInfoObserver(groupInfo, Group.SetGroupInfoReq.SetType.SET_TYPE_GROUP_NOTICE_VALUE)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(data -> {
                    if (data.getResult() != 0) {
                        toast(data.getErrMsg());
                        return;
                    }
                    isDirty = true;
                    groupData.noticeUpdatedTime = System.currentTimeMillis();

                    contentTv.setText(content);
                    updatedTimeTv.setText(simpleDateFormat.format(new Date(groupData.noticeUpdatedTime)));
                    setPreviewed(true);
                })
                .filter(data -> data.getResult() == 0)
                .observeOn(Schedulers.io())
                .doOnNext(data -> DbManager.getDb().getGroupDao().updateGroupNotice(groupData.id, content, groupData.noticeUpdatedTime))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new EmptyHttpRequestDialogObserver(getFragmentManager()));
    }

    @Override
    public void onDestroyView() {
        handleSetGroupInfoRes();
        super.onDestroyView();
    }

    private void handleSetGroupInfoRes() {
        if (!isDirty) {
            return;
        }

        Fragment targetFragment = getTargetFragment();
        if (targetFragment == null) {
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_CONTENT, contentEt.getText().toString());
        intent.putExtra(EXTRA_TIME, groupData.noticeUpdatedTime);
        int targetRequestCode = getTargetRequestCode();
        targetFragment.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent);
        getFragmentManager().popBackStack();
    }

    private void initContentEditText() {
        contentEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        contentEt.setText(groupData.notice);
    }

    private void initView() {
        TextView actionTextView = titleBar.getActionTextView();
        if (groupData.isAdmin()) {
            actionTextView.setVisibility(View.VISIBLE);
            initContentEditText();
        } else {
            actionTextView.setVisibility(View.GONE);
        }

        ImageUtils.loadRadiusImage(avatarIv, groupData.avatar, R.drawable.group_ic_default, AppConfig.IMAGE_RADIUS);
        nicknameTv.setText(groupData.ownerName);
        updatedTimeTv.setText(simpleDateFormat.format(new Date(groupData.noticeUpdatedTime)));
        contentTv.setText(groupData.notice);

        if (!TextUtils.isEmpty(groupData.notice)) {
            viewSwitcher.setDisplayedChild(1);
            setPreviewed(true);
        } else {
            setPreviewed(false);
        }
    }

}
