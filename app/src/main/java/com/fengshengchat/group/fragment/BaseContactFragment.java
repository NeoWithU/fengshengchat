package com.fengshengchat.group.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.view.NoFriendViewBinder;
import com.fengshengchat.app.model.SearchEmpty;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.contacts.model.Section;
import com.fengshengchat.contacts.view.ContactsDecoration;
import com.fengshengchat.contacts.view.ContactsSectionViewBinder;
import com.fengshengchat.db.GroupMember;
import com.fengshengchat.group.binder.ChooseFriendItemViewBinder;
import com.fengshengchat.group.binder.SelectedFriendListViewBinder;
import com.fengshengchat.group.model.SelectedFriendList;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.subjects.PublishSubject;
import me.drakeet.multitype.ItemViewBinder;
import me.drakeet.multitype.MultiTypeAdapter;
import me.drakeet.multitype.TypePool;

public abstract class BaseContactFragment extends ToolbarFragment implements BaseItemViewBinder.OnClickListener<Friend> {

    protected List<Object> items = new ArrayList<>();
    protected MultiTypeAdapter adapter;
    protected PublishSubject<String> subject;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.title_bar)
    TitleBarLayout titleBar;
    @BindView(R.id.search_et)
    EditText searchEt;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_select_contact;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSearchEditText();
        initRecyclerView();
        initSubject();
    }

    protected void initSearchEditText() {
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                subject.onNext(s.toString());
            }
        });
    }

    protected void initRecyclerView() {
        adapter = new MultiTypeAdapter(items);
        adapter.register(Section.class, new ContactsSectionViewBinder());
        adapter.register(SelectedFriendList.class, new SelectedFriendListViewBinder(getOnClickAvatarListener()));
        adapter.register(Friend.class, new ChooseFriendItemViewBinder(this));
        adapter.register(SearchEmpty.class, new NoFriendViewBinder());

        ContactsDecoration decoration = new ContactsDecoration(items);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.addItemDecoration(decoration);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    protected void setKeyWord(String keyWord) {
        TypePool typePool = adapter.getTypePool();
        int i = typePool.firstIndexOf(Friend.class);
        ItemViewBinder<?, ?> itemViewBinder = typePool.getItemViewBinder(i);
        ChooseFriendItemViewBinder viewBinder = (ChooseFriendItemViewBinder) itemViewBinder;
        viewBinder.setKeyWord(keyWord);
    }

    @OnClick(R.id.cancel_tv)
    protected void clickCancelButton() {
        titleBar.getListener().onBackClick();
    }

    private void initSubject() {
        subject = PublishSubject.create();
        subject.debounce(100, TimeUnit.MILLISECONDS)
                .filter(s -> {
                    List<Friend> friendList = getDataSource();
                    return friendList != null && !friendList.isEmpty();
                })
                .map(s -> new Pair<>(getFriendList(s), s))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new BaseObserver<Pair<List<Friend>, String>>() {
                    @Override
                    public void onNext(Pair<List<Friend>, String> pair) {
                        setKeyWord(pair.second);
                        bind(pair.first);
                    }
                });
    }

    protected abstract void bind(List<Friend> friendList);

    private List<Friend> getFriendList(String s) {
        List<Friend> dataSource = getDataSource();
        if (dataSource == null) {
            return new ArrayList<>();
        }

        if (s.length() == 0) {
            return dataSource;
        }

        ArrayList<Friend> list = new ArrayList<>();
        for (Friend friend : dataSource) {
            if (friend.nickname.contains(s)) {
                list.add(friend);
            }
        }
        return list;
    }

    protected abstract List<Friend> getDataSource();

    @NonNull
    protected List<Friend> getFriendList(List<GroupMember> groupMemberList) {
        ArrayList<Friend> friendArrayList = new ArrayList<>(groupMemberList.size());
        for (GroupMember info : groupMemberList) {
            Friend friend = new Friend();
            friend.id = info.userId;
            friend.nickname = info.nickname;
            friend.avatar = info.avatar;
            friend.updateNicknamePinyin();
            friendArrayList.add(friend);
        }
        return friendArrayList;
    }

    public SelectedFriendListViewBinder.OnClickAvatarListener getOnClickAvatarListener() {
        return friend -> {
        };
    }
}
