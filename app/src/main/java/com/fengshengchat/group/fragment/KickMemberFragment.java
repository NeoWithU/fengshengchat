package com.fengshengchat.group.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.contacts.model.ContactsData;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.helper.Task;
import com.fengshengchat.db.helper.TaskHelper;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import yiproto.yichat.group.Group;

/**
 * 移除成员
 */
public class KickMemberFragment extends BaseContactFragment {

    public static final String TAG = "KickMemberFragment";
    public static final String KEY_IDS = "IDS";
    private static final String EXTRA_GROUP_ID = "GROUP_ID";
    private static final String EXTRA_FILTER_IDS = "FILTER_IDS";

    private long groupId;
    private long[] filterIds;
    private List<Friend> selectedFriendList = new ArrayList<>();
    private List<Friend> friendList;

    public static KickMemberFragment newInstance(long groupId, long[] filterIds) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_GROUP_ID, groupId);
        args.putLongArray(EXTRA_FILTER_IDS, filterIds);
        KickMemberFragment fragment = new KickMemberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }

        groupId = args.getLong(EXTRA_GROUP_ID);
        filterIds = args.getLongArray(EXTRA_FILTER_IDS);
    }

    private void requestData() {
        Observable.fromCallable(() -> DbManager.getDb().getGroupMemberDao().findAll(groupId))
                .map(this::getFriendList)
                .doOnNext(this::filter)
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<List<Friend>>() {
                    @Override
                    public void onNext(List<Friend> friendList) {
                        bindFriendList(friendList);
                    }
                });
    }

    private void filter(List<Friend> friendList) {
        for (int i = friendList.size() - 1; i > 0; i--) {
            long id = friendList.get(i).id;
            for (long filterId : filterIds) {
                if (filterId == id) {
                    friendList.remove(i);
                    break;
                }
            }
        }
    }

    private void bindFriendList(List<Friend> friendList) {
        this.friendList = friendList;
        items.addAll(ContactsData.convertSectionData(friendList));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitleBar();
        requestData();
    }

    @Override
    protected void bind(List<Friend> friendList) {
        items.clear();
        items.addAll(ContactsData.convertSectionData(friendList));
        adapter.notifyDataSetChanged();
    }

    @Override
    protected List<Friend> getDataSource() {
        return friendList;
    }

    private void initTitleBar() {
        titleBar.getActionTextView().setEnabled(false);
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                finish();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
                requestKickGroupMember();
            }
        });
        titleBar.setTitleText(R.string.group_delete_member);
    }

    /**
     * 请求删除群成员
     */
    private void requestKickGroupMember() {
        Group.KickMemberReq.Builder body = Group.KickMemberReq
                .newBuilder()
                .setGroupId(groupId);
        long[] ids = new long[selectedFriendList.size()];
        for (int i = 0; i < selectedFriendList.size(); i++) {
            Friend friend = selectedFriendList.get(i);
            ids[i] = friend.id;
            body.addUserIdList(ids[i]);
        }

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.KICK_MEMBER_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.KickMemberRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Group.KickMemberRsp>(getFragmentManager()) {
                    private Group.KickMemberRsp data;

                    @Override
                    public void onNext(Group.KickMemberRsp data) {
                        this.data = data;
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        handleDelGroupMemberRes(data, ids);
                    }
                });
    }

    /**
     * 处理删除群成员结果
     */
    private void handleDelGroupMemberRes(Group.KickMemberRsp data, long[] ids) {
        if (data.getResult() != Group.KickMemberRsp.KickMemberResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(KEY_IDS, ids);
        Fragment fragment = getTargetFragment();
        fragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        getFragmentManager().popBackStack();

        TaskHelper.getInstance().post(Task.TASK_GROUP_MEMBER_LIST);
    }

    @Override
    public void onItemClicked(Friend item) {
        if (item.checked) {
            selectedFriendList.remove(item);
        } else {
            selectedFriendList.add(item);
        }

        TextView actionTextView = titleBar.getActionTextView();
        actionTextView.setEnabled(!selectedFriendList.isEmpty());

        item.checked = !item.checked;
        adapter.notifyDataSetChanged();
    }
}
