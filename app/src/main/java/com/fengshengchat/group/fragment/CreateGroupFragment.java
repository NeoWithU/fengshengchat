package com.fengshengchat.group.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.app.model.SearchEmpty;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.chat.view.ChatActivity;
import com.fengshengchat.contacts.model.ContactsData;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.group.binder.SelectedFriendListViewBinder;
import com.fengshengchat.group.model.SelectedFriendList;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.List;

import yiproto.yichat.group.Group;

/**
 * 创建组
 */
public class CreateGroupFragment extends BaseContactFragment {

    public static final String TAG = "CreateGroupFragment";
    private static final String EXTRA_FRIEND_ID = "FRIEND_ID";
    private final SelectedFriendList selectedFriendList = new SelectedFriendList();
    private long friendId;

    public static CreateGroupFragment newInstance(long friendId) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_FRIEND_ID, friendId);
        CreateGroupFragment fragment = new CreateGroupFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        friendId = args.getLong(EXTRA_FRIEND_ID);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitleBar();
    }

    private List<Friend> dataSource;

    @Override
    protected List<Friend> getDataSource() {
        return dataSource;
    }

    private void initTitleBar() {
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                onBack();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
                requestCreateGroup();
            }
        });
        titleBar.getActionTextView().setEnabled(false);
    }

    /**
     * 请求创建群
     */
    public void requestCreateGroup() {
        Group.CreateGroupReq.Builder body = Group.CreateGroupReq.newBuilder();
        for (Friend item : selectedFriendList.getItems()) {
            body.addMemberList(item.id);
        }

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.CREATE_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.CreateGroupRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Group.CreateGroupRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.CreateGroupRsp data) {
                        bindUi(data);
                    }
                });
    }

    private void bindUi(Group.CreateGroupRsp data) {
        if (data.getResult() != Group.CreateGroupRsp.CreateGroupResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        Group.GroupInfo groupInfo = data.getGroupInfo();
        if (null != groupInfo) {
            ChatActivity.startChat(getContext(), groupInfo.getGroupId(),
                    groupInfo.getGroupName(), "", true,
                    groupInfo.getGroupType(), groupInfo.getGroupMemberCount());
        }
        getActivity().finish();
    }

    @Override
    public void onItemClicked(Friend item) {
        if (item.checked) {
            selectedFriendList.remove(item);
        } else {
            selectedFriendList.add(item);
        }

        TextView actionTextView = titleBar.getActionTextView();
        int checkedCount = selectedFriendList.getItems().size();

        if (checkedCount > 0) {
            actionTextView.setText(getString(R.string.group_finish_format, checkedCount));
        } else {
            actionTextView.setText(R.string.group_finish);
        }
        actionTextView.setEnabled(checkedCount >= 2);

        item.checked = !item.checked;
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Friend.getFriendListObservable()
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<List<Friend>>() {
                    @Override
                    public void onNext(List<Friend> friendList) {
                        bindFriendList(friendList);
                    }
                });
    }

    private void bindFriendList(List<Friend> friendList) {
        dataSource = friendList;

        if (friendList.isEmpty()) {
            items.add(SearchEmpty.INSTANCE);
        } else {
            items.add(selectedFriendList);
            for (Friend friend : friendList) {
                if (friend.id == friendId) {
                    friend.disabled = true;
                    selectedFriendList.getItems().add(friend);
                    break;
                }
            }
            items.addAll(ContactsData.convertSectionData(friendList));
        }
        adapter.notifyDataSetChanged();

        TextView actionTextView = titleBar.getActionTextView();
        actionTextView.setText(getString(R.string.group_finish_format, selectedFriendList.getItems().size()));
    }

    @Override
    protected void bind(List<Friend> friendList) {
        items.clear();
        items.add(selectedFriendList);
        items.addAll(ContactsData.convertSectionData(friendList));
        adapter.notifyDataSetChanged();
    }

    @Override
    public SelectedFriendListViewBinder.OnClickAvatarListener getOnClickAvatarListener() {
        return this::onItemClicked;
    }
}
