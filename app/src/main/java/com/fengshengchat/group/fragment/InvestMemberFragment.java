package com.fengshengchat.group.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.app.model.SearchEmpty;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.contacts.model.ContactsData;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.group.binder.SelectedFriendListViewBinder;
import com.fengshengchat.group.model.GroupMemberData;
import com.fengshengchat.group.model.SelectedFriendList;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.EmptyObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.ArrayList;
import java.util.List;

import yiproto.yichat.group.Group;

/**
 * 邀请进群
 */
public class InvestMemberFragment extends BaseContactFragment {

    public static final String TAG = "InvestMemberFragment";
    public static final String KEY_INVEST_MEMBER = "INVEST_MEMBER";
    private static final String EXTRA_GROUP_ID = "GROUP_ID";
    private static final String EXTRA_MEMBER_ID_LIST = "MEMBER_ID_LIST";

    public static InvestMemberFragment newInstance(long groupId, long[] memberIdList) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_GROUP_ID, groupId);
        args.putLongArray(EXTRA_MEMBER_ID_LIST, memberIdList);
        InvestMemberFragment fragment = new InvestMemberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private long groupId;
    private long[] memberIdList;
    private SelectedFriendList selectedFriendList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        groupId = args.getLong(EXTRA_GROUP_ID);
        memberIdList = args.getLongArray(EXTRA_MEMBER_ID_LIST);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitleBar();
    }

    @Override
    protected void bind(List<Friend> friendList) {
        items.clear();
        items.add(selectedFriendList);
        items.addAll(ContactsData.convertSectionData(friendList));
        adapter.notifyDataSetChanged();
    }

    private void initTitleBar() {
        titleBar.getActionTextView().setEnabled(false);
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                finish();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
                requestAddGroupMember();
            }
        });
    }

    /**
     * 请求添加群成员
     */
    private void requestAddGroupMember() {
        Group.InviteJoinGroupReq.Builder body = Group.InviteJoinGroupReq
                .newBuilder()
                .setGroupId(groupId);
        for (Friend item : selectedFriendList.getItems()) {
            body.addUserIdList(item.id);
        }

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.INVITE_JOIN_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.InviteJoinGroupRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Group.InviteJoinGroupRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.InviteJoinGroupRsp data) {
                        handleAddGroupMemberRes(data);
                    }
                });
    }

    /**
     * 处理加群成员结果
     */
    private void handleAddGroupMemberRes(Group.InviteJoinGroupRsp data) {
        if (data.getResult() != Group.InviteJoinGroupRsp.Result.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        ArrayList<GroupMemberData> list = new ArrayList<>();
        for (Friend friend : selectedFriendList.getItems()) {
            GroupMemberData member = new GroupMemberData();
            member.id = friend.id;
            member.groupId = groupId;
            member.avatar = friend.avatar;
            member.nickname = friend.nickname;
            list.add(member);
        }

        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(KEY_INVEST_MEMBER, list);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        getFragmentManager().popBackStack();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Friend.getFriendListObservable()
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .doOnNext(this::bindFriendList)
                .subscribe(new EmptyObserver());
    }

    /**
     * 绑定好友列表
     */
    private void bindFriendList(List<Friend> friendList) {
        dataSource = friendList;

        if (friendList.isEmpty()) {
            items.add(SearchEmpty.INSTANCE);
        } else {
            selectedFriendList = new SelectedFriendList();
            items.add(selectedFriendList);
            for (Friend friend : friendList) {
                for (long id : memberIdList) {
                    if (friend.id == id) {
                        friend.disabled = true;
                        break;
                    }
                }
            }
            items.addAll(ContactsData.convertSectionData(friendList));
        }
        adapter.notifyDataSetChanged();
    }

    private List<Friend> dataSource;

    @Override
    public List<Friend> getDataSource() {
        return dataSource;
    }

    @Override
    public void onItemClicked(Friend item) {
        if (item.checked) {
            selectedFriendList.remove(item);
        } else {
            selectedFriendList.add(item);
        }

        TextView actionTextView = titleBar.getActionTextView();
        actionTextView.setEnabled(!selectedFriendList.getItems().isEmpty());
        int checkedCount = selectedFriendList.getItems().size();
        if (checkedCount > 0) {
            actionTextView.setText(getString(R.string.group_finish_format, checkedCount));
        } else {
            actionTextView.setText(R.string.group_finish);
        }
        item.checked = !item.checked;
        adapter.notifyDataSetChanged();
    }

    @Override
    public SelectedFriendListViewBinder.OnClickAvatarListener getOnClickAvatarListener() {
        return this::onItemClicked;
    }
}
