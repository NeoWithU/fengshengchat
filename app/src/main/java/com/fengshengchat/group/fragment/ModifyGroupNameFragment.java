package com.fengshengchat.group.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.widget.TitleBarLayout;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.group.Group;

/**
 * 修改群昵称
 */
public class ModifyGroupNameFragment extends ToolbarFragment {

    public static final String TAG = "ModifyNicknameFragment";
    private static final String EXTRA_GROUP_ID = "GROUP_ID";
    public static final String EXTRA_GROUP_NAME = "GROUP_NAME";

    private long groupId;
    private String groupName;

    @BindView(R.id.title_bar)
    TitleBarLayout titleBar;
    @BindView(R.id.nickname_et)
    EditText groupNameEt;

    public static ModifyGroupNameFragment newInstance(long groupId, String groupName) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_GROUP_ID, groupId);
        args.putString(EXTRA_GROUP_NAME, groupName);
        ModifyGroupNameFragment fragment = new ModifyGroupNameFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.modify_group_name_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        groupId = args.getLong(EXTRA_GROUP_ID);
        groupName = args.getString(EXTRA_GROUP_NAME);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitleBar();
        initContentEditText();
    }

    private void initContentEditText() {
        groupNameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                titleBar.getActionTextView().setEnabled(s.length() > 0);
            }
        });
        groupNameEt.setText(groupName);
    }

    private void initTitleBar() {
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                finish();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
                String groupName = groupNameEt.getText().toString().trim();
                if (groupName.length() == 0) {
                    toast("群名称不允许为空格");
                    return;
                }

                saveGroupName(groupName);
            }
        });
    }

    /**
     * 保存群名称
     */
    private void saveGroupName(String groupName) {
        Group.GroupInfo groupInfo = Group.GroupInfo
                .newBuilder()
                .setGroupId(groupId)
                .setGroupName(groupName)
                .build();

        GroupInfoFragment.getSetGroupInfoObserver(groupInfo, Group.SetGroupInfoReq.SetType.SET_TYPE_GROUP_NAME_VALUE)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(this::handleSetGroupInfoRes)
                .filter(data -> data.getResult() == 0)
                .observeOn(Schedulers.io())
                .doOnNext(data -> DbManager.getDb().getGroupDao().updateGroupName(groupId, groupName))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Group.SetGroupInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.SetGroupInfoRsp data) {
                        bindGroupName(groupName);
                    }
                });
    }

    private void bindGroupName(String groupName) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_GROUP_NAME, groupName);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        getFragmentManager().popBackStack();
    }

    private void handleSetGroupInfoRes(Group.SetGroupInfoRsp data) {
        if (data.getResult() != 0) {
            toast(data.getErrMsg());
        }
    }


}
