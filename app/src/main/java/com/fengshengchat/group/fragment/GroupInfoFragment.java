package com.fengshengchat.group.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.MainActivity;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.dialog.ConfirmDialogFragment;
import com.fengshengchat.account.fragment.AvatarEditorFragment;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.app.dialog.BottomDialogFragment;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.HttpRequestObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.db.AppDatabase;
import com.fengshengchat.db.ChatListBean;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.db.helper.Task;
import com.fengshengchat.db.helper.TaskHelper;
import com.fengshengchat.group.binder.GroupMemberViewBinder;
import com.fengshengchat.group.binder.MemberOpViewBinder;
import com.fengshengchat.group.decoration.GroupMemberDecoration;
import com.fengshengchat.group.model.GroupData;
import com.fengshengchat.group.model.GroupMemberData;
import com.fengshengchat.group.model.GroupMergeData;
import com.fengshengchat.group.model.MemberOp;
import com.fengshengchat.group.view.GroupOwnerLayout;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.EmptyHttpRequestDialogObserver;
import com.fengshengchat.net.EmptyObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.ItemLayout;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.drakeet.multitype.MultiTypeAdapter;
import yiproto.yichat.group.Group;

/**
 * 群设置
 */
public class GroupInfoFragment extends ToolbarFragment {

    public static final String TAG = "GroupInfoFragment";
    /**
     * 编辑群公告
     */
    public static final int REQ_CODE_EDIT_NOTICE = 1;
    /**
     * 邀请成员
     */
    public static final int REQ_CODE_INVEST_MEMBER = 2;
    /**
     * 移除成员
     */
    public static final int REQ_CODE_KICK_MEMBER = 3;
    /**
     * 选择群头像
     */
    public static final int REQ_CODE_PICK_PHOTO_TYPE = 4;
    /**
     * 请求码：获取头像地址
     */
    public static final int REQ_CODE_GET_AVATAR_URL = 5;
    /**
     * 请求码：设置管理员
     */
    public static final int REQ_CODE_SET_ADMIN = 6;
    /**
     * 请求码：设置群名称
     */
    public static final int REQ_CODE_SET_GROUP_NAME = 7;
    /**
     * 解散群
     */
    public static final int REQ_CODE_DISMISS_GROUP = 8;
    /**
     * 退出群
     */
    public static final int REQ_CODE_EXIT_GROUP = 9;
    /**
     * 邀请入群
     */
    public static final int REQ_CODE_INVEST_GROUP = 10;
    /**
     * 最多显示数量
     */
    public static final int MAX_SHOW_COUNT = 30;

    private long groupId;
    private MultiTypeAdapter adapter;
    private List<Object> items;
    private GroupData groupData;

    @BindView(R.id.title_bar)
    TitleBarLayout titleBarLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.name_layout)
    ItemLayout nameLayout;
    @BindView(R.id.avatar_layout)
    ItemLayout avatarLayout;
    @BindView(R.id.group_avatar_iv)
    ImageView groupAvatarIv;
    @BindView(R.id.num_layout)
    ItemLayout numLayout;
    @BindView(R.id.notice_layout)
    ItemLayout noticeLayout;
    @BindView(R.id.notice_content_tv)
    TextView noticeContentTv;
    @BindView(R.id.show_more_tv)
    TextView showMoreTv;
    @BindView(R.id.action_btn)
    Button actionBtn;
    @BindView(R.id.manage_view_stub)
    ViewStub manageViewStub;
    @BindView(R.id.pin_session_layout)
    ItemLayout pinSessionLayout;
    @BindView(R.id.msg_not_disturb_layout)
    ItemLayout notDisturbLayout;
    @BindView(R.id.group_avatar_arrow_iv)
    ImageView avatarArrowIv;
    @BindView(R.id.admin_view_stub)
    ViewStub adminViewStub;

    public static GroupInfoFragment newInstance(long groupId) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_DATA, groupId);
        GroupInfoFragment fragment = new GroupInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.group_info_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        groupId = args.getLong(EXTRA_DATA);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
        RxBus.get().register(this);
    }

    public static Observable<Group.SetGroupInfoRsp> getSetGroupInfoObserver(Group.GroupInfo groupInfo, int type) {
        Group.SetGroupInfoReq.Builder body = Group.SetGroupInfoReq
                .newBuilder()
                .addSetTypeList(type)
                .setGroupInfo(groupInfo);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.SET_GROUP_INFO_REQ_CMD_VALUE)
                .setBody(body);

        return ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.SetGroupInfoRsp.parseFrom(msg.getBody()));
    }

    /**
     * 初始化 Recycler View
     */
    private void initRecyclerView() {
        adapter = new MultiTypeAdapter();
        adapter.register(GroupMemberData.class, new GroupMemberViewBinder(item -> {
            BriefData briefData = new BriefData();
            briefData.id = item.userId;
            briefData.nickname = item.nickname;
            briefData.avatar = item.avatar;
            briefData.bigAvatar = item.bigAvatar;
            briefData.relative = BriefData.NOT_SURE_FRIEND;
            briefData.groupId = groupId;
            briefData.groupType = groupData.type;
            Intent intent = new Intent(getContext(), FriendBriefInfoActivity.class);
            intent.putExtra(FriendBriefInfoActivity.KEY_DATA, briefData);
            startActivity(intent);
        }));
        adapter.register(MemberOp.class, new MemberOpViewBinder(item -> {
            switch (item.opType) {
                case MemberOp.OP_ADD_MEMBER:
                    openAddGroupMember();
                    break;

                case MemberOp.OP_DEL_MEMBER:
                    openKickMember();
                    break;

                default:
                    break;
            }
        }));

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), GroupMemberDecoration.N));
        recyclerView.addItemDecoration(new GroupMemberDecoration());
        recyclerView.setAdapter(adapter);
    }

    /**
     * 打开增加成员页面
     */
    private void openAddGroupMember() {
        long[] memberIdList = new long[memberList.size()];
        for (int i = 0; i < memberList.size(); i++) {
            memberIdList[i] = memberList.get(i).userId;
        }

        InvestMemberFragment fragment = InvestMemberFragment.newInstance(groupId, memberIdList);
        fragment.setTargetFragment(this, REQ_CODE_INVEST_MEMBER);
        openFragment(fragment, InvestMemberFragment.TAG);
    }

    /**
     * 打开踢人页面
     */
    private void openKickMember() {
        ArrayList<Long> filterIds = new ArrayList<>();
        filterIds.add(groupData.ownerId);

        long adminId = groupData.getAdminId();
        if (adminId != 0) {
            //  自己不是群主
            if (UserManager.getUser().uid != groupData.ownerId) {
                filterIds.add(adminId);
            }
        }

        long[] ids = new long[filterIds.size()];
        int index = 0;
        for (Long id : filterIds) {
            ids[index++] = id;
        }

        KickMemberFragment fragment = KickMemberFragment.newInstance(groupId, ids);
        fragment.setTargetFragment(this, REQ_CODE_KICK_MEMBER);
        openFragment(fragment, KickMemberFragment.TAG);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestData();
        TaskHelper.getInstance().post(new Task(Task.TASK_UPDATE_GROUP_INFO, groupId));
    }

    /**
     * 请求数据
     */
    private void requestData() {
        Observable.combineLatest(GroupMemberData.getMemberListObservable(groupId), GroupData.getGroupObservable(groupId), GroupMergeData::new)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(mergeData -> sortMemberList(mergeData.groupMembers, mergeData.groupData.ownerId, mergeData.groupData.getAdminId()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .doOnNext(mergeData -> bindUI(mergeData.groupMembers, mergeData.groupData))
                .filter(mergeData -> mergeData.groupData.isAdmin())
                .doOnNext(mergeData -> bindAdminInfo(mergeData.groupData, mergeData.groupMembers))
                .subscribe(new EmptyObserver());
    }

    public static void sortMemberList(List<GroupMemberData> members, long ownerId, long adminId) {
        List<GroupMemberData> tempList = new ArrayList<>(members.size());
        GroupMemberData owner = null, admin = null;

        for (GroupMemberData member : members) {
            if (member.userId == ownerId) {
                owner = member;
            } else if (member.userId == adminId) {
                admin = member;
            } else {
                tempList.add(member);
            }
        }

        Collections.sort(tempList, (o1, o2) -> {
            long v = o1.enterGroupTime - o2.enterGroupTime;
            if (v > 1) {
                return 1;
            } else if (v < 1) {
                return -1;
            } else {
                return 0;
            }
        });

        members.clear();
        if (owner != null) {
            members.add(owner);
        }
        if (admin != null) {
            members.add(admin);
        }
        members.addAll(tempList);
    }

    private List<GroupMemberData> memberList;

    /**
     * 绑定 UI
     */
    private void bindUI(List<GroupMemberData> groupMembers, GroupData groupData) {
        this.memberList = groupMembers;
        this.groupData = groupData;
        boolean admin = groupData.isAdmin();

        bindGroupMember(groupMembers, groupData);

        //  群名称
        nameLayout.getMsgTv().setText(groupData.name);
        nameLayout.setEnabled(admin);

        //  群头像
        ImageUtils.loadRadiusImage(groupAvatarIv, groupData.avatar, R.drawable.group_ic_default, AppConfig.IMAGE_RADIUS);

        //  动作按钮
        if (groupData.isOwner()) {
            actionBtn.setText(R.string.group_dismiss_group);
        } else {
            actionBtn.setText(R.string.group_delete_and_exist);
        }

        avatarArrowIv.setVisibility(admin ? View.VISIBLE : View.GONE);
        nameLayout.setShowArrow(admin);
        avatarLayout.setShowArrow(admin);
        noticeLayout.setShowArrow(admin);

        //  群号
        numLayout.getMsgTv().setText(String.valueOf(groupId));

        //  群公告
        setGroupNotice(groupData.notice);

        //  置顶聊天
        SwitchCompat pinSessionLayoutSwitch = pinSessionLayout.getSwitchCompat();
        pinSessionLayoutSwitch.setChecked(groupData.isPinSession);
        pinSessionLayoutSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonView.isPressed()) {
                return;
            }
            requestSetPinSession(isChecked);
        });

        //  消息免打扰
        SwitchCompat disturbLayoutSwitch = notDisturbLayout.getSwitchCompat();
        disturbLayoutSwitch.setChecked(groupData.notDisturb);
        disturbLayoutSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonView.isPressed()) {
                return;
            }
            requestSetNotDisturb(isChecked);
        });
        disturbLayoutSwitch.setEnabled(groupData.isGeneralGroup());

        //  普通用户
        if (!groupData.isAdmin()) {
            long adminId = groupData.getAdminId();
            if (adminId != 0) {
                bindNonOwnerUi(groupMembers, adminId);
            }
        }
    }

    /**
     * 设置群公告
     */
    private void setGroupNotice(String notice) {
        noticeContentTv.setText(notice);
        if (TextUtils.isEmpty(notice)) {
            noticeLayout.getMsgTv().setVisibility(View.VISIBLE);
            noticeContentTv.setVisibility(View.GONE);
        } else {
            noticeLayout.getMsgTv().setVisibility(View.GONE);
            noticeContentTv.setVisibility(View.VISIBLE);
        }
    }

    private void bindGroupMember(List<GroupMemberData> groupMembers, GroupData groupData) {
        titleBarLayout.setTitleText(getString(R.string.group_chat_info_format, groupMembers.size()));

        //  刷新 RecyclerView
        int size = Math.min(groupMembers.size(), MAX_SHOW_COUNT);
        items = new ArrayList<>(size + 2);
        items.addAll(groupMembers.subList(0, size));
        items.add(MemberOp.ADD_OP);
        if (groupData.isAdmin()) {
            items.add(MemberOp.DEL_OP);
        }

        adapter.setItems(items);
        adapter.notifyDataSetChanged();

        //  人数多玩30，显示显示更多按钮
        showMoreTv.setVisibility(groupMembers.size() > MAX_SHOW_COUNT ? View.VISIBLE : View.GONE);
    }

    /**
     * 请求设置置顶聊天
     */
    private void requestSetPinSession(boolean isChecked) {
        int value = isChecked
                ? Group.MemberInfo.OnTopType.ON_TOP_VALUE
                : Group.MemberInfo.OnTopType.NON_ON_TOP_VALUE;

        Group.MemberInfo.Builder memberInfo = Group.MemberInfo.newBuilder()
                .setOnTop(value);

        Group.SetGroupMemberInfoReq.Builder body = Group.SetGroupMemberInfoReq
                .newBuilder()
                .setGroupId(groupId)
                .setMemberInfo(memberInfo);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.SET_GROUP_MEMBER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.SetGroupMemberInfoRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(GroupInfoFragment.this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Group.SetGroupMemberInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.SetGroupMemberInfoRsp data) {
                        if (data.getResult() != 0) {
                            toast(data.getErrMsg());
                            pinSessionLayout.getSwitchCompat().setChecked(!isChecked);
                            return;
                        }
                        savePinSession(isChecked);
                    }

                    @Override
                    protected void onError(String msg) {
                        super.onError(msg);
                        pinSessionLayout.getSwitchCompat().setChecked(!isChecked);
                    }
                });
    }

    /**
     * 只在置顶聊天于数据库中
     */
    private void savePinSession(boolean isPinSession) {
        Observable.fromCallable(() -> DbManager.getDb().getGroupDao().updatePinSession(groupId, isPinSession))
                .compose(RxSchedulersHelper.applyIOSchedulers())
                .subscribe(new BaseObserver<Integer>() {
                    @Override
                    public void onNext(Integer integer) {
                        LogUtils.e("id: " + integer);
                    }
                });
    }

    /**
     * 请求设置消息免打扰
     */
    private void requestSetNotDisturb(boolean isChecked) {
        int value = isChecked
                ? Group.MemberInfo.MuteNotificationType.NOTIFICATION_MUTE_VALUE
                : Group.MemberInfo.MuteNotificationType.NOTIFICATION_VALUE;

        Group.MemberInfo.Builder memberInfo = Group.MemberInfo.newBuilder()
                .setMuteNotification(value);

        Group.SetGroupMemberInfoReq.Builder body = Group.SetGroupMemberInfoReq
                .newBuilder()
                .setGroupId(groupId)
                .setMemberInfo(memberInfo);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.SET_GROUP_MEMBER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.SetGroupMemberInfoRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(GroupInfoFragment.this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Group.SetGroupMemberInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.SetGroupMemberInfoRsp data) {
                        if (data.getResult() != 0) {
                            toast(data.getErrMsg());
                            notDisturbLayout.getSwitchCompat().setChecked(!isChecked);
                            return;
                        }
                        saveMsgNotDisturbData(isChecked);
                    }

                    @Override
                    protected void onError(String msg) {
                        super.onError(msg);
                        notDisturbLayout.getSwitchCompat().setChecked(!isChecked);
                    }
                });
    }

    /**
     * 保存消息免打扰于数据库中
     */
    private void saveMsgNotDisturbData(boolean notDisturb) {
        Observable.fromCallable(() -> DbManager.getDb().getGroupDao().updateMsgNotDisturb(groupId, notDisturb))
                .compose(RxSchedulersHelper.applyIOSchedulers())
                .subscribe(new BaseObserver<Integer>() {
                    @Override
                    public void onNext(Integer integer) {
                        LogUtils.e("id: " + integer);
                    }
                });
    }

    @OnClick(R.id.clear_chat_data_layout)
    public void onClickClearCache() {
        Disposable subscribe = Observable.fromCallable((Callable<Object>) () -> {
            AppDatabase db = DbManager.getDb();
            db.beginTransaction();
            try {
                ChatListBean chatItem = db.getChatListDao().getChatItem(groupId);
                if (null != chatItem) {
                    chatItem.lastMessage = "";
                    db.getChatListDao().updateChatItem(chatItem);
                }
                db.getRecordDao().deleteAllChatRecord(groupId);
                db.setTransactionSuccessful();
                db.endTransaction();
                return "OK";
            } catch (Exception e) {
                e.printStackTrace();
                db.endTransaction();
            }
            return "Failed";
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                            if ("OK".equals(o)) {
                                RxBus.get().post(BusEvent.TAG_DELETED_CHAT_RECORD, groupId);
                                toast(R.string.clear_success);
                            } else {
                                toast(R.string.error);
                            }
                        },
                        throwable -> toast(R.string.error));
    }

    /**
     * 查看更多联系人
     */
    @OnClick(R.id.show_more_tv)
    public void seeMoreMember() {
        AllGroupMemberFragment fragment = AllGroupMemberFragment.newInstance(groupId,
                groupData.type, groupData.ownerId, groupData.getAdminId());
        openFragment(fragment, AllGroupMemberFragment.TAG);
    }

    /**
     * 打开公告
     */
    @OnClick(R.id.notice_layout)
    public void openNoticeEditor() {
        if (!groupData.isAdmin() && TextUtils.isEmpty(groupData.notice)) {
            return;
        }

        GroupNoticeEditorFragment fragment = GroupNoticeEditorFragment.newInstance(groupData);
        fragment.setTargetFragment(this, REQ_CODE_EDIT_NOTICE);
        openFragment(fragment, GroupNoticeEditorFragment.TAG);
    }

    /**
     * 设置进群邀请确认
     */
    private void requestSetInvestConfirm(boolean isChecked) {
        int inviteConfirm = isChecked
                ? Group.GroupInfo.InviteConfirmType.INVITE_CONFIRM_NEED_CONFIRM_VALUE
                : Group.GroupInfo.InviteConfirmType.INVITE_CONFIRM_NO_NEED_CONFIRM_VALUE;

        Group.GroupInfo groupInfo = Group.GroupInfo.newBuilder()
                .setGroupId(groupId)
                .setInviteConfirm(inviteConfirm)
                .build();

        getSetGroupInfoObserver(groupInfo, Group.SetGroupInfoReq.SetType.SET_TYPE_INVITE_CONFIRM_VALUE)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(this::handleSetGroupInfoRes)
                .filter(data -> data.getResult() == 0)
                .observeOn(Schedulers.io())
                .doOnNext(data -> DbManager.getDb().getGroupDao().updateNeedInvestConfirm(groupId, isChecked))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestObserver<Group.SetGroupInfoRsp>() {
                    @Override
                    public void onNext(Group.SetGroupInfoRsp data) {
                        ownerLayout.investVerifyLayout.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    }

                    @Override
                    protected void onError(String msg) {
                        super.onError(msg);
                        SwitchCompat switchCompat = ownerLayout.ownerInvestVerifyLayout.getSwitchCompat();
                        switchCompat.setChecked(!isChecked);
                    }
                });
    }

    private void handleSetGroupInfoRes(Group.SetGroupInfoRsp data) {
        if (data.getResult() != 0) {
            toast(data.getErrMsg());
        }
    }

    private GroupOwnerLayout ownerLayout;

    private void bindAdminInfo(GroupData groupData, List<GroupMemberData> groupMembers) {
        if (ownerLayout == null) {
            manageViewStub.inflate();
            ownerLayout = findViewById(R.id.group_owner_layout);
        }

        //  群主邀请确认
        SwitchCompat switchCompat = ownerLayout.ownerInvestVerifyLayout.getSwitchCompat();
        switchCompat.setChecked(groupData.needInvestConfirm);
        switchCompat.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonView.isPressed()) {
                return;
            }
            requestSetInvestConfirm(isChecked);
        });
        switchCompat.setEnabled(groupData.isOwner() && groupData.isGeneralGroup());

        //  邀请确认布局
        ownerLayout.investVerifyLayout.setVisibility(groupData.needInvestConfirm ? View.VISIBLE : View.GONE);
//        ownerLayout.investVerifyLayout.setEnabled(groupData.isGeneralGroup());

        //  从群成员中选择一个管理员
        ownerLayout.adminLayout.setEnabled(groupData.isOwner());
        ownerLayout.adminLayout.setOnClickListener(v -> {
            SetGroupAdminFragment fragment = SetGroupAdminFragment.newInstance(groupId, groupData.ownerId, groupData.getAdminId());
            fragment.setTargetFragment(GroupInfoFragment.this, REQ_CODE_SET_ADMIN);
            openFragment(fragment, SetGroupAdminFragment.TAG);
        });
        if (!groupData.isOwner()) {
            ownerLayout.adminNameTv.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        }

        //  显示管理员
        long adminId = groupData.getAdminId();
        if (adminId != 0) {
            for (GroupMemberData member : groupMembers) {
                if (member.userId == adminId) {
                    ownerLayout.adminNameTv.setText(member.nickname);
                    ImageUtils.loadImage(ownerLayout.adminIv, member.avatar, R.drawable.image_default);
                    break;
                }
            }
        }

        //  置顶群公告
        SwitchCompat noticeSwitchCompat = ownerLayout.pinNoticeLayout.getSwitchCompat();
        noticeSwitchCompat.setChecked(groupData.isPinNotice);
        noticeSwitchCompat.setOnCheckedChangeListener((buttonView, isChecked) -> requestSetPinNotice(isChecked));
        noticeSwitchCompat.setEnabled(!TextUtils.isEmpty(groupData.notice));

        //  确认邀请数量
        TextView unhandledInviteCountTv = ownerLayout.verifyInvestLayout.getMsgTv();
        if (groupData.unhandledInviteCount != 0) {
            unhandledInviteCountTv.setText(String.valueOf(groupData.unhandledInviteCount));
        } else {
            unhandledInviteCountTv.setText(null);
        }
        ownerLayout.verifyInvestLayout.setOnClickListener(v -> {
            InvestListFragment fragment = InvestListFragment.newInstance(groupId);
            fragment.setTargetFragment(GroupInfoFragment.this, REQ_CODE_INVEST_GROUP);
            openFragment(fragment, InvestListFragment.TAG);
        });
    }

    private void bindNonOwnerUi(List<GroupMemberData> groupMembers, long adminId) {
        for (GroupMemberData member : groupMembers) {
            if (member.userId == adminId) {
                View adminLayout = adminViewStub.inflate();
                ImageView adminIv = adminLayout.findViewById(R.id.admin_iv);
                TextView adminNameTv = adminLayout.findViewById(R.id.admin_name_tv);
                adminNameTv.setText(member.nickname);
                adminNameTv.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
                ImageUtils.loadImage(adminIv, member.avatar, R.drawable.image_default);
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQ_CODE_EDIT_NOTICE:  //  编辑群公告
                bindGroupNotice(data);
                break;

            case REQ_CODE_INVEST_MEMBER:    //  邀请
                updateInvestMember(data);
                break;

            case REQ_CODE_KICK_MEMBER:  //  踢人
                updateKickMember(data);
                break;

            case REQ_CODE_PICK_PHOTO_TYPE:  //  挑选头像
                handlePickPhotoType(resultCode, data);
                break;

            case REQ_CODE_GET_AVATAR_URL:   //  处理已经上传的头像
                handleAvatarUrl(resultCode, data);
                break;

            case REQ_CODE_SET_ADMIN:    //  设置管理员
                Friend admin = data.getParcelableExtra(EXTRA_DATA);
                if (admin == null) {
                    groupData.adminIds = null;
                    ownerLayout.adminNameTv.setText(null);
                    ImageUtils.loadImage(ownerLayout.adminIv, R.drawable.common_ic_empty);
                } else {
                    groupData.adminIds = String.valueOf(admin.id);
                    ownerLayout.adminNameTv.setText(admin.nickname);
                    ImageUtils.loadImage(ownerLayout.adminIv, admin.avatar, R.drawable.image_default_with_round);
                }
                break;

            case REQ_CODE_SET_GROUP_NAME:   //  群名称
                String groupName = data.getStringExtra(ModifyGroupNameFragment.EXTRA_GROUP_NAME);
                nameLayout.getMsgTv().setText(groupName);
                break;

            case REQ_CODE_DISMISS_GROUP:    //  确认解散群组
                requestDismissGroup();
                break;

            case REQ_CODE_EXIT_GROUP:   //  退出群
                requestExitGroup();
                break;

            case REQ_CODE_INVEST_GROUP: //  邀请入群数量
                TaskHelper.getInstance().post(new Task(Task.TASK_UPDATE_GROUP_INFO, groupId));
                break;

            default:
                break;
        }
    }

    /**
     * 绑定群公告
     */
    private void bindGroupNotice(Intent data) {
        groupData.notice = data.getStringExtra(GroupNoticeEditorFragment.EXTRA_CONTENT);
        groupData.noticeUpdatedTime = data.getLongExtra(GroupNoticeEditorFragment.EXTRA_TIME, 0);
        setGroupNotice(groupData.notice);

        if (!groupData.isAdmin()) {
            return;
        }

        SwitchCompat switchCompat = ownerLayout.pinNoticeLayout.getSwitchCompat();
        switchCompat.setEnabled(!TextUtils.isEmpty(groupData.notice));
    }

    /**
     * 更新邀请群成员
     */
    private void updateInvestMember(Intent data) {
        //  非管理员，且需要确认邀请，不更新UI
        if (groupData.needInvestConfirm && !groupData.isAdmin()) {
            toast("邀请成功");
            return;
        }

        TaskHelper.getInstance().post(Task.TASK_GROUP_MEMBER_LIST);

        ArrayList<GroupMemberData> newGroupMemberList = data.getParcelableArrayListExtra(InvestMemberFragment.KEY_INVEST_MEMBER);
        boolean needAddKickButton = groupData.isAdmin() && memberList.size() < 3;
        items.addAll(memberList.size() - 1, newGroupMemberList);
        memberList.addAll(newGroupMemberList);
        if (needAddKickButton) {
            items.add(MemberOp.DEL_OP);
        }
        adapter.notifyDataSetChanged();

        //  人数多玩30，显示显示更多按钮
        showMoreTv.setVisibility(memberList.size() > MAX_SHOW_COUNT ? View.VISIBLE : View.GONE);
        titleBarLayout.setTitleText(getString(R.string.group_chat_info_format, memberList.size()));
    }

    /**
     * 更新踢人后的成员
     */
    private void updateKickMember(Intent data) {
        //  移除成员
        long[] ids = data.getLongArrayExtra(KickMemberFragment.KEY_IDS);
        for (Long id : ids) {
            for (int i = 0; i < items.size(); i++) {
                Object o = items.get(i);
                if (!(o instanceof GroupMemberData)) {
                    continue;
                }

                if (((GroupMemberData) o).userId == id) {
                    items.remove(i);
                    memberList.remove(o);
                    break;
                }
            }
        }

        //  移除【删除按钮】
        if (memberList.size() < 3) {
            items.remove(items.size() - 1);
        }
        adapter.notifyDataSetChanged();

        //  人数多玩30，显示显示更多按钮
        showMoreTv.setVisibility(memberList.size() > MAX_SHOW_COUNT ? View.VISIBLE : View.GONE);
        titleBarLayout.setTitleText(getString(R.string.group_chat_info_format, memberList.size()));
    }

    @OnClick(R.id.action_btn)
    public void doAction() {
        if (groupData.isOwner()) {
            String title = getString(R.string.group_dismiss_group_confirm);
            String action = getString(R.string.group_dismiss_group);
            ConfirmDialogFragment fragment = ConfirmDialogFragment.newInstance(title, action);
            fragment.setTargetFragment(this, REQ_CODE_DISMISS_GROUP);
            fragment.show(getFragmentManager(), ConfirmDialogFragment.TAG);
        } else {
            String title = getString(R.string.group_exit_group_confirm);
            String action = getString(R.string.group_delete_and_exist);
            ConfirmDialogFragment fragment = ConfirmDialogFragment.newInstance(title, action);
            fragment.setTargetFragment(this, REQ_CODE_EXIT_GROUP);
            fragment.show(getFragmentManager(), ConfirmDialogFragment.TAG);
        }
    }

    /**
     * 请求解散该群
     */
    private void requestDismissGroup() {
        Group.DelGroupReq.Builder body = Group.DelGroupReq
                .newBuilder()
                .setGroupId(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.DEL_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> yiproto.yichat.friend.Friend.DelFriendRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(data -> {
                    if (data.getResult() != 0) {
                        toast(data.getErrMsg());
                    }
                })
                .filter(data -> data.getResult() == 0)
                .observeOn(Schedulers.io())
                .doOnNext(data -> deleteGroupAndMember(groupId))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<yiproto.yichat.friend.Friend.DelFriendRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(yiproto.yichat.friend.Friend.DelFriendRsp data) {
                        handleDelGroupUi();
                    }
                });
    }

    public static void deleteGroupAndMember(long groupId) {
        AppDatabase db = DbManager.getDb();
        db.beginTransaction();
        try {
            db.getChatListDao().deleteChatItem(groupId);
            db.getRecordDao().deleteAllChatRecord(groupId);
            db.getGroupDao().delete(new com.fengshengchat.db.Group(groupId));
            db.getGroupMemberDao().deleteGroupMember(groupId);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    /**
     * 请求退出该群
     */
    private void requestExitGroup() {
        Group.ExitGroupReq.Builder body = Group.ExitGroupReq
                .newBuilder()
                .setGroupId(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.EXIT_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .map(msg -> Group.ExitGroupRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(data -> {
                    if (data.getResult() != 0) {
                        toast(data.getErrMsg());
                    }
                })
                .filter(data -> data.getResult() == 0)
                .observeOn(Schedulers.io())
                .doOnNext(data -> deleteGroupAndMember(groupId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new HttpRequestDialogObserver<Group.ExitGroupRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.ExitGroupRsp data) {
                        handleDelGroupUi();
                    }
                });
    }

    /**
     * 处理退群结果
     */
    private void handleDelGroupUi() {
        RxBus.get().post(BusEvent.TAG_DELETED_GROUP, groupId);
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    /**
     * 显示群二维码
     */
    @OnClick(R.id.qrcode_layout)
    public void showGroupQrcode() {
        GroupQrcodeFragment fragment = GroupQrcodeFragment.newInstance(groupData.id, groupData.name, groupData.avatar);
        openFragment(fragment, GroupQrcodeFragment.TAG);
    }

    /**
     * 修改群头像
     */
    @OnClick(R.id.avatar_layout)
    public void modifyAvatar() {
        if (!groupData.isAdmin()) {
            return;
        }
        Resources res = getResources();
        String[] array = res.getStringArray(R.array.user_picker_type_list);
        BottomDialogFragment fragment = BottomDialogFragment.newInstance(array);
        fragment.setTargetFragment(this, REQ_CODE_PICK_PHOTO_TYPE);
        fragment.show(getFragmentManager(), BottomDialogFragment.TAG);
    }

    /**
     * 处理选择照片方式
     */
    private void handlePickPhotoType(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        int position = data.getIntExtra(BottomDialogFragment.EXTRA_POSITION, -1);
        if (position == -1) {
            return;
        }

        int type = position + 1;
        AvatarEditorFragment fragment = AvatarEditorFragment.newInstance(type);
        fragment.setTargetFragment(this, REQ_CODE_GET_AVATAR_URL);
        openFragment(fragment, AvatarEditorFragment.TAG);
    }

    /**
     * 处理获取头像地址
     */
    private void handleAvatarUrl(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        String avatarUrl = data.getStringExtra(AvatarEditorFragment.EXTRA_SMALL_AVATAR);
        groupData.avatar = avatarUrl;
        ImageUtils.loadRadiusImage(groupAvatarIv, avatarUrl, R.drawable.group_ic_default, AppConfig.IMAGE_RADIUS);
        requestSetGroupAvatar(avatarUrl);
    }

    /**
     * 设置群头像
     */
    private void requestSetGroupAvatar(String avatarUrl) {
        Group.GroupInfo groupInfo = Group.GroupInfo
                .newBuilder()
                .setGroupId(groupId)
                .setGroupLogo(avatarUrl)
                .build();

        getSetGroupInfoObserver(groupInfo, Group.SetGroupInfoReq.SetType.SET_TYPE_GROUP_LOGO_VALUE)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(this::handleSetGroupInfoRes)
                .filter(data -> data.getResult() == 0)
                .observeOn(Schedulers.io())
                .doOnNext(data -> DbManager.getDb().getGroupDao().updateGroupAvatar(groupId, avatarUrl))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new EmptyHttpRequestDialogObserver(getFragmentManager()));
    }

    /**
     * 设置置顶群公告
     */
    private void requestSetPinNotice(boolean isChecked) {
        int topNotice = isChecked
                ? Group.GroupInfo.NoticeType.NOTICE_ON_TOP_VALUE
                : Group.GroupInfo.NoticeType.NOTICE_NON_ON_TOP_VALUE;

        Group.GroupInfo groupInfo = Group.GroupInfo.newBuilder()
                .setGroupId(groupId)
                .setOnTopNotice(topNotice)
                .build();

        getSetGroupInfoObserver(groupInfo, Group.SetGroupInfoReq.SetType.SET_TYPE_ON_TOP_NOTICE_VALUE)
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(this::handleSetGroupInfoRes)
                .filter(data -> data.getResult() == 0)
                .observeOn(Schedulers.io())
                .doOnNext(data -> DbManager.getDb().getGroupDao().updatePinNotice(groupId, isChecked))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new EmptyHttpRequestDialogObserver(getFragmentManager()));
    }

    @Override
    public void onDestroyView() {
        RxBus.get().unregister(this);
        super.onDestroyView();
    }

    @SuppressWarnings("unused")
    @Keep
    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_GROUP_INFO)})
    public void reload(Long groupId) {
        requestData();
    }

    @SuppressWarnings("unused")
    @Keep
    @Subscribe(tags = {@Tag(BusEvent.TAG_GROUP_MEMBER_LIST)})
    public void reloadGroupMember(Long groupId) {
        GroupMemberData.getMemberListObservable(groupId)
                .doOnNext(groupMemberData -> sortMemberList(groupMemberData, groupData.ownerId, groupData.getAdminId()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<List<GroupMemberData>>() {
                    @Override
                    public void onNext(List<GroupMemberData> groupMemberData) {
                        bindGroupMember(groupMemberData, groupData);
                    }
                });
    }

    @OnClick(R.id.name_layout)
    public void modifyGroupName() {
        String groupName = nameLayout.getMsgTv().getText().toString();
        ModifyGroupNameFragment fragment = ModifyGroupNameFragment.newInstance(groupId, groupName);
        fragment.setTargetFragment(this, REQ_CODE_SET_GROUP_NAME);
        openFragment(fragment, ModifyGroupNameFragment.TAG);
    }

    @SuppressWarnings("unused")
    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_GROUP)})
    public void updatedDeletedGroup(Long groupId) {
        if (groupId == this.groupId) {
            try {
                getActivity().finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
