package com.fengshengchat.group.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.group.binder.GroupMemberViewBinder;
import com.fengshengchat.group.decoration.GroupMemberDecoration;
import com.fengshengchat.group.model.GroupMemberData;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import me.drakeet.multitype.MultiTypeAdapter;

/**
 * 所有群成员
 */
public class AllGroupMemberFragment extends ToolbarFragment {

    public static final String TAG = "AllGroupMemberFragment";
    private static final String EXTRA_GROUP_ID = "GROUP_ID";
    private static final String EXTRA_GROUP_TYPE = "GROUP_ID_TYPE";
    private static final String EXTRA_OWNER_ID = "OWNER_ID";
    private static final String EXTRA_ADMIN_ID = "ADMIN_ID";

    private MultiTypeAdapter adapter;
    private List<Object> items;
    private long groupId;
    private int groupType;
    private long ownerId;
    private long adminId;

    @BindView(R.id.title_bar)
    TitleBarLayout titleBarLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    public static AllGroupMemberFragment newInstance(long groupId, int groupType, long ownerId, long adminId) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_GROUP_ID, groupId);
        args.putInt(EXTRA_GROUP_TYPE, groupType);
        args.putLong(EXTRA_OWNER_ID, ownerId);
        args.putLong(EXTRA_ADMIN_ID, adminId);
        AllGroupMemberFragment fragment = new AllGroupMemberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.all_member_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        groupId = args.getLong(EXTRA_GROUP_ID);
        groupType = args.getInt(EXTRA_GROUP_TYPE);
        ownerId = args.getLong(EXTRA_OWNER_ID);
        adminId = args.getLong(EXTRA_ADMIN_ID);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    /**
     * 初始化 Recycler View
     */
    private void initRecyclerView() {
        adapter = new MultiTypeAdapter();
        adapter.register(GroupMemberData.class, new GroupMemberViewBinder(item -> {
            BriefData briefData = new BriefData();
            briefData.id = item.userId;
            briefData.nickname = item.nickname;
            briefData.avatar = item.avatar;
            briefData.bigAvatar = item.bigAvatar;
            briefData.relative = BriefData.NOT_SURE_FRIEND;
            briefData.groupId = groupId;
            briefData.groupType = groupType;
            Intent intent = new Intent(getContext(), FriendBriefInfoActivity.class);
            intent.putExtra(FriendBriefInfoActivity.KEY_DATA, briefData);
            startActivity(intent);
        }));

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), GroupMemberDecoration.N));
        recyclerView.addItemDecoration(new GroupMemberDecoration());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestData();
    }

    private void requestData() {
        GroupMemberData.getMemberListObservable(groupId)
                .doOnNext(memberList -> GroupInfoFragment.sortMemberList(memberList, ownerId, adminId))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<List<GroupMemberData>>() {
                    @Override
                    public void onNext(List<GroupMemberData> groupMemberData) {
                        bindUI(groupMemberData);
                    }
                });
    }

    /**
     * 绑定 UI
     */
    private void bindUI(List<GroupMemberData> groupMembers) {
        titleBarLayout.setTitleText(getString(R.string.group_member_format, groupMembers.size()));

        items = new ArrayList<>(groupMembers);
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }
}
