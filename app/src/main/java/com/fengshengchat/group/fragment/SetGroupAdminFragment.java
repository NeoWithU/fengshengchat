package com.fengshengchat.group.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.contacts.model.ContactsData;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.GroupMember;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.group.Group;

/**
 * 设置群管理员
 */
public class SetGroupAdminFragment extends BaseContactFragment {

    public static final String TAG = "SetGroupAdminFragment";
    private static final String EXTRA_GROUP_ID = "GROUP_ID";
    private static final String EXTRA_ADMIN_ID = "ADMIN_ID";
    private static final String EXTRA_OWNER_ID = "OWNER_ID";

    private long groupId;
    private long adminId;
    private long ownerId;
    private List<Friend> friendList;
    private Friend selectedFriend;

    public static SetGroupAdminFragment newInstance(long groupId, long ownerId, long adminId) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_GROUP_ID, groupId);
        args.putLong(EXTRA_ADMIN_ID, adminId);
        args.putLong(EXTRA_OWNER_ID, ownerId);
        SetGroupAdminFragment fragment = new SetGroupAdminFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null) {
            return;
        }

        groupId = args.getLong(EXTRA_GROUP_ID);
        ownerId = args.getLong(EXTRA_OWNER_ID);
        adminId = args.getLong(EXTRA_ADMIN_ID);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitleBar();
        requestData();
    }

    private void initTitleBar() {
        titleBar.setTitleText(R.string.group_set_admin);
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                finish();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
                reqSetGroupAdmin();
            }
        });
    }

    @Override
    protected void bind(List<Friend> friendList) {
        items.clear();
        items.addAll(ContactsData.convertSectionData(friendList));
        adapter.notifyDataSetChanged();
    }

    @Override
    protected List<Friend> getDataSource() {
        return friendList;
    }

    @Override
    public void onItemClicked(Friend item) {
        if (item == selectedFriend) {
            selectedFriend = null;
        } else if (selectedFriend != null) {
            selectedFriend.checked = false;
            selectedFriend = item;
        } else {
            selectedFriend = item;
        }

        item.checked = !item.checked;
        adapter.notifyDataSetChanged();
//        titleBar.getActionTextView().setEnabled(selectedFriend != null);
    }

    private void requestData() {
        Observable.fromCallable(() -> DbManager.getDb().getGroupMemberDao().findAll(groupId))
                .map(this::getFriendList)
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<List<Friend>>() {
                    @Override
                    public void onNext(List<Friend> friendList) {
                        bindFriendList(friendList);
                    }
                });
    }

    @NonNull
    protected List<Friend> getFriendList(List<GroupMember> groupMemberList) {
        ArrayList<Friend> friendArrayList = new ArrayList<>(groupMemberList.size());
        for (GroupMember info : groupMemberList) {
            if (info.userId == ownerId) {
                continue;
            }

            Friend friend = new Friend();
            friend.id = info.userId;
            friend.nickname = info.nickname;
            friend.avatar = info.avatar;
            boolean checked = info.userId == adminId;
            if (checked) {
                selectedFriend = friend;
                friend.checked = true;
            }
            friend.updateNicknamePinyin();
            friendArrayList.add(friend);
        }
        return friendArrayList;
    }

    private void bindFriendList(List<Friend> friendList) {
        this.friendList = friendList;
        items.addAll(ContactsData.convertSectionData(friendList));
        adapter.notifyDataSetChanged();
    }

    private void reqSetGroupAdmin() {
        if (selectedFriend == null && adminId == 0) {
            return;
        }

        if (selectedFriend != null && selectedFriend.id == adminId) {
            getFragmentManager().popBackStack();
            return;
        }

        Group.SetGroupAdminReq.Builder body = Group.SetGroupAdminReq
                .newBuilder()
                .setGroupId(groupId);
        if (adminId != 0) {
            body.addDelAdminList(adminId);
        }

        if (selectedFriend != null) {
            body.addAddAdminList(selectedFriend.id);
        }

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.SET_GROUP_ADMIN_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.SetGroupAdminRsp.parseFrom(msg.getBody()))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .doOnNext(this::bindSetAdminUi)
                .filter(data -> data.getResult() == Group.SetGroupAdminRsp.SetAdminResult.OK_VALUE)
                .observeOn(Schedulers.io())
                .doOnNext(data -> {
                    String adminIds = selectedFriend == null ? "" : String.valueOf(selectedFriend.id);
                    DbManager.getDb().getGroupDao().updateAdminIds(groupId, adminIds);
                })   //  保存管理员 id
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(new HttpRequestDialogObserver<Group.SetGroupAdminRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.SetGroupAdminRsp data) {
                        Intent intent = new Intent();
                        intent.putExtra(EXTRA_DATA, selectedFriend);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                        getFragmentManager().popBackStack();
                    }
                });
    }

    private void bindSetAdminUi(Group.SetGroupAdminRsp data) {
        if (data.getResult() != Group.SetGroupAdminRsp.SetAdminResult.OK_VALUE) {
            toast(data.getErrMsg());
        }
    }

}
