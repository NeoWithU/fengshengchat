package com.fengshengchat.group.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.group.binder.InvestViewBinder;
import com.fengshengchat.group.model.InvestInfo;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import me.drakeet.multitype.MultiTypeAdapter;
import yiproto.yichat.group.Group;

/**
 * 邀请列表
 */
public class InvestListFragment extends ToolbarFragment {

    public static final String TAG = "InvestListFragment";

    private long groupId;
    private List<Object> items;
    private MultiTypeAdapter adapter;
    private boolean isChanged;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    public static InvestListFragment newInstance(long groupId) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_DATA, groupId);
        InvestListFragment fragment = new InvestListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.group_member_invest_list_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        groupId = args.getLong(EXTRA_DATA);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    private void initRecyclerView() {
        items = new ArrayList<>();
        adapter = new MultiTypeAdapter(items);
        adapter.register(InvestInfo.class, new InvestViewBinder((action, item) -> {
            if (action == InvestViewBinder.ACTION_CONFIRM) {
                requestAgreeJointGroupReq(item);
            } else if (action == InvestViewBinder.ACTION_REJECT) {
                requestRejectJointGroupReq(item);
            }
        }));

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    /**
     * 请求同意加入
     */
    private void requestAgreeJointGroupReq(InvestInfo info) {
        Group.AgreeJoinGroupReq.Builder body = Group.AgreeJoinGroupReq
                .newBuilder()
                .addUserIdList(info.id)
                .setGroupId(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.AGREE_JOIN_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.AgreeJoinGroupRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Group.AgreeJoinGroupRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.AgreeJoinGroupRsp data) {
                        handleAgreeJointReq(data, info);
                    }
                });
    }

    /**
     * 处理同意加入请求
     */
    private void handleAgreeJointReq(Group.AgreeJoinGroupRsp data, InvestInfo info) {
        int result = data.getResult();
        boolean ok = result == Group.AgreeJoinGroupRsp.AgreeJoinGroupResult.OK_VALUE;
        if (ok) {
            info.state = Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_AGREE_VALUE;
            adapter.notifyDataSetChanged();
            isChanged = true;
            return;
        }

        Integer state = data.getRequestStateMapMap().get(info.id);
        if (state != null) {
            if (state == Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_AGREE_VALUE) {
                info.state = Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_AGREE_VALUE;
                adapter.notifyDataSetChanged();
                toast("请求已被同意");
                isChanged = true;
                return;
            } else if (state == Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_REFUSE_VALUE) {
                info.state = Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_REFUSE_VALUE;
                adapter.notifyDataSetChanged();
                toast("请求已被拒绝");
                isChanged = true;
                return;
            }
        }

        toast(data.getErrMsg());
    }

    /**
     * 请求拒绝加入
     */
    private void requestRejectJointGroupReq(InvestInfo info) {
        Group.RefuseJoinGroupReq.Builder body = Group.RefuseJoinGroupReq
                .newBuilder()
                .addUserIdList(info.id)
                .setGroupId(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.REFUSE_JOIN_GROUP_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.RefuseJoinGroupRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Group.RefuseJoinGroupRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.RefuseJoinGroupRsp data) {
                        handleRejectJointReq(data, info);
                    }
                });
    }

    /**
     * 处理拒绝加入请求
     */
    private void handleRejectJointReq(Group.RefuseJoinGroupRsp data, InvestInfo info) {
        int result = data.getResult();
        boolean success = result == Group.RefuseJoinGroupRsp.RefuseJoinGroupResult.OK_VALUE;
        if (success) {
            info.state = Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_REFUSE_VALUE;
            adapter.notifyDataSetChanged();
            isChanged = true;
            return;
        }

        Integer state = data.getRequestStateMapMap().get(info.id);
        if (state != null) {
            if (state == Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_AGREE_VALUE) {
                info.state = Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_AGREE_VALUE;
                adapter.notifyDataSetChanged();
                toast("请求已被同意");
                isChanged = true;
                return;
            } else if (state == Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_REFUSE_VALUE) {
                info.state = Group.GetJoinGroupRequestRsp.RequestUserInfo.State.STATE_REFUSE_VALUE;
                adapter.notifyDataSetChanged();
                toast("请求已被拒绝");
                isChanged = true;
                return;
            }
        }

        toast(data.getErrMsg());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestInvestListReq();
    }

    /**
     * 获取邀请列表
     */
    private void requestInvestListReq() {
        Group.GetJoinGroupRequestReq.Builder body = Group.GetJoinGroupRequestReq
                .newBuilder()
                .setGroupId(groupId)
                .setStart(0)
                .setNum(100);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.GET_JOIN_GROUP_REQUEST_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.GetJoinGroupRequestRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Group.GetJoinGroupRequestRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Group.GetJoinGroupRequestRsp data) {
                        bindInvestReqList(data);
                    }
                });
    }

    /**
     * 绑定邀请请求列表
     */
    private void bindInvestReqList(Group.GetJoinGroupRequestRsp data) {
        if (data.getResult() != Group.GetJoinGroupRequestRsp.Result.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        for (Group.GetJoinGroupRequestRsp.RequestUserInfo item : data.getRequestUserListList()) {
            InvestInfo info = new InvestInfo();
            info.id = item.getUserId();
            info.nickname = item.getNickname();
            info.inviterId = item.getInviterUserId();
            info.inviterNickname = item.getInviterNickname();
            info.updateTime = item.getUpdateTime();
            info.state = item.getState();
            items.add(info);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        if (isChanged) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
        }
        super.onDestroyView();
    }
}
