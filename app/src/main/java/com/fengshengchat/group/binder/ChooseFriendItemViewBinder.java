package com.fengshengchat.group.binder;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.xeoh.android.texthighlighter.TextHighlighter;
import com.fengshengchat.R;
import com.fengshengchat.base.BKViewHolder;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.Friend;

import butterknife.BindView;

/**
 * 选择好友 ViewBinder
 */
public class ChooseFriendItemViewBinder extends BaseItemViewBinder<Friend, ChooseFriendItemViewBinder.ViewHolder> implements View.OnClickListener {

    @Nullable
    private String keyWord;

    public ChooseFriendItemViewBinder(@NonNull OnClickListener<Friend> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.group_friend_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull Friend item) {
        holder.itemView.setEnabled(!item.disabled);
        holder.itemView.setOnClickListener(this);
        holder.itemView.setTag(item);

      //  ImageUtils.loadImage(holder.avatarIv, item.avatar, R.drawable.image_default);
        ImageUtils.loadImage(holder.avatarIv, item.avatar, R.mipmap.default_head);
        holder.nicknameTv.setText(item.nickname);
        holder.textHighlighter.highlight(keyWord, TextHighlighter.BASE_MATCHER);

        holder.checkBox.setChecked(item.checked);
        holder.checkBox.setOnClickListener(this);
        holder.checkBox.setTag(item);
        holder.checkBox.setEnabled(!item.disabled);
    }

    @Override
    public void onClick(View v) {
        Friend friend = (Friend) v.getTag();
        onClickListener.onItemClicked(friend);
    }

    public void setKeyWord(@Nullable String keyWord) {
        this.keyWord = keyWord;
    }

    public static class ViewHolder extends BKViewHolder {

        private final TextHighlighter textHighlighter;
        @BindView(R.id.check_box)
        CheckBox checkBox;
        @BindView(R.id.avatar_iv)
        ImageView avatarIv;
        @BindView(R.id.nickname_tv)
        TextView nicknameTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textHighlighter = new TextHighlighter().setForegroundColor(0xFF7B4FF7).addTarget(nicknameTv);
        }
    }

}
