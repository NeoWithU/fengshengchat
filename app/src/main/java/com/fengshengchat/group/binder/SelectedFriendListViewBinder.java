package com.fengshengchat.group.binder;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.group.decoration.SelectedFriendDecoration;
import com.fengshengchat.group.model.SelectedFriendList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.drakeet.multitype.MultiTypeAdapter;

/**
 * 已经选择的联系人
 */
public class SelectedFriendListViewBinder extends BaseItemViewBinder<SelectedFriendList, SelectedFriendListViewBinder.ViewHolder> {

    private MultiTypeAdapter adapter;

    public interface OnClickAvatarListener {
        void onClick(Friend friend);
    }

    private final OnClickAvatarListener onClickAvatarListener;

    public SelectedFriendListViewBinder(OnClickAvatarListener onClickAvatarListener) {
        this.onClickAvatarListener = onClickAvatarListener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.group_selected_friend_list, parent, false);
        ViewHolder holder = new ViewHolder(view);

        adapter = new MultiTypeAdapter();
        adapter.register(Friend.class, new SelectedFriendViewBinder(onClickAvatarListener::onClick));

        holder.recyclerView.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));
        holder.recyclerView.setAdapter(adapter);
        holder.recyclerView.addItemDecoration(new SelectedFriendDecoration());
        return holder;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull SelectedFriendList item) {
        adapter.setItems(item.getItems());
        adapter.notifyDataSetChanged();
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recycler_view)
        RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
