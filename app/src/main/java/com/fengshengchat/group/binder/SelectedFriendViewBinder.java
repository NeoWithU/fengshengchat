package com.fengshengchat.group.binder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.Friend;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 已经选择的联系人
 */
public class SelectedFriendViewBinder extends BaseItemViewBinder<Friend, SelectedFriendViewBinder.ViewHolder> implements View.OnClickListener {

    public SelectedFriendViewBinder(@NonNull OnClickListener<Friend> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.group_selected_friend_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull Friend item) {
       // ImageUtils.loadImage(holder.avatarIv, item.avatar, R.drawable.image_default);
        ImageUtils.loadImage(holder.avatarIv, item.avatar, R.mipmap.default_head);
        holder.avatarIv.setTag(R.id.avatar_iv, item);
        holder.avatarIv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Friend friend = (Friend) v.getTag(R.id.avatar_iv);
        onClickListener.onItemClicked(friend);
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatar_iv)
        ImageView avatarIv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
