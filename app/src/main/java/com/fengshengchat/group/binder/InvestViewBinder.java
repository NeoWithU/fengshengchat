package com.fengshengchat.group.binder;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.group.model.InvestInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InvestViewBinder extends BaseItemViewBinder<InvestInfo,
        InvestViewBinder.ViewHolder> implements View.OnClickListener {

    public static final int ACTION_CONFIRM = 1;
    public static final int ACTION_REJECT = 2;

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.CHINESE);

    public interface InvestOnClickListener {
        void onItemClicked(int action, InvestInfo item);
    }

    private final InvestOnClickListener onClickListener;

    public InvestViewBinder(InvestOnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.group_invest_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull InvestInfo item) {
        if (item.inviterId == 0) {
            Resources res = holder.itemView.getResources();
            String part1 = item.nickname;
            String part2 = res.getString(R.string.group_invest_info_group);
            SpannableStringBuilder builder = new SpannableStringBuilder()
                    .append(part1)
                    .append(part2);

            int highlightColor = 0xFF1CA7FF;
            int start = 0;
            int end = part1.length();
            builder.setSpan(new ForegroundColorSpan(highlightColor), start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            holder.contentTv.setText(builder);
        } else {
            Resources res = holder.itemView.getResources();
            String part1 = item.inviterNickname;
            String part2 = res.getString(R.string.group_invest);
            String part3 = item.nickname;
            String part4 = res.getString(R.string.group_into_group);
            SpannableStringBuilder builder = new SpannableStringBuilder()
                    .append(part1)
                    .append(part2)
                    .append(part3)
                    .append(part4);

            int highlightColor = 0xFF1CA7FF;
            int start = 0;
            int end = part1.length();
            builder.setSpan(new ForegroundColorSpan(highlightColor), start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            start = end + part2.length();
            end = start + part3.length();
            builder.setSpan(new ForegroundColorSpan(highlightColor), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.contentTv.setText(builder);
        }

//        holder.m1Tv.setText(item.inviterNickname);
//        holder.m2Tv.setText(item.nickname);

        int childCount = holder.actionLayout.getChildCount();
        if (item.state < childCount) {
            for (int i = 0; i < childCount; i++) {
                holder.actionLayout.getChildAt(i).setVisibility(View.GONE);
            }
            holder.actionLayout.getChildAt(item.state).setVisibility(View.VISIBLE);
        }

        holder.timeTv.setText(simpleDateFormat.format(new Date(item.updateTime * 1000L)));
        holder.confirmTv.setOnClickListener(this);
        holder.confirmTv.setTag(R.id.confirm_tv, item);
        holder.rejectTv.setOnClickListener(this);
        holder.rejectTv.setTag(R.id.reject_tv, item);
    }

    @Override
    public void onClick(View v) {
        InvestInfo info = (InvestInfo) v.getTag(R.id.confirm_tv);
        if (info != null) {
            onClickListener.onItemClicked(ACTION_CONFIRM, info);
            return;
        }

        info = (InvestInfo) v.getTag(R.id.reject_tv);
        onClickListener.onItemClicked(ACTION_REJECT, info);
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.content_tv)
        TextView contentTv;
        @BindView(R.id.time_tv)
        TextView timeTv;
        @BindView(R.id.confirm_tv)
        TextView confirmTv;
        @BindView(R.id.reject_tv)
        TextView rejectTv;
        @BindView(R.id.action_layout)
        ViewGroup actionLayout;
//        @BindView(R.id.m_1_tv)
//        TextView m1Tv;
//        @BindView(R.id.m_2_tv)
//        TextView m2Tv;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
