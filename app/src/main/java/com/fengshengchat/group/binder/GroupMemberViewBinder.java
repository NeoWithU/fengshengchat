package com.fengshengchat.group.binder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.group.model.GroupMemberData;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 群成员头像 ViewBinder
 */
public class GroupMemberViewBinder extends BaseItemViewBinder<GroupMemberData, GroupMemberViewBinder.ViewHolder> implements View.OnClickListener {

    public GroupMemberViewBinder(@NonNull OnClickListener<GroupMemberData> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.group_member_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull GroupMemberData item) {
        holder.itemView.setOnClickListener(this);
        holder.itemView.setTag(item);
        holder.nameTv.setText(item.nickname);
     //   ImageUtils.loadRadiusImage(holder.avatarIv, item.avatar, R.drawable.image_default_with_round, 5);
        ImageUtils.loadRadiusImage(holder.avatarIv, item.avatar, R.mipmap.default_head, 50);
    }

    @Override
    public void onClick(View v) {
        GroupMemberData item = (GroupMemberData) v.getTag();
        onClickListener.onItemClicked(item);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatar_iv)
        ImageView avatarIv;
        @BindView(R.id.name_tv)
        TextView nameTv;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
