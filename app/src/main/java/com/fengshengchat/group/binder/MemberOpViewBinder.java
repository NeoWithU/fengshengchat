package com.fengshengchat.group.binder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.group.model.MemberOp;

import butterknife.BindView;

/**
 * 群成员头像 ViewBinder
 */
public class MemberOpViewBinder extends BaseItemViewBinder<MemberOp, MemberOpViewBinder.ViewHolder>
        implements View.OnClickListener {

    public MemberOpViewBinder(@NonNull OnClickListener<MemberOp> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.group_member_op_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MemberOp item) {
        holder.avatarIv.setImageResource(item.iconResId);
        holder.avatarIv.setOnClickListener(this);
        holder.avatarIv.setTag(item);
    }

    @Override
    public void onClick(View v) {
        MemberOp memberOp = (MemberOp) v.getTag();
        onClickListener.onItemClicked(memberOp);
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatar_iv)
        ImageView avatarIv;

        private ViewHolder(View itemView) {
            super(itemView);
            avatarIv = itemView.findViewById(R.id.avatar_iv);
        }
    }

}
