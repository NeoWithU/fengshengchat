package com.fengshengchat.group.decoration;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.blankj.utilcode.util.ConvertUtils;

/**
 * 群成员 decoration
 */
public class GroupMemberDecoration extends RecyclerView.ItemDecoration {

    public static final int N = 5;
    private final int marginBothSide;
    private final int marginTop;

    public GroupMemberDecoration() {
        marginTop = ConvertUtils.dp2px(15);
        marginBothSide = marginTop / 2;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int res = (position + 1) % N;
        if (res == 0) { //  左侧
            outRect.set(marginBothSide, marginTop, marginBothSide, 0);
        } else if (res == 1) {  //  右侧
            outRect.set(marginBothSide, marginTop, marginBothSide, 0);
        } else {    //  中间
            outRect.set(marginBothSide, marginTop, marginBothSide, 0);
        }
    }

}
