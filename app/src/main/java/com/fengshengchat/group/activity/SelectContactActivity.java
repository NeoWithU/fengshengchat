package com.fengshengchat.group.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.contacts.view.SelectContactInfoFragment;
import com.fengshengchat.contacts.view.SelectContactSendToFragment;
import com.fengshengchat.group.fragment.CreateGroupFragment;

/**
 * 创建群
 */
public class SelectContactActivity extends BaseActivity {
    public static final String ENTER_KEY = "enterKey";
    public static final String ENTER_SELECT_CONTACT_SEND_TO = "enterSelectContactSendTo";
    public static final String ENTER_SELECT_CONTACT_CARD = "enterSelectContactCard";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        String stringExtra = getIntent().getStringExtra(ENTER_KEY);
        if(ENTER_SELECT_CONTACT_SEND_TO.equals(stringExtra)){
            int dataIndex = getIntent().getIntExtra("dataIndex", -1);
            SelectContactSendToFragment selectContactFragment = new SelectContactSendToFragment();
            Bundle b = new Bundle();
            b.putInt("dataIndex", dataIndex);
            selectContactFragment.setArguments(b);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, selectContactFragment, SelectContactSendToFragment.TAG)
                    .commit();
            return;
        }else if(ENTER_SELECT_CONTACT_CARD.equals(stringExtra)){
            String name = getIntent().getStringExtra("sendToName");
            String icon = getIntent().getStringExtra("sendToIcon");
            boolean isShareCard = getIntent().getBooleanExtra("isShareCard", false);
            SelectContactInfoFragment selectContactInfoFragment = new SelectContactInfoFragment();
            Bundle b = new Bundle();
            b.putString("name", name);
            b.putString("icon", icon);
            b.putBoolean("isShareCard", isShareCard);
            b.putBoolean("isGroup", getIntent().getBooleanExtra("isGroup", false));
            selectContactInfoFragment.setArguments(b);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, selectContactInfoFragment, SelectContactInfoFragment.TAG)
                    .commit();
            return;
        }

        CreateGroupFragment fragment = CreateGroupFragment.newInstance(0);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, CreateGroupFragment.TAG)
                .commit();
    }
}
