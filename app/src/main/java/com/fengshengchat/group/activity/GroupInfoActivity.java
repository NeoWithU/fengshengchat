package com.fengshengchat.group.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fengshengchat.R;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.group.fragment.GroupInfoFragment;

/**
 * 群信息
 */
public class GroupInfoActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        Intent intent = getIntent();
        long groupId = intent.getLongExtra("UID", 0);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, GroupInfoFragment.newInstance(groupId), GroupInfoFragment.TAG)
                .commit();
    }

}
