package com.fengshengchat.group.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.fengshengchat.R;
import com.fengshengchat.widget.ItemLayout;

public class GroupAdminLayout extends LinearLayout {

    public ItemLayout ownerInvestVerifyLayout;
    public ItemLayout pinNoticeLayout;
    public ItemLayout verifyInvestLayout;

    public GroupAdminLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ownerInvestVerifyLayout = findViewById(R.id.owner_invest_verify_layout);
        pinNoticeLayout = findViewById(R.id.pin_notice_layout);
        verifyInvestLayout = findViewById(R.id.verify_invest_layout);
    }
}
