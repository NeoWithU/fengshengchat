package com.fengshengchat.group.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.widget.ItemLayout;

public class GroupOwnerLayout extends GroupAdminLayout {

    public ItemLayout adminLayout;
    public ItemLayout investVerifyLayout;
    public ImageView adminIv;
    public TextView adminNameTv;

    public GroupOwnerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        adminLayout = findViewById(R.id.admin_layout);
        investVerifyLayout = findViewById(R.id.verify_invest_layout);
        adminIv = adminLayout.findViewById(R.id.admin_iv);
        adminNameTv = adminLayout.findViewById(R.id.admin_name_tv);
    }
}
