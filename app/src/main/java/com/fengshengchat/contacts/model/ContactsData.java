package com.fengshengchat.contacts.model;

import com.blankj.utilcode.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

public class ContactsData {

    public static final String UP = "↑";
    public static final String STAR = "☆";
    public static final String NUMBER_SIGN = "#";
    public static final String[] INDEX_ITEMS = {STAR, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", NUMBER_SIGN};
    public static final LinkedHashMap<String, Integer> INDEX_MAP = new LinkedHashMap<>(INDEX_ITEMS.length);

    static {
        for (int i = 0; i < ContactsData.INDEX_ITEMS.length; i++) {
            String key = ContactsData.INDEX_ITEMS[i];
            INDEX_MAP.put(key, i);
        }
    }

    public final int[] sectionIndexList = new int[INDEX_ITEMS.length];
    private final List<Object> items = new ArrayList<>();
    private final List<FriendOp> friendOpList = new ArrayList<>();

    public ContactsData() {
        //  测试数据
//        ArrayList<Friend> list = new ArrayList<>();
//        list.add(new Friend("赵"));
//        list.add(new Friend("钱"));
//        list.add(new Friend("孙"));
//        list.add(new Friend("李"));
//        list.add(new Friend("李💰"));
//        list.add(new Friend("李💰#"));
//        list.add(new Friend("一"));
//        list.add(new Friend("二"));
//        list.add(new Friend("ʕノ•ᴥ•ʔノ ︵ ┻━┻"));
//        list.add(new Friend("👨"));
//        list.add(new Friend("👩"));
//        list.add(new Friend("🍵"));
//        list.add(new Friend("!"));
//        list.add(new Friend("阿D"));
//        list.add(new Friend("阿1"));
//        list.add(new Friend("阿2"));
//        list.add(new Friend("阿3"));
//        list.add(new Friend("布"));
//        list.add(new Friend("不"));
//        list.add(new Friend("瀑"));
//        contactsData.setFriendList(list);
    }

    @SuppressWarnings("ConstantConditions")
    public void setFriendList(List<Friend> friendList) {
        //  好友操作选项添加到全局列表
        friendOpList.clear();
        friendOpList.addAll(FriendOp.getFriendOp(Objects.requireNonNull(Utils.getApp())));

        //  清空列表
        items.clear();
        items.addAll(friendOpList);

        //  划分成字母列表和非字母列表
        List<Friend> starList = new ArrayList<>();
        List<Friend> alphabetList = new ArrayList<>();
        List<Friend> specialList = new ArrayList<>();
        for (Friend friend : friendList) {
            if (friend.isMarkStar) {
                starList.add(friend);
            }

            if (friend.isAlphabetChar()) {
                alphabetList.add(friend);
            } else {
                specialList.add(friend);
            }
        }

        //  排序数据
        Collections.sort(starList, (o1, o2) -> o1.getNicknamePinyin().compareTo(o2.getNicknamePinyin()));
        Collections.sort(alphabetList, (o1, o2) -> o1.getNicknamePinyin().compareTo(o2.getNicknamePinyin()));
        Collections.sort(specialList, (o1, o2) -> o1.getNicknamePinyin().compareTo(o2.getNicknamePinyin()));

        //  初始化章节索引器
        for (int i = 0; i < sectionIndexList.length; i++) {
            sectionIndexList[i] = -1;
        }

        //  添加带有非字母列表到全局列表中
        if (!starList.isEmpty()) {
            Section section = new Section();
//            section.title = STAR;
            section.title = STAR + "   星标朋友";
            items.add(section);
            items.addAll(starList);
        }
        sectionIndexList[INDEX_MAP.get(STAR)] = 0;
//        sectionIndexList[INDEX_MAP.get(STAR)] = items.size();

        //  添加字母列表添加到全局列表中
        char oldChar = 0;
        int cnt = items.size();
        for (Friend friend : alphabetList) {
            char curChar = friend.getNicknamePinyin().charAt(0);
            if (oldChar != curChar) {
                //  添加章节
                Section section = new Section();
                section.title = String.valueOf(curChar);
                items.add(section);

                //  记录索引记录
                Integer index = INDEX_MAP.get(section.title);
                sectionIndexList[index] = cnt;

                //  更新临时数据
                ++cnt;
                oldChar = curChar;
            }

            //  添加联系人
            items.add(friend);
            ++cnt;
        }

        //  添加带有非字母列表到全局列表中
        if (!specialList.isEmpty()) {
            //  添加章节
            Section section = new Section();
            section.title = String.valueOf(NUMBER_SIGN);
            items.add(section);
            items.addAll(specialList);

            sectionIndexList[INDEX_MAP.get(NUMBER_SIGN)] = cnt;
        }
    }

    public List<Object> getItems() {
        return items;
    }


    /**
     * 转化章节数据
     */
    public static List<Object> convertSectionData(List<Friend> friendList) {
        //  划分成字母列表和非字母列表
        List<Friend> starList = new ArrayList<>();
        List<Friend> alphabetList = new ArrayList<>();
        List<Friend> specialList = new ArrayList<>();
        for (Friend friend : friendList) {
            if (friend.isMarkStar) {
                starList.add(friend);
            }

            if (friend.isAlphabetChar()) {
                alphabetList.add(friend);
            } else {
                specialList.add(friend);
            }
        }

        //  排序数据
        Collections.sort(starList, (o1, o2) -> o1.getNicknamePinyin().compareTo(o2.getNicknamePinyin()));
        Collections.sort(alphabetList, (o1, o2) -> o1.getNicknamePinyin().compareTo(o2.getNicknamePinyin()));
        Collections.sort(specialList, (o1, o2) -> o1.getNicknamePinyin().compareTo(o2.getNicknamePinyin()));

        List<Object> items = new ArrayList<>(starList.size() + alphabetList.size() + specialList.size());

        //  添加带有非字母列表到全局列表中
        if (!starList.isEmpty()) {
            Section section = new Section();
            section.title = STAR + "   星标朋友";
            items.add(section);
            items.addAll(starList);
        }

        //  添加字母列表添加到全局列表中
        char oldChar = 0;
        for (Friend friend : alphabetList) {
            char curChar = friend.getNicknamePinyin().charAt(0);
            if (oldChar != curChar) {
                //  添加章节
                Section section = new Section();
                section.title = String.valueOf(curChar);
                items.add(section);

                oldChar = curChar;
            }

            //  添加联系人
            items.add(friend);
        }

        //  添加带有非字母列表到全局列表中
        if (!specialList.isEmpty()) {
            //  添加章节
            Section section = new Section();
            section.title = String.valueOf(ContactsData.NUMBER_SIGN);
            items.add(section);
            items.addAll(specialList);
        }

        return items;
    }


}
