package com.fengshengchat.contacts.model;

import java.util.Objects;

/**
 * 章节
 */
public class Section {
    public String title;
    public int count;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return count == section.count &&
                Objects.equals(title, section.title);
    }

    @Override
    public int hashCode() {

        return Objects.hash(title, count);
    }
}
