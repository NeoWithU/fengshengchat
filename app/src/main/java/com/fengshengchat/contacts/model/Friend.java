package com.fengshengchat.contacts.model;

import android.os.Parcel;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.github.promeg.pinyinhelper.Pinyin;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.DbManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class Friend extends Account {

    /**
     * 昵称拼音
     */
    private String nicknamePinyin;

    /**
     * 是否已经选中
     */
    public boolean checked;

    /**
     * 是否可用
     */
    public boolean disabled;

    /**
     * 是否已经加入
     */
    public boolean joined;

    public Friend() {
    }

    public Friend(Account account) {
        id = account.id;
        nickname = account.nickname;
        avatar = account.avatar;
        bigAvatar = account.bigAvatar;
        isBlacklist = account.isBlacklist;
        isMarkStar = account.isMarkStar;
        gender = account.gender;
        isPinSession = account.isPinSession;
        notDisturb = account.notDisturb;
        updatedTime = account.updatedTime;
        province = account.province;
        city = account.city;
        area = account.area;
        isDeleted = account.isDeleted;
        updateNicknamePinyin();
    }

    @NonNull
    public String getNicknamePinyin() {
        return TextUtils.isEmpty(nicknamePinyin) ? " " : nicknamePinyin;
    }

    public void updateNicknamePinyin() {
        String pinyin = Pinyin.toPinyin(nickname, "");
        if (!TextUtils.isEmpty(pinyin)) {
            nicknamePinyin = pinyin.toUpperCase();
        }
    }

    /**
     * 昵称拼音是否是字母字符
     */
    public boolean isAlphabetChar() {
        if (TextUtils.isEmpty(nicknamePinyin)) {
            return false;
        }

        char c = nicknamePinyin.charAt(0);
        return c >= 'A' && c <= 'Z';
    }

    public static List<Friend> convert(List<Account> accountList) {
        ArrayList<Friend> list = new ArrayList<>(accountList.size());
        for (Account account : accountList) {
            Friend accountData = new Friend(account);
            list.add(accountData);
        }
        return list;
    }

    public static Observable<List<Friend>> getFriendListObservable() {
        return Observable.fromCallable(() -> DbManager.getDb().getAccountDao().findAll())
                .map(Friend::convert);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.nicknamePinyin);
        dest.writeByte(this.checked ? (byte) 1 : (byte) 0);
        dest.writeByte(this.disabled ? (byte) 1 : (byte) 0);
        dest.writeByte(this.joined ? (byte) 1 : (byte) 0);
    }

    protected Friend(Parcel in) {
        super(in);
        this.nicknamePinyin = in.readString();
        this.checked = in.readByte() != 0;
        this.disabled = in.readByte() != 0;
        this.joined = in.readByte() != 0;
    }

    public static final Creator<Friend> CREATOR = new Creator<Friend>() {
        @Override
        public Friend createFromParcel(Parcel source) {
            return new Friend(source);
        }

        @Override
        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };
}
