package com.fengshengchat.contacts.model;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;

import com.fengshengchat.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 好友功能操作
 */
public class FriendOp {
    public final int id;
    public final String name;
    public final int iconResId;

    public FriendOp(int id, String name, int avatarResId) {
        this.id = id;
        this.name = name;
        this.iconResId = avatarResId;
    }

    public static final int OP_NEW_FRIEND = 1;
    public static final int OP_GROUP_CHAT = 2;
    public static final int OP_TAG = 3;

//    private static final int[] ids = new int[]{OP_NEW_FRIEND, OP_GROUP_CHAT, OP_TAG};
//    private static final int[] ids = new int[]{OP_NEW_FRIEND, OP_GROUP_CHAT};
    private static final int[] ids = new int[]{ OP_GROUP_CHAT};

    public static List<FriendOp> getFriendOp(Context context) {
        Resources res = context.getResources();
        String[] titles = res.getStringArray(R.array.ContactsOpTitles);
        TypedArray iconResIds = res.obtainTypedArray(R.array.ContactsOpIcons);

        ArrayList<FriendOp> items = new ArrayList<>(titles.length);
        for (int i = 0; i < ids.length; i++) {
            FriendOp op = new FriendOp(ids[i], titles[i], iconResIds.getResourceId(i, 0));
            items.add(op);
        }

        iconResIds.recycle();
        return items;
    }
}
