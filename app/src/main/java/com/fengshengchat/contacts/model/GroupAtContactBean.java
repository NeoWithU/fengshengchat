package com.fengshengchat.contacts.model;

public class GroupAtContactBean {
    public long gid;
    public String name;
    public String avatarUrl;
    public String groupId;
}
