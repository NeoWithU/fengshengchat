package com.fengshengchat.contacts.model;

import com.flyco.tablayout.listener.CustomTabEntity;
import com.fengshengchat.R;

/**
 * @author KRT
 * 2018/12/5
 */
public class ContactsListTabEntity implements CustomTabEntity {
    private String title;

    public ContactsListTabEntity(String title){
        this.title = title;
    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public int getTabSelectedIcon() {
        return R.mipmap.contact_sel;
    }

    @Override
    public int getTabUnselectedIcon() {
        return R.mipmap.contact_nor;
    }
}
