package com.fengshengchat.contacts.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.fengshengchat.R;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.Friend;

public class SelectContactInfoFragment extends SelectContactSendToFragment {
    public static final String TAG = "SelectContactInfoFragment";

    @Override
    public void requestCreateGroup() {}

    @Override
    public void onItemClicked(Friend item) {
        String name = "";
        String icon = "";
        boolean isShareCard = false;
        boolean isGroup = false;
        try {
            Bundle arguments = getArguments();
            name = arguments.getString("name");
            icon = arguments.getString("icon");
            isShareCard = arguments.getBoolean("isShareCard", false);
            isGroup = arguments.getBoolean("isGroup", false);
        }catch(Exception e){
            e.printStackTrace();
        }
        chooseFriend = item;

        View inflate = View.inflate(getContext(), R.layout.dialog_send_to_layout, null);
        ImageView imageView = inflate.findViewById(R.id.icon);
        TextView textView = inflate.findViewById(R.id.nick_name);
        TextView textView1 = inflate.findViewById(R.id.contact_content);

        if(isShareCard){
            if(isGroup){
                ImageUtils.loadRadiusImage(imageView, icon, R.drawable.group_ic_default, 3);
            }else {
                ImageUtils.loadRadiusImage(imageView, icon, R.mipmap.default_head_icon, 3);
            }
            textView.setText(name);
            textView1.setText(getResources().getString(R.string.contact_card_for_send) + item.nickname);
            textView1.setVisibility(View.VISIBLE);
        }else{
            ImageUtils.loadRadiusImage(imageView, item.avatar, R.mipmap.default_head_icon, 3);
            textView.setText(item.nickname);
            textView1.setText(getResources().getString(R.string.contact_card_for_send) + name);
            textView1.setVisibility(View.VISIBLE);
        }

        MyListener l = new MyListener();
        inflate.findViewById(R.id.cancel).setOnClickListener(l);
        inflate.findViewById(R.id.confirm).setOnClickListener(l);
        editText = inflate.findViewById(R.id.edit);

        mDialog = new AlertDialog.Builder(getActivity(), R.style.dialog_soft_input)
                .setView(inflate)
                .create();
        Window window = mDialog.getWindow();
        if(null != window){
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mDialog.setOnDismissListener(dialog -> {
            try {
                KeyboardUtils.hideSoftInput(getActivity());
            }catch(Exception e){
                e.printStackTrace();
            }
        });
        mDialog.show();
    }

    @Override
    public void sendTo(Friend item) {
        try {
            Intent intent = new Intent();
            intent.putExtra("uid", item.id);
            intent.putExtra("icon", item.avatar);
            intent.putExtra("name", item.nickname);
            intent.putExtra("textMessage", editText.getText().toString().trim());
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
