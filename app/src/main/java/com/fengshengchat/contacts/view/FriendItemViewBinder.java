package com.fengshengchat.contacts.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.Friend;

public class FriendItemViewBinder extends BaseItemViewBinder<Friend, FriendItemViewBinder.ViewHolder> implements View.OnClickListener {

    public FriendItemViewBinder(@NonNull OnClickListener<Friend> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.contacts_friend_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull Friend item) {
        holder.itemView.setOnClickListener(this);
        holder.itemView.setTag(item);
     //   ImageUtils.loadImage(holder.avatarIv, item.avatar, R.drawable.image_default);
        ImageUtils.loadImage(holder.avatarIv, item.avatar, R.mipmap.default_head_icon);
        holder.nicknameTv.setText(item.nickname);
    }

    @Override
    public void onClick(View v) {
        Friend friend = (Friend) v.getTag();
        onClickListener.onItemClicked(friend);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView avatarIv;
        final TextView nicknameTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            avatarIv = itemView.findViewById(R.id.avatar_iv);
            nicknameTv = itemView.findViewById(R.id.nickname_tv);
        }
    }

}
