package com.fengshengchat.contacts.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.blankj.utilcode.util.LogUtils;
import com.gjiazhe.wavesidebar.WaveSideBar;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.FriendBriefInfoActivity;
import com.fengshengchat.account.activity.NewFriendReqActivity;
import com.fengshengchat.account.model.BriefData;
import com.fengshengchat.base.BKFragment;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.contacts.model.ContactsData;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.contacts.model.FriendOp;
import com.fengshengchat.contacts.model.Section;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.net.BaseObserver;

import java.util.List;

import butterknife.BindView;
import me.drakeet.multitype.MultiTypeAdapter;

/**
 * 联系人列表
 */
public class ContactsListFragment extends BKFragment {

    public static final String TAG = "ContactsListFragment";

    public static ContactsListFragment newInstance() {
        Bundle args = new Bundle();
        ContactsListFragment fragment = new ContactsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_contacts_list_layout;
    }

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.side_bar)
    WaveSideBar waveSideBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    private ContactsData contactsData;

    private void initData() {
        contactsData = new ContactsData();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
        initSlideBar();
        LogUtils.e("find-toke", "contact fragment: " + this);
    }

    @SuppressWarnings("ConstantConditions")
    private void initSlideBar() {
        waveSideBar.setIndexItems(ContactsData.INDEX_ITEMS);
        waveSideBar.setOnSelectIndexItemListener(index -> {
            Integer key = ContactsData.INDEX_MAP.get(index);
            int pos = contactsData.sectionIndexList[key];
            if (pos != -1) {
                linearLayoutManager.scrollToPositionWithOffset(pos, 0);
            }
        });
    }

    private LinearLayoutManager linearLayoutManager;
    private MultiTypeAdapter adapter;

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getContext());

        adapter = new MultiTypeAdapter(contactsData.getItems());
        adapter.register(FriendOp.class, new FriendOpItemViewBinder(this::handleFriendOp));
        adapter.register(Section.class, new ContactsSectionViewBinder());
        adapter.register(Friend.class, new FriendItemViewBinder(this::startChat));

        recyclerView.addItemDecoration(new ContactsDecoration(contactsData.getItems()));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void startChat(Friend friend) {
        Intent intent = new Intent(getContext(), FriendBriefInfoActivity.class);
        BriefData briefData = new BriefData();
        briefData.id = friend.id;
        briefData.relative = BriefData.FRIEND;
        briefData.nickname = friend.nickname;
        briefData.avatar = friend.avatar;
        briefData.bigAvatar = friend.bigAvatar;
        briefData.gender = friend.gender;
        briefData.isMarkStar = friend.isMarkStar;
        intent.putExtra(FriendBriefInfoActivity.KEY_DATA, briefData);
        startActivity(intent);
    }

    /**
     * 处理好友操作
     */
    private void handleFriendOp(FriendOp friendOp) {
        switch (friendOp.id) {
            case FriendOp.OP_NEW_FRIEND:    //  新的朋友
                Intent intent = new Intent(getActivity(), NewFriendReqActivity.class);
                startActivity(intent);
                break;

            case FriendOp.OP_GROUP_CHAT:    //  群聊
                startActivity(new Intent(getContext(), GroupListActivity.class));
                break;

            case FriendOp.OP_TAG:   //  标签
                break;

            default:
                break;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            loadData();
        }
    }

    protected void loadData() {
        Friend.getFriendListObservable()
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new BaseObserver<List<Friend>>() {
                    @Override
                    public void onNext(List<Friend> friendList) {
                        contactsData.setFriendList(friendList);
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        RxBus.get().register(this);
        loadData();
    }

    @Override
    public void onPause() {
        super.onPause();
        RxBus.get().unregister(this);
    }

    @Keep
    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_FRIEND)})
    public void updateFriendList(Boolean success) {
        loadData();
    }

}
