package com.fengshengchat.contacts.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.contacts.model.Section;

public class ContactsSectionViewBinder extends BaseItemViewBinder<Section, ContactsSectionViewBinder.ViewHolder> {

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.contacts_section_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull Section item) {
        holder.titleTv.setText(item.title);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView titleTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTv = itemView.findViewById(R.id.title_tv);
        }
    }
}
