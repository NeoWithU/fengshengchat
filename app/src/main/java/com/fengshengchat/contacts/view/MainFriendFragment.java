package com.fengshengchat.contacts.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.fengshengchat.MainActivity;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.SearchFriendOrGroupActivity;
import com.fengshengchat.account.fragment.NewFriendFragment;
import com.fengshengchat.app.SearchActivity;
import com.fengshengchat.base.BKFragment;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.widget.TitleBarLayout;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;

public class MainFriendFragment extends BKFragment {

    private ContactsListFragment contactsListFragment;
    private NewFriendFragment newFriendFragment;
    private Fragment currFragment;

    @BindView(R.id.divider_1)
    View dividerV1;
    @BindView(R.id.divider_2)
    View dividerV2;

    @BindView(R.id.friend_item)
    TextView friendItem;
    @BindView(R.id.new_friend_item)
    TextView newFriendItem;

    @BindView(R.id.title_bar)
    TitleBarLayout titleBar;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_main_friend;
    }

    private void initTitleBar() {
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
            }

            @Override
            public void onActionImageClick() {
                Intent intent = new Intent(getActivity(), SearchFriendOrGroupActivity.class);
                startActivity(intent);
            }

            @Override
            public void onActionClick() {
            }
        });
        ToolbarFragment.hackStatusBar(titleBar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitleBar();
        changeFragment(getContactsListFragment());
    }

    @OnClick(R.id.friend_item)
    public void openFriend() {
        setSelectFriendItem(true);
        changeFragment(getContactsListFragment());
    }

    @OnClick(R.id.new_friend_item)
    public void openNewFriendItem() {
        setSelectFriendItem(false);

        FragmentActivity activity = getActivity();
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).updateTabRedDot(1, false);
        }
        changeFragment(getNewFriendFragment());
    }

    private void setSelectFriendItem(boolean selected) {
        if (selected) {
            friendItem.setEnabled(false);
            newFriendItem.setEnabled(true);

            friendItem.setTextSize(18);
            newFriendItem.setTextSize(15);

            dividerV1.setVisibility(View.VISIBLE);
            dividerV2.setVisibility(View.GONE);
        } else {
            friendItem.setEnabled(true);
            newFriendItem.setEnabled(false);

            friendItem.setTextSize(15);
            newFriendItem.setTextSize(18);

            dividerV1.setVisibility(View.GONE);
            dividerV2.setVisibility(View.VISIBLE);
        }
    }

    private ContactsListFragment getContactsListFragment() {
        if (contactsListFragment == null) {
            contactsListFragment = ContactsListFragment.newInstance();
        }
        return contactsListFragment;
    }

    public NewFriendFragment getNewFriendFragment() {
        if (newFriendFragment == null) {
            newFriendFragment = NewFriendFragment.newInstance();
        }
        return newFriendFragment;
    }

    private void changeFragment(Fragment toFragment) {
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (currFragment != null) {
            ft.hide(currFragment);
        }

        if (toFragment.isAdded()) {
            ft.show(toFragment);
        } else {
            ft.add(R.id.content_fragment, toFragment);
        }

        ft.commitAllowingStateLoss();
        currFragment = toFragment;
    }

    @OnClick(R.id.search_layout)
    public void search() {
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        startActivity(intent);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (contactsListFragment != null) {
            contactsListFragment.onHiddenChanged(hidden);
        }

        if (newFriendFragment != null) {
            newFriendFragment.onHiddenChanged(hidden);
        }
    }
}
