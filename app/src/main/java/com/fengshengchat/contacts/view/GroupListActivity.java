package com.fengshengchat.contacts.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.fengshengchat.R;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.base.base.BaseArrayListRecyclerAdapter;
import com.fengshengchat.base.base.RecyclerViewDivider;
import com.fengshengchat.base.base.ViewHolder;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.chat.view.ChatActivity;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.db.helper.Task;
import com.fengshengchat.db.helper.TaskHelper;
import com.fengshengchat.group.model.GroupData;
import com.fengshengchat.net.BaseObserver;
import com.fengshengchat.widget.TitleBarLayout;

import java.util.ArrayList;
import java.util.List;

public class GroupListActivity extends BaseActivity {

    private Adapter mAdapter;

    private ViewSwitcher viewSwitcher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTransparentForWindow(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list_layout);
        ToolbarFragment.hackStatusBar(getRootView());
        initView();
        RxBus.get().register(this);
        TaskHelper.getInstance().post(Task.TASK_UPDATE_GROUP_LIST);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
    }

    private void initView() {
        TitleBarLayout titleBar = findViewById(R.id.title_bar);
        titleBar.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {
                finish();
            }

            @Override
            public void onActionImageClick() {
            }

            @Override
            public void onActionClick() {
            }
        });

        mAdapter = new Adapter();
        mAdapter.setClickable(true);

        RecyclerView recyclerView = findViewById(R.id.list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new RecyclerViewDivider(1, 0xFFEEEEEE));
        recyclerView.setAdapter(mAdapter);

        viewSwitcher = findViewById(R.id.view_switcher);
    }

    private static class Adapter extends BaseArrayListRecyclerAdapter<GroupData> {

        @Override
        protected void onBindData(ViewHolder holder, int position, GroupData data) {
            holder.setText(R.id.name_tv, data.name);
            holder.setText(R.id.id_tv, "群号：" + String.valueOf(data.id));
            ImageView avatarIv = holder.getImageView(R.id.avatar_iv);
            ImageUtils.loadRadiusImage(avatarIv, data.avatar, R.drawable.group_ic_default, AppConfig.IMAGE_RADIUS);
        }

        @Override
        protected int getLayoutId(int viewType) {
            return R.layout.list_group_at_contact_item_layout;
        }

        @Override
        protected void onItemClick(int position, GroupData data, ViewHolder viewHolder) {
            ChatActivity.startChat(viewHolder.getContext(), data.id, true);
        }
    }

    private void loadData() {
        GroupData.getGroupListObservable()
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .compose(this.bindUntilEvent(ActivityEvent.DESTROY))
                .subscribe(new BaseObserver<List<GroupData>>() {
                    @Override
                    public void onNext(List<GroupData> groups) {
                        mAdapter.clearData();
                        mAdapter.addData((ArrayList<GroupData>) groups);
                        mAdapter.notifyDataSetChanged();
                        viewSwitcher.setDisplayedChild(groups.isEmpty() ? 1 : 0);
                    }
                });
    }

    @SuppressWarnings("unused")
    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_GROUP_LIST)})
    public void updateGroupList(Boolean success) {
        loadData();
    }

    @SuppressWarnings("unused")
    @Subscribe(tags = {@Tag(BusEvent.TAG_DELETED_GROUP)})
    public void updatedDeletedGroup(Long groupId) {
        mAdapter.getData().remove(new GroupData(groupId));
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }
}
