package com.fengshengchat.contacts.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.fengshengchat.R;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.group.fragment.CreateGroupFragment;

public class SelectContactSendToFragment extends CreateGroupFragment {
    public static final String TAG = "SelectContactFragment";

    @Override
    public void requestCreateGroup() {}

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            ((ViewGroup) view).getChildAt(0).setBackgroundResource(R.drawable.common_bg_navigation);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_select_contact_2;
    }

    protected EditText editText;
    protected Friend chooseFriend;
    protected AlertDialog mDialog;
    @Override
    public void onItemClicked(Friend item) {
        chooseFriend = item;

        View inflate = View.inflate(getContext(), R.layout.dialog_send_to_layout, null);
        ImageView imageView = inflate.findViewById(R.id.icon);
        ImageUtils.loadRadiusImage(imageView, item.avatar, R.mipmap.default_head_icon, 3);
        TextView textView = inflate.findViewById(R.id.nick_name);
        textView.setText(item.nickname);
        MyListener l = new MyListener();
        inflate.findViewById(R.id.cancel).setOnClickListener(l);
        inflate.findViewById(R.id.confirm).setOnClickListener(l);
        editText = inflate.findViewById(R.id.edit);

        mDialog = new AlertDialog.Builder(getActivity(), R.style.dialog_soft_input)
                .setView(inflate)
                .create();
        Window window = mDialog.getWindow();
        if(null != window){
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mDialog.setOnDismissListener(dialog -> {
            try {
                KeyboardUtils.hideSoftInput(getActivity());
            }catch(Exception e){
                e.printStackTrace();
            }
        });
        mDialog.show();
    }

    public void sendTo(Friend item){
        try {
            int dataIndex = -1;
            Bundle arguments = getArguments();
            if(null != arguments){
                dataIndex = arguments.getInt("dataIndex", -1);
            }
            Intent intent = new Intent();
            intent.putExtra("uid", item.id);
            intent.putExtra("dataIndex", dataIndex);
            intent.putExtra("textMessage", editText.getText().toString().trim());
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public class MyListener implements View.OnClickListener{
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.cancel:
                    mDialog.dismiss();
                    break;
                case R.id.confirm:
                    mDialog.dismiss();
                    sendTo(chooseFriend);
                    break;
            }

        }
    }

}
