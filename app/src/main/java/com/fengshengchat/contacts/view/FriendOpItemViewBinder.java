package com.fengshengchat.contacts.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fengshengchat.R;
import com.fengshengchat.base.BaseItemViewBinder;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.contacts.model.FriendOp;

public class FriendOpItemViewBinder extends BaseItemViewBinder<FriendOp, FriendOpItemViewBinder.ViewHolder> implements View.OnClickListener {

    public FriendOpItemViewBinder(@NonNull OnClickListener<FriendOp> onClickListener) {
        super(onClickListener);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.contacts_friend_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull FriendOp item) {
        holder.itemView.setOnClickListener(this);
        holder.itemView.setTag(item);
        ImageUtils.loadImage(holder.avatarIv, item.iconResId);
        holder.nicknameTv.setText(item.name);
    }

    @Override
    public void onClick(View v) {
        FriendOp friendOp = (FriendOp) v.getTag();
        onClickListener.onItemClicked(friendOp);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView avatarIv;
        final TextView nicknameTv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            avatarIv = itemView.findViewById(R.id.avatar_iv);
            nicknameTv = itemView.findViewById(R.id.nickname_tv);
        }
    }

}
