package com.fengshengchat.contacts.view;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.Utils;
import com.fengshengchat.R;
import com.fengshengchat.contacts.model.Friend;
import com.fengshengchat.contacts.model.FriendOp;

import java.util.List;

/**
 * 联系人 Decoration
 */
public class ContactsDecoration extends RecyclerView.ItemDecoration {

    private final List<Object> items;
    private final Drawable dividerDrawable;
    private final int marginLeft;

    public ContactsDecoration(List<Object> items) {
        this.items = items;
        dividerDrawable = ContextCompat.getDrawable(Utils.getApp(), R.drawable.common_divider);
        marginLeft = ConvertUtils.dp2px(10);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position + 1 >= items.size()) {
            return;
        }

        Object o1 = items.get(position);
        Object o2 = items.get(position + 1);
        boolean needDraw = (o1 instanceof Friend && o2 instanceof Friend)
                || (o1 instanceof FriendOp && o2 instanceof FriendOp);
        if (!needDraw) {
            return;
        }

        outRect.set(0, 0, 0, dividerDrawable.getIntrinsicHeight());
    }

    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            final int left = child.getLeft() - params.leftMargin + marginLeft;
            final int right = child.getRight() + params.rightMargin;
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + dividerDrawable.getIntrinsicHeight();
            dividerDrawable.setBounds(left, top, right, bottom);
            dividerDrawable.draw(c);
        }
    }

}
