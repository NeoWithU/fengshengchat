package com.fengshengchat.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {ChatRecord.class, ChatListBean.class, Account.class, Group.class, GroupMember.class}, version = 9, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ChatRecordDao getRecordDao();

    public abstract ChatListDao getChatListDao();

    public abstract AccountDao getAccountDao();

    public abstract GroupDao getGroupDao();

    public abstract GroupMemberDao getGroupMemberDao();
}
