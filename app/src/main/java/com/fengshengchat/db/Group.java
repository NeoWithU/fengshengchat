package com.fengshengchat.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.Objects;

@Entity(tableName = "group", indices = {@Index(value = "updated_time")})
public class Group implements Parcelable {
    /**
     * 群 id
     */
    @PrimaryKey
    public long id;

    /**
     * 群名称
     */
    public String name;

    /**
     * 群主 id
     */
    @ColumnInfo(name = "owner_id")
    public long ownerId;

    /**
     * 群主名称
     */
    @ColumnInfo(name = "owner_name")
    public String ownerName;

    /**
     * 群头像
     */
    public String avatar;

    /**
     * 群公告
     */
    public String notice;

    /**
     * 群类型
     */
    public int type;

    /**
     * 群成员数量
     */
    @ColumnInfo(name = "member_count")
    public int memberCount;

    /**
     * 置顶群公告
     */
    @ColumnInfo(name = "is_pin_notice")
    public boolean isPinNotice;

    /**
     * 管理员 id
     */
    @ColumnInfo(name = "admin_ids")
    public String adminIds;

    /**
     * 邀请确认
     */
    @ColumnInfo(name = "need_invest_confirm")
    public boolean needInvestConfirm;

    /**
     * 是否置顶聊天
     */
    @ColumnInfo(name = "is_pin_session")
    public boolean isPinSession;

    /**
     * 消息免打扰
     */
    @ColumnInfo(name = "not_disturb")
    public boolean notDisturb;

    /**
     * 更新时间
     */
    @ColumnInfo(name = "updated_time")
    public long updatedTime;

    /**
     * 未处理邀请成员数量
     */
    @ColumnInfo(name = "unhandled_invite_count")
    public long unhandledInviteCount;

    /**
     * 群公告更新时间
     */
    @ColumnInfo(name = "notice_updated_time")
    public long noticeUpdatedTime;

    public Group() {
    }

    @Ignore
    public Group(long id) {
        this.id = id;
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group account = (Group) o;
        return id == account.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public long getAdminId() {
        return getAdmin(adminIds);
    }

    public static long getAdmin(String adminIds) {
         if (TextUtils.isEmpty(adminIds)) {
            return 0;
        }

        String[] ids = adminIds.split(",");
        if (ids.length == 0) {
            return 0;
        }

        return Long.parseLong(ids[0]);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeLong(this.ownerId);
        dest.writeString(this.ownerName);
        dest.writeString(this.avatar);
        dest.writeString(this.notice);
        dest.writeInt(this.type);
        dest.writeInt(this.memberCount);
        dest.writeByte(this.isPinNotice ? (byte) 1 : (byte) 0);
        dest.writeString(this.adminIds);
        dest.writeByte(this.needInvestConfirm ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isPinSession ? (byte) 1 : (byte) 0);
        dest.writeByte(this.notDisturb ? (byte) 1 : (byte) 0);
        dest.writeLong(this.updatedTime);
        dest.writeLong(this.unhandledInviteCount);
        dest.writeLong(this.noticeUpdatedTime);
    }

    protected Group(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.ownerId = in.readLong();
        this.ownerName = in.readString();
        this.avatar = in.readString();
        this.notice = in.readString();
        this.type = in.readInt();
        this.memberCount = in.readInt();
        this.isPinNotice = in.readByte() != 0;
        this.adminIds = in.readString();
        this.needInvestConfirm = in.readByte() != 0;
        this.isPinSession = in.readByte() != 0;
        this.notDisturb = in.readByte() != 0;
        this.updatedTime = in.readLong();
        this.unhandledInviteCount = in.readLong();
        this.noticeUpdatedTime = in.readLong();
    }

}
