package com.fengshengchat.db;

import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;

import com.blankj.utilcode.util.Utils;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.user.UserManager;

public class DbManager {

    private static DbManager INSTANCE;

    @NonNull
    public static AppDatabase getDb() {
        long currentUid = UserManager.getUser().uid;
        if(currentUid != lastUserId || null == INSTANCE){
            synchronized (DbManager.class) {
                if(currentUid != lastUserId || null == INSTANCE) {
                    Debug.e("test", "------------create db " + currentUid);
                    lastUserId = currentUid;
                    INSTANCE = new DbManager(lastUserId + ".db");
                }
            }
        }
        return INSTANCE.db;
    }

    private final AppDatabase db;

    public static void close(){
        getDb().close();
        lastUserId = -1L;
    }

    private static long lastUserId = -1L;
    private DbManager(String dbName) {
        db = Room.databaseBuilder(Utils.getApp(), AppDatabase.class, dbName).build();
    }

    public static ChatRecordDao getRecordDao(){
        return getDb().getRecordDao();
    }

    public static ChatListDao getChatListDao(){
        return getDb().getChatListDao();
    }

    public static AccountDao getAccountDao(){
        return getDb().getAccountDao();
    }

}
