package com.fengshengchat.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface GroupDao {

    @Insert
    long save(Group group);

    @Query("select * from `group`")
    List<Group> findAll();

    @Query("select a.* from `group` as a " +
            "left join `group_member` as b " +
            "on a.id = b.group_id and b.user_id = :userId where b.is_deleted = 0 " +
            "order by b.enter_group_time desc")
    List<Group> findAll(long userId);

    @Query("select id from `group`")
    long[] findAllId();

    @Update
    void update(Group group);

    @Query("SELECT * FROM `group` WHERE id = :id LIMIT 1")
    Group findById(long id);

    @Query("update `group` set is_pin_session = :isPinSession, not_disturb = :notDisturb where id = :id")
    void update(long id, boolean isPinSession, boolean notDisturb);

    @Query("update `group` set is_pin_session = :isPinSession where id = :id")
    int updatePinSession(long id, boolean isPinSession);

    @Query("update `group` set not_disturb = :notDisturb where id = :id")
    int updateMsgNotDisturb(long id, boolean notDisturb);

    @Query("update `group` set is_pin_notice = :isPinNotice where id = :id")
    int updatePinNotice(long id, boolean isPinNotice);

    @Query("update `group` set need_invest_confirm = :needInvestConfirm where id = :id")
    int updateNeedInvestConfirm(long id, boolean needInvestConfirm);

    @Query("update `group` set admin_ids = :adminIds where id = :id")
    void updateAdminIds(long id, String adminIds);

    @Query("update `group` set avatar = :avatar where id = :id")
    void updateGroupAvatar(long id, String avatar);

    @Query("update `group` set name = :name where id = :id")
    void updateGroupName(long id, String name);

    @Query("update `group` set notice = :notice, notice_updated_time = :noticeUpdatedTime where id = :id")
    void updateGroupNotice(long id, String notice, long noticeUpdatedTime);

    @Delete
    void delete(Group group);

    @Query("select max(updated_time) from `group`")
    long getLatestUpdatedTime();
}
