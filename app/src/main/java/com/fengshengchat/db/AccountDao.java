package com.fengshengchat.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface AccountDao {

    @Insert
    void save(List<Account> accountList);

    @Insert
    void save(Account account);

    @Query("SELECT * FROM account where is_blacklist != 1 and is_deleted != 1")
    List<Account> findAll();

    @Update
    void update(Account account);

    @Query("SELECT * FROM account WHERE id = :id LIMIT 1")
    Account findById(long id);

    @Query("SELECT * FROM account WHERE id = :id and is_deleted = 0 LIMIT 1")
    Account findFriendById(long id);

    @Query("DELETE FROM account WHERE updated_time != :updateTime")
    void deleteByUpdateTime(long updateTime);

    @Query("select * from account where nickname like '%' || :key || '%' or cast(id as text) like '%' || :key || '%' limit 4")
    List<Account> searchByNicknameOrId(String key);

    @Query("select * from account where nickname like '%' || :nickname || '%' limit 4")
    List<Account> searchByNickname(String nickname);

    @Query("select * from account where nickname like '%' || :nickname || '%'")
    List<Account> searchAllByNickname(String nickname);

    @Query("select * from account where nickname like '%' || :key || '%' or cast(id as text) like '%' || :key || '%'")
    List<Account> searchAllByNicknameOrId(String key);

    @Query("update account set is_mark_star = :isMarkStar where id = :id")
    int updateSetStarState(long id, boolean isMarkStar);

    @Query("update account set is_blacklist = :isBlacklist where id = :id")
    int updateBlacklistState(long id, boolean isBlacklist);

    @Query("update account set is_pin_session = :pinSession where id = :id")
    int updatePinSession(long id, boolean pinSession);

    @Query("update account set not_disturb = :notDisturb where id = :id")
    int updateMsgNotDisturb(long id, boolean notDisturb);

    @Query("select * from account where is_blacklist = :isBlacklist and is_deleted = 0")
    List<Account> getBlacklist(boolean isBlacklist);

    @Query("update account set is_deleted = :isDeleted where id = :id")
    int updateDeleteFriendState(long id, boolean isDeleted);

    @Query("select max(updated_time) from account")
    long getLatestUpdatedTime();

//    @Query("update account " +
//            "set nickname = :nickname, avatar = :avatar, big_avatar = :bigAvatar, province = :province, city = :city, area = :area " +
//            "where id = :id")
//    void updateInfo(long id, String nickname, String avatar, String bigAvatar, String province, String city, String area);

}
