package com.fengshengchat.db;

import java.util.List;

public class DbFriend {

    public static Account getFriend(long uid){
        return DbManager.getAccountDao().findById(uid);
    }

    public static Group getGroupInfo(long gid){
        return DbManager.getDb().getGroupDao().findById(gid);
    }

    public static List<Account> getAllFriend(){
        return DbManager.getDb().getAccountDao().findAll();
    }

    public static List<Group> getAllGroup(){
        return DbManager.getDb().getGroupDao().findAll();
    }

    public static GroupMember getNonFriendInfoFromGroup(long gid, long uid){
        List<GroupMember> all = DbManager.getDb().getGroupMemberDao().findAll(gid);
        if(null != all){
            for(GroupMember member : all){
                if(member.userId == uid){
                    return member;
                }
            }
        }
        return null;
    }

}
