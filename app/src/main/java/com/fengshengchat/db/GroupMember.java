package com.fengshengchat.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Objects;

/**
 * 群组成员
 */
@Entity(tableName = "group_member", indices = {
        @Index("updated_time"),
        @Index(value = {"group_id", "user_id"}, unique = true),
        @Index(value = "enter_group_time")
})
public class GroupMember implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    public long id;

    /**
     * 群 id
     */
    @ColumnInfo(name = "group_id")
    public long groupId;

    /**
     * 用户 id
     */
    @ColumnInfo(name = "user_id")
    public long userId;

    /**
     * 群昵称
     */
    public String nickname;

    /**
     * 群头像
     */
    public String avatar;

    /**
     * 大头像
     */
    @ColumnInfo(name = "big_avatar")
    public String bigAvatar;

    @ColumnInfo(name = "updated_time")
    public long updatedTime;

    /**
     * 是否已经被删除
     */
    @ColumnInfo(name = "is_deleted")
    public int isDeleted;

    /**
     * 进群时间
     */
    @ColumnInfo(name = "enter_group_time")
    public long enterGroupTime;

    public GroupMember() {
    }

    @NonNull
    @Override
    public String toString() {
        return nickname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupMember member = (GroupMember) o;
        return id == member.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.groupId);
        dest.writeLong(this.userId);
        dest.writeString(this.nickname);
        dest.writeString(this.avatar);
        dest.writeString(this.bigAvatar);
        dest.writeLong(this.updatedTime);
        dest.writeInt(this.isDeleted);
        dest.writeLong(this.enterGroupTime);
    }

    protected GroupMember(Parcel in) {
        this.id = in.readLong();
        this.groupId = in.readLong();
        this.userId = in.readLong();
        this.nickname = in.readString();
        this.avatar = in.readString();
        this.bigAvatar = in.readString();
        this.updatedTime = in.readLong();
        this.isDeleted = in.readInt();
        this.enterGroupTime = in.readLong();
    }

    public static final Creator<GroupMember> CREATOR = new Creator<GroupMember>() {
        @Override
        public GroupMember createFromParcel(Parcel source) {
            return new GroupMember(source);
        }

        @Override
        public GroupMember[] newArray(int size) {
            return new GroupMember[size];
        }
    };
}
