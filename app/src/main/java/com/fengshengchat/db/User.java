package com.fengshengchat.db;

import android.arch.persistence.room.Ignore;

public class User {
    public int id;

    public String nickName;

    public String accountName;

    public long uid;

    public String icon;

    public long sendUid; //send uid when this user is group

    public User(){}

    @Ignore
    public User(long uid){
        this.uid = uid;
    }

    @Ignore
    public User(long uid, String nickName){
        this.uid = uid;
        this.nickName = nickName;
    }

    @Ignore
    public User(long uid, String nickName, String icon){
        this.uid = uid;
        this.nickName = nickName;
        this.icon = icon;
    }
}
