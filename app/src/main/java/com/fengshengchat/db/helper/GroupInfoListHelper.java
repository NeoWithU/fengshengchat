package com.fengshengchat.db.helper;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.Group;
import com.fengshengchat.db.GroupDao;
import com.fengshengchat.group.fragment.GroupInfoFragment;
import com.fengshengchat.net.RetrofitClient;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import yiproto.yichat.msg.MsgOuterClass;

/**
 * 群信息列表
 */
public class GroupInfoListHelper implements RequestHelper {

    private final long updateTime;
    private final OkHttpClient client;

    public GroupInfoListHelper(long updateTime) {
        this.updateTime = updateTime;
        this.client = new RetrofitClient().getClient();
    }

    @NonNull
    private Request getRequest(int pageNum) {
        yiproto.yichat.group.Group.GetJoinGroupInfoListReq.Builder body = yiproto.yichat.group.Group.GetJoinGroupInfoListReq.newBuilder()
                .setStart(pageNum * AppConfig.PAGE_SIZE)
                .setUpdateTime(updateTime)
                .setNum(AppConfig.PAGE_SIZE);

        PBMessage pbMessage = new PBMessage()
                .setCmd(yiproto.yichat.group.Group.GroupYimCMD.GET_JOIN_GROUP_INFO_LIST_REQ_CMD_VALUE)
                .setBody(body);

        return new Request(pbMessage);
    }

    private void requestGroupList(int pageNum) {
        okhttp3.Request request = RequestHelper.getRequest(getRequest(pageNum));

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                return;
            }

            ResponseBody content = response.body();
            if (content == null) {
                return;
            }

            String string = content.string();
            if (TextUtils.isEmpty(string)) {
                return;
            }

            com.fengshengchat.net.bean.Response rsp = new Gson().fromJson(string, com.fengshengchat.net.bean.Response.class);
            MsgOuterClass.Msg outerMsg = rsp.getMessage();
            if (outerMsg == null) {
                return;
            }

            yiproto.yichat.group.Group.GetJoinGroupInfoListRsp data = yiproto.yichat.group.Group.GetJoinGroupInfoListRsp.parseFrom(outerMsg.getBody());
            if (data.getResult() != yiproto.yichat.group.Group.GetJoinGroupInfoListRsp.Result.OK_VALUE) {
                return;
            }

            if (data.getGroupInfoListCount() == 0) {
                return;
            }

            saveGroup(data.getGroupInfoListList());

            if (data.getGroupInfoListCount() == AppConfig.PAGE_SIZE) {
                requestGroupList(pageNum + 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void saveGroup(List<yiproto.yichat.group.Group.GroupInfo> list) {
        GroupDao groupDao = DbManager.getDb().getGroupDao();
        for (yiproto.yichat.group.Group.GroupInfo info : list) {
            yiproto.yichat.group.Group.MemberInfo self = info.getSelfMemberInfo();
            int delete = self.getDelete();
            if (delete == yiproto.yichat.group.Group.DeleteFlag.DELETE_KICK_VALUE) {
                groupDao.delete(new Group(info.getGroupId()));
                continue;
            } else if (delete == yiproto.yichat.group.Group.DeleteFlag.DELETE_DISMIS_VALUE || delete == yiproto.yichat.group.Group.DeleteFlag.DELETE_EXIT_VALUE) {
                GroupInfoFragment.deleteGroupAndMember(info.getGroupId());
                continue;
            }

            Group group = getGroup(info);
            if (groupDao.findById(group.id) == null) {
                groupDao.save(group);
            } else {
                groupDao.update(group);
            }
        }
    }

    @NonNull
    private static Group getGroup(yiproto.yichat.group.Group.GroupInfo info) {
        Group group = new Group();
        group.id = info.getGroupId();
        group.name = info.getGroupName();
        group.ownerId = info.getGroupOwner();
        group.ownerName = info.getGroupOwnerInfo().getGroupNickname();
        group.avatar = info.getGroupLogo();
        group.notice = info.getGroupNotice();
        group.memberCount = info.getGroupMemberCount();
        group.type = info.getGroupType();
        group.noticeUpdatedTime = info.getGroupNoticeUpdateTime() * 1000L;
        group.needInvestConfirm = info.getInviteConfirm() == yiproto.yichat.group.Group.GroupInfo.InviteConfirmType.INVITE_CONFIRM_NEED_CONFIRM_VALUE;
        group.unhandledInviteCount = info.getUnhandleInviteCount();
        group.isPinNotice = info.getOnTopNotice() == yiproto.yichat.group.Group.GroupInfo.NoticeType.NOTICE_ON_TOP_VALUE;

        //  管理员id
        StringBuilder adminIds = new StringBuilder();
        for (yiproto.yichat.group.Group.GroupMemberInfo item : info.getAdminListList()) {
            adminIds.append(item.getUserId()).append(",");
        }
        if (adminIds.length() != 0) {
            adminIds.deleteCharAt(adminIds.length() - 1);
        }
        group.adminIds = adminIds.toString();

        //  娱乐群默认开启
        if (info.getGroupType() == yiproto.yichat.group.Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE) {
            group.notDisturb = false;
        }

        yiproto.yichat.group.Group.MemberInfo selfMemberInfo = info.getSelfMemberInfo();
        if (selfMemberInfo != null) {
            group.notDisturb = selfMemberInfo.getMuteNotification() == yiproto.yichat.group.Group.MemberInfo.MuteNotificationType.NOTIFICATION_MUTE_VALUE;
            group.isPinSession = selfMemberInfo.getOnTop() == yiproto.yichat.group.Group.MemberInfo.MuteNotificationType.NOTIFICATION_MUTE_VALUE;
            group.updatedTime = selfMemberInfo.getUpdateTime();
        }
        return group;
    }

    @Override
    public void start() {
        requestGroupList(0);
    }

}
