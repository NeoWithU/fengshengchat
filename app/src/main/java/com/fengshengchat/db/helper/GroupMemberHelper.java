package com.fengshengchat.db.helper;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.db.GroupMember;
import com.fengshengchat.db.GroupMemberDao;
import com.fengshengchat.net.RetrofitClient;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.user.UserManager;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import yiproto.yichat.group.Group;
import yiproto.yichat.msg.MsgOuterClass;

public class GroupMemberHelper implements RequestHelper {

    private final long updateTime;
    private final long groupId;
    private final OkHttpClient client;

    public GroupMemberHelper(long groupId, long updateTime) {
        this.groupId = groupId;
        this.updateTime = updateTime;
        this.client = new RetrofitClient().getClient();
    }

    /**
     * 请求群成员列表
     */
    private void requestGroupMember(int pageNum) {
        okhttp3.Request request = RequestHelper.getRequest(getRequestMsg(pageNum));

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                return;
            }

            ResponseBody content = response.body();
            if (content == null) {
                return;
            }

            String string = content.string();
            if (TextUtils.isEmpty(string)) {
                return;
            }

            com.fengshengchat.net.bean.Response rsp = new Gson().fromJson(string, com.fengshengchat.net.bean.Response.class);
            MsgOuterClass.Msg outerMsg = rsp.getMessage();
            if (outerMsg == null) {
                return;
            }

            Group.GetIncrementGroupMemberListRsp data = Group.GetIncrementGroupMemberListRsp
                    .parseFrom(outerMsg.getBody());
            if (data.getResult() != Group.GetGroupMemberInfoRsp.GetGroupMemberInfoResult.OK_VALUE) {
                return;
            }

            if (data.getMemberInfoListCount() == 0) {
                return;
            }

            saveGroupMemberList(data.getMemberInfoListList());

            if (data.getMemberInfoListCount() == AppConfig.PAGE_SIZE) {
                requestGroupMember(pageNum + 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private Request getRequestMsg(int pageNum) {
        Group.GetIncrementGroupMemberListReq.Builder body = Group.GetIncrementGroupMemberListReq
                .newBuilder()
                .setUpdateTime(updateTime)
                .setGroupId(groupId)
                .setStart(pageNum * AppConfig.PAGE_SIZE)
                .setNum(AppConfig.PAGE_SIZE);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.GET_INCREMENT_GROUP_MEMBER_LIST_REQ_CMD_VALUE)
                .setBody(body);

        return new Request(pbMessage);
    }

    /**
     * 绑定群成员列表
     */
    private void saveGroupMemberList(List<Group.MemberInfo> list) {
        long selfUserId = UserManager.getUser().uid;

        GroupMemberDao dao = DbManager.getDb().getGroupMemberDao();
        for (Group.MemberInfo info : list) {
            GroupMember member = getGroupMember(info);

            GroupMember old = dao.find(member.groupId, member.userId);
            if (old != null) {
                member.id = old.id;
                dao.update(member);
            } else {
                dao.save(member);
            }

//            //  保存消息置顶 & 免打扰
//            if (info.getUserId() == selfUserId) {
//                boolean isPinSession = info.getOnTop() == Group.MemberInfo.OnTopType.ON_TOP_VALUE;
//                boolean notDisturb = info.getMuteNotification() == Group.MemberInfo.MuteNotificationType.NOTIFICATION_MUTE_VALUE;
//                DbManager.getDb().getGroupDao().update(groupId, isPinSession, notDisturb);
//            }
        }
    }

    @NonNull
    private GroupMember getGroupMember(Group.MemberInfo info) {
        GroupMember member = new GroupMember();
        member.userId = info.getUserId();
        member.groupId = groupId;
        member.nickname = info.getGroupNickname();
        member.avatar = info.getSmallAvatar();
        member.bigAvatar = info.getBigAvatar();
        member.updatedTime = info.getUpdateTime();
        member.isDeleted = info.getDelete();
        member.enterGroupTime = info.getEnterGroupTime();
        return member;
    }

    @Override
    public void start() {
        requestGroupMember(0);
    }

}
