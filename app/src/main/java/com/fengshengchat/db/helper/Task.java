package com.fengshengchat.db.helper;

import android.support.annotation.Nullable;

public class Task {
    /**
     * 更新好友
     */
    public static final int TASK_UPDATE_FRIEND = 1;

    /**
     * 更新群信息：群资料 + 成员
     */
    public static final int TASK_UPDATE_GROUP_INFO = 2;

    /**
     * 更新群组列表
     */
    public static final int TASK_UPDATE_GROUP_LIST = 3;

    /**
     * 更新群成员列表
     */
    public static final int TASK_GROUP_MEMBER_LIST = 4;

    private final int id;
    private final Object data;

    public Task(int id) {
        this(id, null);
    }

    public Task(int id, Object data) {
        this.id = id;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    @Nullable
    public Object getData() {
        return data;
    }

}
