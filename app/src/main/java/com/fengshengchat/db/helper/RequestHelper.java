package com.fengshengchat.db.helper;

import com.google.gson.Gson;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.Protocol;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public interface RequestHelper {

    void start();

    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    static okhttp3.Request getRequest(Request request) {
        String json = new Gson().toJson(request);
        RequestBody body = RequestBody.create(JSON, json);
        return new okhttp3.Request.Builder()
                .url(Protocol.getBusinessUrl() + "web_gateway")
                .post(body)
                .build();
    }
}
