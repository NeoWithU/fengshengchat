package com.fengshengchat.db.helper;

public interface BusEvent {
    /**
     * 更新所有好友列表
     */
    String TAG_UPDATE_FRIEND = "TAG_UPDATE_FRIEND";
    /**
     * 更新单个群信息
     */
    String TAG_UPDATE_GROUP_INFO = "TAG_UPDATE_GROUP_INFO";
    /**
     * 更新群列表
     */
    String TAG_UPDATE_GROUP_LIST = "TAG_UPDATE_GROUP_LIST";
    /**
     * 更新单个群成员
     */
    String TAG_GROUP_MEMBER_LIST = "GROUP_MEMBER_LIST";

    /**
     * 更新单个好友
     */
    String TAG_UPDATE_SINGLE_FRIEND = "UPDATE_SINGLE_FRIEND";

    /**
     * 删除群
     */
    String TAG_DELETED_GROUP = "TAG_DELETED_GROUP";

    /**
     * 删除好友
     */
    String TAG_DELETED_FRIEND = "TAG_DELETED_FRIEND";

    /**
     * 清除聊天记录
     */
    String TAG_DELETED_CHAT_RECORD = "TAG_DELETED_CHAT_RECORD";
}
