package com.fengshengchat.db.helper;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.db.Account;
import com.fengshengchat.db.AccountDao;
import com.fengshengchat.db.DbManager;
import com.fengshengchat.net.RetrofitClient;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.msg.MsgOuterClass;

/**
 * 获取好友数据
 */
public class AccountHelper implements RequestHelper {

    private final long updateTime;
    private final OkHttpClient client;

    public AccountHelper(long updateTime) {
        this.updateTime = updateTime;
        this.client = new RetrofitClient().getClient();
    }

    /**
     * 请求好友列表
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint("CheckResult")
    private void requestAccountList(int pageNum) {
        okhttp3.Request request = RequestHelper.getRequest(getRequestMsg(pageNum));

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                return;
            }

            ResponseBody content = response.body();
            if (content == null) {
                return;
            }

            String string = content.string();
            if (TextUtils.isEmpty(string)) {
                return;
            }

            com.fengshengchat.net.bean.Response rsp = new Gson().fromJson(string, com.fengshengchat.net.bean.Response.class);
            MsgOuterClass.Msg outerMsg = rsp.getMessage();
            if (outerMsg == null) {
                return;
            }

            Friend.GetIncrementFriendListRsp data = Friend.GetIncrementFriendListRsp.parseFrom(outerMsg.getBody());
            if (data.getResult() != Friend.GetIncrementFriendListRsp.Result.OK_VALUE) {
                return;
            }

            int count = data.getFriendListCount();
            if (count == 0) {
                return;
            }

            saveAccountList(data.getFriendListList());

            if (count == AppConfig.PAGE_SIZE) {
                requestAccountList(pageNum + 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private Request getRequestMsg(int pageNum) {
        Friend.GetIncrementFriendListReq.Builder body = Friend.GetIncrementFriendListReq
                .newBuilder()
                .setStart(pageNum * AppConfig.PAGE_SIZE)
                .setNum(AppConfig.PAGE_SIZE)
                .setUpdateTime(updateTime);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Friend.FriendYimCMD.GET_INCREMET_FRIEND_LIST_REQ_CMD_VALUE)
                .setBody(body);

        return new Request(pbMessage);
    }

    /**
     * 处理获取好友列表请求
     */
    private void saveAccountList(List<Friend.FriendUserInfo> friendListList) {
        AccountDao accountDao = DbManager.getDb().getAccountDao();
        for (Friend.FriendUserInfo userInfo : friendListList) {
            Account account = getAccount(userInfo);
            if (accountDao.findById(account.id) == null) {
                accountDao.save(account);
            } else {
                accountDao.update(account);
            }
        }
    }

    /**
     * UserInfo 转 Account
     */
    @NonNull
    public static Account getAccount(Friend.FriendUserInfo userInfo) {
        Account account = new Account();
        account.id = userInfo.getUserId();
        account.nickname = userInfo.getNickName();
        account.avatar = userInfo.getSmallAvatar();
        account.bigAvatar = userInfo.getBigAvatar();
        account.isBlacklist = userInfo.getBlackType() == 1;
        account.isMarkStar = userInfo.getStarredType() == 1;
        account.gender = userInfo.getSex();
        account.isPinSession = userInfo.getOnTop() == 1;
        account.notDisturb = userInfo.getMuteNotification() == 1;
        account.updatedTime = userInfo.getUpdateTime();
        account.province = userInfo.getProvince();
        account.city = userInfo.getCity();
        account.area = userInfo.getArea();
        account.isDeleted = userInfo.getDelete() == 1;
        return account;
    }

    @Override
    public void start() {
        requestAccountList(0);
    }
}
