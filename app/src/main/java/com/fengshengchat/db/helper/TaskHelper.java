package com.fengshengchat.db.helper;

import android.support.annotation.NonNull;

import com.blankj.utilcode.util.LogUtils;
import com.hwangjr.rxbus.RxBus;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.db.DbManager;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class TaskHelper {

    private TaskHelper() {
    }

    private final Queue<Task> queue = new LinkedBlockingQueue<>();

    public void post(int taskId) {
        post(new Task(taskId));
    }

    public void post(Task task) {
        queue.offer(task);
        if (!isWorking) {
            start();
        }
    }

    private volatile boolean isWorking;

    private synchronized void start() {
        if (isWorking) {
            return;
        }

        Observable.fromCallable(() -> {
            handle();
            return TaskHelper.this;
        }).compose(RxSchedulersHelper.applyIOSchedulers())
                .subscribe(new Observer<TaskHelper>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        isWorking = true;
                    }

                    @Override
                    public void onNext(TaskHelper taskHelper) {
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e(e);
                        isWorking = false;
                    }

                    @Override
                    public void onComplete() {
                        isWorking = false;
                    }
                });
    }

    private void handle() {
        Task task = queue.poll();
        if (task == null) {
            return;
        }

        switch (task.getId()) {
            case Task.TASK_UPDATE_FRIEND: {
                long latestUpdatedTime = DbManager.getDb().getAccountDao().getLatestUpdatedTime();
                new AccountHelper(latestUpdatedTime).start();
                RxBus.get().post(BusEvent.TAG_UPDATE_FRIEND, Boolean.TRUE);
                break;
            }

            case Task.TASK_UPDATE_GROUP_INFO: {
                Long id = (long) task.getData();
                new GroupInfoHelper(id).start();
                long latestUpdatedTime = DbManager.getDb().getGroupMemberDao().getLatestUpdatedTime(id);
                new GroupMemberHelper(id, latestUpdatedTime).start();
                RxBus.get().post(BusEvent.TAG_UPDATE_GROUP_INFO, id);
                break;
            }

            case Task.TASK_UPDATE_GROUP_LIST: {
                long latestUpdatedTime = DbManager.getDb().getGroupDao().getLatestUpdatedTime();
                new GroupInfoListHelper(latestUpdatedTime).start();
                RxBus.get().post(BusEvent.TAG_UPDATE_GROUP_LIST, Boolean.TRUE);
                break;
            }

            case Task.TASK_GROUP_MEMBER_LIST: {
                Long id = (long) task.getData();
                long latestUpdatedTime = DbManager.getDb().getGroupMemberDao().getLatestUpdatedTime(id);
                new GroupMemberHelper(id, latestUpdatedTime).start();
                RxBus.get().post(BusEvent.TAG_GROUP_MEMBER_LIST, id);
                break;
            }

            default:
                break;
        }

        handle();
    }

    private static final class SingletonHolder {
        private static final TaskHelper INSTANCE = new TaskHelper();
    }

    @NonNull
    public static TaskHelper getInstance() {
        return SingletonHolder.INSTANCE;
    }

}
