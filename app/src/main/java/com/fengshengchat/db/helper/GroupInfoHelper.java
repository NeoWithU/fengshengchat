package com.fengshengchat.db.helper;

import android.annotation.SuppressLint;

import com.blankj.utilcode.util.LogUtils;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;

import yiproto.yichat.group.Group;

public class GroupInfoHelper implements RequestHelper {

    private final long groupId;

    public GroupInfoHelper(long groupId) {
        this.groupId = groupId;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint("CheckResult")
    @Override
    public void start() {
        Group.GetGroupInfoReq.Builder body = Group.GetGroupInfoReq
                .newBuilder()
                .addGroupIdList(groupId);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Group.GroupYimCMD.GET_GROUP_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Group.GetGroupInfoRsp.parseFrom(msg.getBody()))
                .filter(data -> data.getResult() == Group.GetGroupInfoRsp.GetGroupInfoResult.OK_VALUE && data.getGroupInfoListCount() != 0)
                .subscribe(data -> GroupInfoListHelper.saveGroup(data.getGroupInfoListList()), LogUtils::e);
    }

}
