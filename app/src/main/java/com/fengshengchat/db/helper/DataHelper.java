package com.fengshengchat.db.helper;

import android.support.annotation.NonNull;

import com.fengshengchat.db.DbManager;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class DataHelper {

    public Observable<Boolean> getDataObservable() {
        return Observable.combineLatest(getAccountObservable(), getGroupObservable(), (b1, b2) -> b1 && b2);
    }

    private Observable<Boolean> getAccountObservable() {
        return Observable.fromCallable(this::getAccount)
                .onErrorReturnItem(Boolean.FALSE)
                .subscribeOn(Schedulers.io());
    }

    @NonNull
    private Boolean getAccount() {
        long latestUpdatedTime = DbManager.getDb().getAccountDao().getLatestUpdatedTime();
        new AccountHelper(latestUpdatedTime).start();
        return Boolean.TRUE;
    }

    private Observable<Boolean> getGroupObservable() {
        return Observable.fromCallable(this::getGroup)
                .onErrorResumeNext((Function<Throwable, ObservableSource<Boolean>>) throwable -> Observable.just(Boolean.FALSE))
                .map(aBoolean -> DbManager.getDb().getGroupDao().findAllId())
                .doOnNext(this::requestGroupMember)
                .map(groups -> Boolean.TRUE)
                .onErrorReturnItem(Boolean.FALSE)
                .observeOn(Schedulers.io());
    }

    @NonNull
    private Boolean getGroup() {
        long latestUpdatedTime = DbManager.getDb().getGroupDao().getLatestUpdatedTime();
        new GroupInfoListHelper(latestUpdatedTime).start();
        return Boolean.TRUE;
    }

    private void requestGroupMember(long[] ids) {
        for (long id : ids) {
            long latestUpdatedTime = DbManager.getDb().getGroupMemberDao().getLatestUpdatedTime(id);
            new GroupMemberHelper(id, latestUpdatedTime).start();
        }
    }

    public static Observable<Boolean> getBaseDataObservable() {
        return new DataHelper().getDataObservable();
    }
}
