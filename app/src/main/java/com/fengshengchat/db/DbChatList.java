package com.fengshengchat.db;

import android.util.LongSparseArray;

import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.utils.TimeUtils;
import com.fengshengchat.protocol.DataTypeConverter;
import com.fengshengchat.test.RefreshTEST;

import java.util.List;

import yiproto.yichat.chat.Chat;
import yiproto.yichat.groupchat.Groupchat;

public class DbChatList {

    public static List<ChatListBean> getChatList(){
        return DbManager.getChatListDao().getChatList();
    }

    public static List<ChatListBean> getChatListWithTempEmptyGroup(){
        return DbManager.getChatListDao().getChatListWithTempEmptyGroup();
    }

    public static ChatListBean getChatItem(long hisUid){
        return DbManager.getChatListDao().getChatItem(hisUid);
    }

    public static void insertChatItem(ChatListBean chatListBean){
        Debug.e("DbChatList", "------insert: " + chatListBean);
//        if(null == chatListBean.name || chatListBean.name.length() <= 0){
//            chatListBean.name = chatListBean.hisUid + "";
//        }
        DbManager.getChatListDao().insertChatItem(chatListBean);
    }

    public static void insertChatList(List<ChatListBean> list){
        //TODO
        for(ChatListBean bean : list) {
            insertChatItem(bean);
        }
    }

    public static void insertEmptyGroupItem(ChatListBean chatListBean){
//        chatListBean.lastMessage = "SearchEmpty";
        chatListBean.isTempEmptyGroup = true;
        chatListBean.time = TimeUtils.getTimeSeconds();
        insertChatItem(chatListBean);
    }

    public static void deleteChatItem(long hisUid){
        DbManager.getChatListDao().deleteChatItem(hisUid);
    }

    public static void updateChatItem(ChatListBean chatListBean){
        DbManager.getChatListDao().updateChatItem(chatListBean);
    }

    public static void updateChatItem(int increaseUnreadCount, ChatListBean chatListBean){
        //TODO 需要删除再插入的方式更新，以便时间排序
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(chatListBean.hisUid);
        if(null != chatItem){
            chatListBean.id = chatItem.id;
            chatListBean.unreadCount = chatItem.unreadCount + increaseUnreadCount;
            updateChatItem(chatListBean);
        }else{
            chatListBean.unreadCount = increaseUnreadCount;
            if(Constant.CHAT_TYPE_SINGLE == chatListBean.chatType){
                Account friend = DbFriend.getFriend(chatListBean.hisUid);
                if(null != friend) {
                    chatListBean.name = friend.nickname;
                    chatListBean.icon = friend.avatar;
                    chatListBean.isPin = friend.isPinSession;
                    chatListBean.disableNotify = friend.notDisturb;
                }
            }else{
                Group groupInfo = DbFriend.getGroupInfo(chatListBean.hisUid);
                if(null != groupInfo){
                    chatListBean.name = groupInfo.name;
                    chatListBean.icon = groupInfo.avatar;
                    chatListBean.groupType = groupInfo.type;
                    chatListBean.isPin = groupInfo.isPinSession;
                    chatListBean.disableNotify = groupInfo.notDisturb;
                }
            }
            insertChatItem(chatListBean);
        }
    }

    public static void updateChatItemPin(long hisUid, boolean pin){
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(hisUid);
        if(null != chatItem){
            chatItem.isPin = pin;
            updateChatItem(chatItem);
        }
    }

    public static void updateChatItemNotify(long hisUid, boolean notify){
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(hisUid);
        if(null != chatItem){
            chatItem.disableNotify = notify;
            updateChatItem(chatItem);
        }
    }

    public static void updateChatItemKeepState(int increaseUnreadCount, ChatListBean chatListBean){
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(chatListBean.hisUid);
        if(null != chatItem){
            chatListBean.isPin = chatItem.isPin;
            chatListBean.disableNotify = chatItem.disableNotify;
        }
        updateChatItem(increaseUnreadCount, chatListBean);
    }

    public static void updateChatItemKeepStateAndName(int increaseUnreadCount, ChatListBean chatListBean){
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(chatListBean.hisUid);
        if(null != chatItem){
            chatListBean.isPin = chatItem.isPin;
            chatListBean.disableNotify = chatItem.disableNotify;
            chatListBean.name = chatItem.name;
            chatListBean.icon = chatItem.icon;
        }
        updateChatItem(increaseUnreadCount, chatListBean);
    }

    public static void updateChatItemKeepStateAndNameAndContent(int increaseUnreadCount, ChatListBean chatListBean){
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(chatListBean.hisUid);
        if(null != chatItem){
            chatListBean.isPin = chatItem.isPin;
            chatListBean.disableNotify = chatItem.disableNotify;
            chatListBean.name = chatItem.name;
            chatListBean.icon = chatItem.icon;
            chatListBean.lastMessage = chatItem.lastMessage;
            chatListBean.time = chatItem.time;
        }
        updateChatItem(increaseUnreadCount, chatListBean);
    }

    public static void updateChatItemSingleInfo(Account friend){
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(friend.id);
        if(null != chatItem){
            chatItem.name = friend.nickname;
            chatItem.icon = friend.avatar;
            DbManager.getChatListDao().updateChatItem(chatItem);
        }else{
//            chatItem = new ChatListBean();
//            chatItem.hisUid = group.id;
//            chatItem.name = group.name;
//            chatItem.icon = group.avatar;
//            chatItem.time = TimeUtils.getTimeSeconds();
//            insertChatItem(chatItem);
        }
    }

    public static void updateChatItemGroupInfo(Group group){
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(group.id);
        if(null != chatItem){
            chatItem.name = group.name;
            chatItem.icon = group.avatar;
            DbManager.getChatListDao().updateChatItem(chatItem);
        }else{
//            chatItem = new ChatListBean();
//            chatItem.hisUid = group.id;
//            chatItem.name = group.name;
//            chatItem.icon = group.avatar;
//            chatItem.time = TimeUtils.getTimeSeconds();
//            insertChatItem(chatItem);
        }
    }

    public static void updateAlreadyReadSequence(long hisUid, long newMessageSequence){
        DbManager.getChatListDao().updateAlreadyReadSequence(hisUid, newMessageSequence);
    }

    public static void updateItemFriendInfo(Account friend){
        if(null != friend) {
            DbManager.getChatListDao().updateItemFriendInfo(friend.id, friend.nickname, friend.avatar);
        }
    }

    public static void updateItemGroupInfo(Group group){
        if(null != group) {
            DbManager.getChatListDao().updateItemFriendInfo(group.id, group.name, group.avatar);
        }
    }

    public static void updateLastSequence(long hisUid, long lastSequence){
        DbManager.getChatListDao().updateLastSequence(hisUid, lastSequence);
    }

    public static void updateLastSequence(LongSparseArray<Long> list) {
        for(int i = 0; i < list.size(); i++){
            updateLastSequence(list.keyAt(i), list.valueAt(i));
        }
    }

    public static void updateGroupOnPullBack(List<Groupchat.GroupChatMsg> listList){
        int normalMsgSize = listList.size();
        Groupchat.GroupChatMsg last = listList.get(listList.size() - 1);
        Groupchat.GroupChatMsg find = null;
        Groupchat.GroupChatMsg msg;
        RefreshTEST.testPoint("拉取到群广播");
//        for(; normalMsgSize > 0; normalMsgSize--){
//            msg = listList.get(normalMsgSize - 1);
//            if(Chat.MsgType.MSGTYPE_GROUP_SYSTEM_VALUE != msg.getMsgType()){
//                find = msg;
//                break;
//            }
//        }



        if(Chat.MsgType.MSGTYPE_GROUP_SYSTEM_VALUE == last.getMsgType()){
            normalMsgSize--;
        }

        ChatListBean temp;

        find = last;
        if(null != find){
            temp = DataTypeConverter.convertChatListBean(find);
            temp.alreadyPullSequence = last.getMsgSeq();
            temp.lastReceiveSequence = last.getMsgSeq();
            updateOnPullBack(normalMsgSize, temp);
        }else{
            temp = DataTypeConverter.convertChatListBean(last);
            updateChatItemKeepStateAndNameAndContent(0, temp);
        }
    }

    public static void updateOnPullBack(int increaseUnreadCount, ChatListBean chatListBean){
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(chatListBean.hisUid);
        if(null != chatItem){
            chatListBean.isPin = chatItem.isPin;
            chatListBean.disableNotify = chatItem.disableNotify;
            chatListBean.lastReceiveSequence = chatItem.lastReceiveSequence;
        }
        updateChatItem(increaseUnreadCount, chatListBean);
    }

    public static void updateOnDeleteLastChatRecord(long hisUid){
        List<ChatRecord> chatRecords = DbManager.getRecordDao().getChatRecords(hisUid, 0, 1);
        ChatListBean chatItem = DbManager.getChatListDao().getChatItem(hisUid);

        if(null != chatRecords && chatRecords.size() > 0){
            ChatRecord record = chatRecords.get(0);
            chatItem.messageType = record.messageType;
            chatItem.lastMessage = record.content;
            chatItem.time = record.time;
        }else{
            chatItem.messageType = Chat.MsgType.MSGTYPE_TEXT_VALUE;
            chatItem.lastMessage = "";
            chatItem.time = TimeUtils.getTimeSeconds();
        }
        DbManager.getChatListDao().updateChatItem(chatItem);
    }

}
