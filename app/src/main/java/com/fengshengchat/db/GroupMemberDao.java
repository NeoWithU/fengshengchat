package com.fengshengchat.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.fengshengchat.app.model.SearchGroupMember;

import java.util.List;

@Dao
public interface GroupMemberDao {

    @Insert
    long save(GroupMember groupMember);

    @Query("SELECT * FROM group_member where group_id = :groupId and is_deleted = 0")
    List<GroupMember> findAll(long groupId);

    @Query("SELECT * FROM group_member WHERE group_id = :groupId AND user_id = :userId AND is_deleted = :deleteFlag")
    GroupMember find(long groupId, long userId, int deleteFlag);

    @Query("SELECT * FROM group_member WHERE group_id = :groupId AND user_id = :userId")
    GroupMember find(long groupId, long userId);

    @Update
    int update(GroupMember groupMember);

    @Query("select member.user_id as id, member.nickname, `group`.name as groupName, `group`.avatar, group_id as groupId  " +
            "from `group_member` as member, `group` " +
            "where member.group_id = `group`.id and member.nickname like '%' || :nickname || '%' and member.user_id != :selfId and is_deleted = 0 " +
            "limit 4")
    List<SearchGroupMember> searchByNickname(String nickname, long selfId);

    @Query("select member.user_id as id, member.nickname, `group`.name as groupName, `group`.avatar, group_id as groupId  " +
            "from `group_member` as member, `group` " +
            "where member.group_id = `group`.id and member.user_id != :selfId and is_deleted = 0 " +
            "and (member.nickname like '%' || :key || '%' or cast(member.user_id as text) like '%' || :key || '%')" +
            "limit 4")
    List<SearchGroupMember> searchByNicknameOrId(String key, long selfId);

    @Query("select member.user_id as id, member.nickname, `group`.name as groupName, `group`.avatar, group_id as groupId  " +
            "from `group_member` as member, `group` " +
            "where member.group_id = `group`.id and member.nickname like '%' || :nickname || '%' and member.user_id != :selfId and is_deleted = 0 ")
    List<SearchGroupMember> searchAllByNickname(String nickname, long selfId);

    @Query("select member.user_id as id, member.nickname, `group`.name as groupName, `group`.avatar, group_id as groupId " +
            "from `group_member` as member, `group` " +
            "where member.group_id = `group`.id and member.user_id != :selfId and is_deleted = 0 " +
            "and (member.nickname like '%' || :key || '%' or cast(member.user_id as text) like '%' || :key || '%')")
    List<SearchGroupMember> searchAllByNicknameOrId(String key, long selfId);

    @Query("SELECT count(id) FROM group_member WHERE group_id = :groupId and is_deleted = 0")
    int getGroupMemberCount(long groupId);

    @Query("delete from group_member where group_id = :groupId")
    void deleteGroupMember(long groupId);

    @Query("select max(updated_time) from `group_member` where group_id = :groupId")
    long getLatestUpdatedTime(long groupId);

}