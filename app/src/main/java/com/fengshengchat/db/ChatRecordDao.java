package com.fengshengchat.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ChatRecordDao {

    @Insert
    void insert(ChatRecord record);

    @Query("SELECT * FROM chat_record WHERE his_uid = :toUid ORDER BY id DESC LIMIT 0, 1")
    ChatRecord getChatLastRecord(long toUid);

    @Query("SELECT * FROM chat_record WHERE his_uid = :toUid ORDER BY id DESC LIMIT :offset, :size")
    List<ChatRecord> getChatRecords(long toUid, int offset, int size);

    @Query("UPDATE chat_record SET msg_seq = :serverSeq, msg_state = :state " +
            "WHERE msg_seq = :localSeq")
    void updateMessageSendState(long localSeq, long serverSeq, int state);

    @Query("UPDATE chat_record SET has_voice_play = 1 WHERE his_uid = :hisUid AND msg_seq = :msgSeq")
    void updateVoiceReadState(long hisUid, long msgSeq);

    @Query("DELETE FROM chat_record WHERE msg_seq = :sendSeq AND his_uid = :hisUid")
    void deleteOneChatRecord(long hisUid, long sendSeq);

    @Query("DELETE FROM chat_record WHERE his_uid = :hisUid")
    void deleteAllChatRecord(long hisUid);

}
