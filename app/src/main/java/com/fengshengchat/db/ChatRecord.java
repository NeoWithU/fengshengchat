package com.fengshengchat.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "chat_record")
public class ChatRecord {
    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "chat_type")
    public int chatType;

    @Embedded(prefix = "my_")
    public User my;

    @Embedded(prefix = "his_")
    public User his;

    @ColumnInfo(name = "msg_seq")
    public long messageSequence;

    @ColumnInfo(name = "content")
    public String content;

    @ColumnInfo(name = "raw_content")
    public String rawContent; //image or voice ..

    @ColumnInfo(name = "msg_type")
    public int messageType;

    @ColumnInfo(name = "show_type")
    public int showType; //just for fun group

    @ColumnInfo(name = "msg_state")
    public int messageState; // 0: nor, 1: sending, 2: failed

    @ColumnInfo(name = "time")
    public long time;

    @ColumnInfo(name = "is_send_msg")
    public boolean isSendMessage = false; // is 发出去的消息?

    @ColumnInfo(name = "group_type")
    public int groupType; //if is group chat, record group type Group.GROUP_TYPE

    @Ignore
    public boolean isVoicePlaying;

    @ColumnInfo(name = "has_voice_play")
    public boolean hasVoicePlay;

    @ColumnInfo(name = "duration")
    public int duration;

    @ColumnInfo(name = "ext_1")
    public String ext_1;
    @ColumnInfo(name = "ext_2")
    public String ext_2;
    @ColumnInfo(name = "ext_3")
    public String ext_3;
    @ColumnInfo(name = "ext_4")
    public String ext_4;
    @ColumnInfo(name = "ext_5")
    public String ext_5;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getChatType() {
        return chatType;
    }

    public void setChatType(int chatType) {
        this.chatType = chatType;
    }

    public User getMy() {
        return my;
    }

    public void setMy(User my) {
        this.my = my;
    }

    public User getHis() {
        return his;
    }

    public void setHis(User his) {
        this.his = his;
    }

    public long getMessageSequence() {
        return messageSequence;
    }

    public void setMessageSequence(long messageSequence) {
        this.messageSequence = messageSequence;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getMessageState() {
        return messageState;
    }

    public void setMessageState(int messageState) {
        this.messageState = messageState;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isSendMessage() {
        return isSendMessage;
    }

    public void setSendMessage(boolean sendMessage) {
        isSendMessage = sendMessage;
    }

    @Override
    public String toString() {
        return "ChatRecord{" +
                "id=" + id +
                ", chatType=" + chatType +
                ", my=" + my +
                ", his=" + his +
                ", messageSequence=" + messageSequence +
                ", content='" + content + '\'' +
                ", rawContent='" + rawContent + '\'' +
                ", messageType=" + messageType +
                ", showType=" + showType +
                ", messageState=" + messageState +
                ", time=" + time +
                ", isSendMessage=" + isSendMessage +
                ", groupType=" + groupType +
                ", isVoicePlaying=" + isVoicePlaying +
                ", hasVoicePlay=" + hasVoicePlay +
                ", duration=" + duration +
                ", ext_1='" + ext_1 + '\'' +
                ", ext_2='" + ext_2 + '\'' +
                ", ext_3='" + ext_3 + '\'' +
                ", ext_4='" + ext_4 + '\'' +
                ", ext_5='" + ext_5 + '\'' +
                '}';
    }
}
