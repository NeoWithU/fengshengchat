package com.fengshengchat.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.Objects;

/**
 * 账户信息
 */
@Entity(tableName = "account", indices = {@Index(value = "updated_time")})
public class Account implements Parcelable {

    /**
     * 用户id
     */
    @PrimaryKey
    public long id;

    /**
     * 昵称
     */
    public String nickname;

    /**
     * 头像小图
     */
    public String avatar;

    /**
     * 头像大图
     */
    @ColumnInfo(name = "big_avatar")
    public String bigAvatar;

    /**
     * 是否在黑名单中
     */
    @ColumnInfo(name = "is_blacklist")
    public boolean isBlacklist;

    /**
     * 是否标星
     */
    @ColumnInfo(name = "is_mark_star")
    public boolean isMarkStar;

    /**
     * Gender
     */
    public int gender;

    /**
     * 是否置顶聊天
     */
    @ColumnInfo(name = "is_pin_session")
    public boolean isPinSession;

    /**
     * 消息免打扰
     */
    @ColumnInfo(name = "not_disturb")
    public boolean notDisturb;

    /**
     * 更新时间
     */
    @ColumnInfo(name = "updated_time")
    public long updatedTime;

    /**
     * 是否已经删除
     */
    @ColumnInfo(name = "is_deleted")
    public boolean isDeleted;

    /**
     * 省
     */
    @ColumnInfo(name = "province")
    public String province;
    /**
     * 市
     */
    @ColumnInfo(name = "city")
    public String city;
    /**
     * 区
     */
    @ColumnInfo(name = "area")
    public String area;

    public Account() {
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    @Override
    public String toString() {
        return nickname;
    }

    @Nullable
    public String getAddress() {
        return getAddress(province, city);
    }

    @Nullable
    public static String getAddress(String province, String city) {
//        if (TextUtils.isEmpty(province)) {
//            return null;
//        }

        return province + " " + city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.nickname);
        dest.writeString(this.avatar);
        dest.writeString(this.bigAvatar);
        dest.writeByte(this.isBlacklist ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isMarkStar ? (byte) 1 : (byte) 0);
        dest.writeInt(this.gender);
        dest.writeByte(this.isPinSession ? (byte) 1 : (byte) 0);
        dest.writeByte(this.notDisturb ? (byte) 1 : (byte) 0);
        dest.writeLong(this.updatedTime);
        dest.writeByte(this.isDeleted ? (byte) 1 : (byte) 0);
        dest.writeString(this.province);
        dest.writeString(this.city);
        dest.writeString(this.area);
    }

    protected Account(Parcel in) {
        this.id = in.readLong();
        this.nickname = in.readString();
        this.avatar = in.readString();
        this.bigAvatar = in.readString();
        this.isBlacklist = in.readByte() != 0;
        this.isMarkStar = in.readByte() != 0;
        this.gender = in.readInt();
        this.isPinSession = in.readByte() != 0;
        this.notDisturb = in.readByte() != 0;
        this.updatedTime = in.readLong();
        this.isDeleted = in.readByte() != 0;
        this.province = in.readString();
        this.city = in.readString();
        this.area = in.readString();
    }

}
