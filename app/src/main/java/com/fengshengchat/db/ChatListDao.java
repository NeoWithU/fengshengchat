package com.fengshengchat.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ChatListDao {

    @Query("SELECT * FROM chat_list WHERE is_temp_empty_group = 0 ORDER BY is_pin DESC, time DESC")
    List<ChatListBean> getChatList();

    @Query("SELECT * FROM chat_list ORDER BY time DESC")
    List<ChatListBean> getChatListWithTempEmptyGroup();

    @Query("UPDATE chat_list SET already_pull_seq = :newMessageSequence, last_receive_seq =:newMessageSequence, " +
            "unread_count = 0 WHERE his_uid = :hisUid")
    void updateAlreadyReadSequence(long hisUid, long newMessageSequence);

    @Query("UPDATE chat_list SET already_pull_seq = :pullSequence, unread_count = :unreadCount" +
            " WHERE his_uid = :hisUid")
    void updateAlreadyPullSequence(long hisUid, long pullSequence, int unreadCount);

    @Query("UPDATE chat_list SET last_receive_seq = :lastSequence WHERE his_uid = :hisUid")
    void updateLastSequence(long hisUid, long lastSequence);

    @Query("UPDATE chat_list SET name = :nickname, icon = :iconUrl WHERE his_uid = :hidUid")
    void updateItemFriendInfo(long hidUid, String nickname, String iconUrl);

    @Update
    void updateChatItem(ChatListBean chatListBean);

    @Query("SELECT * FROM chat_list WHERE his_uid = :hisUid")
    ChatListBean getChatItem(long hisUid);

    @Query("DELETE FROM chat_list WHERE his_uid = :hisUid")
    void deleteChatItem(long hisUid);

    @Insert
    void insertChatItem(ChatListBean chatListBean);

}
