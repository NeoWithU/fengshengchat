package com.fengshengchat.db;

import com.fengshengchat.chat.view.ChatActivity;
import com.fengshengchat.protocol.DataTypeConverter;
import com.fengshengchat.user.UserManager;

import java.util.List;

import yiproto.yichat.chat.Chat;
import yiproto.yichat.friend.Friend;
import yiproto.yichat.groupchat.Groupchat;

public class DbChatRecord {

    public static void saveSendMessage(ChatRecord chatRecord){
        DbManager.getRecordDao().insert(chatRecord);
    }

    public static void saveReceiveSingleChatRecord(Chat.ChatMsg chatMsg){
        boolean isSend = chatMsg.getFromUid() == UserManager.getUser().uid;
        ChatRecord record = DataTypeConverter.convertChatRecord(Constant.CHAT_TYPE_SINGLE, isSend, chatMsg);
        DbManager.getRecordDao().insert(record);
    }

    public static void saveReceiveSingleChatRecord(List<Chat.ChatMsg> list){
        for(int i = 0; i < list.size(); i++){
            saveReceiveSingleChatRecord(list.get(i));
        }
    }

    public static void saveReceiveGroupChatRecord(Groupchat.GroupChatMsg groupChatMsg){
        boolean isSend = groupChatMsg.getUid() == UserManager.getUser().uid;
        ChatRecord record = DataTypeConverter.convertChatRecord(Constant.CHAT_TYPE_GROUP, isSend, groupChatMsg);
        DbManager.getRecordDao().insert(record);
    }

    public static void saveReceiveGroupChatRecord(List<Groupchat.GroupChatMsg> list){
        for(int i = 0; i < list.size(); i++){
            saveReceiveGroupChatRecord(list.get(i));
        }
    }

    public static void saveReceiveGroupBroadcastRecord(Groupchat.GroupSystemBroadcast groupBroadcast){
        ChatRecord record = DataTypeConverter.convertChatRecord(groupBroadcast);
        DbManager.getRecordDao().insert(record);
    }

    public static void saveAddFriend(Friend.NotifyUpdateReq.FriendGreetingSystemMsg greetingMsg){
        ChatRecord record = DataTypeConverter.convertChatRecord(greetingMsg);
        DbManager.getRecordDao().insert(record);
    }

    public static ChatRecord getChatLastRecord(long hisUid){
        return DbManager.getRecordDao().getChatLastRecord(hisUid);
    }

    public static List<ChatRecord> getChatRecords(long toUid, int offset){
        return DbManager.getRecordDao().getChatRecords(toUid, offset, ChatActivity.LOAD_PAGE_SIZE);
    }

    public static void deleteOneChatRecord(ChatRecord chatRecord){
        DbManager.getRecordDao().deleteOneChatRecord(chatRecord.his.uid, chatRecord.messageSequence);
    }

    public static void deleteAllChatRecord(long hisUid){
        DbManager.getRecordDao().deleteAllChatRecord(hisUid);
    }

    public static void updateSendResultState(long localSeq, long serverSeq, int state){
        DbManager.getRecordDao().updateMessageSendState(localSeq, serverSeq, state);
    }

    public static void updateVoiceReadState(long hisUid, long msgSeq){
        DbManager.getRecordDao().updateVoiceReadState(hisUid, msgSeq);
    }

}
