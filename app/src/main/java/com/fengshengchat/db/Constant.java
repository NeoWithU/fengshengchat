package com.fengshengchat.db;

import yiproto.yichat.group.Group;

public class Constant {
    public static final int CHAT_TYPE_SINGLE = 1;
    public static final int CHAT_TYPE_GROUP = 2;
    public static final int CHAT_TYPE_SEARCH = 3;

    public static final int CHAT_TYPE_FUN_GROUP = Group.GROUP_TYPE.GROUP_TYPE_SPORTFUL_VALUE;

//    public static final int MESSAGE_TYPE_TEXT = 3;
//    public static final int MESSAGE_TYPE_IMAGE = 4;
//    public static final int MESSAGE_TYPE_VOICE = 5;
    public static final int MESSAGE_TYPE_TIP = 201;
    public static final int MESSAGE_TYPE_LOAD_DATA = 202;

    public static final int MESSAGE_STATE_OK = 6;
    public static final int MESSAGE_STATE_FAILED = 7;
    public static final int MESSAGE_STATE_SENDING = 8;

    public static final int INTERNAL_CMD_TYPE = -100;
    public static final int INTERNAL_CMD_SOCKET_CONNECTING = 1000;
    public static final int INTERNAL_CMD_SOCKET_CONNECT_FALIED = 1001;
    public static final int INTERNAL_CMD_SOCKET_CONNECT_SUCCESS = 1002;

    public static final int INTERNAL_CMD_REFRESH_GROUP_INFO = 2000;

}
