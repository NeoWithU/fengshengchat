package com.fengshengchat.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "chat_list")
public class ChatListBean {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "chat_type")
    public int chatType;

    @ColumnInfo(name = "group_type")
    public int groupType;

    @ColumnInfo(name = "msg_type")
    public int messageType;

    @ColumnInfo(name = "his_uid")
    public long hisUid;

    public String name;

    public String icon;

    @ColumnInfo(name = "last_message")
    public String lastMessage;

    public long time;

    @ColumnInfo(name = "is_pin")
    public boolean isPin;

    @ColumnInfo(name = "already_pull_seq")
    public long alreadyPullSequence;

    @ColumnInfo(name = "last_receive_seq")
    public long lastReceiveSequence;

    @Ignore
    public int resultCount; //Just for server use

    @ColumnInfo(name = "unread_count")
    public int unreadCount;

    @ColumnInfo(name = "disable_notify")
    public boolean disableNotify;

    @ColumnInfo(name = "is_temp_empty_group")
    public boolean isTempEmptyGroup; //temp empty group, just create one record in DB for pull new message.

    @ColumnInfo(name = "group_member_count")
    public int groupMemberCount;

    @ColumnInfo(name = "ext_1")
    public String ext_1;
    @ColumnInfo(name = "ext_2")
    public String ext_2;
    @ColumnInfo(name = "ext_3")
    public String ext_3;
    @ColumnInfo(name = "ext_4")
    public String ext_4;
    @ColumnInfo(name = "ext_5")
    public String ext_5;

    @Override
    public String toString() {
        return "ChatListBean{" +
                "id=" + id +
                ", chatType=" + chatType +
                ", groupType=" + groupType +
                ", messageType=" + messageType +
                ", hisUid=" + hisUid +
                ", name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", lastMessage='" + lastMessage + '\'' +
                ", time=" + time +
                ", isPin=" + isPin +
                ", alreadyPullSequence=" + alreadyPullSequence +
                ", lastReceiveSequence=" + lastReceiveSequence +
                ", resultCount=" + resultCount +
                ", unreadCount=" + unreadCount +
                ", disableNotify=" + disableNotify +
                ", isTempEmptyGroup=" + isTempEmptyGroup +
                ", groupMemberCount=" + groupMemberCount +
                ", ext_1='" + ext_1 + '\'' +
                ", ext_2='" + ext_2 + '\'' +
                ", ext_3='" + ext_3 + '\'' +
                ", ext_4='" + ext_4 + '\'' +
                ", ext_5='" + ext_5 + '\'' +
                '}';
    }
}
