package com.fengshengchat.net;

/**
 * @author KRT
 * 2018/11/22
 */
public abstract class ObserverCallbackAdapter<T> extends DefaultObserverCallback<T> {

    @Override
    public void onError(Throwable e) {}

    @Override
    public void onComplete() {}

    @Override
    public void onFinally() {}

}
