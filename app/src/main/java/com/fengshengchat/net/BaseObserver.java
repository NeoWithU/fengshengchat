package com.fengshengchat.net;

import com.blankj.utilcode.util.LogUtils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public abstract class BaseObserver<T> implements Observer<T> {

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onError(Throwable e) {
        LogUtils.e(e);
    }

    @Override
    public void onComplete() {
    }
}
