package com.fengshengchat.net;

import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.utils.NetUtils;

import io.reactivex.functions.Predicate;

/**
 * @author KRT
 * 2018/11/22
 */
public class RetryStrategy implements Predicate<Throwable> {

    protected boolean canRetry(Throwable throwable){
        return NetUtils.isConnected();
    }

    @Override
    public final boolean test(Throwable throwable) throws Exception {
        Debug.d("RetryStrategy", "-----Retry----- " + throwable.getMessage());
        return canRetry(throwable);
    }
}
