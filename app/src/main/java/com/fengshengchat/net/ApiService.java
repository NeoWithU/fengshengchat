package com.fengshengchat.net;

import com.fengshengchat.account.model.Avatar;
import com.fengshengchat.net.bean.Response;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiService {

    @POST("web_gateway")
    @Headers("Content-Type:application/json; charset=utf-8")
    Observable<Response> post(@Body RequestBody body);

    /*
     * 头像上传
     */
    @Multipart
    @POST
    Observable<Avatar> uploadPicture(@Url String url, @Part MultipartBody.Part part1, @Part MultipartBody.Part part2);

}
