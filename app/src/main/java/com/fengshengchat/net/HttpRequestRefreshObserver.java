package com.fengshengchat.net;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.fengshengchat.base.HttpRequestObserver;

public abstract class HttpRequestRefreshObserver<T> extends HttpRequestObserver<T> {

    private final SmartRefreshLayout refreshLayout;

    public HttpRequestRefreshObserver(SmartRefreshLayout refreshLayout) {
        this.refreshLayout = refreshLayout;
    }

    @Override
    public void onComplete() {
        super.onComplete();
        this.refreshLayout.finishRefresh();
    }
}
