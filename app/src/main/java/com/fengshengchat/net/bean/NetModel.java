package com.fengshengchat.net.bean;

import com.google.gson.Gson;
import com.fengshengchat.base.mvp.IBaseModel;
import com.fengshengchat.net.RetrofitWrapper;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * @author KRT
 * 2018/11/22
 */
public class NetModel<A> implements IBaseModel {
    private A mAPI;
    protected Disposable disposable;

    public NetModel(Class<A> service){
        mAPI = RetrofitWrapper.getRetrofit().create(service);
    }

    public NetModel(Class<A> service, String baseUrl){
        mAPI = RetrofitWrapper.getRetrofit(baseUrl).create(service);
    }

    protected A getAPI(){
        return mAPI;
    }

    protected <T> void request(Observable<T> observable, Observer<T> callback){
        //TODO disposable is not init
        RetrofitWrapper.request(observable, callback);
    }

    protected RequestBody createJsonRequestBody(Request request){
        String json = new Gson().toJson(request);
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
    }

    private void cancel(){
        //TODO Retrofit cancel
        if(null != disposable && !disposable.isDisposed()){
            disposable.dispose();
        }
    }

    @Override
    public void onDestroy() {
        cancel();
    }
}
