package com.fengshengchat.net.bean;

import android.support.annotation.Nullable;

import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;

import yiproto.yichat.version_info.VersionInfoOuterClass;

/**
 * @author KRT
 * 2018/11/28
 */
public class Request {
    private String version_info;
    private String msg;

    public Request() {
        this(null);
    }

    public Request(@Nullable PBMessage pbMessage) {
        setVersion_info(getDefaultVersionInfo());
        if (pbMessage != null) {
            setMessage(pbMessage.PB());
        }
    }

    public void setVersion_info(VersionInfoOuterClass.VersionInfo version_info) {
        this.version_info = "";
        if (null != version_info) {
            this.version_info = encode(version_info.toByteArray());
        }
    }

    public void setMessage(byte[] message) {
        this.msg = "";
        if (null != message) {
            this.msg = encode(message);
        }
    }

    public String getVersionInfo(){
        return version_info;
    }

    public String getMsg(){
        return msg;
    }

    private String encode(byte[] input) {
        return Protocol.encodeBase64ToString(input);
    }

    private VersionInfoOuterClass.VersionInfo getDefaultVersionInfo() {
        VersionInfoOuterClass.VersionInfo.Builder version = VersionInfoOuterClass.VersionInfo.newBuilder();
        version.setClientVersion(1);
        version.setEncryptVersion(1);
        return version.build();
    }

}
