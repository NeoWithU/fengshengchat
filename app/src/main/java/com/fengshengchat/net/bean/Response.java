package com.fengshengchat.net.bean;

import com.fengshengchat.protocol.Protocol;

import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/11/28
 */
public class Response {
    private String msg;

    public MsgOuterClass.Msg getMessage(){
        try {
            return MsgOuterClass.Msg.parseFrom(decode(msg));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setRawMsg(String msg){
        this.msg = msg;
    }

    private byte[] decode(String input){
        return Protocol.decodeBase64(input.getBytes());
    }

}
