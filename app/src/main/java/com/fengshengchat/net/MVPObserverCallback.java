package com.fengshengchat.net;

import com.fengshengchat.base.mvp.IBaseView;

/**
 * @author KRT
 * 2018/11/29
 */
public abstract class MVPObserverCallback<T> extends DefaultObserverCallback<T> {
    private IBaseView mBaseView;

    public MVPObserverCallback(IBaseView view){
        mBaseView = view;
    }

    @Override
    public void onComplete() {
        if(null != mBaseView){
            mBaseView.clearErrorState();
        }
    }

    @Override
    public void onFinally() {
        if(null != mBaseView){
            mBaseView.onUpdateViewFinally();
        }
    }
}
