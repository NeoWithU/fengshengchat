package com.fengshengchat.net;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.net.bean.Response;
import com.fengshengchat.protocol.Protocol;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import yiproto.yichat.msg.MsgOuterClass;

public final class ApiManager {

    @NonNull
    private final ApiService apiService;

    public ApiManager() {
        apiService = RetrofitWrapper.getRetrofit(Protocol.getBusinessUrl())
                .create(ApiService.class);
    }

    private static final class SingletonHolder {
        private static final ApiManager INSTANCE = new ApiManager();
    }

    @NonNull
    public static ApiManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public Observable<MsgOuterClass.Msg> post(Request request) {
        String json = new Gson().toJson(request);
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
        return apiService.post(body).map(Response::getMessage);
    }

    @NonNull
    public ApiService getApiService() {
        return apiService;
    }
}

