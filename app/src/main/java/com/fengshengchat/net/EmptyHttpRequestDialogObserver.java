package com.fengshengchat.net;

import android.support.v4.app.FragmentManager;

import com.fengshengchat.base.HttpRequestDialogObserver;

public class EmptyHttpRequestDialogObserver extends HttpRequestDialogObserver<Object> {

    public EmptyHttpRequestDialogObserver(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public void onNext(Object o) {
    }
}
