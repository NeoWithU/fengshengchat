package com.fengshengchat.net;

import io.reactivex.disposables.Disposable;

/**
 * @author KRT
 * 2018/11/22
 */
public abstract class DefaultObserverCallback<T> implements IObserverCallback<T> {
    @Override
    public final void onSubscribe(Disposable d) {
        onStart();
    }

    @Override
    public final void onNext(T t) {
        onResponse(t);
    }

    public void onStart(){}

    public abstract void onResponse(T response);

    @Override
    public abstract void onError(Throwable e);

    @Override
    public abstract void onComplete();

    @Override
    public abstract void onFinally();

}
