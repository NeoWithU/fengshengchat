package com.fengshengchat.mine.bean;

import com.flyco.tablayout.listener.CustomTabEntity;
import com.fengshengchat.R;

/**
 * @author KRT
 * 2018/12/5
 */
public class MineTabEntity implements CustomTabEntity {
    private String title;

    public MineTabEntity(String title){
        this.title = title;
    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public int getTabSelectedIcon() {
        return R.mipmap.mine_sel;
    }

    @Override
    public int getTabUnselectedIcon() {
        return R.mipmap.mine_nor;
    }
}
