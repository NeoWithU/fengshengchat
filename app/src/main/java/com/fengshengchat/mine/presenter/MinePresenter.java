package com.fengshengchat.mine.presenter;

import com.fengshengchat.base.mvp.BasePresenter;
import com.fengshengchat.mine.model.MineModel;
import com.fengshengchat.mine.view.IMineView;

public class MinePresenter extends BasePresenter<MineModel, IMineView> {
    public MinePresenter(IMineView view) {
        super(new MineModel(), view);
    }
}
