package com.fengshengchat.mine.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.fengshengchat.BuildConfig;
import com.fengshengchat.R;
import com.fengshengchat.account.activity.AccountActivity;
import com.fengshengchat.account.activity.BlacklistActivity;
import com.fengshengchat.account.activity.NotificationManagementActivity;
import com.fengshengchat.account.activity.PersonalInfoActivity;
import com.fengshengchat.app.AppConfig;
import com.fengshengchat.app.OtherActivity;
import com.fengshengchat.base.BKFragment;
import com.fengshengchat.base.HttpRequestDialogObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.ToolbarFragment;
import com.fengshengchat.base.utils.ImageUtils;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.test.LogActivity;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;
import com.fengshengchat.widget.ImagePreviewActivity;
import com.fengshengchat.widget.TitleBarLayout;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import yiproto.yichat.info.Info;

/**
 * 我的页面
 */
public class MineFragment extends BKFragment {

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_layout;
    }

    @BindView(R.id.title_bar)
    TitleBarLayout titleBar;
    @BindView(R.id.avatar_iv)
    ImageView avatarIv;
    @BindView(R.id.name_tv)
    TextView nameTv;
    @BindView(R.id.id_tv)
    TextView idTv;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ToolbarFragment.hackStatusBar(titleBar);
        requestUserInfo();
    }

    /**
     * 请求用户信息
     */
    private void requestUserInfo() {
        Info.GetUserInfoReq.Builder body = Info.GetUserInfoReq
                .newBuilder()
                .addUserIdList(UserManager.getUser().uid);

        PBMessage pbMessage = new PBMessage()
                .setCmd(Info.InfoYimCMD.GET_USER_INFO_REQ_CMD_VALUE)
                .setBody(body);

        ApiManager.getInstance()
                .post(new Request(pbMessage))
                .map(msg -> Info.GetUserInfoRsp.parseFrom(msg.getBody()))
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestDialogObserver<Info.GetUserInfoRsp>(getFragmentManager()) {
                    @Override
                    public void onNext(Info.GetUserInfoRsp data) {
                        bindUserInfo(data);
                    }
                });
    }

//    private boolean isLoaded;
//
//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (hidden && !isLoaded) {
//            requestUserInfo();
//            isLoaded = true;
//        }
//    }

    @Override
    public void onResume() {
        super.onResume();
        User user = UserManager.getUser();
       // ImageUtils.loadRadiusImage(avatarIv, user.icon, R.drawable.image_default_with_round, AppConfig.IMAGE_RADIUS);
        ImageUtils.loadRadiusImage(avatarIv, user.icon, R.mipmap.default_head_icon, AppConfig.IMAGE_RADIUS);
        nameTv.setText(user.nickname);
        idTv.setText(getString(R.string.user_yichat_num_format, user.getUid()));
    }

    /**
     * 绑定用户信息
     */
    private void bindUserInfo(Info.GetUserInfoRsp data) {
        if (data.getResult() != Info.GetUserInfoRsp.GetUserInfoResult.OK_VALUE) {
            toast(data.getErrMsg());
            return;
        }

        if (data.getUserInfoListCount() == 0) {
            return;
        }

        Info.UserInfoDetail detail = data.getUserInfoList(0);
        String nickname = detail.getNickname();
        String smallAvatar = detail.getSmallAvatar();
        String bigAvatar = detail.getBigAvatar();
        LogUtils.e("find-toke", "mine fragment: " + this);
        LogUtils.e("find-toke", "mine: " + UserManager.getUser());
        LogUtils.e("find-token", "mine " + UserManager.getUser().token);
        UserManager.save(nickname, smallAvatar, bigAvatar);

        nameTv.setText(nickname);
      //  ImageUtils.loadRadiusImage(avatarIv, smallAvatar, R.drawable.image_default_with_round, AppConfig.IMAGE_RADIUS);
        ImageUtils.loadRadiusImage(avatarIv, smallAvatar, R.mipmap.default_head_icon, AppConfig.IMAGE_RADIUS);
    }

    @OnClick(R.id.user_info_layout)
    public void openPersonalInfo() {
        Intent intent = new Intent(getContext(), PersonalInfoActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.recent_file_layout)
    public void openRecentFile() {
    }

    @OnClick(R.id.account_and_safe_layout)
    public void openAccountAndSafe() {
        Intent intent = new Intent(getContext(), AccountActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.min_program_layout)
    public void openMinProgram() {
    }

    @OnClick(R.id.notice_manage_layout)
    public void openNoticeManage() {
        Intent intent = new Intent(getContext(), NotificationManagementActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.black_list_layout)
    public void openBlackList() {
        Intent intent = new Intent(getContext(), BlacklistActivity.class);
        startActivity(intent);
    }

    @OnLongClick(R.id.other_set_layout)
    public boolean openOtherSetTest() {
        if (BuildConfig.DEBUG) {
            startActivity(new Intent(getContext(), LogActivity.class));
            return true;
        }
        return false;
    }

    @OnClick(R.id.other_set_layout)
    public void openOtherSet() {
        startActivity(new Intent(getContext(), OtherActivity.class));
    }

    @OnClick(R.id.avatar_iv)
    public void showBigAvatar() {
        Intent intent = new Intent(getContext(), ImagePreviewActivity.class);
        intent.putExtra("imagePath", UserManager.getUser().bigIcon);
        intent.putExtra("thumbImagePath", UserManager.getUser().icon);
        if (Build.VERSION.SDK_INT >= 22) {
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), avatarIv, getString(R.string.share_element_tag));
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

}
