package com.fengshengchat.push;

import com.fengshengchat.protocol.BaseMessage;
import com.fengshengchat.protocol.PBMessage;

/**
 * @author KRT
 * 2018/11/27
 */
public class Message extends BaseMessage {

    public Message(byte[] messageBytes){
        setData(messageBytes);
    }

    public Message(PBMessage pbMessage){
        setData(pbMessage.PB());
    }

}
