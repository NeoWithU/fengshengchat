package com.fengshengchat.push;

import com.google.protobuf.GeneratedMessageV3;
import com.fengshengchat.base.push.AbstractPushService;
import com.fengshengchat.base.push.IPCMessage;
import com.fengshengchat.base.push.PushInternalCmd;
import com.fengshengchat.db.Constant;
import com.fengshengchat.protocol.BaseHandler;
import com.fengshengchat.protocol.BaseMessage;
import com.fengshengchat.protocol.IMessageHandler;
import com.fengshengchat.protocol.MsgWrapper;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.msg.MsgOuterClass;
import yiproto.yichat.verifyconn.VerifyConn;

/**
 * @author KRT
 * 2018/11/27
 */
public class YiPushService extends AbstractPushService<YiPusher> {
    public static final String CMD_RESET_PRIVATE_KEY = "resetPrivateKey";
    public static final String CMD_UPDATE_PRIVATE_KEY = "updatePrivateKey";

    private static final String HOST = Protocol.getSocketUrl();
    private static final int PORT = Protocol.getSocketPort();

    @Override
    protected String getHost() {
        return HOST;
    }

    @Override
    protected int getPort() {
        return PORT;
    }

    private PBMessage verifyMessage;
    private void sendVerifyMessage(PBMessage message){
        getPusher().sendVerifyMessage(new Message(message));
    }

    private void startPingPong(int interval){
        getPusher().setHeartbeatInterval(interval);
    }

    @Override
    protected void start(IPCMessage message) {
        if(null == verifyMessage){
            verifyMessage = new PBMessage();
        }
        if(null == exchangePubKeyPB){
            exchangePubKeyPB = new PBMessage();
        }

        resetPrivateKeyDH();
        BaseMessage.clearUncompletedCache();

        List<MsgOuterClass.Msg> messages = BaseMessage.getMessages(message.getData());
        for(MsgOuterClass.Msg msg : messages){
            if(null != msg && msg.getCmd() == MsgOuterClass.YimCMD.VERIFY_CONN_REQ_CMD_VALUE){
                verifyMessage.setCmd(MsgOuterClass.YimCMD.VERIFY_CONN_REQ_CMD_VALUE);
                verifyMessage.setUid(msg.getUid());
                verifyMessage.setLoginInfoToken(msg.getLoginInfo().getToken());

                VerifyConn.VerifyConnReq.Builder verifyReq = VerifyConn.VerifyConnReq.newBuilder();
                verifyReq.setUid(msg.getUid());
                verifyReq.setToken(msg.getLoginInfo().getToken());
                verifyReq.setImei(PBMessage.getIMEI());
                verifyReq.setMac(PBMessage.getMAC());
                verifyReq.setPlatform(PBMessage.getPlatform());
                verifyReq.setDeviceInfo(PBMessage.getDeviceInfoPB());
                verifyMessage.setBody(verifyReq);

                exchangePubKeyPB.setCmd(MsgOuterClass.YimCMD.EXCHANGE_PUB_KEY_REQ_CMD_VALUE);
                exchangePubKeyPB.setUid(msg.getUid());
                exchangePubKeyPB.setLoginInfoToken(msg.getLoginInfo().getToken());
                exchangePubKeyPB.setBody(VerifyConn.ExchangeKeyReq.newBuilder()
                        .setUid(msg.getUid()));

                getPusher().setUserInfo(msg.getUid(), msg.getLoginInfo().getToken());
                getPusher().setEnableAutoRestart(true);
                getPusher().start(getHost(), getPort());
                break;
            }
        }
    }

    @Override
    protected YiPusher createPusher() {
        return new YiPusher();
    }

    private void resetPrivateKeyDH(){
        Protocol.resetPrivateKeyDH();
        sendInternalCmd(CMD_RESET_PRIVATE_KEY, PushInternalCmd.VALUE_NONE);
    }

    @Override
    protected void onPreConnect() {
        super.onPreConnect();
        resetPrivateKeyDH();
        BaseMessage.clearUncompletedCache();
        getPusher().clearPingPongMessage();
        sendInternalCmd(Constant.INTERNAL_CMD_SOCKET_CONNECTING);
    }

    @Override
    protected void onConnectSuccess() {
        //move to verify OK
//        sendInternalCmd(Constant.INTERNAL_CMD_SOCKET_CONNECT_SUCCESS);
        sendVerifyMessage(verifyMessage);
    }

    @Override
    protected void onNetworkRegain() {
        printe("-----onNetworkRegain-----");
        if(getPusher().isInitOK()) {
            getPusher().setEnableAutoRestart(true);
            getPusher().start(getHost(), getPort());
        }
    }

    @Override
    protected void onConnectCancel() {
        resetPrivateKeyDH();
        BaseMessage.clearUncompletedCache();
        sendInternalCmd(Constant.INTERNAL_CMD_SOCKET_CONNECT_FALIED);
    }

    private void sendInternalCmd(int internalCmd){
        MsgOuterClass.Msg msg = MsgOuterClass.Msg.newBuilder()
                .setBigCmd(Constant.INTERNAL_CMD_TYPE)
                .setCmd(internalCmd)
                .build();
        super.handleReceiveData(new Message(msg.toByteArray()).getData());
    }

    private IMessageHandler preHandler = new PreHandler();
    @Override
    protected void handleReceiveData(byte[] receiveData) {
        List<MsgOuterClass.Msg> messages = BaseMessage.getMessages(receiveData);
        MsgWrapper msgWrapper;
        for(MsgOuterClass.Msg msg : messages){
            msgWrapper = new MsgWrapper();
            msgWrapper.inMsg = msg;
            if(preHandler.handleMessage(msgWrapper)){
                return;
            }
        }
        super.handleReceiveData(receiveData);
    }

    private boolean exchangePubKeyResponse = false;
    private Disposable subscribe;
    private void checkExchangePubKeyRunnable(){
        printe("-------post checkExchangePubKeyRunnable--------");
        if(null != subscribe){
            subscribe.dispose();
        }
        subscribe = Observable.just("")
                .delay(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .subscribe(s -> {
                    printe("-------check exchangePubKeyRunnable-------- " + exchangePubKeyResponse);
                    if (!exchangePubKeyResponse) {
                        closePusher(false);
                    }
                }, throwable -> closePusher(false));
    }

    private int pingPongFreq;
    private PBMessage exchangePubKeyPB;
    private void onVerifyOK(){
        printe("---onVerifyOK encrypt version: " + Protocol.ENCRYPT_VERSION);
        if(Protocol.ENCRYPT_VERSION == Protocol.ENCRYPT_VERSION_DH){
            GeneratedMessageV3.Builder bodyBuilder = exchangePubKeyPB.getBodyBuilder();
            if(bodyBuilder instanceof VerifyConn.ExchangeKeyReq.Builder){
                ((VerifyConn.ExchangeKeyReq.Builder) bodyBuilder).setPublicKey(Protocol.getPublicKeyWithDH());
                exchangePubKeyPB.setBody(bodyBuilder);
                getPusher().push(new Message(exchangePubKeyPB));
                checkExchangePubKeyRunnable();
            }
        }else{
            notifyConnectSuccess();
        }
    }

    private void notifyConnectSuccess(){
        sendInternalCmd(Constant.INTERNAL_CMD_SOCKET_CONNECT_SUCCESS);
        startPingPong(pingPongFreq);
    }

    private void onExchangePubKeyOK(MsgOuterClass.Msg msg){
        exchangePubKeyResponse = true;
        if(BaseHandler.isNoError(msg.getCode())){
            try {
                VerifyConn.ExchangeKeyRsp exchangeKeyRsp = VerifyConn.ExchangeKeyRsp.parseFrom(msg.getBody());
                byte[] publicKeySvr = Protocol.decodeBase64(exchangeKeyRsp.getPublicKeySvr().getBytes());
                Protocol.generatePrivateKeyWithDH(publicKeySvr);
                sendInternalCmd(CMD_UPDATE_PRIVATE_KEY, Protocol.getPrivateKey());
                notifyConnectSuccess();
                printe("----onExchangePubKey OK ---");
                if(null != subscribe){
                    subscribe.dispose();
                }
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            closePusher(false);
        }
        printe("----onExchangePubKey: " + msg.getErrMsg());
    }

    private void closePusher(boolean force){
        if(force){
            getPusher().forceClose();
        }else {
            getPusher().close();
        }
    }

    private class PreHandler extends BaseHandler{
        @Override
        protected boolean canHandle(int bigCmd) {
            //TODO
            return true;
        }

        @Override
        public boolean handle(MsgWrapper msgWrapper) {
            int errorCode = msgWrapper.inMsg.getCode();
            switch(msgWrapper.inMsg.getCmd()){
                default:
                    return false;
                case MsgOuterClass.YimCMD.VERIFY_CONN_RSP_CMD_VALUE: //verify
                    if(isNoError(errorCode)
                            || MsgOuterClass.YiChatErrCode.YICHAT_VERIFY_CONN_REPEAT_VALUE == errorCode) {
                        try {
                            pingPongFreq = (int) VerifyConn.VerifyConnRsp.parseFrom(msgWrapper.inMsg.getBody())
                                    .getHeartbeatFrequency();
                        } catch (Exception e) {
                            e.printStackTrace();
                            pingPongFreq = 20;
                        }
                        onVerifyOK();
                    }else if(MsgOuterClass.YiChatErrCode.YICHAT_VERIFY_CONN_ERROR_VALUE == errorCode
                            || MsgOuterClass.YiChatErrCode.YICHAT_VERIFY_CONN_TOKEN_LOGIN_REPLACE_ERROR_VALUE == errorCode){
                        printe("------Invalid token------");
                        closePusher(true);
                    }
                    //break; maybe need login again
                    return false;
                case MsgOuterClass.YimCMD.VERIFY_CONN_TIMEOUT_CMD_VALUE: //verify timeout
                case MsgOuterClass.YimCMD.CONN_TIMOUT_CMD_VALUE: //ping pong timeout
//                    sendVerifyMessage(verifyMessage);
                    printe("--- timeout: " + msgWrapper.inMsg.getCmd());
                    break;

                case MsgOuterClass.YimCMD.CONN_PONG_CMD_VALUE: //pong
                    printe("- PONG -");
                    break;
                case MsgOuterClass.YimCMD.CONN_KICK_OUT_CMD_VALUE: //kick out
                    printi("--->> KICK OUT <<---");
                    closePusher(true);
                    return false; //break; means close by server, need login again

                case MsgOuterClass.YimCMD.EXCHANGE_PUB_KEY_RSP_CMD_VALUE:
                    onExchangePubKeyOK(msgWrapper.inMsg);
                    break;
            }
            super.handle(msgWrapper);
            return true;
        }

    }
}
