package com.fengshengchat.push;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.fengshengchat.base.IOnReceiveInterface;
import com.fengshengchat.base.IPushAidlInterface;
import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.push.IPCMessage;
import com.fengshengchat.base.push.PushInternalCmd;
import com.fengshengchat.db.Constant;
import com.fengshengchat.db.DbChatList;
import com.fengshengchat.db.DbChatRecord;
import com.fengshengchat.db.DbFriend;
import com.fengshengchat.db.Group;
import com.fengshengchat.db.helper.BusEvent;
import com.fengshengchat.db.helper.TaskHelper;
import com.fengshengchat.protocol.BaseMessage;
import com.fengshengchat.protocol.MsgWrapper;
import com.fengshengchat.protocol.PBHandler;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.protocol.Protocol;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import yiproto.yichat.msg.MsgOuterClass;

public class HandlePushService extends Service {
    private IPushAidlInterface mPushInterface;
    private ServiceConnection mRemoteService;
    private final Set<IPushObserver> mObserverList = new HashSet<>();

    private static final int SEND_RESPONSE_TIMEOUT = 1000 * 5; //ms
    private final List<Task> mSendTask = new ArrayList<>();
    private Handler mCheckTaskHandler = new CheckTaskHandler();

    public class HandlePushServiceInterface extends Binder {
        public boolean isReady(){
            return null != mPushInterface;
        }
        public void start(PBMessage msg){
            try {
                Protocol.resetPrivateKeyDH();
                BaseMessage.clearUncompletedCache();
                mPushInterface.start(new Message(msg));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public void close(){
            try {
                mPushInterface.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public boolean isRunning(){
            try {
                return mPushInterface.isRunning();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
        public void push(PBMessage msg) {
            try {
                addTask(msg);
                mPushInterface.push(new Message(msg));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public void addObserver(IPushObserver o){
            mObserverList.add(o);
        }
        public void removeObserver(IPushObserver o){
            mObserverList.remove(o);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new HandlePushServiceInterface();
    }

    @Override
    public void onCreate() {
        printi("-------onCreate-------");
        super.onCreate();
        RxBus.get().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        printi("-------onStartCommand-------");
        bindRemotePushService();
        return super.onStartCommand(intent, flags, startId);
    }

    private void bindRemotePushService(){
        Intent intent = new Intent(this, YiPushService.class);
        startService(intent);
        mRemoteService = new RemoteServiceConnector();
        bindService(intent, mRemoteService, Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        printi("-------onUnbind-------");
        return true;
    }

    @Override
    public void onRebind(Intent intent) {
        printi("-------onRebind-------");
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        printi("-------onDestroy-------");
        RxBus.get().unregister(this);
        try {
            unbindService(mRemoteService);
        }catch(Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void notifyObservers(MsgWrapper msgWrapper){
        Disposable subscribe = Observable.just("")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    for (IPushObserver observer : mObserverList) {
                        observer.onReceive(msgWrapper);
                    }
                });
    }

    private final Set<Long> needRefreshGroupInfoList = new HashSet<>();
    private void clearNeedRefreshList(){
        printe("-------clearNeedRefreshList----------");
        needRefreshGroupInfoList.clear();
    }

    private boolean preHandle(MsgWrapper msgWrapper){
        if(null == msgWrapper){
            return true;
        }

        if(msgWrapper.extRefreshGroupInfo > 0L) {
            printe("---------add refresh group info task : " + msgWrapper.extRefreshGroupInfo);
            needRefreshGroupInfoList.add(msgWrapper.extRefreshGroupInfo); //avoid duplicate
            refreshGroupInfo(msgWrapper.extRefreshGroupInfo);
        }

        if(null != msgWrapper.inMsg && Constant.INTERNAL_CMD_TYPE == msgWrapper.inMsg.getBigCmd()){
            return handleInternalCmd(msgWrapper.inMsg);
        }

        if(null != msgWrapper.outMsg){
            if(Constant.INTERNAL_CMD_TYPE == msgWrapper.outMsg.getBigCmd()){
                return handleInternalCmd(msgWrapper.outMsg);
            }else {
                return handleOutMsg(msgWrapper.outMsg);
            }
        }

        if(null != msgWrapper.outMsgs && msgWrapper.outMsgs.size() > 0){
            for(MsgOuterClass.Msg msg : msgWrapper.outMsgs){
                handleOutMsg(msg);
            }
        }
        return false;
    }

    private void addTask(PBMessage pbMessage){
        int cmd = pbMessage.getMsg().getCmd();
        if(needMonitorSendTask(cmd)) {
            printe("---------needMonitorTask-------- : " + cmd);
            Task task = new Task(pbMessage.getMessageSequence(), cmd);
            mSendTask.add(task);
            android.os.Message handlerMessage = getHandlerMessage();
            handlerMessage.what = 0;
            handlerMessage.obj = task;
            mCheckTaskHandler.sendMessageDelayed(handlerMessage, SEND_RESPONSE_TIMEOUT);

            printe("-----check task: " + mSendTask);
        }
    }

    private void checkTaskResponse(MsgOuterClass.Msg response){
        if(needMonitorReceiveTask(response.getCmd())){
            printe("------------remove task: " + response.getCmd() + "  seq: " + response.getSeq());
            removeTask(response.getSeq());
        }
    }

    private void removeTask(long msgSequence){
        Task task;
        for(int i = 0; i < mSendTask.size(); i++){
            task = mSendTask.get(i);
            if(task.sequence == msgSequence){
                mSendTask.remove(i);
                mCheckTaskHandler.removeMessages(0, task);
                break;
            }
        }
    }

    private boolean needMonitorSendTask(int cmd){
        return cmd == MsgOuterClass.YimCMD.SEND_CHAT_REQ_CMD_VALUE
                || cmd == MsgOuterClass.YimCMD.SEND_GROUP_CHAT_REQ_CMD_VALUE
                || cmd == MsgOuterClass.YimCMD.SEND_SPORTFULGROUP_CHAT_REQ_CMD_VALUE
                || cmd == MsgOuterClass.YimCMD.GET_SPORTFULGROUP_RECORD_REQ_CMD_VALUE;
    }

    private boolean needMonitorReceiveTask(int cmd){
        return cmd == MsgOuterClass.YimCMD.SEND_CHAT_RSP_CMD_VALUE
                || cmd == MsgOuterClass.YimCMD.SEND_GROUP_CHAT_RSP_CMD_VALUE
                || cmd == MsgOuterClass.YimCMD.SEND_SPORTFULGROUP_CHAT_RSP_CMD_VALUE
                || cmd == MsgOuterClass.YimCMD.GET_SPORTFULGROUP_RECORD_RSP_CMD_VALUE;
    }

    private android.os.Message getHandlerMessage(){
        android.os.Message message = mCheckTaskHandler.obtainMessage();
        if(null == message){
            return new android.os.Message();
        }
        return message;
    }

    private boolean handleInternalCmd(MsgOuterClass.Msg msg){
        printi("---handle <internal> msg: " + msg.getCmd());
        switch(msg.getCmd()){
            case Constant.INTERNAL_CMD_REFRESH_GROUP_INFO:
                refreshGroupInfo(msg.getUid());
                return false;
            case Constant.INTERNAL_CMD_SOCKET_CONNECT_FALIED:
                clearNeedRefreshList();
                return false;
        }
        return false;
    }

    private void refreshGroupInfo(long gid){
        TaskHelper.getInstance().post(
                new com.fengshengchat.db.helper.Task(com.fengshengchat.db.helper.Task.TASK_UPDATE_GROUP_INFO, gid));
    }

    @Subscribe(tags = {@Tag(BusEvent.TAG_UPDATE_GROUP_INFO)})
    public void groupInfoBack(Long gid) {
        Disposable subscribe = Observable.fromCallable(() -> {
                Group groupInfo = DbFriend.getGroupInfo(gid);
                printe("----------groupInfoBack: " + groupInfo);
                if(null != groupInfo) {
                    DbChatList.updateChatItemGroupInfo(groupInfo);
                }
                return groupInfo;
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(group -> {
                    if(null != group){
                        MsgWrapper msgWrapper = new MsgWrapper();
                        msgWrapper.inMsg = MsgOuterClass.Msg.newBuilder()
                                .setBigCmd(Constant.INTERNAL_CMD_TYPE)
                                .setCmd(Constant.INTERNAL_CMD_REFRESH_GROUP_INFO)
                                .setUid(gid)
                                .build();
                        notifyObservers(msgWrapper);
                    }
                }, throwable -> printe(throwable.getMessage()));
    }

    private boolean handleOutMsg(MsgOuterClass.Msg msg){
        printe("---handle out msg cmd: " + msg.getCmd());
        switch(msg.getCmd()){
            case MsgOuterClass.YimCMD.GET_SESSION_NEW_MSG_REQ_CMD_VALUE:
            case MsgOuterClass.YimCMD.GET_GROUP_NEW_MSG_REQ_CMD_VALUE:
                handleOutMsgPushTask(msg);
                return true;
        }

        return false;
    }

    private void handleOutMsgPushTask(MsgOuterClass.Msg msg){
        try {
            mPushInterface.push(new Message(msg.toByteArray()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class OnReceiveMessageListener extends IOnReceiveInterface.Stub {
        public void onReceiveInternalCmd(String cmd, String value) {
            printe("---Push service internal cmd: " + cmd + " value: " + value);
            switch(cmd){
                case PushInternalCmd.CMD_PREPARE_CONNECT:
                    BaseMessage.clearUncompletedCache();
                    break;
                case YiPushService.CMD_UPDATE_PRIVATE_KEY:
                    Protocol.updateDHPrivateKeyForMain(value);
                    break;
                case YiPushService.CMD_RESET_PRIVATE_KEY:
                    Protocol.resetPrivateKeyDH();
                    break;
            }
        }

        public void onReceive(IPCMessage msg){
            List<MsgOuterClass.Msg> messages = BaseMessage.getMessagesForMainProcess(msg.getData());
            MsgWrapper msgWrapper;
            for(MsgOuterClass.Msg message : messages) {
                msgWrapper = new MsgWrapper();
                msgWrapper.inMsg = message;
                checkTaskResponse(message);
                PBHandler.getInstance().handleMessage(getApplicationContext(), msgWrapper);
                if(!preHandle(msgWrapper)) {
                    notifyObservers(msgWrapper);
                }
            }
        }
    }

    private class RemoteServiceConnector implements ServiceConnection{
        public void onServiceConnected(ComponentName name, IBinder service) {
            print("------ Remote service connected ------");
            mPushInterface = IPushAidlInterface.Stub.asInterface(service);
            try {
                mPushInterface.registerCallback(new OnReceiveMessageListener());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public void onServiceDisconnected(ComponentName name) {
            print("------ Remote service disconnected ------");
        }
    }

    private void print(String msg){
        printi(msg);
    }

    private void printi(String msg){
        Debug.i("HandlePushService", msg);
    }

    private void printe(String msg){
        Debug.e("HandlePushService", msg);
    }

    private class CheckTaskHandler extends Handler{
        public void handleMessage(android.os.Message msg) {
            printe("------CheckTaskHandler------ " + msg.obj);
            if(0 == msg.what && msg.obj instanceof HandlePushService.Task){
                HandlePushService.Task check = (HandlePushService.Task) msg.obj;
                for(int i = 0; i < mSendTask.size(); i++){
                    printe("------cmd in task: " + check.cmd);
                    if(mSendTask.get(i).sequence == check.sequence){
                        recordSendFailedState(check.sequence);

                        mSendTask.remove(i);
                        MsgWrapper msgWrapper = new MsgWrapper();
                        msgWrapper.inMsg = MsgOuterClass.Msg.newBuilder()
                                .setCmd(responseCmd(check.cmd))
                                .setSeq((int) check.sequence)
                                .setCode(-1000)
                                .setErrMsg("")
                                .build();
                        notifyObservers(msgWrapper);
                        break;
                    }
                }
            }
        }

        private int responseCmd(int sendCmd) {
            switch (sendCmd) {
                case MsgOuterClass.YimCMD.SEND_CHAT_REQ_CMD_VALUE:
                    return MsgOuterClass.YimCMD.SEND_CHAT_RSP_CMD_VALUE;
                case MsgOuterClass.YimCMD.SEND_GROUP_CHAT_REQ_CMD_VALUE:
                    return MsgOuterClass.YimCMD.SEND_GROUP_CHAT_RSP_CMD_VALUE;
                case MsgOuterClass.YimCMD.SEND_SPORTFULGROUP_CHAT_REQ_CMD_VALUE:
                    return MsgOuterClass.YimCMD.SEND_SPORTFULGROUP_CHAT_RSP_CMD_VALUE;
                case MsgOuterClass.YimCMD.GET_SPORTFULGROUP_RECORD_REQ_CMD_VALUE:
                    return MsgOuterClass.YimCMD.GET_SPORTFULGROUP_RECORD_RSP_CMD_VALUE;
            }
            return 0;
        }

        private void recordSendFailedState(long localSeq){
            Disposable subscribe = Observable.just("")
                    .subscribeOn(Schedulers.io())
                    .subscribe(s ->
                            DbChatRecord.updateSendResultState(
                                    localSeq, localSeq, Constant.MESSAGE_STATE_FAILED));
        }
    }

    private class Task{
        public long sequence;
        public int cmd;

        public Task(long seq, int cmd){
            sequence = seq;
            this.cmd = cmd;
        }
    }

}
