package com.fengshengchat.push;

import com.fengshengchat.base.debug.Debug;
import com.fengshengchat.base.push.AbstractPusher;
import com.fengshengchat.base.push.IMessage;
import com.fengshengchat.base.push.IPCMessage;
import com.fengshengchat.protocol.BaseMessage;
import com.fengshengchat.protocol.PBMessage;

import yiproto.yichat.heartbeat.Heartbeat;
import yiproto.yichat.msg.MsgOuterClass;

public class YiPusher extends AbstractPusher {
    private static final boolean DEBUG = true;
    private long uid;
    private String token;

    @Override
    protected boolean isEOF(byte[] rawData) {
        if(null == rawData || rawData.length < 2)
            return false;
        if(DEBUG) {
//            printe("---isEOF, data length: " + rawData.length + "  pack length: " + BaseMessage.getPackSize(rawData));
        }
        return rawData.length >= BaseMessage.getPackSize(rawData);
    }

    private void printe(String s){
        if(DEBUG) {
            Debug.e("YiPusher", s);
        }
    }

    public void setUserInfo(long uid, String token){
        this.uid = uid;
        this.token = token;
    }

    public boolean isInitOK(){
        return uid > 0 && null != token && token.length() > 0;
    }

    public void sendVerifyMessage(IPCMessage message){
        printe("--- send verify --- " + Thread.currentThread().getName());
        push(message);
    }

    @Override
    public void forceClose() {
        super.forceClose();
        uid = 0;
        token = "";
    }

    @Override
    protected IMessage generateHeartbeatMessage() {
        PBMessage pbMessage = new PBMessage();
        pbMessage.setUid(uid);
        pbMessage.setLoginInfoToken(token);
        pbMessage.setCmd(MsgOuterClass.YimCMD.CONN_PING_CMD_VALUE);

        Heartbeat.ConnPing.Builder ping = Heartbeat.ConnPing.newBuilder();
        ping.setUid(uid);

        pbMessage.setBody(ping);
        return new Message(pbMessage);
    }
}
