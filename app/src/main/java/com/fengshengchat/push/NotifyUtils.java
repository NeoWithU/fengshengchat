package com.fengshengchat.push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.RemoteViews;

import com.blankj.utilcode.util.SPUtils;
import com.ycbjie.notificationlib.NotificationUtils;
import com.fengshengchat.MainActivity;
import com.fengshengchat.R;
import com.fengshengchat.base.debug.Debug;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotifyUtils {
    public static boolean enableNotify = true;
    public static boolean enableSound = true;
    public static boolean enableVibrate = true;

    private static NotificationManager notifyManager;
    private static NotificationCompat.Builder mBuilderCompat;
    private static Notification.Builder mBuilder;
    private static long currentChatID = -1L;
    private static NotificationUtils notificationUtils;
    private static boolean useNew = true;
    private static Vibrator mVibrator;

    public static void init(Context context){
        long uid = com.fengshengchat.user.UserManager.getUser().uid;
        SPUtils instance = SPUtils.getInstance();
        enableNotify = instance.getBoolean(uid + "enableNotify", true);
        enableSound = instance.getBoolean(uid + "enableSound", true);
        enableVibrate = instance.getBoolean(uid + "enableVibrate", (Build.VERSION.SDK_INT < Build.VERSION_CODES.O));

        newMessageTip = context.getResources().getString(R.string.new_message_remind);
        tipString = context.getResources().getString(R.string.tip);
        requestString = context.getResources().getString(R.string.request_add_friend);
        agreeString = context.getResources().getString(R.string.agree_add_friend);

        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if(useNew){
            notificationUtils = new NotificationUtils(context);
            return;
        }

        mAddFriendRemoteViews = new RemoteViews(context.getPackageName(), R.layout.notify_add_friend_layout);
        notifyManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    "channelId", "channelName", NotificationManager.IMPORTANCE_HIGH);
            mChannel.enableLights(true);
            mChannel.enableVibration(true);
            mChannel.setSound(null, null);

            notifyManager.createNotificationChannel(mChannel);
            mBuilder = new Notification.Builder(context, "channelId")
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher_round);
//                    .setContentIntent(pendingIntent);
        }else{
            mBuilderCompat = new NotificationCompat.Builder(context);
            mBuilderCompat.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher_round)
//                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND);
        }
    }

    public static void setEnableNotify(boolean enable){
        enableNotify = enable;
        long uid = com.fengshengchat.user.UserManager.getUser().uid;
        SPUtils.getInstance().put(uid + "enableNotify", enable);
    }

    public static boolean isEnableNotify(){
        boolean b = NotificationManagerCompat.from(notificationUtils.getApplicationContext()).areNotificationsEnabled();
        Debug.e("test", "----------enable " + b);
        return enableNotify && b;
    }

    public static void setEnableSound(boolean enable){
        enableSound = enable;
        long uid = com.fengshengchat.user.UserManager.getUser().uid;
        SPUtils.getInstance().put(uid + "enableSound", enable);
    }

    public static boolean isEnableSound(){
        return enableSound;
    }

    public static void setEnableVibrate(boolean enable){
        enableVibrate = enable;
        long uid = com.fengshengchat.user.UserManager.getUser().uid;
        SPUtils.getInstance().put(uid + "enableVibrate", enable);
    }

    public static boolean isEnableVibrate(){
        return enableVibrate;
    }

    public static void setCurrrentChatID(long uid){
        currentChatID = uid;
    }

    public static void clearNotify(){
        notificationUtils.clearNotification();
    }

    private static void sendNotify(int id, long uid, String title, String content){
        if(!isEnableNotify()){
            return;
        }

        notificationUtils.setOngoing(false);
        if (isEnableSound()) {
            notificationUtils.setSound(android.provider.Settings.System.DEFAULT_NOTIFICATION_URI);
        }else{
            notificationUtils.setSound(null);
        }

        Intent resultIntent = new Intent(notificationUtils, MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);           //添加为栈顶Activity
        resultIntent.putExtra("fromNotify", true);
        resultIntent.putExtra("uid", uid);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(notificationUtils,2,resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        if(10 == id){
            title = notificationUtils.getApplicationContext().getString(R.string.app_name);
        }

        notificationUtils.setVibrate(null);
        notificationUtils.setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(resultPendingIntent)
                .sendNotification(id, title, content, R.mipmap.ic_launcher);

        if(isEnableVibrate()){
            vibrate();
        }
    }

    private static void vibrate(){
        if(null != mVibrator){
            long[] v = new long[]{0, 300, 200, 300};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mVibrator.vibrate(VibrationEffect.createWaveform(v, -1));
            } else {
                mVibrator.vibrate(v, -1);
            }
        }
    }

    private static String tag = "newMessage";
    private static String newMessageTip;
    public static void notifyNewMessage(long uid, String title, String content) {
        if(uid == currentChatID){
            return;
        }

        if(useNew){
            content = newMessageTip;
            sendNotify(10, uid, title, content);
            return;
        }
        content = newMessageTip;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBuilder.setContentTitle(title)
                    .setContentText(content)
                    .setWhen(System.currentTimeMillis());
            notifyManager.notify(tag, 10, mBuilder.build());
        }else{
            mBuilderCompat.setContentTitle(title)
                    .setContentText(content)
                    .setWhen(System.currentTimeMillis());
            notifyManager.notify(tag, 10, mBuilderCompat.build());
        }
    }

    private static String tipString;
    private static String requestString;
    private static String agreeString;
    private static RemoteViews mAddFriendRemoteViews;
    public static void notifyAddFriend(String who, boolean isAgreed){
        if(useNew){
            sendNotify(11, 0, null == who ? tipString : who, isAgreed ? agreeString : requestString);
            return;
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBuilder.setCustomBigContentView(mAddFriendRemoteViews)
                    .setContentTitle(null == who ? tipString : who)
                    .setContentText(isAgreed ? agreeString : requestString)
                    .setWhen(System.currentTimeMillis());
            notifyManager.notify(10, mBuilder.build());
        }else{
            mBuilderCompat.setCustomBigContentView(mAddFriendRemoteViews)
                    .setWhen(System.currentTimeMillis());
            notifyManager.notify(10, mBuilderCompat.build());
        }
    }
//
//
//    private void testNotify() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            String channelId = "chat";
//            String channelName = "聊天消息";
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            createNotificationChannel(channelId, channelName, importance);
//            channelId = "subscribe";
//            channelName = "订阅消息";
//            importance = NotificationManager.IMPORTANCE_DEFAULT;
////            createNotificationChannel(channelId, channelName, importance);
//        }
//    }
//
//    @TargetApi(Build.VERSION_CODES.O)
//    private void createNotificationChannel(String channelId, String channelName, int importance) {
//        NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
//        NotificationManager notificationManager = (NotificationManager) getView().getContext().getSystemService(NOTIFICATION_SERVICE);
//        notificationManager.createNotificationChannel(channel);
//        Notification notification = new NotificationCompat.Builder(getView().getContext(), "chat")
//                .setContentTitle("收到一条聊天消息")
//                .setContentText("今天中午吃什么？")
//                .setWhen(System.currentTimeMillis())
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setLargeIcon(BitmapFactory.decodeResource(getView().getContext().getResources(), R.mipmap.ic_launcher))
//                .setAutoCancel(true).build();
//        notification.contentView = new RemoteViews(getView().getContext().getPackageName(), R.layout.notify_add_friend_layout);
//        notificationManager.notify(1, notification);
//    }
//
//    public void sendChatMsg(View view) {
//        NotificationManager manager = (NotificationManager) getView().getContext().getSystemService(NOTIFICATION_SERVICE);
//        Notification notification = new NotificationCompat.Builder(getView().getContext(), "chat").setContentTitle("收到一条聊天消息").setContentText("今天中午吃什么？").setWhen(System.currentTimeMillis()).setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(BitmapFactory.decodeResource(getView().getContext().getResources(), R.mipmap.ic_launcher)).setAutoCancel(true).build();
//        manager.notify(1, notification);
//    }
//
//    public void sendSubscribeMsg(View view) {
//        NotificationManager manager = (NotificationManager) getView().getContext().getSystemService(NOTIFICATION_SERVICE);
//        Notification notification = new NotificationCompat.Builder(getView().getContext(), "subscribe").setContentTitle("收到一条订阅消息").setContentText("地铁沿线30万商铺抢购中！").setWhen(System.currentTimeMillis()).setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(BitmapFactory.decodeResource(getView().getContext().getResources(), R.mipmap.ic_launcher)).setAutoCancel(true).build();
//        manager.notify(2, notification);
//    }

}
