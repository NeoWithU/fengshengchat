package com.fengshengchat.push;

import com.fengshengchat.protocol.MsgWrapper;

public interface IPushObserver {
    void onReceive(MsgWrapper msgWrapper);
}
