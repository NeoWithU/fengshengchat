package com.fengshengchat.file;

import android.content.Context;
import android.os.Environment;

public class FileConstant {

    public static String getFileStorageRootPath(Context context){
        return context.getFilesDir().getAbsolutePath();
    }

    public static boolean isMediaMounted(){
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    public static String getFileExternalStorageRootPath(){
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    public static String getFileStorageParentPath(){
        return "/YiChat";
    }

    public static String getFileExternalStorageImageAbsolutePath(){
        return getFileExternalStorageRootPath() + getFileStorageParentPath() + "/image";
    }

    public static String getFileExternalStorageVoiceAbsolutePath(){
        return getFileExternalStorageRootPath() + getFileStorageParentPath() + "/voice";
    }

    public static String getFileExternalStorageVideoAbsolutePath(){
        return getFileExternalStorageRootPath() + getFileStorageParentPath() + "/video";
    }

    public static String getFileExternalStorageFileAbsolutePath(){
        return getFileExternalStorageRootPath() + getFileStorageParentPath() + "/file";
    }

}
