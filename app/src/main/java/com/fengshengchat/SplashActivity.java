package com.fengshengchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.google.protobuf.InvalidProtocolBufferException;
import com.fengshengchat.account.activity.LoginActivity;
import com.fengshengchat.account.fragment.LoginFragment;
import com.fengshengchat.base.HttpRequestObserver;
import com.fengshengchat.base.RxSchedulersHelper;
import com.fengshengchat.base.base.BaseActivity;
import com.fengshengchat.base.utils.DeviceUtils;
import com.fengshengchat.base.utils.PermissionUtils;
import com.fengshengchat.net.ApiManager;
import com.fengshengchat.net.bean.Request;
import com.fengshengchat.protocol.PBMessage;
import com.fengshengchat.push.HandlePushService;
import com.fengshengchat.user.User;
import com.fengshengchat.user.UserManager;

import yiproto.yichat.login.Login;
import yiproto.yichat.msg.MsgOuterClass;

/**
 * @author KRT
 * 2018/11/28
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    private void initPush(){
        Intent pushService = new Intent(this, HandlePushService.class);
        startService(pushService);
    }

    private void init(){
        initPush();
        runDelay(this::requestBasePermissions, 300);
    }

    private void requestBasePermissions() {
        String[] devicePermissions = DeviceUtils.getUsePermissions();
        String[] basePermissions = devicePermissions;
        requestPermission(basePermissions, granted -> {
            if (granted) {
                User user = UserManager.getUser();
                printUserInfo(user);
                if(null == user || user.uid <= 0 || null == user.token || user.token.length() <= 0){
                    printi("--------user info invalid go to login---------");
                    LoginActivity.toLogin(this, "", false);
                }else{
                    checkToken(user);
                }
            } else {
                PermissionUtils.jump2Setting(getContext());
                finish();
            }
        });
    }

    private void printUserInfo(User user){
        if(null == user){
            printe("-------user is null--------");
        }else{
            printi("-----uid: " + user.uid);
            printi("-----token: " + user.token);
            printi("-----expireTime: " + user.expireTime);
        }
    }

    private void checkToken(User user){
        if(user.expireTime - (60 * 60 * 24 * 5) > (System.currentTimeMillis() / 1000)){
            printi("-------to main activity-------");
            MainActivity.toMainActivity(this);
        }else{
            printi("-------token invalid, refresh token-------");
            refreshToken(user);
        }
    }

    @Override
    protected void initNavigationBar() {
        setTransparentForWindow(this);
//        hideBottomUIMenu();
    }

    @Override
    public void onBackPressed() {}

    private void refreshToken(User user){
        PBMessage pbMessage = new PBMessage();
        pbMessage.setCmd(Login.LoginYimCMD.AUTO_LOGIN_REQ_CMD_VALUE)
                .setBody(Login.AutoLoginReq.newBuilder()
                        .setUid(user.uid)
                        .setToken(user.token)
                        .setPlatform(PBMessage.getPlatform()));

        ApiManager.getInstance().post(new Request(pbMessage))
                .compose(RxSchedulersHelper.applyIO2MainSchedulers())
                .subscribe(new HttpRequestObserver<MsgOuterClass.Msg>() {
                    public void onNext(MsgOuterClass.Msg msg) {
                        handleAutoLoginResponse(msg);
                    }
                    protected void onError(String msg) {
                        printe("----- 自动登录失败 1-----");
                        LoginActivity.toLogin(SplashActivity.this, "", false);
                    }
                });
    }

    private void handleAutoLoginResponse(MsgOuterClass.Msg response) {
        try {
            Login.AutoLoginRsp autoLoginRsp = Login.AutoLoginRsp.parseFrom(response.getBody());
            if (autoLoginRsp.getResult() == Login.AutoLoginRsp.AutoLoginResult.OK_VALUE) {
                User user = UserManager.getUser();
                user.token = autoLoginRsp.getToken();
                user.expireTime = autoLoginRsp.getExpireTime();
                LogUtils.e("find-token", "splash-activity" + user.token);
                UserManager.saveUser(user);
                MainActivity.toMainActivity(this);
                return;
            }
            printe("----- 自动登录失败 2-----" + autoLoginRsp.getResult() + " " + autoLoginRsp.getErrMsg());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        LoginActivity.toLogin(this, LoginFragment.REASON_TOKEN_INVALID, false);
    }
}
