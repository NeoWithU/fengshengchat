package com.fengshengchat.base.view;

import android.content.Context;
import android.util.AttributeSet;

public class EmojiEditText extends com.vanniktech.emoji.EmojiEditText {
    public EmojiEditText(Context context) {
        super(context);
    }

    public EmojiEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
