package com.fengshengchat.base.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

public class EmojiTextView extends com.vanniktech.emoji.EmojiTextView {
    public EmojiTextView(Context context) {
        super(context);
    }

    public EmojiTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
