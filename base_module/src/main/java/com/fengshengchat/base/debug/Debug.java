package com.fengshengchat.base.debug;

import android.util.Log;

import com.blankj.utilcode.util.LogUtils;

/**
 * @author KRT
 * 2018/11/20
 */
public class Debug {
    private static final boolean ENABLE_DEBUG = true;
    private static boolean log2File = false;

    public static boolean isLog2File() {
        return log2File;
    }

    private Debug() {}

    public static boolean isEnableDebug() {
        return ENABLE_DEBUG;
    }

    public static void d(String tag, String msg) {
        if (isEnableDebug()) {
            if(isLog2File()){
                LogUtils.dTag(tag, checkNull(msg));
            }else {
                Log.d(tag, checkNull(msg));
            }
        }
    }

    public static void i(String tag, String msg) {
        if (isEnableDebug()) {
            if(isLog2File()){
                LogUtils.iTag(tag, checkNull(msg));
            }else {
                Log.i(tag, checkNull(msg));
            }
        }
    }

    public static void e(String tag, String msg) {
        if (isEnableDebug()) {
            if(isLog2File()){
                LogUtils.eTag(tag, checkNull(msg));
            }else {
                Log.e(tag, checkNull(msg));
            }
        }
    }

    private static String checkNull(String msg) {
        return null == msg ? "[NULL]" : msg;
    }

}
