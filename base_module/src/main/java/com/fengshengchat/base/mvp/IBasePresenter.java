package com.fengshengchat.base.mvp;

/**
 * @author KRT
 * 2018/11/20
 */
public interface IBasePresenter {
    void onDestroy();
}
