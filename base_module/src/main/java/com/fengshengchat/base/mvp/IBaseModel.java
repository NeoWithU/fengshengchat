package com.fengshengchat.base.mvp;

/**
 * @author KRT
 * 2018/12/5
 */
public interface IBaseModel {
    void onDestroy();
}
