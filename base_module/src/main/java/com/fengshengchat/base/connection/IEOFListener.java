package com.fengshengchat.base.connection;

/**
 * @author KRT
 * 2018/11/27
 */
public interface IEOFListener {
    boolean isEOF(byte[] rawData);
}
