package com.fengshengchat.base.connection;

public interface INetworkStateProvider {
    boolean isNetworkAvailable();
}
