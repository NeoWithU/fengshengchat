package com.fengshengchat.base.connection;

public interface IOnConnectListener {
    void onPreConnect();
    void onConnectSuccess();
}
