package com.fengshengchat.base.base;

public interface OnBackPressedListener {
    boolean onBackPressed();
}
