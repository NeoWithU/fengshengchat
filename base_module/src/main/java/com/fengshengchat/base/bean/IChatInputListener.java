package com.fengshengchat.base.bean;

import com.fengshengchat.base.view.ChatInputView;

public interface IChatInputListener extends IPressToSpeakListener{
    void onShowPanelTypeChanged(@ChatInputView.ShowType int showType);
    void onSendTextClick(String content);
    String getSaveFilePath();
    void onInputLengthExceed(CharSequence s, int maxLength);
}
