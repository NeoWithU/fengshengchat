package com.fengshengchat.base;

import com.fengshengchat.base.push.IPCMessage;
import com.fengshengchat.base.IOnReceiveInterface;

interface IPushAidlInterface {
    void start(in IPCMessage startMsg);
    void close();
    boolean isRunning();
    void push(in IPCMessage msg);
    void registerCallback(in IOnReceiveInterface callback);
    void unregisterCallback(in IOnReceiveInterface callback);
}
