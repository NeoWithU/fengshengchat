package com.fengshengchat.base;

import com.fengshengchat.base.push.IPCMessage;

interface IOnReceiveInterface {
    void onReceiveInternalCmd(String cmd, String value);
    void onReceive(in IPCMessage msg);
}
